import { createServer } from "http";
import { createWriteStream } from "fs";

const server = createServer(function (request, response) {
  if (request.method == "OPTIONS") {
    console.log("[2/4] Responding to preflight request");
    // PreFlight request
    response.writeHead(200, {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "POST",
      "Access-Control-Allow-Headers": "Content-Type",
      "Access-Control-Max-Age": 86400,
    });
    response.end();
    return;
  }
  console.log("[3/4] Receiving data");
  var f = createWriteStream("src/data/raw.ts");
  request.pipe(f);
  request.on("end", function () {
    console.log("[4/4] Data received");
    response.writeHead(200);
    response.end("OK");
    server.close();
  });
});

const port = 8888;
server.listen(port);
console.log(`[1/4] Listengin on :${port}`);

import { Armor, Damage } from "./damage";
import { PlayableUnit, RawUnit, Waves as TWaves, WaveUnit } from "./unit_class";

/* eslint-disable prettier/prettier */

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "earthking_unit_id",
  "name": "Earth King",
  "image": "Icons/EarthKing.png",
  "splash": "splashes/EarthKing.png",
  "sketchfabUrl": "",
  "description": "The great Earth King of Nova.",
  "cost": 0,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Pure",
  "attackTypeDescription": "100% to all defense types.",
  "armorType": "Immaterial",
  "armorTypeDescription": "100% from all attack types.",
  "hp": 2000,
  "mana": 5,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.2000000476837158,
  "attackSpeedDescriptor": "Average",
  "mspd": 270,
  "dps": 40,
  "damage": 48,
  "range": 575,
  "totalValue": 0,
  "bounty": 0,
  "tier": "0",
  "legion": "Aspect",
  "legionType": "aspect_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.5,
  "spawnDelay": 0,
  "effectiveDps": 40,
  "effectiveThreat": 40,
  "movementSize": 1.5,
  "backswing": 0.5,
  "projectileSpeed": 3000,
  "mastermindGroups": "None",
  "flags": "Boss, Building, Ground",
  "color": "49565b",
  "turnRate": 0,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Aspect ",
  "suffix": "(Special)",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "boss_status_ability_id",
      "abilityClass": "",
      "name": "Boss Unit",
      "image": "Icons/BossUnit.png",
      "description": "Reduces the duration or effectiveness of most debuffs",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/BossUnit.png' /> Boss Unit<br /><span style='color: #ffcc00'>Caster Buff:</span> Boss Unit"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_earthking_unit_id: RawUnit = {
  name: "Earth King",
  sketchfabUrl: "",
  image: require("#LTD2/Icons/EarthKing.png"),
  splash: require("#LTD2/splashes/EarthKing.png"),
  description: "The great Earth King of Nova.",

  damage: Damage.Pure,
  atkSpeed: 0.8333333002196431,
  onHitDmg: 48,

  armor: Armor.Immaterial,
  hp: 2000,
};

export const earthking_unit_id: PlayableUnit = {
  ...raw_earthking_unit_id,
  price: 0,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "skyking_unit_id",
  "name": "Sky King",
  "image": "Icons/SkyKing.png",
  "splash": "splashes/SkyKing.png",
  "sketchfabUrl": "",
  "description": "The great Sky King of Nova.",
  "cost": 0,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Pure",
  "attackTypeDescription": "100% to all defense types.",
  "armorType": "Immaterial",
  "armorTypeDescription": "100% from all attack types.",
  "hp": 2000,
  "mana": 5,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.2000000476837158,
  "attackSpeedDescriptor": "Average",
  "mspd": 270,
  "dps": 40,
  "damage": 48,
  "range": 575,
  "totalValue": 0,
  "bounty": 0,
  "tier": "0",
  "legion": "Aspect",
  "legionType": "aspect_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.5,
  "spawnDelay": 0,
  "effectiveDps": 40,
  "effectiveThreat": 40,
  "movementSize": 1.5,
  "backswing": 0.5,
  "projectileSpeed": 3000,
  "mastermindGroups": "None",
  "flags": "Boss, Building, Ground",
  "color": "49565b",
  "turnRate": 0,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Aspect ",
  "suffix": "(Special)",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "boss_status_ability_id",
      "abilityClass": "",
      "name": "Boss Unit",
      "image": "Icons/BossUnit.png",
      "description": "Reduces the duration or effectiveness of most debuffs",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/BossUnit.png' /> Boss Unit<br /><span style='color: #ffcc00'>Caster Buff:</span> Boss Unit"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_skyking_unit_id: RawUnit = {
  name: "Sky King",
  sketchfabUrl: "",
  image: require("#LTD2/Icons/SkyKing.png"),
  splash: require("#LTD2/splashes/SkyKing.png"),
  description: "The great Sky King of Nova.",

  damage: Damage.Pure,
  atkSpeed: 0.8333333002196431,
  onHitDmg: 48,

  armor: Armor.Immaterial,
  hp: 2000,
};

export const skyking_unit_id: PlayableUnit = {
  ...raw_skyking_unit_id,
  price: 0,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "hydraling_unit_id",
  "name": "Hydraling",
  "image": "Icons/Hydraling.png",
  "splash": "splashes/Hydraling.png",
  "sketchfabUrl": "https://sketchfab.com/models/ec853dee71f14d6dad575971fe29c4a4",
  "description": "A mini version of a Hydra that spawns when a Hydra dies.",
  "cost": 0,
  "mythium": 0,
  "income": 0,
  "supply": 1,
  "attackType": "Impact",
  "attackTypeDescription": "115% to Arcane<br />115% to Fortified<br />90% to Natural<br />80% to Swift",
  "armorType": "Natural",
  "armorTypeDescription": "90% from Impact<br />125% from Magic<br />85% from Pierce",
  "hp": 910,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.2000000476837158,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 35,
  "damage": 42,
  "range": 100,
  "totalValue": 250,
  "bounty": 0,
  "tier": "6",
  "legion": "Atlantean",
  "legionType": "atlantean_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.10000000149011612,
  "spawnDelay": 0,
  "effectiveDps": 35,
  "effectiveThreat": 35,
  "movementSize": 0.1875,
  "backswing": 0.6499999761581421,
  "projectileSpeed": 3000,
  "mastermindGroups": "None",
  "flags": "Ground",
  "color": "2c9a98",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Atlantean ",
  "suffix": "(Special)",
  "abilities": [],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_hydraling_unit_id: RawUnit = {
  name: "Hydraling",
  sketchfabUrl: "https://sketchfab.com/models/ec853dee71f14d6dad575971fe29c4a4",
  image: require("#LTD2/Icons/Hydraling.png"),
  splash: require("#LTD2/splashes/Hydraling.png"),
  description: "A mini version of a Hydra that spawns when a Hydra dies.",

  damage: Damage.Impact,
  atkSpeed: 0.8333333002196431,
  onHitDmg: 42,

  armor: Armor.Natural,
  hp: 910,
};

export const hydraling_unit_id: PlayableUnit = {
  ...raw_hydraling_unit_id,
  price: 0,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "pollywog_unit_id",
  "name": "Pollywog",
  "image": "Icons/Pollywog.png",
  "splash": "splashes/Pollywog.png",
  "sketchfabUrl": "https://sketchfab.com/models/baa8b551258947bca5db5ea99758f676",
  "description": "Flying. Can upgrade into two different forms.",
  "cost": 15,
  "mythium": 0,
  "income": 0,
  "supply": 1,
  "attackType": "Magic",
  "attackTypeDescription": "75% to Arcane<br />105% to Fortified<br />125% to Natural<br />100% to Swift",
  "armorType": "Swift",
  "armorTypeDescription": "80% from Impact<br />100% from Magic<br />120% from Pierce",
  "hp": 80,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.949999988079071,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 7,
  "damage": 7,
  "range": 350,
  "totalValue": 15,
  "bounty": 0,
  "tier": "1",
  "legion": "Atlantean",
  "legionType": "atlantean_legion_id",
  "movementType": "Air",
  "spawnBias": 0.8999999761581421,
  "spawnDelay": 0,
  "effectiveDps": 7.369999885559082,
  "effectiveThreat": 7.369999885559082,
  "movementSize": 0.1875,
  "backswing": 0.44999998807907104,
  "projectileSpeed": 3000,
  "mastermindGroups": "SwiftOrNat, Magic",
  "flags": "Air",
  "color": "2c9a98",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Atlantean T1",
  "suffix": "",
  "abilities": [],
  "upgradesTo": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "unitType": "devilfish_unit_id",
      "name": "Devilfish",
      "image": "Icons/Devilfish.png"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "unitType": "seraphin_unit_id",
      "name": "Seraphin",
      "image": "Icons/Seraphin.png"
    }
  ],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_pollywog_unit_id: RawUnit = {
  name: "Pollywog",
  sketchfabUrl: "https://sketchfab.com/models/baa8b551258947bca5db5ea99758f676",
  image: require("#LTD2/Icons/Pollywog.png"),
  splash: require("#LTD2/splashes/Pollywog.png"),
  description: "Flying. Can upgrade into two different forms.",

  damage: Damage.Magic,
  atkSpeed: 1.0526315921561542,
  onHitDmg: 7,

  armor: Armor.Swift,
  hp: 80,
};

export const pollywog_unit_id: PlayableUnit = {
  ...raw_pollywog_unit_id,
  price: 15,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "seraphin_unit_id",
  "name": "Seraphin",
  "image": "Icons/Seraphin.png",
  "splash": "splashes/Seraphin.png",
  "sketchfabUrl": "https://sketchfab.com/models/5cac2121efc8412d869183074ff90d0f",
  "description": "Flying. Ranged damage-dealer. Can be Adapted into a Devilfish.",
  "cost": 80,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Magic",
  "attackTypeDescription": "75% to Arcane<br />105% to Fortified<br />125% to Natural<br />100% to Swift",
  "armorType": "Swift",
  "armorTypeDescription": "80% from Impact<br />100% from Magic<br />120% from Pierce",
  "hp": 610,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.9300000071525574,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 54,
  "damage": 50,
  "range": 350,
  "totalValue": 95,
  "bounty": 0,
  "tier": "1",
  "legion": "Atlantean",
  "legionType": "atlantean_legion_id",
  "movementType": "Air",
  "spawnBias": 0.8999999761581421,
  "spawnDelay": 0,
  "effectiveDps": 53.7599983215332,
  "effectiveThreat": 53.7599983215332,
  "movementSize": 0.1875,
  "backswing": 0.5,
  "projectileSpeed": 3000,
  "mastermindGroups": "None",
  "flags": "Air",
  "color": "2c9a98",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Atlantean T1U",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "adapt_into_devilfish_ability_id",
      "abilityClass": "",
      "name": "Adapt",
      "image": "Icons/Adapt(intoDevilfish).png",
      "description": "Changes form into a Devilfish. Can only be used during the build phase.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Adapt(intoDevilfish).png' /> Adapt<br /><span style='color: #ffcc00'> Order String:</span> Adapt<br /><span style='color: #ffcc00'> Unit:</span> <img class='tooltip-icon' src='hud/img/Icons/Devilfish.png' />Devilfish"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "unitType": "pollywog_unit_id",
      "name": "Pollywog",
      "image": "Icons/Pollywog.png"
    }
  ],
  "amountSpawned": 0
}
*/

export const raw_seraphin_unit_id: RawUnit = {
  name: "Seraphin",
  sketchfabUrl: "https://sketchfab.com/models/5cac2121efc8412d869183074ff90d0f",
  image: require("#LTD2/Icons/Seraphin.png"),
  splash: require("#LTD2/splashes/Seraphin.png"),
  description: "Flying. Ranged damage-dealer. Can be Adapted into a Devilfish.",

  damage: Damage.Magic,
  atkSpeed: 1.0752688089344926,
  onHitDmg: 50,

  armor: Armor.Swift,
  hp: 610,
};

export const seraphin_unit_id: PlayableUnit = {
  ...raw_seraphin_unit_id,
  price: 80,
  evolFrom: pollywog_unit_id,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "devilfish_unit_id",
  "name": "Devilfish",
  "image": "Icons/Devilfish.png",
  "splash": "splashes/Devilfish.png",
  "sketchfabUrl": "https://sketchfab.com/models/186b893bd8cc4c1ca2e13ad123da7684",
  "description": "Flying. Melee tank. Can be Adapted into a Seraphin.",
  "cost": 80,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Magic",
  "attackTypeDescription": "75% to Arcane<br />105% to Fortified<br />125% to Natural<br />100% to Swift",
  "armorType": "Swift",
  "armorTypeDescription": "80% from Impact<br />100% from Magic<br />120% from Pierce",
  "hp": 1120,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.100000023841858,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 35,
  "damage": 38,
  "range": 100,
  "totalValue": 95,
  "bounty": 0,
  "tier": "1",
  "legion": "Atlantean",
  "legionType": "atlantean_legion_id",
  "movementType": "Air",
  "spawnBias": 0.20000000298023224,
  "spawnDelay": 0,
  "effectiveDps": 34.54999923706055,
  "effectiveThreat": 34.54999923706055,
  "movementSize": 0.1875,
  "backswing": 0.6499999761581421,
  "projectileSpeed": 1200,
  "mastermindGroups": "None",
  "flags": "Air",
  "color": "2c9a98",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Atlantean T1U",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "adapt_into_seraphin_ability_id",
      "abilityClass": "",
      "name": "Adapt",
      "image": "Icons/Adapt(intoSeraphin).png",
      "description": "Changes form into a Seraphin. Can only be used during the build phase.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Adapt(intoSeraphin).png' /> Adapt<br /><span style='color: #ffcc00'> Order String:</span> Adapt<br /><span style='color: #ffcc00'> Unit:</span> <img class='tooltip-icon' src='hud/img/Icons/Seraphin.png' />Seraphin"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "unitType": "pollywog_unit_id",
      "name": "Pollywog",
      "image": "Icons/Pollywog.png"
    }
  ],
  "amountSpawned": 0
}
*/

export const raw_devilfish_unit_id: RawUnit = {
  name: "Devilfish",
  sketchfabUrl: "https://sketchfab.com/models/186b893bd8cc4c1ca2e13ad123da7684",
  image: require("#LTD2/Icons/Devilfish.png"),
  splash: require("#LTD2/splashes/Devilfish.png"),
  description: "Flying. Melee tank. Can be Adapted into a Seraphin.",

  damage: Damage.Magic,
  atkSpeed: 0.9090908893868948,
  onHitDmg: 38,

  armor: Armor.Swift,
  hp: 1120,
};

export const devilfish_unit_id: PlayableUnit = {
  ...raw_devilfish_unit_id,
  price: 80,
  evolFrom: pollywog_unit_id,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "angler_unit_id",
  "name": "Angler",
  "image": "Icons/Angler.png",
  "splash": "splashes/Angler.png",
  "sketchfabUrl": "https://sketchfab.com/models/98184134fa1f494a80ac001bf2d725ed",
  "description": "Accumulates \"stacks\" when enemies die. The more stacks it has, the stronger its final form will be.",
  "cost": 40,
  "mythium": 0,
  "income": 0,
  "supply": 1,
  "attackType": "Pierce",
  "attackTypeDescription": "115% to Arcane<br />80% to Fortified<br />85% to Natural<br />120% to Swift",
  "armorType": "Fortified",
  "armorTypeDescription": "115% from Impact<br />105% from Magic<br />80% from Pierce",
  "hp": 400,
  "mana": 75,
  "hpRegen": 0,
  "manaRegen": 0.0010000000474974513,
  "attackSpeed": 1.1200000047683716,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 17,
  "damage": 19,
  "range": 100,
  "totalValue": 40,
  "bounty": 0,
  "tier": "2",
  "legion": "Atlantean",
  "legionType": "atlantean_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.4000000059604645,
  "spawnDelay": 0,
  "effectiveDps": 16.959999084472656,
  "effectiveThreat": 16.959999084472656,
  "movementSize": 0.1875,
  "backswing": 0.5,
  "projectileSpeed": 1200,
  "mastermindGroups": "Pierce, Special1, Special2, Special3, Special4",
  "flags": "Ground",
  "color": "2c9a98",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Atlantean T2",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "junior_fisherman_ability_id",
      "abilityClass": "",
      "name": "Junior Fisherman",
      "image": "Icons/JuniorFisherman.png",
      "description": "Gains +1.5 mana when a nearby enemy dies. <span style='color: #ffcc00'>Mana carries over when upgraded. Upgraded forms will utilize mana.</span>",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/JuniorFisherman.png' /> Junior Fisherman<br /><span style='color: #ffcc00'> Order String:</span> FishermanTraining<br /><span style='color: #ffcc00'> Percentage:</span> 0.0000<br /><span style='color: #ffcc00'> Area of Effect:</span> 800<br /><span style='color: #ffcc00'> Custom Value 1:</span> 1.50000<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "mana_transfer_on_upgrade_ability_id",
      "abilityClass": "hidden",
      "name": "Mana Transfer On Upgrade",
      "image": "Icons/ManaTransferOnUpgrade.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Mana Transfer On Upgrade<br /><span style='color: #ffcc00'> Order String:</span> ManaTransferOnUpgrade<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "abilityType": "disable_mana_regen_tower_only_ability_id",
      "abilityClass": "hidden",
      "name": "Disable Mana Regen (Tower Only)",
      "image": "Icons/DisableManaRegen(TowerOnly).png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Disable Mana Regen (Tower Only)<br /><span style='color: #ffcc00'> Order String:</span> DisableManaRegen"
    }
  ],
  "upgradesTo": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 3,
      "unitType": "bounty_hunter_unit_id",
      "name": "Bounty Hunter",
      "image": "Icons/BountyHunter.png"
    }
  ],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_angler_unit_id: RawUnit = {
  name: "Angler",
  sketchfabUrl: "https://sketchfab.com/models/98184134fa1f494a80ac001bf2d725ed",
  image: require("#LTD2/Icons/Angler.png"),
  splash: require("#LTD2/splashes/Angler.png"),
  description: "Accumulates \"stacks\" when enemies die. The more stacks it has, the stronger its final form will be.",

  damage: Damage.Pierce,
  atkSpeed: 0.8928571390558262,
  onHitDmg: 19,

  armor: Armor.Fortified,
  hp: 400,
};

export const angler_unit_id: PlayableUnit = {
  ...raw_angler_unit_id,
  price: 40,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "bounty_hunter_unit_id",
  "name": "Bounty Hunter",
  "image": "Icons/BountyHunter.png",
  "splash": "splashes/BountyHunter.png",
  "sketchfabUrl": "https://sketchfab.com/models/71584856dabd47a2842f07d9ee4c2aac",
  "description": "A more proficient fisherman, it can now accumulate more stacks.",
  "cost": 125,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Pierce",
  "attackTypeDescription": "115% to Arcane<br />80% to Fortified<br />85% to Natural<br />120% to Swift",
  "armorType": "Fortified",
  "armorTypeDescription": "115% from Impact<br />105% from Magic<br />80% from Pierce",
  "hp": 1660,
  "mana": 150,
  "hpRegen": 0,
  "manaRegen": 0.0010000000474974513,
  "attackSpeed": 1.1200000047683716,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 70,
  "damage": 78,
  "range": 100,
  "totalValue": 165,
  "bounty": 0,
  "tier": "2",
  "legion": "Atlantean",
  "legionType": "atlantean_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.4000000059604645,
  "spawnDelay": 0,
  "effectiveDps": 69.63999938964844,
  "effectiveThreat": 69.63999938964844,
  "movementSize": 0.1875,
  "backswing": 0.5,
  "projectileSpeed": 1200,
  "mastermindGroups": "None",
  "flags": "Ground",
  "color": "2c9a98",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Atlantean T2U",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "commercial_fisherman_ability_id",
      "abilityClass": "",
      "name": "Commercial Fisherman",
      "image": "Icons/CommercialFisherman.png",
      "description": "Gains +1 mana when a nearby enemy dies. <span style='color: #ffcc00'>Mana carries over when upgraded. Upgraded forms will utilize mana.</span>",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/CommercialFisherman.png' /> Commercial Fisherman<br /><span style='color: #ffcc00'> Order String:</span> FishermanTraining<br /><span style='color: #ffcc00'> Percentage:</span> 0.0000<br /><span style='color: #ffcc00'> Area of Effect:</span> 800<br /><span style='color: #ffcc00'> Custom Value 1:</span> 1.0000<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "mana_transfer_on_upgrade_ability_id",
      "abilityClass": "hidden",
      "name": "Mana Transfer On Upgrade",
      "image": "Icons/ManaTransferOnUpgrade.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Mana Transfer On Upgrade<br /><span style='color: #ffcc00'> Order String:</span> ManaTransferOnUpgrade<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "abilityType": "disable_mana_regen_tower_only_ability_id",
      "abilityClass": "hidden",
      "name": "Disable Mana Regen (Tower Only)",
      "image": "Icons/DisableManaRegen(TowerOnly).png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Disable Mana Regen (Tower Only)<br /><span style='color: #ffcc00'> Order String:</span> DisableManaRegen"
    }
  ],
  "upgradesTo": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 4,
      "unitType": "kingpin_unit_id",
      "name": "Kingpin",
      "image": "Icons/Kingpin.png"
    }
  ],
  "upgradesFrom": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 3,
      "unitType": "angler_unit_id",
      "name": "Angler",
      "image": "Icons/Angler.png"
    }
  ],
  "amountSpawned": 0
}
*/

export const raw_bounty_hunter_unit_id: RawUnit = {
  name: "Bounty Hunter",
  sketchfabUrl: "https://sketchfab.com/models/71584856dabd47a2842f07d9ee4c2aac",
  image: require("#LTD2/Icons/BountyHunter.png"),
  splash: require("#LTD2/splashes/BountyHunter.png"),
  description: "A more proficient fisherman, it can now accumulate more stacks.",

  damage: Damage.Pierce,
  atkSpeed: 0.8928571390558262,
  onHitDmg: 78,

  armor: Armor.Fortified,
  hp: 1660,
};

export const bounty_hunter_unit_id: PlayableUnit = {
  ...raw_bounty_hunter_unit_id,
  price: 125,
  evolFrom: angler_unit_id,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "kingpin_unit_id",
  "name": "Kingpin",
  "image": "Icons/Kingpin.png",
  "splash": "splashes/Kingpin.png",
  "sketchfabUrl": "https://sketchfab.com/models/03e78a5052044e40bc3f795f178e4e89",
  "description": "The more stacks it has, the stronger its attack. However, it no longer accumulates new stacks. ",
  "cost": 250,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Pierce",
  "attackTypeDescription": "115% to Arcane<br />80% to Fortified<br />85% to Natural<br />120% to Swift",
  "armorType": "Fortified",
  "armorTypeDescription": "115% from Impact<br />105% from Magic<br />80% from Pierce",
  "hp": 4830,
  "mana": 150,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.399999976158142,
  "attackSpeedDescriptor": "Slow",
  "mspd": 300,
  "dps": 139,
  "damage": 195,
  "range": 100,
  "totalValue": 415,
  "bounty": 0,
  "tier": "2",
  "legion": "Atlantean",
  "legionType": "atlantean_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.4000000059604645,
  "spawnDelay": 0,
  "effectiveDps": 139.2899932861328,
  "effectiveThreat": 139.2899932861328,
  "movementSize": 0.1875,
  "backswing": 0.6499999761581421,
  "projectileSpeed": 1200,
  "mastermindGroups": "None",
  "flags": "Ground",
  "color": "2c9a98",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Atlantean T2UU",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "elite_fisherman_ability_id",
      "abilityClass": "",
      "name": "Elite Fisherman",
      "image": "Icons/EliteFisherman.png",
      "description": "No longer gains any mana. Deals +2 bonus damage for every 1 mana gained from previous forms.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/EliteFisherman.png' /> Elite Fisherman<br /><span style='color: #ffcc00'> Order String:</span> EliteFisherman<br /><span style='color: #ffcc00'> Damage Base:</span> 2.00000 (Ability Damage)<br /><span style='color: #ffcc00'>Target Buff:</span> Elite Fisherman"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "mana_transfer_on_upgrade_ability_id",
      "abilityClass": "hidden",
      "name": "Mana Transfer On Upgrade",
      "image": "Icons/ManaTransferOnUpgrade.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Mana Transfer On Upgrade<br /><span style='color: #ffcc00'> Order String:</span> ManaTransferOnUpgrade<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "abilityType": "disable_mana_regen_ability_id",
      "abilityClass": "hidden",
      "name": "Disable Mana Regen",
      "image": "Icons/DisableManaRegen.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Disable Mana Regen<br /><span style='color: #ffcc00'> Order String:</span> DisableManaRegen"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 3,
      "unitType": "bounty_hunter_unit_id",
      "name": "Bounty Hunter",
      "image": "Icons/BountyHunter.png"
    }
  ],
  "amountSpawned": 0
}
*/

export const raw_kingpin_unit_id: RawUnit = {
  name: "Kingpin",
  sketchfabUrl: "https://sketchfab.com/models/03e78a5052044e40bc3f795f178e4e89",
  image: require("#LTD2/Icons/Kingpin.png"),
  splash: require("#LTD2/splashes/Kingpin.png"),
  description: "The more stacks it has, the stronger its attack. However, it no longer accumulates new stacks. ",

  damage: Damage.Pierce,
  atkSpeed: 0.7142857264499277,
  onHitDmg: 195,

  armor: Armor.Fortified,
  hp: 4830,
};

export const kingpin_unit_id: PlayableUnit = {
  ...raw_kingpin_unit_id,
  price: 250,
  evolFrom: bounty_hunter_unit_id,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "sea_serpent_unit_id",
  "name": "Sea Serpent",
  "image": "Icons/SeaSerpent.png",
  "splash": "splashes/SeaSerpent.png",
  "sketchfabUrl": "https://sketchfab.com/models/dca25996d20346b290e4b02cfbae2624",
  "description": "Burrows into the ground, then unburrows itself 5 seconds later. Good when placed up front.",
  "cost": 90,
  "mythium": 0,
  "income": 0,
  "supply": 1,
  "attackType": "Pierce",
  "attackTypeDescription": "115% to Arcane<br />80% to Fortified<br />85% to Natural<br />120% to Swift",
  "armorType": "Swift",
  "armorTypeDescription": "80% from Impact<br />100% from Magic<br />120% from Pierce",
  "hp": 570,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.1299999952316284,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 66,
  "damage": 75,
  "range": 100,
  "totalValue": 90,
  "bounty": 0,
  "tier": "3",
  "legion": "Atlantean",
  "legionType": "atlantean_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.800000011920929,
  "spawnDelay": 0,
  "effectiveDps": 66.37000274658203,
  "effectiveThreat": 66.37000274658203,
  "movementSize": 0.1875,
  "backswing": 0.550000011920929,
  "projectileSpeed": 1200,
  "mastermindGroups": "Pierce, Special1, Special2, Special3, Special4",
  "flags": "Ground, Organic",
  "color": "2c9a98",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Atlantean T3",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "ambush_ability_id",
      "abilityClass": "",
      "name": "Ambush",
      "image": "Icons/Ambush.png",
      "description": "Starts the battle burrowed and unburrows 5 seconds after being aggro'd",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Ambush.png' /> Ambush<br /><span style='color: #ffcc00'> Order String:</span> Ambush<br /><span style='color: #ffcc00'> Duration:</span> 5.0000<br /><span style='color: #ffcc00'> Duration (Boss):</span> 5.0000<br /><span style='color: #ffcc00'>Target Buff:</span> Ambush<br /><span style='color: #efe1a7'>--- Statuses:</span> Stasis<br /><span style='color: #ffcc00'>Caster Buff:</span> Invulnerable<br /><span style='color: #efe1a7'>--- Statuses:</span> Stasis"
    }
  ],
  "upgradesTo": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "unitType": "deepcoiler_unit_id",
      "name": "Deepcoiler",
      "image": "Icons/Deepcoiler.png"
    }
  ],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_sea_serpent_unit_id: RawUnit = {
  name: "Sea Serpent",
  sketchfabUrl: "https://sketchfab.com/models/dca25996d20346b290e4b02cfbae2624",
  image: require("#LTD2/Icons/SeaSerpent.png"),
  splash: require("#LTD2/splashes/SeaSerpent.png"),
  description: "Burrows into the ground, then unburrows itself 5 seconds later. Good when placed up front.",

  damage: Damage.Pierce,
  atkSpeed: 0.8849557559467238,
  onHitDmg: 75,

  armor: Armor.Swift,
  hp: 570,
};

export const sea_serpent_unit_id: PlayableUnit = {
  ...raw_sea_serpent_unit_id,
  price: 90,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "deepcoiler_unit_id",
  "name": "Deepcoiler",
  "image": "Icons/Deepcoiler.png",
  "splash": "splashes/Deepcoiler.png",
  "sketchfabUrl": "https://sketchfab.com/models/0635e3bd04784d5d8936bb8522ce53ac",
  "description": "Its teeth deal massive damage, but it is greatly lacking in health.",
  "cost": 185,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Pierce",
  "attackTypeDescription": "115% to Arcane<br />80% to Fortified<br />85% to Natural<br />120% to Swift",
  "armorType": "Swift",
  "armorTypeDescription": "80% from Impact<br />100% from Magic<br />120% from Pierce",
  "hp": 1740,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.2400000095367432,
  "attackSpeedDescriptor": "Slow",
  "mspd": 300,
  "dps": 203,
  "damage": 252,
  "range": 100,
  "totalValue": 275,
  "bounty": 0,
  "tier": "3",
  "legion": "Atlantean",
  "legionType": "atlantean_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.800000011920929,
  "spawnDelay": 0,
  "effectiveDps": 203.22999572753906,
  "effectiveThreat": 203.22999572753906,
  "movementSize": 0.1875,
  "backswing": 0.6000000238418579,
  "projectileSpeed": 1200,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "2c9a98",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Atlantean T3U",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "ambush_ability_id",
      "abilityClass": "",
      "name": "Ambush",
      "image": "Icons/Ambush.png",
      "description": "Starts the battle burrowed and unburrows 5 seconds after being aggro'd",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Ambush.png' /> Ambush<br /><span style='color: #ffcc00'> Order String:</span> Ambush<br /><span style='color: #ffcc00'> Duration:</span> 5.0000<br /><span style='color: #ffcc00'> Duration (Boss):</span> 5.0000<br /><span style='color: #ffcc00'>Target Buff:</span> Ambush<br /><span style='color: #efe1a7'>--- Statuses:</span> Stasis<br /><span style='color: #ffcc00'>Caster Buff:</span> Invulnerable<br /><span style='color: #efe1a7'>--- Statuses:</span> Stasis"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "unitType": "sea_serpent_unit_id",
      "name": "Sea Serpent",
      "image": "Icons/SeaSerpent.png"
    }
  ],
  "amountSpawned": 0
}
*/

export const raw_deepcoiler_unit_id: RawUnit = {
  name: "Deepcoiler",
  sketchfabUrl: "https://sketchfab.com/models/0635e3bd04784d5d8936bb8522ce53ac",
  image: require("#LTD2/Icons/Deepcoiler.png"),
  splash: require("#LTD2/splashes/Deepcoiler.png"),
  description: "Its teeth deal massive damage, but it is greatly lacking in health.",

  damage: Damage.Pierce,
  atkSpeed: 0.8064516067008695,
  onHitDmg: 252,

  armor: Armor.Swift,
  hp: 1740,
};

export const deepcoiler_unit_id: PlayableUnit = {
  ...raw_deepcoiler_unit_id,
  price: 185,
  evolFrom: sea_serpent_unit_id,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "grarl_unit_id",
  "name": "Grarl",
  "image": "Icons/Grarl.png",
  "splash": "splashes/Grarl.png",
  "sketchfabUrl": "https://sketchfab.com/models/2679e485574f4815b4eed2ac4bd87982",
  "description": "Owing to its tasty meat, it can be sold for a high percentage of its original cost.",
  "cost": 145,
  "mythium": 0,
  "income": 0,
  "supply": 1,
  "attackType": "Impact",
  "attackTypeDescription": "115% to Arcane<br />115% to Fortified<br />90% to Natural<br />80% to Swift",
  "armorType": "Natural",
  "armorTypeDescription": "90% from Impact<br />125% from Magic<br />85% from Pierce",
  "hp": 1400,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.1699999570846558,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 52,
  "damage": 61,
  "range": 100,
  "totalValue": 145,
  "bounty": 0,
  "tier": "4",
  "legion": "Atlantean",
  "legionType": "atlantean_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.30000001192092896,
  "spawnDelay": 0,
  "effectiveDps": 52.13999938964844,
  "effectiveThreat": 52.13999938964844,
  "movementSize": 0.1875,
  "backswing": 0.6000000238418579,
  "projectileSpeed": 1200,
  "mastermindGroups": "FortOrNat, SwiftOrNat, Impact",
  "flags": "Ground, Organic",
  "color": "2c9a98",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Atlantean T4",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "delicacy_hardcoded_ability_id",
      "abilityClass": "",
      "name": "Delicacy",
      "image": "Icons/Delicacy.png",
      "description": "May be sold for 90% of its total cost on any wave",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Delicacy.png' /> Delicacy<br /><span style='color: #ffcc00'> Order String:</span> JuiceUp"
    }
  ],
  "upgradesTo": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "unitType": "ocean_templar_unit_id",
      "name": "Ocean Templar",
      "image": "Icons/OceanTemplar.png"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "unitType": "king_claw_unit_id",
      "name": "King Claw",
      "image": "Icons/KingClaw.png"
    }
  ],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_grarl_unit_id: RawUnit = {
  name: "Grarl",
  sketchfabUrl: "https://sketchfab.com/models/2679e485574f4815b4eed2ac4bd87982",
  image: require("#LTD2/Icons/Grarl.png"),
  splash: require("#LTD2/splashes/Grarl.png"),
  description: "Owing to its tasty meat, it can be sold for a high percentage of its original cost.",

  damage: Damage.Impact,
  atkSpeed: 0.8547008860510964,
  onHitDmg: 61,

  armor: Armor.Natural,
  hp: 1400,
};

export const grarl_unit_id: PlayableUnit = {
  ...raw_grarl_unit_id,
  price: 145,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "king_claw_unit_id",
  "name": "King Claw",
  "image": "Icons/KingClaw.png",
  "splash": "splashes/KingClaw.png",
  "sketchfabUrl": "https://sketchfab.com/models/856ce781721747e6869ebcb67ed92ec4",
  "description": "Tanky. Stomps the ground to damage and slow enemies.",
  "cost": 275,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Impact",
  "attackTypeDescription": "115% to Arcane<br />115% to Fortified<br />90% to Natural<br />80% to Swift",
  "armorType": "Natural",
  "armorTypeDescription": "90% from Impact<br />125% from Magic<br />85% from Pierce",
  "hp": 4450,
  "mana": 12,
  "hpRegen": 0,
  "manaRegen": 1.1200000047683716,
  "attackSpeed": 1.2200000286102295,
  "attackSpeedDescriptor": "Slow",
  "mspd": 300,
  "dps": 102,
  "damage": 125,
  "range": 100,
  "totalValue": 420,
  "bounty": 0,
  "tier": "4",
  "legion": "Atlantean",
  "legionType": "atlantean_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.10000000149011612,
  "spawnDelay": 0,
  "effectiveDps": 102.45999908447266,
  "effectiveThreat": 102.45999908447266,
  "movementSize": 0.1875,
  "backswing": 0.6200000047683716,
  "projectileSpeed": 1200,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "2c9a98",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Atlantean T4U",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "shell_shock_ability_id",
      "abilityClass": "",
      "name": "Shell Shock",
      "image": "Icons/ShellShock.png",
      "description": "Deals 75 impact damage to nearby enemies and slows their attack speed by 5%. Ability damage.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/ShellShock.png' /> Shell Shock<br /><span style='color: #ffcc00'> Order String:</span> VioletWind<br /><span style='color: #ffcc00'> Projectile - Speed:</span> 2500<br /><span style='color: #ffcc00'> Attack Type:</span> <img class='tooltip-icon' src='hud/img/icons/Impact.png' /> Impact<br /><span style='color: #ffcc00'> Mana Cost:</span> 10.50000<br /><span style='color: #ffcc00'> Damage Base:</span> 75.00000 (Ability Damage)<br /><span style='color: #ffcc00'> Area of Effect:</span> 250<br /><span style='color: #ffcc00'> Duration:</span> 3.0000<br /><span style='color: #ffcc00'> Duration (Boss):</span> 3.0000<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy<br /><span style='color: #ffcc00'>Target Buff:</span> Shell Shock<br /><span style='color: #efe1a7'>--- Max Stacks:</span> 40<br /><span style='color: #efe1a7'>--- Stats - Attack Rate Bonus (%):</span> -0.05000"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "unitType": "grarl_unit_id",
      "name": "Grarl",
      "image": "Icons/Grarl.png"
    }
  ],
  "amountSpawned": 0
}
*/

export const raw_king_claw_unit_id: RawUnit = {
  name: "King Claw",
  sketchfabUrl: "https://sketchfab.com/models/856ce781721747e6869ebcb67ed92ec4",
  image: require("#LTD2/Icons/KingClaw.png"),
  splash: require("#LTD2/splashes/KingClaw.png"),
  description: "Tanky. Stomps the ground to damage and slow enemies.",

  damage: Damage.Impact,
  atkSpeed: 0.8196721119254039,
  onHitDmg: 125,

  armor: Armor.Natural,
  hp: 4450,
};

export const king_claw_unit_id: PlayableUnit = {
  ...raw_king_claw_unit_id,
  price: 275,
  evolFrom: grarl_unit_id,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "ocean_templar_unit_id",
  "name": "Ocean Templar",
  "image": "Icons/OceanTemplar.png",
  "splash": "splashes/OceanTemplar.png",
  "sketchfabUrl": "https://sketchfab.com/models/e101933716fd4091af82131ccb426d6a",
  "description": "Aura. Passively heals and grants magic resistance to adjacently-built units",
  "cost": 250,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Pure",
  "attackTypeDescription": "100% to all defense types.",
  "armorType": "Natural",
  "armorTypeDescription": "90% from Impact<br />125% from Magic<br />85% from Pierce",
  "hp": 2000,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.1799999475479126,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 140,
  "damage": 165,
  "range": 450,
  "totalValue": 395,
  "bounty": 0,
  "tier": "4",
  "legion": "Atlantean",
  "legionType": "atlantean_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.699999988079071,
  "spawnDelay": 0,
  "effectiveDps": 139.8300018310547,
  "effectiveThreat": 139.8300018310547,
  "movementSize": 0.1875,
  "backswing": 0.6000000238418579,
  "projectileSpeed": 2500,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "2c9a98",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Atlantean T4U",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "resonance_oracle_ability_id",
      "abilityClass": "",
      "name": "Resonance",
      "image": "Icons/Resonance.png",
      "description": "Grants to adjacently-built units 2% missing health regeneration and 20% damage reduction from magic damage. Does not stack.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Resonance.png' /> Resonance<br /><span style='color: #ffcc00'> Order String:</span> Oracle<br /><span style='color: #ffcc00'> Area of Effect:</span> 120<br /><span style='color: #ffcc00'> Targets Considered:</span> Allied, Invulnerable<br /><span style='color: #ffcc00'>Target Buff:</span> Resonance<br /><span style='color: #efe1a7'>--- Stats - HP Regeneration Bonus (% of Missing HP):</span> 0.02000"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "resonance_ability_id",
      "abilityClass": "hidden",
      "name": "Resonance",
      "image": "Icons/Resonance.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Resonance<br /><span style='color: #ffcc00'> Order String:</span> SimpleAura<br /><span style='color: #ffcc00'> Area of Effect:</span> 120<br /><span style='color: #ffcc00'> Targets Considered:</span> Allied<br /><span style='color: #ffcc00'> Targets Excluded:</span> Boss<br /><span style='color: #ffcc00'>Target Buff:</span> Resonance (Night Buff)<br /><span style='color: #ffcc00'>Caster Buff:</span> Resonance (Caster Buff)"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "abilityType": "resonance_oracle_gate_ability_id",
      "abilityClass": "hidden",
      "name": "Resonance (Gate Oracle version)",
      "image": "Icons/Resonance.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Resonance (Gate Oracle version)<br /><span style='color: #ffcc00'> Order String:</span> Oracle<br /><span style='color: #ffcc00'> Area of Effect:</span> 120<br /><span style='color: #ffcc00'> Targets Considered:</span> Allied, NotSelf, Invulnerable<br /><span style='color: #ffcc00'> Unit:</span> <img class='tooltip-icon' src='hud/img/Icons/SoulGate.png' />Soul Gate<br /><span style='color: #ffcc00'> Unit 2:</span> <img class='tooltip-icon' src='hud/img/Icons/HellGate.png' />Hell Gate<br /><span style='color: #ffcc00'>Target Buff:</span> Resonance (Gate Buff)"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 3,
      "abilityType": "resonance_gate_ability_id",
      "abilityClass": "hidden",
      "name": "Resonance (Gate)",
      "image": "Icons/Resonance.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Resonance (Gate)<br /><span style='color: #ffcc00'> Order String:</span> SimpleAura<br /><span style='color: #ffcc00'> Area of Effect:</span> 120<br /><span style='color: #ffcc00'> Targets Considered:</span> Allied, NotSelf<br /><span style='color: #ffcc00'> Targets Excluded:</span> Boss<br /><span style='color: #ffcc00'> Unit:</span> <img class='tooltip-icon' src='hud/img/Icons/SoulGate.png' />Soul Gate<br /><span style='color: #ffcc00'> Unit 2:</span> <img class='tooltip-icon' src='hud/img/Icons/HellGate.png' />Hell Gate<br /><span style='color: #ffcc00'>Target Buff:</span> Resonance (Gate Buff)"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 4,
      "unitType": "grarl_unit_id",
      "name": "Grarl",
      "image": "Icons/Grarl.png"
    }
  ],
  "amountSpawned": 0
}
*/

export const raw_ocean_templar_unit_id: RawUnit = {
  name: "Ocean Templar",
  sketchfabUrl: "https://sketchfab.com/models/e101933716fd4091af82131ccb426d6a",
  image: require("#LTD2/Icons/OceanTemplar.png"),
  splash: require("#LTD2/splashes/OceanTemplar.png"),
  description: "Aura. Passively heals and grants magic resistance to adjacently-built units",

  damage: Damage.Pure,
  atkSpeed: 0.8474576647889183,
  onHitDmg: 165,

  armor: Armor.Natural,
  hp: 2000,
};

export const ocean_templar_unit_id: PlayableUnit = {
  ...raw_ocean_templar_unit_id,
  price: 250,
  evolFrom: grarl_unit_id,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "priestess_of_the_abyss_unit_id",
  "name": "Priestess of the Abyss",
  "image": "Icons/PriestessOfTheAbyss.png",
  "splash": "splashes/PriestessOfTheAbyss.png",
  "sketchfabUrl": "https://sketchfab.com/models/831f38088a3140cb8b50224480fd0deb",
  "description": "Periodically boosts its attack speed and heals itself.",
  "cost": 190,
  "mythium": 0,
  "income": 0,
  "supply": 1,
  "attackType": "Magic",
  "attackTypeDescription": "75% to Arcane<br />105% to Fortified<br />125% to Natural<br />100% to Swift",
  "armorType": "Arcane",
  "armorTypeDescription": "115% from Impact<br />75% from Magic<br />115% from Pierce",
  "hp": 1000,
  "mana": 13,
  "hpRegen": 0,
  "manaRegen": 1.149999976158142,
  "attackSpeed": 1.0299999713897705,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 23,
  "damage": 24,
  "range": 600,
  "totalValue": 190,
  "bounty": 0,
  "tier": "5",
  "legion": "Atlantean",
  "legionType": "atlantean_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.800000011920929,
  "spawnDelay": 0,
  "effectiveDps": 23.299999237060547,
  "effectiveThreat": 23.299999237060547,
  "movementSize": 0.1875,
  "backswing": 0.4399999976158142,
  "projectileSpeed": 2000,
  "mastermindGroups": "Magic, Special1, Special2, Special3, Special4",
  "flags": "Ground",
  "color": "2c9a98",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Atlantean T5",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "burst_attack_ability_id",
      "abilityClass": "",
      "name": "Burst Attack",
      "image": "Icons/BurstAttack.png",
      "description": "Each attack launches 2 extra missiles, each dealing 100% base damage. Ability damage.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/BurstAttack.png' /> Burst Attack<br /><span style='color: #ffcc00'>Damage Type:</span> Ability Damage<br /><span style='color: #ffcc00'> Order String:</span> BurstAttack<br /><span style='color: #ffcc00'> Projectile - Speed:</span> 2400<br /><span style='color: #ffcc00'> Percentage:</span> 1.0000<br /><span style='color: #ffcc00'> Max Targets:</span> 2<br /><span style='color: #ffcc00'> Custom Value 1:</span> 0.1500<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "invigorate_ability_id",
      "abilityClass": "",
      "name": "Invigorate",
      "image": "Icons/Invigorate.png",
      "description": "Increases her attack speed by 100% and heals for 8% missing health/sec for 5 seconds",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Invigorate.png' /> Invigorate<br /><span style='color: #ffcc00'> Order String:</span> Mindwarp<br /><span style='color: #ffcc00'> Projectile - Speed:</span> 3000<br /><span style='color: #ffcc00'> Mana Cost:</span> 11.70000<br /><span style='color: #ffcc00'> Duration:</span> 5.0000<br /><span style='color: #ffcc00'> Duration (Boss):</span> 5.0000<br /><span style='color: #ffcc00'> Targets Considered:</span> Self, Invulnerable<br /><span style='color: #ffcc00'>Target Buff:</span> Invigorate<br /><span style='color: #efe1a7'>--- Stats - Attack Rate Bonus (%):</span> 1.0000<br /><span style='color: #efe1a7'>--- Stats - HP Regeneration Bonus (% of Missing HP):</span> 0.08000"
    }
  ],
  "upgradesTo": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "unitType": "azeria_unit_id",
      "name": "Azeria",
      "image": "Icons/Azeria.png"
    }
  ],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_priestess_of_the_abyss_unit_id: RawUnit = {
  name: "Priestess of the Abyss",
  sketchfabUrl: "https://sketchfab.com/models/831f38088a3140cb8b50224480fd0deb",
  image: require("#LTD2/Icons/PriestessOfTheAbyss.png"),
  splash: require("#LTD2/splashes/PriestessOfTheAbyss.png"),
  description: "Periodically boosts its attack speed and heals itself.",

  damage: Damage.Magic,
  atkSpeed: 0.970873813375653,
  onHitDmg: 24,

  armor: Armor.Arcane,
  hp: 1000,
};

export const priestess_of_the_abyss_unit_id: PlayableUnit = {
  ...raw_priestess_of_the_abyss_unit_id,
  price: 190,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "azeria_unit_id",
  "name": "Azeria",
  "image": "Icons/Azeria.png",
  "splash": "splashes/Azeria.png",
  "sketchfabUrl": "https://sketchfab.com/models/4e08e05c790f42d986338d7e20ad72ee",
  "description": "Its attacks now also deal % health damage.",
  "cost": 350,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Magic",
  "attackTypeDescription": "75% to Arcane<br />105% to Fortified<br />125% to Natural<br />100% to Swift",
  "armorType": "Arcane",
  "armorTypeDescription": "115% from Impact<br />75% from Magic<br />115% from Pierce",
  "hp": 3250,
  "mana": 18,
  "hpRegen": 0,
  "manaRegen": 1.600000023841858,
  "attackSpeed": 1.0299999713897705,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 64,
  "damage": 66,
  "range": 600,
  "totalValue": 540,
  "bounty": 0,
  "tier": "5",
  "legion": "Atlantean",
  "legionType": "atlantean_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.800000011920929,
  "spawnDelay": 0,
  "effectiveDps": 64.08000183105469,
  "effectiveThreat": 64.08000183105469,
  "movementSize": 0.1875,
  "backswing": 0.28999999165534973,
  "projectileSpeed": 2000,
  "mastermindGroups": "None",
  "flags": "Ground",
  "color": "2c9a98",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Atlantean T5U",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "invigorate_azeria_ability_id",
      "abilityClass": "",
      "name": "Invigorate",
      "image": "Icons/Invigorate.png",
      "description": "Increases her attack speed by 100% and heals for 8% missing health/sec for 5 seconds",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Invigorate.png' /> Invigorate<br /><span style='color: #ffcc00'> Order String:</span> Mindwarp<br /><span style='color: #ffcc00'> Projectile - Speed:</span> 3000<br /><span style='color: #ffcc00'> Mana Cost:</span> 16.30000<br /><span style='color: #ffcc00'> Duration:</span> 5.0000<br /><span style='color: #ffcc00'> Duration (Boss):</span> 5.0000<br /><span style='color: #ffcc00'> Targets Considered:</span> Self, Invulnerable<br /><span style='color: #ffcc00'>Target Buff:</span> Invigorate<br /><span style='color: #efe1a7'>--- Stats - Attack Rate Bonus (%):</span> 1.0000<br /><span style='color: #efe1a7'>--- Stats - HP Regeneration Bonus (% of Missing HP):</span> 0.08000"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "echostrike_azeria_ability_id",
      "abilityClass": "",
      "name": "Echostrike",
      "image": "Icons/Echostrike.png",
      "description": "Each attack launches 2 extra missiles, each dealing 100% base damage plus bonus damage equal to 0.04% target max health. Ability damage.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Echostrike.png' /> Echostrike<br /><span style='color: #ffcc00'>Damage Type:</span> Ability Damage<br /><span style='color: #ffcc00'> Order String:</span> BurstAttack<br /><span style='color: #ffcc00'> Projectile - Speed:</span> 2400<br /><span style='color: #ffcc00'> Percentage:</span> 1.0000<br /><span style='color: #ffcc00'> Max Targets:</span> 2<br /><span style='color: #ffcc00'> Custom Value 1:</span> 0.1500<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "unitType": "priestess_of_the_abyss_unit_id",
      "name": "Priestess of the Abyss",
      "image": "Icons/PriestessOfTheAbyss.png"
    }
  ],
  "amountSpawned": 0
}
*/

export const raw_azeria_unit_id: RawUnit = {
  name: "Azeria",
  sketchfabUrl: "https://sketchfab.com/models/4e08e05c790f42d986338d7e20ad72ee",
  image: require("#LTD2/Icons/Azeria.png"),
  splash: require("#LTD2/splashes/Azeria.png"),
  description: "Its attacks now also deal % health damage.",

  damage: Damage.Magic,
  atkSpeed: 0.970873813375653,
  onHitDmg: 66,

  armor: Armor.Arcane,
  hp: 3250,
};

export const azeria_unit_id: PlayableUnit = {
  ...raw_azeria_unit_id,
  price: 350,
  evolFrom: priestess_of_the_abyss_unit_id,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "eggsack_unit_id",
  "name": "Eggsack",
  "image": "Icons/Eggsack.png",
  "splash": "splashes/Eggsack.png",
  "sketchfabUrl": "https://sketchfab.com/models/f2ace6888907494aa7eca8d3f933dc77",
  "description": "Unique fighter. It can't move or attack, but it reflects damage. It evolves into a Hydra after 2 waves, or earlier - in a weakened form - if the egg is destroyed.",
  "cost": 275,
  "mythium": 0,
  "income": 0,
  "supply": 1,
  "attackType": "Impact",
  "attackTypeDescription": "115% to Arcane<br />115% to Fortified<br />90% to Natural<br />80% to Swift",
  "armorType": "Natural",
  "armorTypeDescription": "90% from Impact<br />125% from Magic<br />85% from Pierce",
  "hp": 2200,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.0399999618530273,
  "attackSpeedDescriptor": "Average",
  "mspd": 0,
  "dps": 0,
  "damage": 0,
  "range": 100,
  "totalValue": 275,
  "bounty": 0,
  "tier": "6",
  "legion": "Atlantean",
  "legionType": "atlantean_legion_id",
  "movementType": "None",
  "spawnBias": 0.5,
  "spawnDelay": 0,
  "effectiveDps": 0,
  "effectiveThreat": 0,
  "movementSize": 0.25,
  "backswing": 0.15000000596046448,
  "projectileSpeed": 1200,
  "mastermindGroups": "Special1, Special2, Special3",
  "flags": "Ground",
  "color": "2c9a98",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Atlantean T6",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "incubation_ability_id",
      "abilityClass": "",
      "name": "Incubation",
      "image": "Icons/Incubation.png",
      "description": "If killed before hatching naturally, it will hatch with penalized stats. Each stack reduces damage dealt by 17.5% and increases damage taken by 17.5%.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Incubation.png' /> Incubation<br /><span style='color: #ffcc00'> Order String:</span> Incubation<br /><span style='color: #ffcc00'> Custom Value 1:</span> 2.0000<br /><span style='color: #ffcc00'> Custom Value 2:</span> -1.00000<br /><span style='color: #ffcc00'> Unit:</span> <img class='tooltip-icon' src='hud/img/Icons/Hydra.png' />Hydra<br /><span style='color: #ffcc00'>Target Buff:</span> Incubation<br /><span style='color: #efe1a7'>--- Max Stacks:</span> 9"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "ambernite_shell_ability_id",
      "abilityClass": "",
      "name": "Azurite Shell",
      "image": "Icons/AzuriteShell.png",
      "description": "Whenever this unit is attacked, it reflects 10 damage back as pure damage",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/AzuriteShell.png' /> Azurite Shell<br /><span style='color: #ffcc00'> Order String:</span> Thorns<br /><span style='color: #ffcc00'> Damage Base:</span> 10.00000 (Ability Damage)<br /><span style='color: #ffcc00'>Caster Buff:</span> eggsack_damage_reduction"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "abilityType": "eggsplosion_ability_id",
      "abilityClass": "",
      "name": "Eggsplosion",
      "image": "Icons/Eggsplosion.png",
      "description": "When this unit dies, it explodes, dealing 80 Pure damage to nearby enemies.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Eggsplosion.png' /> Eggsplosion<br /><span style='color: #ffcc00'> Order String:</span> ExplodeOnDeath<br /><span style='color: #ffcc00'> Attack Type:</span> <img class='tooltip-icon' src='hud/img/icons/Magic.png' /> Magic<br /><span style='color: #ffcc00'> Damage Base:</span> 80.00000 (Ability Damage)<br /><span style='color: #ffcc00'> Area of Effect:</span> 150<br /><span style='color: #ffcc00'> Max Targets:</span> 20<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_eggsack_unit_id: RawUnit = {
  name: "Eggsack",
  sketchfabUrl: "https://sketchfab.com/models/f2ace6888907494aa7eca8d3f933dc77",
  image: require("#LTD2/Icons/Eggsack.png"),
  splash: require("#LTD2/splashes/Eggsack.png"),
  description: "Unique fighter. It can't move or attack, but it reflects damage. It evolves into a Hydra after 2 waves, or earlier - in a weakened form - if the egg is destroyed.",

  damage: Damage.Impact,
  atkSpeed: 0.9615384968074834,
  onHitDmg: 0,

  armor: Armor.Natural,
  hp: 2200,
};

export const eggsack_unit_id: PlayableUnit = {
  ...raw_eggsack_unit_id,
  price: 275,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "hydra_unit_id",
  "name": "Hydra",
  "image": "Icons/Hydra.png",
  "splash": "splashes/Hydra.png",
  "sketchfabUrl": "https://sketchfab.com/models/b975825ec7f14054b27eb873412a7dcf",
  "description": "An ancient diety. When it dies, it splits into 3 hydralings.",
  "cost": 0,
  "mythium": 0,
  "income": 0,
  "supply": 1,
  "attackType": "Impact",
  "attackTypeDescription": "115% to Arcane<br />115% to Fortified<br />90% to Natural<br />80% to Swift",
  "armorType": "Natural",
  "armorTypeDescription": "90% from Impact<br />125% from Magic<br />85% from Pierce",
  "hp": 2730,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.2000000476837158,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 105,
  "damage": 126,
  "range": 100,
  "totalValue": 420,
  "bounty": 0,
  "tier": "6",
  "legion": "Atlantean",
  "legionType": "atlantean_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.10000000149011612,
  "spawnDelay": 0,
  "effectiveDps": 105,
  "effectiveThreat": 105,
  "movementSize": 0.1875,
  "backswing": 0.6499999761581421,
  "projectileSpeed": 1200,
  "mastermindGroups": "None",
  "flags": "Ground",
  "color": "2c9a98",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Atlantean T6U",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "hydralings_ability_id",
      "abilityClass": "",
      "name": "Hydralings",
      "image": "Icons/Hydralings.png",
      "description": "When this unit dies, it splits into 3 hydralings.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Hydralings.png' /> Hydralings<br /><span style='color: #ffcc00'> Order String:</span> SplitOnDeath<br /><span style='color: #ffcc00'> Unit:</span> <img class='tooltip-icon' src='hud/img/Icons/Hydraling.png' />Hydraling"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_hydra_unit_id: RawUnit = {
  name: "Hydra",
  sketchfabUrl: "https://sketchfab.com/models/b975825ec7f14054b27eb873412a7dcf",
  image: require("#LTD2/Icons/Hydra.png"),
  splash: require("#LTD2/splashes/Hydra.png"),
  description: "An ancient diety. When it dies, it splits into 3 hydralings.",

  damage: Damage.Impact,
  atkSpeed: 0.8333333002196431,
  onHitDmg: 126,

  armor: Armor.Natural,
  hp: 2730,
};

export const hydra_unit_id: PlayableUnit = {
  ...raw_hydra_unit_id,
  price: 0,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "chained_fist_unit_id",
  "name": "Chained Fist",
  "image": "Icons/ChainedFist.png",
  "splash": "splashes/ChainedFist.png",
  "sketchfabUrl": "https://sketchfab.com/models/26d169e48b814dc7a9087ed3ca0d25c7",
  "description": "Tanky. Attacks quickly when low life.",
  "cost": 20,
  "mythium": 0,
  "income": 0,
  "supply": 1,
  "attackType": "Impact",
  "attackTypeDescription": "115% to Arcane<br />115% to Fortified<br />90% to Natural<br />80% to Swift",
  "armorType": "Fortified",
  "armorTypeDescription": "115% from Impact<br />105% from Magic<br />80% from Pierce",
  "hp": 200,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.3799999952316284,
  "attackSpeedDescriptor": "Slow",
  "mspd": 300,
  "dps": 6,
  "damage": 8,
  "range": 100,
  "totalValue": 20,
  "bounty": 0,
  "tier": "1",
  "legion": "Divine",
  "legionType": "divine_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.10000000149011612,
  "spawnDelay": 0,
  "effectiveDps": 5.800000190734863,
  "effectiveThreat": 5.800000190734863,
  "movementSize": 0.1875,
  "backswing": 0.47999998927116394,
  "projectileSpeed": 1200,
  "mastermindGroups": "FortOrNat, Special1",
  "flags": "Ground, Organic",
  "color": "b89b41",
  "turnRate": 0.10000000149011612,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Divine T1",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "unchained_rage_activation_ability_id",
      "abilityClass": "",
      "name": "Unchained Rage",
      "image": "Icons/UnchainedRage.png",
      "description": "Once it reaches 50% life, it attacks 260% faster, but hits random targets.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/UnchainedRage.png' /> Unchained Rage<br /><span style='color: #ffcc00'> Linked Abilities:</span> <img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Unchained Rage (Effect)<br /><span style='color: #ffcc00'> Order String:</span> ActivateRageMode<br /><span style='color: #ffcc00'> Percentage:</span> 0.50000"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "unchained_rage_ability_id",
      "abilityClass": "linked",
      "name": "Unchained Rage (Effect)",
      "image": "Icons/UnchainedRage.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Unchained Rage (Effect)<br /><span style='color: #ffcc00'>Damage Type:</span> Ability Damage<br /><span style='color: #ffcc00'> Order String:</span> SilentScream<br /><span style='color: #ffcc00'> Projectile - Speed:</span> 0<br /><span style='color: #ffcc00'> Damage Base:</span> 0.00000 (Ability Damage)<br /><span style='color: #ffcc00'> Max Targets:</span> 1<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy<br /><span style='color: #ffcc00'>Caster Buff:</span> Unchained Rage (Buff)<br /><span style='color: #efe1a7'>--- Stats - Attack Rate Bonus (%):</span> 2.60000"
    }
  ],
  "upgradesTo": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "unitType": "oathbreaker_unit_id",
      "name": "Oathbreaker",
      "image": "Icons/Oathbreaker.png"
    }
  ],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_chained_fist_unit_id: RawUnit = {
  name: "Chained Fist",
  sketchfabUrl: "https://sketchfab.com/models/26d169e48b814dc7a9087ed3ca0d25c7",
  image: require("#LTD2/Icons/ChainedFist.png"),
  splash: require("#LTD2/splashes/ChainedFist.png"),
  description: "Tanky. Attacks quickly when low life.",

  damage: Damage.Impact,
  atkSpeed: 0.7246376836632911,
  onHitDmg: 8,

  armor: Armor.Fortified,
  hp: 200,
};

export const chained_fist_unit_id: PlayableUnit = {
  ...raw_chained_fist_unit_id,
  price: 20,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "oathbreaker_unit_id",
  "name": "Oathbreaker",
  "image": "Icons/Oathbreaker.png",
  "splash": "splashes/Oathbreaker.png",
  "sketchfabUrl": "https://sketchfab.com/models/3cd702c974dd485791db52f5894a8d91",
  "description": "Tanky. Attacks quickly when low life.",
  "cost": 75,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Impact",
  "attackTypeDescription": "115% to Arcane<br />115% to Fortified<br />90% to Natural<br />80% to Swift",
  "armorType": "Fortified",
  "armorTypeDescription": "115% from Impact<br />105% from Magic<br />80% from Pierce",
  "hp": 1080,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.3799999952316284,
  "attackSpeedDescriptor": "Slow",
  "mspd": 300,
  "dps": 25,
  "damage": 35,
  "range": 100,
  "totalValue": 95,
  "bounty": 0,
  "tier": "1",
  "legion": "Divine",
  "legionType": "divine_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.10000000149011612,
  "spawnDelay": 0,
  "effectiveDps": 25.360000610351562,
  "effectiveThreat": 25.360000610351562,
  "movementSize": 0.1875,
  "backswing": 0.47999998927116394,
  "projectileSpeed": 1200,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "b89b41",
  "turnRate": 0.10000000149011612,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Divine T1U",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "unchained_rage_activation_ability_id",
      "abilityClass": "",
      "name": "Unchained Rage",
      "image": "Icons/UnchainedRage.png",
      "description": "Once it reaches 50% life, it attacks 260% faster, but hits random targets.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/UnchainedRage.png' /> Unchained Rage<br /><span style='color: #ffcc00'> Linked Abilities:</span> <img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Unchained Rage (Effect)<br /><span style='color: #ffcc00'> Order String:</span> ActivateRageMode<br /><span style='color: #ffcc00'> Percentage:</span> 0.50000"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "unchained_rage_ability_id",
      "abilityClass": "linked",
      "name": "Unchained Rage (Effect)",
      "image": "Icons/UnchainedRage.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Unchained Rage (Effect)<br /><span style='color: #ffcc00'>Damage Type:</span> Ability Damage<br /><span style='color: #ffcc00'> Order String:</span> SilentScream<br /><span style='color: #ffcc00'> Projectile - Speed:</span> 0<br /><span style='color: #ffcc00'> Damage Base:</span> 0.00000 (Ability Damage)<br /><span style='color: #ffcc00'> Max Targets:</span> 1<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy<br /><span style='color: #ffcc00'>Caster Buff:</span> Unchained Rage (Buff)<br /><span style='color: #efe1a7'>--- Stats - Attack Rate Bonus (%):</span> 2.60000"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "unitType": "chained_fist_unit_id",
      "name": "Chained Fist",
      "image": "Icons/ChainedFist.png"
    }
  ],
  "amountSpawned": 0
}
*/

export const raw_oathbreaker_unit_id: RawUnit = {
  name: "Oathbreaker",
  sketchfabUrl: "https://sketchfab.com/models/3cd702c974dd485791db52f5894a8d91",
  image: require("#LTD2/Icons/Oathbreaker.png"),
  splash: require("#LTD2/splashes/Oathbreaker.png"),
  description: "Tanky. Attacks quickly when low life.",

  damage: Damage.Impact,
  atkSpeed: 0.7246376836632911,
  onHitDmg: 35,

  armor: Armor.Fortified,
  hp: 1080,
};

export const oathbreaker_unit_id: PlayableUnit = {
  ...raw_oathbreaker_unit_id,
  price: 75,
  evolFrom: chained_fist_unit_id,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "golden_buckler_unit_id",
  "name": "Golden Buckler",
  "image": "Icons/GoldenBuckler.png",
  "splash": "splashes/GoldenBuckler.png",
  "sketchfabUrl": "https://sketchfab.com/models/4301cc18e6bb4751b0994a83f5b03da6",
  "description": "Tanky. Takes reduced damage from ranged units.",
  "cost": 50,
  "mythium": 0,
  "income": 0,
  "supply": 1,
  "attackType": "Impact",
  "attackTypeDescription": "115% to Arcane<br />115% to Fortified<br />90% to Natural<br />80% to Swift",
  "armorType": "Natural",
  "armorTypeDescription": "90% from Impact<br />125% from Magic<br />85% from Pierce",
  "hp": 570,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.3200000524520874,
  "attackSpeedDescriptor": "Slow",
  "mspd": 300,
  "dps": 16,
  "damage": 21,
  "range": 100,
  "totalValue": 50,
  "bounty": 0,
  "tier": "2",
  "legion": "Divine",
  "legionType": "divine_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.10000000149011612,
  "spawnDelay": 0,
  "effectiveDps": 15.90999984741211,
  "effectiveThreat": 15.90999984741211,
  "movementSize": 0.1875,
  "backswing": 0.4000000059604645,
  "projectileSpeed": 1200,
  "mastermindGroups": "FortOrNat, SwiftOrNat, Impact",
  "flags": "Ground, Organic",
  "color": "b89b41",
  "turnRate": 0.10000000149011612,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Divine T2",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "deflection_ability_id",
      "abilityClass": "",
      "name": "Deflection",
      "image": "Icons/Deflection.png",
      "description": "Reduces damage taken from ranged units by 15%",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Deflection.png' /> Deflection<br /><span style='color: #ffcc00'> Order String:</span> RangedDamageReduction<br /><span style='color: #ffcc00'> Percentage:</span> 0.150"
    }
  ],
  "upgradesTo": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "unitType": "royal_guard_unit_id",
      "name": "Royal Guard",
      "image": "Icons/RoyalGuard.png"
    }
  ],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_golden_buckler_unit_id: RawUnit = {
  name: "Golden Buckler",
  sketchfabUrl: "https://sketchfab.com/models/4301cc18e6bb4751b0994a83f5b03da6",
  image: require("#LTD2/Icons/GoldenBuckler.png"),
  splash: require("#LTD2/splashes/GoldenBuckler.png"),
  description: "Tanky. Takes reduced damage from ranged units.",

  damage: Damage.Impact,
  atkSpeed: 0.7575757274724029,
  onHitDmg: 21,

  armor: Armor.Natural,
  hp: 570,
};

export const golden_buckler_unit_id: PlayableUnit = {
  ...raw_golden_buckler_unit_id,
  price: 50,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "royal_guard_unit_id",
  "name": "Royal Guard",
  "image": "Icons/RoyalGuard.png",
  "splash": "splashes/RoyalGuard.png",
  "sketchfabUrl": "https://sketchfab.com/models/aa4bfc92139747e19e9f22c489d7ff8a",
  "description": "Tanky. Takes reduced damage from ranged units.",
  "cost": 135,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Impact",
  "attackTypeDescription": "115% to Arcane<br />115% to Fortified<br />90% to Natural<br />80% to Swift",
  "armorType": "Natural",
  "armorTypeDescription": "90% from Impact<br />125% from Magic<br />85% from Pierce",
  "hp": 2080,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.149999976158142,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 58,
  "damage": 67,
  "range": 100,
  "totalValue": 185,
  "bounty": 0,
  "tier": "2",
  "legion": "Divine",
  "legionType": "divine_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.10000000149011612,
  "spawnDelay": 0,
  "effectiveDps": 58.2599983215332,
  "effectiveThreat": 58.2599983215332,
  "movementSize": 0.1875,
  "backswing": 0.4000000059604645,
  "projectileSpeed": 1200,
  "mastermindGroups": "FortOrNat, SwiftOrNat, Impact, Special1",
  "flags": "Ground, Organic",
  "color": "b89b41",
  "turnRate": 0.10000000149011612,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Divine T2U",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "deflection_ability_id",
      "abilityClass": "",
      "name": "Deflection",
      "image": "Icons/Deflection.png",
      "description": "Reduces damage taken from ranged units by 15%",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Deflection.png' /> Deflection<br /><span style='color: #ffcc00'> Order String:</span> RangedDamageReduction<br /><span style='color: #ffcc00'> Percentage:</span> 0.150"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "unitType": "golden_buckler_unit_id",
      "name": "Golden Buckler",
      "image": "Icons/GoldenBuckler.png"
    }
  ],
  "amountSpawned": 0
}
*/

export const raw_royal_guard_unit_id: RawUnit = {
  name: "Royal Guard",
  sketchfabUrl: "https://sketchfab.com/models/aa4bfc92139747e19e9f22c489d7ff8a",
  image: require("#LTD2/Icons/RoyalGuard.png"),
  splash: require("#LTD2/splashes/RoyalGuard.png"),
  description: "Tanky. Takes reduced damage from ranged units.",

  damage: Damage.Impact,
  atkSpeed: 0.8695652354191746,
  onHitDmg: 67,

  armor: Armor.Natural,
  hp: 2080,
};

export const royal_guard_unit_id: PlayableUnit = {
  ...raw_royal_guard_unit_id,
  price: 135,
  evolFrom: golden_buckler_unit_id,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "sacred_steed_unit_id",
  "name": "Sacred Steed",
  "image": "Icons/SacredSteed.png",
  "splash": "splashes/SacredSteed.png",
  "sketchfabUrl": "https://sketchfab.com/models/ff6364fd39f640baacb0e64ad5704cf1",
  "description": "Restores mana to nearby allies",
  "cost": 80,
  "mythium": 0,
  "income": 0,
  "supply": 1,
  "attackType": "Magic",
  "attackTypeDescription": "75% to Arcane<br />105% to Fortified<br />125% to Natural<br />100% to Swift",
  "armorType": "Arcane",
  "armorTypeDescription": "115% from Impact<br />75% from Magic<br />115% from Pierce",
  "hp": 740,
  "mana": 5,
  "hpRegen": 0,
  "manaRegen": 0.5,
  "attackSpeed": 1.2000000476837158,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 28,
  "damage": 33,
  "range": 100,
  "totalValue": 80,
  "bounty": 0,
  "tier": "3",
  "legion": "Divine",
  "legionType": "divine_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.5,
  "spawnDelay": 0,
  "effectiveDps": 27.5,
  "effectiveThreat": 27.5,
  "movementSize": 0.1875,
  "backswing": 0.6800000071525574,
  "projectileSpeed": 1200,
  "mastermindGroups": "Arcane, Magic",
  "flags": "Ground, Organic",
  "color": "b89b41",
  "turnRate": 0.10000000149011612,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Divine T3",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "mana_blessing_ability_id",
      "abilityClass": "",
      "name": "Mana Blessing",
      "image": "Icons/ManaBlessing.png",
      "description": "Restore the mana of nearby allies. Each point of mana restores 1 mana. Extra mana will bounce to nearby allies. If there are no nearby allies in need of mana, instead boosts its own attack speed and damage reduction by 20% for 5 seconds.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/ManaBlessing.png' /> Mana Blessing<br /><span style='color: #ffcc00'> Order String:</span> ManaBlessing<br /><span style='color: #ffcc00'> Projectile - Speed:</span> 5000<br /><span style='color: #ffcc00'> Mana Cost:</span> 0.00000<br /><span style='color: #ffcc00'> Percentage:</span> 1.00000<br /><span style='color: #ffcc00'> Area of Effect:</span> 1000<br /><span style='color: #ffcc00'> Custom Value 1:</span> 1.00000<br /><span style='color: #ffcc00'> Targets Considered:</span> Allied, NonBoss, Invulnerable<br /><span style='color: #ffcc00'> Targets Excluded:</span> Self"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "mana_blessing_self_buff_ability_id",
      "abilityClass": "hidden",
      "name": "Mana Blessing (Self Buff)",
      "image": "Icons/ManaBlessing.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Mana Blessing (Self Buff)<br /><span style='color: #ffcc00'> Order String:</span> Mindwarp<br /><span style='color: #ffcc00'> Projectile - Speed:</span> 3000<br /><span style='color: #ffcc00'> Mana Cost:</span> 4.50000<br /><span style='color: #ffcc00'> Duration:</span> 5.0000<br /><span style='color: #ffcc00'> Duration (Boss):</span> 5.0000<br /><span style='color: #ffcc00'>Target Buff:</span> Mana Blessing (Buff)<br /><span style='color: #efe1a7'>--- Stats - Attack Rate Bonus (%):</span> 0.20000<br /><span style='color: #efe1a7'>--- Stats - Damage Reduction (% Reduction):</span> 0.20000"
    }
  ],
  "upgradesTo": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "unitType": "pegasus_unit_id",
      "name": "Pegasus",
      "image": "Icons/Pegasus.png"
    }
  ],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_sacred_steed_unit_id: RawUnit = {
  name: "Sacred Steed",
  sketchfabUrl: "https://sketchfab.com/models/ff6364fd39f640baacb0e64ad5704cf1",
  image: require("#LTD2/Icons/SacredSteed.png"),
  splash: require("#LTD2/splashes/SacredSteed.png"),
  description: "Restores mana to nearby allies",

  damage: Damage.Magic,
  atkSpeed: 0.8333333002196431,
  onHitDmg: 33,

  armor: Armor.Arcane,
  hp: 740,
};

export const sacred_steed_unit_id: PlayableUnit = {
  ...raw_sacred_steed_unit_id,
  price: 80,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "pegasus_unit_id",
  "name": "Pegasus",
  "image": "Icons/Pegasus.png",
  "splash": "splashes/Pegasus.png",
  "sketchfabUrl": "https://sketchfab.com/models/9e8b65330939456b948ccbcb71414065",
  "description": "Faithful devotion has heightened its restorative powers",
  "cost": 190,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Magic",
  "attackTypeDescription": "75% to Arcane<br />105% to Fortified<br />125% to Natural<br />100% to Swift",
  "armorType": "Arcane",
  "armorTypeDescription": "115% from Impact<br />75% from Magic<br />115% from Pierce",
  "hp": 2510,
  "mana": 10,
  "hpRegen": 0,
  "manaRegen": 1,
  "attackSpeed": 1.2000000476837158,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 93,
  "damage": 112,
  "range": 100,
  "totalValue": 270,
  "bounty": 0,
  "tier": "3",
  "legion": "Divine",
  "legionType": "divine_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.5,
  "spawnDelay": 0,
  "effectiveDps": 93.33000183105469,
  "effectiveThreat": 93.33000183105469,
  "movementSize": 0.1875,
  "backswing": 0.6800000071525574,
  "projectileSpeed": 1200,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "b89b41",
  "turnRate": 0.10000000149011612,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Divine T3U",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "mana_miracle_ability_id",
      "abilityClass": "",
      "name": "Mana Miracle",
      "image": "Icons/ManaMiracle.png",
      "description": "Restore the mana of nearby allies. Each point of mana restores 1.5 mana, up to a maximum of 3 per target. Extra mana will bounce to nearby allies. If there are no nearby allies in need of mana, instead boosts its own attack speed and damage reduction by 20% for 5 seconds.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/ManaMiracle.png' /> Mana Miracle<br /><span style='color: #ffcc00'> Order String:</span> ManaBlessing<br /><span style='color: #ffcc00'> Projectile - Speed:</span> 5000<br /><span style='color: #ffcc00'> Mana Cost:</span> 0.00000<br /><span style='color: #ffcc00'> Percentage:</span> 1.50000<br /><span style='color: #ffcc00'> Area of Effect:</span> 1000<br /><span style='color: #ffcc00'> Custom Value 1:</span> 3.00000<br /><span style='color: #ffcc00'> Targets Considered:</span> Allied, NonBoss, Invulnerable<br /><span style='color: #ffcc00'> Targets Excluded:</span> Self"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "mana_miracle_self_buff_ability_id",
      "abilityClass": "hidden",
      "name": "Mana Miracle (Self Buff)",
      "image": "Icons/ManaMiracle.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Mana Miracle (Self Buff)<br /><span style='color: #ffcc00'> Order String:</span> Mindwarp<br /><span style='color: #ffcc00'> Projectile - Speed:</span> 3000<br /><span style='color: #ffcc00'> Mana Cost:</span> 9.00000<br /><span style='color: #ffcc00'> Duration:</span> 5.0000<br /><span style='color: #ffcc00'> Duration (Boss):</span> 5.0000<br /><span style='color: #ffcc00'>Target Buff:</span> Mana Miracle (Buff)<br /><span style='color: #efe1a7'>--- Stats - Attack Rate Bonus (%):</span> 0.20000<br /><span style='color: #efe1a7'>--- Stats - Damage Reduction (% Reduction):</span> 0.20000"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "unitType": "sacred_steed_unit_id",
      "name": "Sacred Steed",
      "image": "Icons/SacredSteed.png"
    }
  ],
  "amountSpawned": 0
}
*/

export const raw_pegasus_unit_id: RawUnit = {
  name: "Pegasus",
  sketchfabUrl: "https://sketchfab.com/models/9e8b65330939456b948ccbcb71414065",
  image: require("#LTD2/Icons/Pegasus.png"),
  splash: require("#LTD2/splashes/Pegasus.png"),
  description: "Faithful devotion has heightened its restorative powers",

  damage: Damage.Magic,
  atkSpeed: 0.8333333002196431,
  onHitDmg: 112,

  armor: Armor.Arcane,
  hp: 2510,
};

export const pegasus_unit_id: PlayableUnit = {
  ...raw_pegasus_unit_id,
  price: 190,
  evolFrom: sacred_steed_unit_id,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "elite_archer_unit_id",
  "name": "Elite Archer",
  "image": "Icons/EliteArcher.png",
  "splash": "splashes/EliteArcher.png",
  "sketchfabUrl": "https://sketchfab.com/models/b384c0ed6a434a16844bcb8304e8b9da",
  "description": "Fires four arrows every attack.",
  "cost": 145,
  "mythium": 0,
  "income": 0,
  "supply": 1,
  "attackType": "Pierce",
  "attackTypeDescription": "115% to Arcane<br />80% to Fortified<br />85% to Natural<br />120% to Swift",
  "armorType": "Arcane",
  "armorTypeDescription": "115% from Impact<br />75% from Magic<br />115% from Pierce",
  "hp": 900,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.9900000095367432,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 25,
  "damage": 25,
  "range": 450,
  "totalValue": 145,
  "bounty": 0,
  "tier": "4",
  "legion": "Divine",
  "legionType": "divine_legion_id",
  "movementType": "Hover",
  "spawnBias": 0.8999999761581421,
  "spawnDelay": 0,
  "effectiveDps": 25.25,
  "effectiveThreat": 25.25,
  "movementSize": 0.1875,
  "backswing": 0.3199999928474426,
  "projectileSpeed": 3000,
  "mastermindGroups": "Pierce, Special1, Special2, Special3, Special4",
  "flags": "Ground, Organic",
  "color": "b89b41",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Divine T4",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "Multishot_ability_id",
      "abilityClass": "",
      "name": "Multishot",
      "image": "Icons/Multishot.png",
      "description": "Fires 3 additional arrows at enemies near its attack target. Multiple arrows can hit the same target. Ability damage.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Multishot.png' /> Multishot<br /><span style='color: #ffcc00'>Damage Type:</span> Ability Damage<br /><span style='color: #ffcc00'> Order String:</span> BounceAttack<br /><span style='color: #ffcc00'> Projectile - Speed:</span> 3000<br /><span style='color: #ffcc00'> Attack Type:</span> <img class='tooltip-icon' src='hud/img/icons/Pierce.png' /> Pierce<br /><span style='color: #ffcc00'> Percentage:</span> 1.000<br /><span style='color: #ffcc00'> Max Targets:</span> 3<br /><span style='color: #ffcc00'> Custom Value 3:</span> 200.00000"
    }
  ],
  "upgradesTo": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "unitType": "trinity_archer_unit_id",
      "name": "Trinity Archer",
      "image": "Icons/TrinityArcher.png"
    }
  ],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_elite_archer_unit_id: RawUnit = {
  name: "Elite Archer",
  sketchfabUrl: "https://sketchfab.com/models/b384c0ed6a434a16844bcb8304e8b9da",
  image: require("#LTD2/Icons/EliteArcher.png"),
  splash: require("#LTD2/splashes/EliteArcher.png"),
  description: "Fires four arrows every attack.",

  damage: Damage.Pierce,
  atkSpeed: 1.0101010003706326,
  onHitDmg: 25,

  armor: Armor.Arcane,
  hp: 900,
};

export const elite_archer_unit_id: PlayableUnit = {
  ...raw_elite_archer_unit_id,
  price: 145,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "trinity_archer_unit_id",
  "name": "Trinity Archer",
  "image": "Icons/TrinityArcher.png",
  "splash": "splashes/TrinityArcher.png",
  "sketchfabUrl": "https://sketchfab.com/models/96240b6a3f9c4cc881fa65857003a9b6",
  "description": "Wielding an arbalest, it now fires five arrows.",
  "cost": 265,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Pierce",
  "attackTypeDescription": "115% to Arcane<br />80% to Fortified<br />85% to Natural<br />120% to Swift",
  "armorType": "Arcane",
  "armorTypeDescription": "115% from Impact<br />75% from Magic<br />115% from Pierce",
  "hp": 2530,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.9900000095367432,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 58,
  "damage": 57,
  "range": 450,
  "totalValue": 410,
  "bounty": 0,
  "tier": "4",
  "legion": "Divine",
  "legionType": "divine_legion_id",
  "movementType": "Hover",
  "spawnBias": 0.8999999761581421,
  "spawnDelay": 0,
  "effectiveDps": 57.58000183105469,
  "effectiveThreat": 57.58000183105469,
  "movementSize": 0.1875,
  "backswing": 0.3199999928474426,
  "projectileSpeed": 3000,
  "mastermindGroups": "Pierce, Special1, Special2, Special3, Special4",
  "flags": "Ground, Organic",
  "color": "b89b41",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Divine T4U",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "hypershot_ability_id",
      "abilityClass": "",
      "name": "Hypershot",
      "image": "Icons/Hypershot.png",
      "description": "Fires 4 additional arrows at enemies near its attack target. Multiple arrows can hit the same target. Ability damage.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Hypershot.png' /> Hypershot<br /><span style='color: #ffcc00'>Damage Type:</span> Ability Damage<br /><span style='color: #ffcc00'> Order String:</span> BounceAttack<br /><span style='color: #ffcc00'> Projectile - Speed:</span> 3000<br /><span style='color: #ffcc00'> Attack Type:</span> <img class='tooltip-icon' src='hud/img/icons/Pierce.png' /> Pierce<br /><span style='color: #ffcc00'> Percentage:</span> 1.000<br /><span style='color: #ffcc00'> Max Targets:</span> 4<br /><span style='color: #ffcc00'> Custom Value 3:</span> 200.00000"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "unitType": "elite_archer_unit_id",
      "name": "Elite Archer",
      "image": "Icons/EliteArcher.png"
    }
  ],
  "amountSpawned": 0
}
*/

export const raw_trinity_archer_unit_id: RawUnit = {
  name: "Trinity Archer",
  sketchfabUrl: "https://sketchfab.com/models/96240b6a3f9c4cc881fa65857003a9b6",
  image: require("#LTD2/Icons/TrinityArcher.png"),
  splash: require("#LTD2/splashes/TrinityArcher.png"),
  description: "Wielding an arbalest, it now fires five arrows.",

  damage: Damage.Pierce,
  atkSpeed: 1.0101010003706326,
  onHitDmg: 57,

  armor: Armor.Arcane,
  hp: 2530,
};

export const trinity_archer_unit_id: PlayableUnit = {
  ...raw_trinity_archer_unit_id,
  price: 265,
  evolFrom: elite_archer_unit_id,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "radiant_halo_unit_id",
  "name": "Radiant Halo",
  "image": "Icons/RadiantHalo.png",
  "splash": "splashes/RadiantHalo.png",
  "sketchfabUrl": "https://sketchfab.com/models/31d453b35f6c4802950bd09f68f5cca6",
  "description": "Long-ranged unit with ramping damage",
  "cost": 200,
  "mythium": 0,
  "income": 0,
  "supply": 1,
  "attackType": "Pure",
  "attackTypeDescription": "100% to all defense types.",
  "armorType": "Fortified",
  "armorTypeDescription": "115% from Impact<br />105% from Magic<br />80% from Pierce",
  "hp": 1200,
  "mana": 11,
  "hpRegen": 0,
  "manaRegen": 0.8399999737739563,
  "attackSpeed": 0.8999999761581421,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 50,
  "damage": 45,
  "range": 650,
  "totalValue": 200,
  "bounty": 0,
  "tier": "5",
  "legion": "Divine",
  "legionType": "divine_legion_id",
  "movementType": "Hover",
  "spawnBias": 1,
  "spawnDelay": 0,
  "effectiveDps": 50,
  "effectiveThreat": 50,
  "movementSize": 0.1875,
  "backswing": 0.3199999928474426,
  "projectileSpeed": 3000,
  "mastermindGroups": "Special1, Special2, Special3, Special4",
  "flags": "Ground, Organic",
  "color": "b89b41",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Divine T5",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "particle_wave_ability_id",
      "abilityClass": "",
      "name": "Particle Wave",
      "image": "Icons/ParticleWave.png",
      "description": "Deals 9 bonus pure damage (3 to bosses) for each consecutive attack against the same target. Ability damage.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/ParticleWave.png' /> Particle Wave<br /><span style='color: #ffcc00'> Order String:</span> RampingDamage<br /><span style='color: #ffcc00'> Damage Base:</span> 9.00000 (Ability Damage)<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy<br /><span style='color: #ffcc00'> Targets Excluded:</span> Boss"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "particle_wave_boss_ability_id",
      "abilityClass": "hidden",
      "name": "Particle Wave (Boss)",
      "image": "Icons/ParticleWave.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Particle Wave (Boss)<br /><span style='color: #ffcc00'> Order String:</span> RampingDamage<br /><span style='color: #ffcc00'> Damage Base:</span> 3.00000 (Ability Damage)<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy<br /><span style='color: #ffcc00'> Targets Excluded:</span> NonBoss"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "abilityType": "solar_flare_ability_id",
      "abilityClass": "",
      "name": "Solar Flare",
      "image": "Icons/SolarFlare.png",
      "description": "Increases attack speed by 100% for 5 seconds",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/SolarFlare.png' /> Solar Flare<br /><span style='color: #ffcc00'> Order String:</span> Mindwarp<br /><span style='color: #ffcc00'> Projectile - Speed:</span> 3000<br /><span style='color: #ffcc00'> Mana Cost:</span> 10.00000<br /><span style='color: #ffcc00'> Duration:</span> 5.0000<br /><span style='color: #ffcc00'> Duration (Boss):</span> 5.0000<br /><span style='color: #ffcc00'> Targets Considered:</span> Self, Invulnerable<br /><span style='color: #ffcc00'>Target Buff:</span> Solar Flare<br /><span style='color: #efe1a7'>--- Stats - Attack Rate Bonus (%):</span> 1.0000"
    }
  ],
  "upgradesTo": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 3,
      "unitType": "arc_of_justice_unit_id",
      "name": "Arc of Justice",
      "image": "Icons/ArcOfJustice.png"
    }
  ],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_radiant_halo_unit_id: RawUnit = {
  name: "Radiant Halo",
  sketchfabUrl: "https://sketchfab.com/models/31d453b35f6c4802950bd09f68f5cca6",
  image: require("#LTD2/Icons/RadiantHalo.png"),
  splash: require("#LTD2/splashes/RadiantHalo.png"),
  description: "Long-ranged unit with ramping damage",

  damage: Damage.Pure,
  atkSpeed: 1.1111111405455043,
  onHitDmg: 45,

  armor: Armor.Fortified,
  hp: 1200,
};

export const radiant_halo_unit_id: PlayableUnit = {
  ...raw_radiant_halo_unit_id,
  price: 200,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "arc_of_justice_unit_id",
  "name": "Arc of Justice",
  "image": "Icons/ArcOfJustice.png",
  "splash": "splashes/ArcOfJustice.png",
  "sketchfabUrl": "https://sketchfab.com/models/1bcfc646f058448d835a050acbd93d15",
  "description": "Long-ranged unit with ramping damage",
  "cost": 345,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Pure",
  "attackTypeDescription": "100% to all defense types.",
  "armorType": "Fortified",
  "armorTypeDescription": "115% from Impact<br />105% from Magic<br />80% from Pierce",
  "hp": 3250,
  "mana": 19,
  "hpRegen": 0,
  "manaRegen": 1.4500000476837158,
  "attackSpeed": 0.8999999761581421,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 136,
  "damage": 122,
  "range": 650,
  "totalValue": 545,
  "bounty": 0,
  "tier": "5",
  "legion": "Divine",
  "legionType": "divine_legion_id",
  "movementType": "Hover",
  "spawnBias": 1,
  "spawnDelay": 0,
  "effectiveDps": 135.55999755859375,
  "effectiveThreat": 135.55999755859375,
  "movementSize": 0.1875,
  "backswing": 0.3199999928474426,
  "projectileSpeed": 3000,
  "mastermindGroups": "Special1, Special2, Special3, Special4",
  "flags": "Ground, Organic",
  "color": "b89b41",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Divine T5U",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "purification_beam_boss_ability_id",
      "abilityClass": "hidden",
      "name": "Purification Beam (Boss)",
      "image": "Icons/PurificationBeam.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Purification Beam (Boss)<br /><span style='color: #ffcc00'> Order String:</span> RampingDamage<br /><span style='color: #ffcc00'> Damage Base:</span> 8.00000 (Ability Damage)<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy<br /><span style='color: #ffcc00'> Targets Excluded:</span> NonBoss"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "purification_beam_ability_id",
      "abilityClass": "",
      "name": "Purification Beam",
      "image": "Icons/PurificationBeam.png",
      "description": "Deals 24 bonus pure damage (8 to bosses) for each consecutive attack against the same target. Ability damage.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/PurificationBeam.png' /> Purification Beam<br /><span style='color: #ffcc00'> Order String:</span> RampingDamage<br /><span style='color: #ffcc00'> Damage Base:</span> 24.00000 (Ability Damage)<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy<br /><span style='color: #ffcc00'> Targets Excluded:</span> Boss"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "abilityType": "solar_flare_arc_of_justice_ability_id",
      "abilityClass": "",
      "name": "Solar Flare",
      "image": "Icons/SolarFlare.png",
      "description": "Increases attack speed by 100% for 5 seconds",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/SolarFlare.png' /> Solar Flare<br /><span style='color: #ffcc00'> Order String:</span> Mindwarp<br /><span style='color: #ffcc00'> Projectile - Speed:</span> 3000<br /><span style='color: #ffcc00'> Mana Cost:</span> 17.55000<br /><span style='color: #ffcc00'> Duration:</span> 5.0000<br /><span style='color: #ffcc00'> Duration (Boss):</span> 5.0000<br /><span style='color: #ffcc00'> Targets Considered:</span> Self, Invulnerable<br /><span style='color: #ffcc00'>Target Buff:</span> Solar Flare<br /><span style='color: #efe1a7'>--- Stats - Attack Rate Bonus (%):</span> 1.0000"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 3,
      "unitType": "radiant_halo_unit_id",
      "name": "Radiant Halo",
      "image": "Icons/RadiantHalo.png"
    }
  ],
  "amountSpawned": 0
}
*/

export const raw_arc_of_justice_unit_id: RawUnit = {
  name: "Arc of Justice",
  sketchfabUrl: "https://sketchfab.com/models/1bcfc646f058448d835a050acbd93d15",
  image: require("#LTD2/Icons/ArcOfJustice.png"),
  splash: require("#LTD2/splashes/ArcOfJustice.png"),
  description: "Long-ranged unit with ramping damage",

  damage: Damage.Pure,
  atkSpeed: 1.1111111405455043,
  onHitDmg: 122,

  armor: Armor.Fortified,
  hp: 3250,
};

export const arc_of_justice_unit_id: PlayableUnit = {
  ...raw_arc_of_justice_unit_id,
  price: 345,
  evolFrom: radiant_halo_unit_id,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "holy_avenger_unit_id",
  "name": "Holy Avenger",
  "image": "Icons/HolyAvenger.png",
  "splash": "splashes/HolyAvenger.png",
  "sketchfabUrl": "",
  "description": "Gains mana whenever attacking or being attacked. The more mana she has, the stronger she becomes.",
  "cost": 270,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Impact",
  "attackTypeDescription": "115% to Arcane<br />115% to Fortified<br />90% to Natural<br />80% to Swift",
  "armorType": "Swift",
  "armorTypeDescription": "80% from Impact<br />100% from Magic<br />120% from Pierce",
  "hp": 2430,
  "mana": 80,
  "hpRegen": 0,
  "manaRegen": 0.0010000000474974513,
  "attackSpeed": 0.800000011920929,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 121,
  "damage": 97,
  "range": 100,
  "totalValue": 270,
  "bounty": 0,
  "tier": "6",
  "legion": "Divine",
  "legionType": "divine_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.4000000059604645,
  "spawnDelay": 0,
  "effectiveDps": 121.25,
  "effectiveThreat": 121.25,
  "movementSize": 0.1875,
  "backswing": 0.4000000059604645,
  "projectileSpeed": 1200,
  "mastermindGroups": "SwiftOrNat, Impact, Special1",
  "flags": "Ground, Organic",
  "color": "b89b41",
  "turnRate": 0.10000000149011612,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Divine T6",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "aegis_ability_id",
      "abilityClass": "",
      "name": "Aegis",
      "image": "Icons/Aegis.png",
      "description": "Gains mana every time it attacks and is attacked. Each 10% mana increases attack speed and damage reduction by 4%. At max mana, attacks deal 25% splash damage in a small radius.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Aegis.png' /> Aegis<br /><span style='color: #ffcc00'> Linked Abilities:</span> <img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Aegis (Maxed), <img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Aegis (Flying)<br /><span style='color: #ffcc00'> Order String:</span> Aegis<br /><span style='color: #ffcc00'> Mana Cost:</span> 8.00000<br /><span style='color: #ffcc00'> Percentage:</span> 0.0000<br /><span style='color: #ffcc00'> Custom Value 1:</span> 0.10000<br /><span style='color: #ffcc00'> Targets Excluded:</span> Building<br /><span style='color: #ffcc00'>Target Buff:</span> Aegis<br /><span style='color: #efe1a7'>--- Max Stacks:</span> 100<br /><span style='color: #efe1a7'>--- Stats - Attack Rate Bonus (%):</span> 0.04000<br /><span style='color: #efe1a7'>--- Stats - Damage Reduction (% Reduction):</span> 0.04000"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "aegis_maxed_ability_id",
      "abilityClass": "linked",
      "name": "Aegis (Maxed)",
      "image": "Icons/AegisMaxed.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Aegis (Maxed)<br /><span style='color: #ffcc00'>Damage Type:</span> Ability Damage<br /><span style='color: #ffcc00'> Order String:</span> SplashAttack<br /><span style='color: #ffcc00'> Percentage:</span> 0.25000<br /><span style='color: #ffcc00'> Area of Effect:</span> 150<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy<br /><span style='color: #ffcc00'> Targets Excluded:</span> Air<br /><span style='color: #ffcc00'>Caster Buff:</span> Aegis (Maxed) (Cosmetic Buff)"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "abilityType": "aegis_maxed_air_ability_id",
      "abilityClass": "linked",
      "name": "Aegis (Flying)",
      "image": "Icons/AegisMaxed.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Aegis Maxed (Flying)<br /><span style='color: #ffcc00'>Damage Type:</span> Ability Damage<br /><span style='color: #ffcc00'> Order String:</span> SplashAttack<br /><span style='color: #ffcc00'> Percentage:</span> 0.25000<br /><span style='color: #ffcc00'> Area of Effect:</span> 150<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy, Air"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 3,
      "abilityType": "disable_mana_regen_tower_only_ability_id",
      "abilityClass": "hidden",
      "name": "Disable Mana Regen (Tower Only)",
      "image": "Icons/DisableManaRegen(TowerOnly).png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Disable Mana Regen (Tower Only)<br /><span style='color: #ffcc00'> Order String:</span> DisableManaRegen"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 4,
      "abilityType": "aegis_hidden_effect_ability_id",
      "abilityClass": "hidden",
      "name": "aegis_hidden_effect",
      "image": "Icons/aegis_hidden_effect.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> aegis_hidden_effect<br /><span style='color: #ffcc00'> Order String:</span> MagneticAccelerator<br /><span style='color: #ffcc00'> Mana Cost:</span> 2.00000"
    }
  ],
  "upgradesTo": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 5,
      "unitType": "sovereign_unit_id",
      "name": "Sovereign",
      "image": "Icons/Sovereign.png"
    }
  ],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_holy_avenger_unit_id: RawUnit = {
  name: "Holy Avenger",
  sketchfabUrl: "",
  image: require("#LTD2/Icons/HolyAvenger.png"),
  splash: require("#LTD2/splashes/HolyAvenger.png"),
  description: "Gains mana whenever attacking or being attacked. The more mana she has, the stronger she becomes.",

  damage: Damage.Impact,
  atkSpeed: 1.2499999813735487,
  onHitDmg: 97,

  armor: Armor.Swift,
  hp: 2430,
};

export const holy_avenger_unit_id: PlayableUnit = {
  ...raw_holy_avenger_unit_id,
  price: 270,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "sovereign_unit_id",
  "name": "Sovereign",
  "image": "Icons/Sovereign.png",
  "splash": "splashes/Sovereign.png",
  "sketchfabUrl": "",
  "description": "Millennia have passed; his divine authority remains unchallenged.",
  "cost": 430,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Impact",
  "attackTypeDescription": "115% to Arcane<br />115% to Fortified<br />90% to Natural<br />80% to Swift",
  "armorType": "Swift",
  "armorTypeDescription": "80% from Impact<br />100% from Magic<br />120% from Pierce",
  "hp": 6050,
  "mana": 120,
  "hpRegen": 0,
  "manaRegen": 0.0010000000474974513,
  "attackSpeed": 0.800000011920929,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 295,
  "damage": 236,
  "range": 100,
  "totalValue": 700,
  "bounty": 0,
  "tier": "6",
  "legion": "Divine",
  "legionType": "divine_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.4000000059604645,
  "spawnDelay": 0,
  "effectiveDps": 295,
  "effectiveThreat": 295,
  "movementSize": 0.1875,
  "backswing": 0.4000000059604645,
  "projectileSpeed": 1200,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "b89b41",
  "turnRate": 0.10000000149011612,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Divine T6U",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "final_judgment_ability_id",
      "abilityClass": "",
      "name": "Final Judgment",
      "image": "Icons/FinalJudgment.png",
      "description": "Gains mana every time it attacks and is attacked. Each 10% mana increases attack speed and damage reduction by 4%. At max mana, attacks deal 25% splash damage in a large radius.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/FinalJudgment.png' /> Final Judgment<br /><span style='color: #ffcc00'> Linked Abilities:</span> <img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Final Judgment (Maxed), <img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Final Judgment (Flying)<br /><span style='color: #ffcc00'> Order String:</span> Aegis<br /><span style='color: #ffcc00'> Mana Cost:</span> 12.00000<br /><span style='color: #ffcc00'> Percentage:</span> 0.0000<br /><span style='color: #ffcc00'> Custom Value 1:</span> 0.15000<br /><span style='color: #ffcc00'> Targets Excluded:</span> Building<br /><span style='color: #ffcc00'>Target Buff:</span> Final Judgment<br /><span style='color: #efe1a7'>--- Max Stacks:</span> 100<br /><span style='color: #efe1a7'>--- Stats - Attack Rate Bonus (%):</span> 0.04000<br /><span style='color: #efe1a7'>--- Stats - Damage Reduction (% Reduction):</span> 0.04000"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "final_judgment_maxed_ability_id",
      "abilityClass": "linked",
      "name": "Final Judgment (Maxed)",
      "image": "Icons/FinalJudgmentMaxed.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Final Judgment (Maxed)<br /><span style='color: #ffcc00'>Damage Type:</span> Ability Damage<br /><span style='color: #ffcc00'> Order String:</span> SplashAttack<br /><span style='color: #ffcc00'> Percentage:</span> 0.25000<br /><span style='color: #ffcc00'> Area of Effect:</span> 500<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy<br /><span style='color: #ffcc00'> Targets Excluded:</span> Air<br /><span style='color: #ffcc00'>Caster Buff:</span> Final Judgment (Maxed) (Cosmetic Buff)"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "abilityType": "final_judgment_maxed_flying_ability_id",
      "abilityClass": "linked",
      "name": "Final Judgment (Flying)",
      "image": "Icons/FinalJudgmentMaxed.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Final Judgment Maxed (Flying)<br /><span style='color: #ffcc00'>Damage Type:</span> Ability Damage<br /><span style='color: #ffcc00'> Order String:</span> SplashAttack<br /><span style='color: #ffcc00'> Percentage:</span> 0.25000<br /><span style='color: #ffcc00'> Area of Effect:</span> 500<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy, Air"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 3,
      "abilityType": "disable_mana_regen_tower_only_ability_id",
      "abilityClass": "hidden",
      "name": "Disable Mana Regen (Tower Only)",
      "image": "Icons/DisableManaRegen(TowerOnly).png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Disable Mana Regen (Tower Only)<br /><span style='color: #ffcc00'> Order String:</span> DisableManaRegen"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 4,
      "abilityType": "final_judgment_hidden_effect_ability_id",
      "abilityClass": "hidden",
      "name": "final_judgment_hidden_effect",
      "image": "Icons/final_judgment_hidden_effect.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> final_judgment_hidden_effect<br /><span style='color: #ffcc00'> Order String:</span> MagneticAccelerator<br /><span style='color: #ffcc00'> Mana Cost:</span> 3.00000"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 5,
      "unitType": "holy_avenger_unit_id",
      "name": "Holy Avenger",
      "image": "Icons/HolyAvenger.png"
    }
  ],
  "amountSpawned": 0
}
*/

export const raw_sovereign_unit_id: RawUnit = {
  name: "Sovereign",
  sketchfabUrl: "",
  image: require("#LTD2/Icons/Sovereign.png"),
  splash: require("#LTD2/splashes/Sovereign.png"),
  description: "Millennia have passed; his divine authority remains unchallenged.",

  damage: Damage.Impact,
  atkSpeed: 1.2499999813735487,
  onHitDmg: 236,

  armor: Armor.Swift,
  hp: 6050,
};

export const sovereign_unit_id: PlayableUnit = {
  ...raw_sovereign_unit_id,
  price: 430,
  evolFrom: holy_avenger_unit_id,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "proton_unit_id",
  "name": "Proton",
  "image": "Icons/Proton.png",
  "splash": "splashes/Proton.png",
  "sketchfabUrl": "",
  "description": "Passively gains attack speed the longer it survives.",
  "cost": 20,
  "mythium": 0,
  "income": 0,
  "supply": 1,
  "attackType": "Pure",
  "attackTypeDescription": "100% to all defense types.",
  "armorType": "Immaterial",
  "armorTypeDescription": "100% from all attack types.",
  "hp": 150,
  "mana": 17,
  "hpRegen": 0,
  "manaRegen": 0.17000000178813934,
  "attackSpeed": 0.9200000166893005,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 9,
  "damage": 8,
  "range": 300,
  "totalValue": 20,
  "bounty": 0,
  "tier": "1",
  "legion": "Element",
  "legionType": "element_legion_id",
  "movementType": "Hover",
  "spawnBias": 0.800000011920929,
  "spawnDelay": 0,
  "effectiveDps": 8.699999809265137,
  "effectiveThreat": 8.699999809265137,
  "movementSize": 0.1875,
  "backswing": 0.18000000715255737,
  "projectileSpeed": 3000,
  "mastermindGroups": "Special1, Special2, Special3",
  "flags": "Ground",
  "color": "bc6701",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Element T1",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "generator_ability_id",
      "abilityClass": "",
      "name": "Generator",
      "image": "Icons/Generator.png",
      "description": "Starts with 0 mana, but gains mana per second",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Generator.png' /> Generator<br /><span style='color: #ffcc00'> Order String:</span> Generator<br /><span style='color: #ffcc00'> Percentage:</span> 0.0000<br /><span style='color: #ffcc00'> Targets Excluded:</span> Building<br /><span style='color: #ffcc00'>Target Buff:</span> Generator<br /><span style='color: #efe1a7'>--- Stats - Mana Regeneration Bonus (Flat):</span> 0.0000"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "ionic_force_proton_ability_id",
      "abilityClass": "",
      "name": "Ionic Force",
      "image": "Icons/IonicForce.png",
      "description": "Increases attack speed by 20% for each 10% current mana",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/IonicForce.png' /> Ionic Force<br /><span style='color: #ffcc00'> Order String:</span> Acrobatics<br /><span style='color: #ffcc00'> Mana Cost:</span> 1.70000<br /><span style='color: #ffcc00'> Percentage:</span> 0.0000<br /><span style='color: #ffcc00'> Custom Value 1:</span> 0.00000<br /><span style='color: #ffcc00'> Targets Excluded:</span> Building<br /><span style='color: #ffcc00'>Target Buff:</span> Ionic Force<br /><span style='color: #efe1a7'>--- Max Stacks:</span> 100<br /><span style='color: #efe1a7'>--- Stats - Attack Rate Bonus (%):</span> 0.20000"
    }
  ],
  "upgradesTo": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "unitType": "atom_unit_id",
      "name": "Atom",
      "image": "Icons/Atom.png"
    }
  ],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_proton_unit_id: RawUnit = {
  name: "Proton",
  sketchfabUrl: "",
  image: require("#LTD2/Icons/Proton.png"),
  splash: require("#LTD2/splashes/Proton.png"),
  description: "Passively gains attack speed the longer it survives.",

  damage: Damage.Pure,
  atkSpeed: 1.086956502021148,
  onHitDmg: 8,

  armor: Armor.Immaterial,
  hp: 150,
};

export const proton_unit_id: PlayableUnit = {
  ...raw_proton_unit_id,
  price: 20,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "atom_unit_id",
  "name": "Atom",
  "image": "Icons/Atom.png",
  "splash": "splashes/Atom.png",
  "sketchfabUrl": "",
  "description": "Now charged with electricity, an all-around upgrade.",
  "cost": 70,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Pure",
  "attackTypeDescription": "100% to all defense types.",
  "armorType": "Immaterial",
  "armorTypeDescription": "100% from all attack types.",
  "hp": 780,
  "mana": 50,
  "hpRegen": 0,
  "manaRegen": 0.5,
  "attackSpeed": 0.9200000166893005,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 41,
  "damage": 38,
  "range": 300,
  "totalValue": 90,
  "bounty": 0,
  "tier": "1",
  "legion": "Element",
  "legionType": "element_legion_id",
  "movementType": "Hover",
  "spawnBias": 0.800000011920929,
  "spawnDelay": 0,
  "effectiveDps": 41.29999923706055,
  "effectiveThreat": 41.29999923706055,
  "movementSize": 0.1875,
  "backswing": 0.18000000715255737,
  "projectileSpeed": 3000,
  "mastermindGroups": "None",
  "flags": "Ground",
  "color": "bc6701",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Element T1U",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "generator_ability_id",
      "abilityClass": "",
      "name": "Generator",
      "image": "Icons/Generator.png",
      "description": "Starts with 0 mana, but gains mana per second",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Generator.png' /> Generator<br /><span style='color: #ffcc00'> Order String:</span> Generator<br /><span style='color: #ffcc00'> Percentage:</span> 0.0000<br /><span style='color: #ffcc00'> Targets Excluded:</span> Building<br /><span style='color: #ffcc00'>Target Buff:</span> Generator<br /><span style='color: #efe1a7'>--- Stats - Mana Regeneration Bonus (Flat):</span> 0.0000"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "ionic_force_aspd_ability_id",
      "abilityClass": "",
      "name": "Ionic Force",
      "image": "Icons/IonicForce.png",
      "description": "Increases attack speed by 20% for each 10% current mana",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/IonicForce.png' /> Ionic Force<br /><span style='color: #ffcc00'> Order String:</span> Acrobatics<br /><span style='color: #ffcc00'> Mana Cost:</span> 5.00000<br /><span style='color: #ffcc00'> Percentage:</span> 0.0000<br /><span style='color: #ffcc00'> Custom Value 1:</span> 0.00000<br /><span style='color: #ffcc00'> Targets Excluded:</span> Building<br /><span style='color: #ffcc00'>Target Buff:</span> Ionic Force<br /><span style='color: #efe1a7'>--- Max Stacks:</span> 100<br /><span style='color: #efe1a7'>--- Stats - Attack Rate Bonus (%):</span> 0.20000"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "unitType": "proton_unit_id",
      "name": "Proton",
      "image": "Icons/Proton.png"
    }
  ],
  "amountSpawned": 0
}
*/

export const raw_atom_unit_id: RawUnit = {
  name: "Atom",
  sketchfabUrl: "",
  image: require("#LTD2/Icons/Atom.png"),
  splash: require("#LTD2/splashes/Atom.png"),
  description: "Now charged with electricity, an all-around upgrade.",

  damage: Damage.Pure,
  atkSpeed: 1.086956502021148,
  onHitDmg: 38,

  armor: Armor.Immaterial,
  hp: 780,
};

export const atom_unit_id: PlayableUnit = {
  ...raw_atom_unit_id,
  price: 70,
  evolFrom: proton_unit_id,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "aqua_spirit_unit_id",
  "name": "Aqua Spirit",
  "image": "Icons/AquaSpirit.png",
  "splash": "splashes/AquaSpirit.png",
  "sketchfabUrl": "https://sketchfab.com/models/5462b43879914fc0b030de8b558431ce",
  "description": "Has a bouncing attack. ",
  "cost": 50,
  "mythium": 0,
  "income": 0,
  "supply": 1,
  "attackType": "Pierce",
  "attackTypeDescription": "115% to Arcane<br />80% to Fortified<br />85% to Natural<br />120% to Swift",
  "armorType": "Arcane",
  "armorTypeDescription": "115% from Impact<br />75% from Magic<br />115% from Pierce",
  "hp": 380,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.2999999523162842,
  "attackSpeedDescriptor": "Slow",
  "mspd": 300,
  "dps": 18,
  "damage": 24,
  "range": 400,
  "totalValue": 50,
  "bounty": 0,
  "tier": "2",
  "legion": "Element",
  "legionType": "element_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.800000011920929,
  "spawnDelay": 0,
  "effectiveDps": 18.459999084472656,
  "effectiveThreat": 18.459999084472656,
  "movementSize": 0.1875,
  "backswing": 0.24899999797344208,
  "projectileSpeed": 2000,
  "mastermindGroups": "Pierce, Special1, Special2, Special3, Special4",
  "flags": "Ground",
  "color": "bc6701",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Element T2",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "water_bounce_ability_id",
      "abilityClass": "",
      "name": "Water Bounce",
      "image": "Icons/WaterBounce.png",
      "description": "Each attack bounces to a nearby enemy. Ability damage.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/WaterBounce.png' /> Water Bounce<br /><span style='color: #ffcc00'>Damage Type:</span> Ability Damage<br /><span style='color: #ffcc00'> Order String:</span> BounceAttack<br /><span style='color: #ffcc00'> Projectile - Speed:</span> 2000<br /><span style='color: #ffcc00'> Percentage:</span> 1.00000<br /><span style='color: #ffcc00'> Bounces:</span> 1<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy"
    }
  ],
  "upgradesTo": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "unitType": "rogue_wave_unit_id",
      "name": "Rogue Wave",
      "image": "Icons/RogueWave.png"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "unitType": "fire_elemental_unit_id",
      "name": "Fire Elemental",
      "image": "Icons/FireElemental.png"
    }
  ],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_aqua_spirit_unit_id: RawUnit = {
  name: "Aqua Spirit",
  sketchfabUrl: "https://sketchfab.com/models/5462b43879914fc0b030de8b558431ce",
  image: require("#LTD2/Icons/AquaSpirit.png"),
  splash: require("#LTD2/splashes/AquaSpirit.png"),
  description: "Has a bouncing attack. ",

  damage: Damage.Pierce,
  atkSpeed: 0.7692307974459868,
  onHitDmg: 24,

  armor: Armor.Arcane,
  hp: 380,
};

export const aqua_spirit_unit_id: PlayableUnit = {
  ...raw_aqua_spirit_unit_id,
  price: 50,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "fire_elemental_unit_id",
  "name": "Fire Elemental",
  "image": "Icons/FireElemental.png",
  "splash": "splashes/FireElemental.png",
  "sketchfabUrl": "https://sketchfab.com/models/a321a9bb78f840a0900fceb60f6c1625",
  "description": "Has a bouncing attack that burns enemies.",
  "cost": 140,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Pierce",
  "attackTypeDescription": "115% to Arcane<br />80% to Fortified<br />85% to Natural<br />120% to Swift",
  "armorType": "Arcane",
  "armorTypeDescription": "115% from Impact<br />75% from Magic<br />115% from Pierce",
  "hp": 1350,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.2400000095367432,
  "attackSpeedDescriptor": "Slow",
  "mspd": 300,
  "dps": 26,
  "damage": 32,
  "range": 400,
  "totalValue": 190,
  "bounty": 0,
  "tier": "2",
  "legion": "Element",
  "legionType": "element_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.800000011920929,
  "spawnDelay": 0,
  "effectiveDps": 25.809999465942383,
  "effectiveThreat": 25.809999465942383,
  "movementSize": 0.1875,
  "backswing": 0.15000000596046448,
  "projectileSpeed": 2000,
  "mastermindGroups": "None",
  "flags": "Ground",
  "color": "bc6701",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Element T2U",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "combustion_ability_id",
      "abilityClass": "",
      "name": "Combustion",
      "image": "Icons/Combustion.png",
      "description": "Each attack bounces to 2 nearby enemies, dealing 25 magic ability damage over 1 second. Stacks with itself. Ability damage. ",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Combustion.png' /> Combustion<br /><span style='color: #ffcc00'>Damage Type:</span> Ability Damage<br /><span style='color: #ffcc00'> Order String:</span> BounceAttack<br /><span style='color: #ffcc00'> Projectile - Speed:</span> 2000<br /><span style='color: #ffcc00'> Attack Type:</span> <img class='tooltip-icon' src='hud/img/icons/Magic.png' /> Magic<br /><span style='color: #ffcc00'> Percentage:</span> 1.0000<br /><span style='color: #ffcc00'> Bounces:</span> 2<br /><span style='color: #ffcc00'> Duration:</span> 1.00000<br /><span style='color: #ffcc00'> Duration (Boss):</span> 1.00000<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy<br /><span style='color: #ffcc00'>Target Buff:</span> Combustion<br /><span style='color: #efe1a7'>--- Attack Type:</span> <img class='tooltip-icon' src='hud/img/icons/Magic.png' />Magic<br /><span style='color: #efe1a7'>--- Damage Per Second:</span> 25.00000 (Ability Damage)<br /><span style='color: #efe1a7'>--- Max Stacks:</span> 50"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "unitType": "aqua_spirit_unit_id",
      "name": "Aqua Spirit",
      "image": "Icons/AquaSpirit.png"
    }
  ],
  "amountSpawned": 0
}
*/

export const raw_fire_elemental_unit_id: RawUnit = {
  name: "Fire Elemental",
  sketchfabUrl: "https://sketchfab.com/models/a321a9bb78f840a0900fceb60f6c1625",
  image: require("#LTD2/Icons/FireElemental.png"),
  splash: require("#LTD2/splashes/FireElemental.png"),
  description: "Has a bouncing attack that burns enemies.",

  damage: Damage.Pierce,
  atkSpeed: 0.8064516067008695,
  onHitDmg: 32,

  armor: Armor.Arcane,
  hp: 1350,
};

export const fire_elemental_unit_id: PlayableUnit = {
  ...raw_fire_elemental_unit_id,
  price: 140,
  evolFrom: aqua_spirit_unit_id,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "rogue_wave_unit_id",
  "name": "Rogue Wave",
  "image": "Icons/RogueWave.png",
  "splash": "splashes/RogueWave.png",
  "sketchfabUrl": "https://sketchfab.com/models/ffa9a08f8202452f9d1b80bc7e88af67",
  "description": "Has a bouncing attack that slows enemies and makes them more vulnerable to ability damage.",
  "cost": 115,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Pierce",
  "attackTypeDescription": "115% to Arcane<br />80% to Fortified<br />85% to Natural<br />120% to Swift",
  "armorType": "Arcane",
  "armorTypeDescription": "115% from Impact<br />75% from Magic<br />115% from Pierce",
  "hp": 1150,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.3700000047683716,
  "attackSpeedDescriptor": "Slow",
  "mspd": 300,
  "dps": 35,
  "damage": 48,
  "range": 400,
  "totalValue": 165,
  "bounty": 0,
  "tier": "2",
  "legion": "Element",
  "legionType": "element_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.800000011920929,
  "spawnDelay": 0,
  "effectiveDps": 35.040000915527344,
  "effectiveThreat": 35.040000915527344,
  "movementSize": 0.1875,
  "backswing": 0.15000000596046448,
  "projectileSpeed": 2000,
  "mastermindGroups": "None",
  "flags": "Ground",
  "color": "bc6701",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Element T2U",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "deluge_ability_id",
      "abilityClass": "",
      "name": "Delusion",
      "image": "Icons/Delusion.png",
      "description": "Each attack bounces to 2 nearby enemies, amplifying ability damage taken by 14% (7% against bosses). Does not stack. Ability damage. ",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Delusion.png' /> Delusion<br /><span style='color: #ffcc00'>Damage Type:</span> Ability Damage<br /><span style='color: #ffcc00'> Order String:</span> BounceAttack<br /><span style='color: #ffcc00'> Projectile - Speed:</span> 2000<br /><span style='color: #ffcc00'> Percentage:</span> 1.0000<br /><span style='color: #ffcc00'> Bounces:</span> 2<br /><span style='color: #ffcc00'> Duration:</span> 2.000<br /><span style='color: #ffcc00'> Duration (Boss):</span> 2.000<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy<br /><span style='color: #ffcc00'>Target Buff:</span> Delusion<br /><span style='color: #efe1a7'>--- Stats - Damage Reduction from Spells (%):</span> -0.14000"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "unitType": "aqua_spirit_unit_id",
      "name": "Aqua Spirit",
      "image": "Icons/AquaSpirit.png"
    }
  ],
  "amountSpawned": 0
}
*/

export const raw_rogue_wave_unit_id: RawUnit = {
  name: "Rogue Wave",
  sketchfabUrl: "https://sketchfab.com/models/ffa9a08f8202452f9d1b80bc7e88af67",
  image: require("#LTD2/Icons/RogueWave.png"),
  splash: require("#LTD2/splashes/RogueWave.png"),
  description: "Has a bouncing attack that slows enemies and makes them more vulnerable to ability damage.",

  damage: Damage.Pierce,
  atkSpeed: 0.729927004758713,
  onHitDmg: 48,

  armor: Armor.Arcane,
  hp: 1150,
};

export const rogue_wave_unit_id: PlayableUnit = {
  ...raw_rogue_wave_unit_id,
  price: 115,
  evolFrom: aqua_spirit_unit_id,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "windhawk_unit_id",
  "name": "Windhawk",
  "image": "Icons/Windhawk.png",
  "splash": "splashes/Windhawk.png",
  "sketchfabUrl": "https://sketchfab.com/models/62d6ad6986c24647a7578cc9516161b9",
  "description": "Flying. Attacks enemies with gusts of wind.",
  "cost": 85,
  "mythium": 0,
  "income": 0,
  "supply": 1,
  "attackType": "Impact",
  "attackTypeDescription": "115% to Arcane<br />115% to Fortified<br />90% to Natural<br />80% to Swift",
  "armorType": "Natural",
  "armorTypeDescription": "90% from Impact<br />125% from Magic<br />85% from Pierce",
  "hp": 760,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.7900000214576721,
  "attackSpeedDescriptor": "Fast",
  "mspd": 300,
  "dps": 44,
  "damage": 35,
  "range": 100,
  "totalValue": 85,
  "bounty": 0,
  "tier": "3",
  "legion": "Element",
  "legionType": "element_legion_id",
  "movementType": "Air",
  "spawnBias": 0.6000000238418579,
  "spawnDelay": 0,
  "effectiveDps": 44.29999923706055,
  "effectiveThreat": 44.29999923706055,
  "movementSize": 0.1875,
  "backswing": 0.20000000298023224,
  "projectileSpeed": 1200,
  "mastermindGroups": "Impact, Special1, Special2, Special3, Special4",
  "flags": "Air",
  "color": "bc6701",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Element T3",
  "suffix": "",
  "abilities": [],
  "upgradesTo": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "unitType": "violet_unit_id",
      "name": "Violet",
      "image": "Icons/Violet.png"
    }
  ],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_windhawk_unit_id: RawUnit = {
  name: "Windhawk",
  sketchfabUrl: "https://sketchfab.com/models/62d6ad6986c24647a7578cc9516161b9",
  image: require("#LTD2/Icons/Windhawk.png"),
  splash: require("#LTD2/splashes/Windhawk.png"),
  description: "Flying. Attacks enemies with gusts of wind.",

  damage: Damage.Impact,
  atkSpeed: 1.2658227504283424,
  onHitDmg: 35,

  armor: Armor.Natural,
  hp: 760,
};

export const windhawk_unit_id: PlayableUnit = {
  ...raw_windhawk_unit_id,
  price: 85,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "violet_unit_id",
  "name": "Violet",
  "image": "Icons/Violet.png",
  "splash": "splashes/Violet.png",
  "sketchfabUrl": "https://sketchfab.com/models/410d39cf3030422797abdcd9826a1311",
  "description": "Flying. Now periodically shocks enemies with lightning.",
  "cost": 180,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Impact",
  "attackTypeDescription": "115% to Arcane<br />115% to Fortified<br />90% to Natural<br />80% to Swift",
  "armorType": "Natural",
  "armorTypeDescription": "90% from Impact<br />125% from Magic<br />85% from Pierce",
  "hp": 2340,
  "mana": 11,
  "hpRegen": 0,
  "manaRegen": 1.0700000524520874,
  "attackSpeed": 1.1100000143051147,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 66,
  "damage": 73,
  "range": 100,
  "totalValue": 265,
  "bounty": 0,
  "tier": "3",
  "legion": "Element",
  "legionType": "element_legion_id",
  "movementType": "Air",
  "spawnBias": 0.6000000238418579,
  "spawnDelay": 0,
  "effectiveDps": 65.7699966430664,
  "effectiveThreat": 65.7699966430664,
  "movementSize": 0.1875,
  "backswing": 0.23000000417232513,
  "projectileSpeed": 1200,
  "mastermindGroups": "None",
  "flags": "Air",
  "color": "bc6701",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Element T3U",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "blue_wind_ability_id",
      "abilityClass": "",
      "name": "Lightning Strike",
      "image": "Icons/LightningStrike.png",
      "description": "Shocks up to 5 nearby enemies for 120 + 0.3% (0.15% to bosses) max health magic damage. Ability damage.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/LightningStrike.png' /> Lightning Strike<br /><span style='color: #ffcc00'> Order String:</span> VioletWind<br /><span style='color: #ffcc00'> Projectile - Speed:</span> 2500<br /><span style='color: #ffcc00'> Attack Type:</span> <img class='tooltip-icon' src='hud/img/icons/Magic.png' /> Magic<br /><span style='color: #ffcc00'> Mana Cost:</span> 9.80000<br /><span style='color: #ffcc00'> Damage Base:</span> 120.00000 (Ability Damage)<br /><span style='color: #ffcc00'> Area of Effect:</span> 500<br /><span style='color: #ffcc00'> Max Targets:</span> 5<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "unitType": "windhawk_unit_id",
      "name": "Windhawk",
      "image": "Icons/Windhawk.png"
    }
  ],
  "amountSpawned": 0
}
*/

export const raw_violet_unit_id: RawUnit = {
  name: "Violet",
  sketchfabUrl: "https://sketchfab.com/models/410d39cf3030422797abdcd9826a1311",
  image: require("#LTD2/Icons/Violet.png"),
  splash: require("#LTD2/splashes/Violet.png"),
  description: "Flying. Now periodically shocks enemies with lightning.",

  damage: Damage.Impact,
  atkSpeed: 0.900900889290549,
  onHitDmg: 73,

  armor: Armor.Natural,
  hp: 2340,
};

export const violet_unit_id: PlayableUnit = {
  ...raw_violet_unit_id,
  price: 180,
  evolFrom: windhawk_unit_id,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "mudman_unit_id",
  "name": "Mudman",
  "image": "Icons/Mudman.png",
  "splash": "splashes/Mudman.png",
  "sketchfabUrl": "https://sketchfab.com/models/6fcd3f52e9d243ecba6ea0d795ec5e0d",
  "description": "Tanky. You can activate Harden, which grants damage reduction for the next wave, but weakens it the following wave.",
  "cost": 140,
  "mythium": 0,
  "income": 0,
  "supply": 1,
  "attackType": "Impact",
  "attackTypeDescription": "115% to Arcane<br />115% to Fortified<br />90% to Natural<br />80% to Swift",
  "armorType": "Fortified",
  "armorTypeDescription": "115% from Impact<br />105% from Magic<br />80% from Pierce",
  "hp": 1660,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.3300000429153442,
  "attackSpeedDescriptor": "Slow",
  "mspd": 300,
  "dps": 40,
  "damage": 53,
  "range": 100,
  "totalValue": 140,
  "bounty": 0,
  "tier": "4",
  "legion": "Element",
  "legionType": "element_legion_id",
  "movementType": "Ground",
  "spawnBias": 0,
  "spawnDelay": 0,
  "effectiveDps": 39.849998474121094,
  "effectiveThreat": 39.849998474121094,
  "movementSize": 0.1875,
  "backswing": 0.4300000071525574,
  "projectileSpeed": 1200,
  "mastermindGroups": "FortOrNat, Impact, Special1",
  "flags": "Ground",
  "color": "bc6701",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Element T4",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "harden_ability_id",
      "abilityClass": "",
      "name": "Harden",
      "image": "Icons/Harden.png",
      "description": "Grants 3 + 15% damage reduction for the next wave. The wave after, it recovers and takes 3 + 15% extra damage.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Harden.png' /> Harden<br /><span style='color: #ffcc00'> Linked Abilities:</span> <img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Harden, <img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Undo<br /><span style='color: #ffcc00'> Order String:</span> JuiceUp"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "harden_active_ability_id",
      "abilityClass": "linked",
      "name": "Harden",
      "image": "Icons/Harden(Active).png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Harden<br /><span style='color: #ffcc00'>Caster Buff:</span> Harden (Active) (Buff)<br /><span style='color: #efe1a7'>--- Stats - Damage Reduction (% Reduction):</span> 0.1500<br /><span style='color: #efe1a7'>--- Stats - Damage Reduction (Flat):</span> 3.00000"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "abilityType": "undo_harden_ability_id",
      "abilityClass": "linked",
      "name": "Undo",
      "image": "Icons/UndoHarden.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Undo<br /><span style='color: #ffcc00'> Linked Abilities:</span> <img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Harden<br /><span style='color: #ffcc00'> Order String:</span> JuiceUp"
    }
  ],
  "upgradesTo": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 3,
      "unitType": "golem_unit_id",
      "name": "Golem",
      "image": "Icons/Golem.png"
    }
  ],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_mudman_unit_id: RawUnit = {
  name: "Mudman",
  sketchfabUrl: "https://sketchfab.com/models/6fcd3f52e9d243ecba6ea0d795ec5e0d",
  image: require("#LTD2/Icons/Mudman.png"),
  splash: require("#LTD2/splashes/Mudman.png"),
  description: "Tanky. You can activate Harden, which grants damage reduction for the next wave, but weakens it the following wave.",

  damage: Damage.Impact,
  atkSpeed: 0.7518796749870864,
  onHitDmg: 53,

  armor: Armor.Fortified,
  hp: 1660,
};

export const mudman_unit_id: PlayableUnit = {
  ...raw_mudman_unit_id,
  price: 140,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "golem_unit_id",
  "name": "Golem",
  "image": "Icons/Golem.png",
  "splash": "splashes/Golem.png",
  "sketchfabUrl": "https://sketchfab.com/models/8ec211deab6040cc9175ee277a4706c8",
  "description": "Encases its body in metal to become even tankier.",
  "cost": 265,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Impact",
  "attackTypeDescription": "115% to Arcane<br />115% to Fortified<br />90% to Natural<br />80% to Swift",
  "armorType": "Fortified",
  "armorTypeDescription": "115% from Impact<br />105% from Magic<br />80% from Pierce",
  "hp": 4750,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.3300000429153442,
  "attackSpeedDescriptor": "Slow",
  "mspd": 300,
  "dps": 114,
  "damage": 151,
  "range": 100,
  "totalValue": 405,
  "bounty": 0,
  "tier": "4",
  "legion": "Element",
  "legionType": "element_legion_id",
  "movementType": "Ground",
  "spawnBias": 0,
  "spawnDelay": 0,
  "effectiveDps": 113.52999877929688,
  "effectiveThreat": 113.52999877929688,
  "movementSize": 0.1875,
  "backswing": 0.5,
  "projectileSpeed": 1200,
  "mastermindGroups": "None",
  "flags": "Ground",
  "color": "bc6701",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Element T4U",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "harden_golem_ability_id",
      "abilityClass": "",
      "name": "Harden",
      "image": "Icons/Harden.png",
      "description": "Grants 3 + 15% damage reduction for the next wave. The wave after, it recovers and takes 3 + 15% extra damage.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Harden.png' /> Harden<br /><span style='color: #ffcc00'> Linked Abilities:</span> <img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Harden (Golem), <img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Undo (Golem)<br /><span style='color: #ffcc00'> Order String:</span> JuiceUp"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "harden_active_golem_ability_id",
      "abilityClass": "linked",
      "name": "Harden (Golem)",
      "image": "Icons/Harden(Active).png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Harden (Golem)<br /><span style='color: #ffcc00'>Caster Buff:</span> Harden (Active) (Buff) (Golem)<br /><span style='color: #efe1a7'>--- Stats - Damage Reduction (% Reduction):</span> 0.1500<br /><span style='color: #efe1a7'>--- Stats - Damage Reduction (Flat):</span> 3.00000"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "abilityType": "undo_harden_golem_ability_id",
      "abilityClass": "linked",
      "name": "Undo (Golem)",
      "image": "Icons/UndoHarden.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Undo (Golem)<br /><span style='color: #ffcc00'> Linked Abilities:</span> <img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Harden (Golem)<br /><span style='color: #ffcc00'> Order String:</span> JuiceUp"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 3,
      "unitType": "mudman_unit_id",
      "name": "Mudman",
      "image": "Icons/Mudman.png"
    }
  ],
  "amountSpawned": 0
}
*/

export const raw_golem_unit_id: RawUnit = {
  name: "Golem",
  sketchfabUrl: "https://sketchfab.com/models/8ec211deab6040cc9175ee277a4706c8",
  image: require("#LTD2/Icons/Golem.png"),
  splash: require("#LTD2/splashes/Golem.png"),
  description: "Encases its body in metal to become even tankier.",

  damage: Damage.Impact,
  atkSpeed: 0.7518796749870864,
  onHitDmg: 151,

  armor: Armor.Fortified,
  hp: 4750,
};

export const golem_unit_id: PlayableUnit = {
  ...raw_golem_unit_id,
  price: 265,
  evolFrom: mudman_unit_id,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "disciple_unit_id",
  "name": "Disciple",
  "image": "Icons/Disciple.png",
  "splash": "splashes/Disciple.png",
  "sketchfabUrl": "https://sketchfab.com/models/3e3d7e3f168348f2bf12d8970d23d6ac",
  "description": "Periodically deals massive damage to a single target.",
  "cost": 195,
  "mythium": 0,
  "income": 0,
  "supply": 1,
  "attackType": "Pierce",
  "attackTypeDescription": "115% to Arcane<br />80% to Fortified<br />85% to Natural<br />120% to Swift",
  "armorType": "Swift",
  "armorTypeDescription": "80% from Impact<br />100% from Magic<br />120% from Pierce",
  "hp": 1200,
  "mana": 9,
  "hpRegen": 0,
  "manaRegen": 1.399999976158142,
  "attackSpeed": 1.149999976158142,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 65,
  "damage": 75,
  "range": 500,
  "totalValue": 195,
  "bounty": 0,
  "tier": "5",
  "legion": "Element",
  "legionType": "element_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.8999999761581421,
  "spawnDelay": 0,
  "effectiveDps": 65.22000122070312,
  "effectiveThreat": 65.22000122070312,
  "movementSize": 0.1875,
  "backswing": 0.30000001192092896,
  "projectileSpeed": 1750,
  "mastermindGroups": "Pierce, Special1, Special2, Special3, Special4",
  "flags": "Ground, Organic",
  "color": "bc6701",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Element T5",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "mana_burst_ability_id",
      "abilityClass": "",
      "name": "Mana Burst",
      "image": "Icons/ManaBurst.png",
      "description": "Deals 320 pure damage to a single target. Ability damage.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/ManaBurst.png' /> Mana Burst<br /><span style='color: #ffcc00'> Order String:</span> EmpoweredAttack<br /><span style='color: #ffcc00'> Projectile - Speed:</span> 2500<br /><span style='color: #ffcc00'> Mana Cost:</span> 7.30000<br /><span style='color: #ffcc00'> Damage Base:</span> 320.00000 (Ability Damage)<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "intellect_ability_id",
      "abilityClass": "",
      "name": "Intellect",
      "image": "Icons/Intellect.png",
      "description": "Killing a unit restores 15% of max mana",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Intellect.png' /> Intellect<br /><span style='color: #ffcc00'> Order String:</span> Intellect<br /><span style='color: #ffcc00'> Percentage:</span> 0.15000<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "abilityType": "start_with_0_mana_ability_id",
      "abilityClass": "hidden",
      "name": "Start With 0 Mana",
      "image": "Icons/StartWith0Mana.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Start With 0 Mana<br /><span style='color: #ffcc00'> Order String:</span> Generator<br /><span style='color: #ffcc00'> Percentage:</span> 0.0000<br /><span style='color: #ffcc00'> Targets Excluded:</span> Building"
    }
  ],
  "upgradesTo": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 3,
      "unitType": "starcaller_unit_id",
      "name": "Starcaller",
      "image": "Icons/Starcaller.png"
    }
  ],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_disciple_unit_id: RawUnit = {
  name: "Disciple",
  sketchfabUrl: "https://sketchfab.com/models/3e3d7e3f168348f2bf12d8970d23d6ac",
  image: require("#LTD2/Icons/Disciple.png"),
  splash: require("#LTD2/splashes/Disciple.png"),
  description: "Periodically deals massive damage to a single target.",

  damage: Damage.Pierce,
  atkSpeed: 0.8695652354191746,
  onHitDmg: 75,

  armor: Armor.Swift,
  hp: 1200,
};

export const disciple_unit_id: PlayableUnit = {
  ...raw_disciple_unit_id,
  price: 195,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "starcaller_unit_id",
  "name": "Starcaller",
  "image": "Icons/Starcaller.png",
  "splash": "splashes/Starcaller.png",
  "sketchfabUrl": "https://sketchfab.com/models/6f0e725957d54e3c8a76d33ba6c84312",
  "description": "Aura. Boosts the mana regeneration of adjacently-built units",
  "cost": 335,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Pierce",
  "attackTypeDescription": "115% to Arcane<br />80% to Fortified<br />85% to Natural<br />120% to Swift",
  "armorType": "Swift",
  "armorTypeDescription": "80% from Impact<br />100% from Magic<br />120% from Pierce",
  "hp": 2960,
  "mana": 11,
  "hpRegen": 0,
  "manaRegen": 1.0499999523162842,
  "attackSpeed": 1.149999976158142,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 161,
  "damage": 185,
  "range": 500,
  "totalValue": 530,
  "bounty": 0,
  "tier": "5",
  "legion": "Element",
  "legionType": "element_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.8999999761581421,
  "spawnDelay": 0,
  "effectiveDps": 160.8699951171875,
  "effectiveThreat": 160.8699951171875,
  "movementSize": 0.1875,
  "backswing": 0.3499999940395355,
  "projectileSpeed": 1750,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "bc6701",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Element T5U",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "amplify_magic_oracle_ability_id",
      "abilityClass": "",
      "name": "Amplify Magic",
      "image": "Icons/AmplifyMagic.png",
      "description": "Increases the mana regeneration of adjacently-built units by 0.7 per second.  Does not stack.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/AmplifyMagic.png' /> Amplify Magic<br /><span style='color: #ffcc00'> Order String:</span> Oracle<br /><span style='color: #ffcc00'> Area of Effect:</span> 120<br /><span style='color: #ffcc00'> Targets Considered:</span> Allied, Invulnerable<br /><span style='color: #ffcc00'>Target Buff:</span> Amplify Magic<br /><span style='color: #efe1a7'>--- Stats - Mana Regeneration Bonus (Flat):</span> 0.70000"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "enhanced_mana_burst_ability_id",
      "abilityClass": "",
      "name": "Asteroid",
      "image": "Icons/Asteroid.png",
      "description": "Deals 500 + 1% max health pure damage to a single target. Ability damage.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Asteroid.png' /> Asteroid<br /><span style='color: #ffcc00'> Order String:</span> EmpoweredAttack<br /><span style='color: #ffcc00'> Projectile - Speed:</span> 2500<br /><span style='color: #ffcc00'> Mana Cost:</span> 9.00000<br /><span style='color: #ffcc00'> Damage Base:</span> 500.00000 (Ability Damage)<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "abilityType": "amplify_magic_ability_id",
      "abilityClass": "hidden",
      "name": "Amplify Magic",
      "image": "Icons/AmplifyMagic.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Amplify Magic<br /><span style='color: #ffcc00'> Order String:</span> SimpleAura<br /><span style='color: #ffcc00'> Area of Effect:</span> 120<br /><span style='color: #ffcc00'> Targets Considered:</span> Allied<br /><span style='color: #ffcc00'> Targets Excluded:</span> Boss<br /><span style='color: #ffcc00'>Target Buff:</span> Amplify Magic (Buff)<br /><span style='color: #efe1a7'>--- Stats - Mana Regeneration Bonus (Flat):</span> 0.70000<br /><span style='color: #ffcc00'>Caster Buff:</span> Amplify Magic (Caster Buff)"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 3,
      "abilityType": "intellect_ability_id",
      "abilityClass": "",
      "name": "Intellect",
      "image": "Icons/Intellect.png",
      "description": "Killing a unit restores 15% of max mana",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Intellect.png' /> Intellect<br /><span style='color: #ffcc00'> Order String:</span> Intellect<br /><span style='color: #ffcc00'> Percentage:</span> 0.15000<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 4,
      "abilityType": "start_with_0_mana_ability_id",
      "abilityClass": "hidden",
      "name": "Start With 0 Mana",
      "image": "Icons/StartWith0Mana.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Start With 0 Mana<br /><span style='color: #ffcc00'> Order String:</span> Generator<br /><span style='color: #ffcc00'> Percentage:</span> 0.0000<br /><span style='color: #ffcc00'> Targets Excluded:</span> Building"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 5,
      "unitType": "disciple_unit_id",
      "name": "Disciple",
      "image": "Icons/Disciple.png"
    }
  ],
  "amountSpawned": 0
}
*/

export const raw_starcaller_unit_id: RawUnit = {
  name: "Starcaller",
  sketchfabUrl: "https://sketchfab.com/models/6f0e725957d54e3c8a76d33ba6c84312",
  image: require("#LTD2/Icons/Starcaller.png"),
  splash: require("#LTD2/splashes/Starcaller.png"),
  description: "Aura. Boosts the mana regeneration of adjacently-built units",

  damage: Damage.Pierce,
  atkSpeed: 0.8695652354191746,
  onHitDmg: 185,

  armor: Armor.Swift,
  hp: 2960,
};

export const starcaller_unit_id: PlayableUnit = {
  ...raw_starcaller_unit_id,
  price: 335,
  evolFrom: disciple_unit_id,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "fire_lord_unit_id",
  "name": "Fire Lord",
  "image": "Icons/FireLord.png",
  "splash": "splashes/FireLord.png",
  "sketchfabUrl": "https://sketchfab.com/models/8f1a0a0779a34abcba0b6cdbd96e8f5e",
  "description": "Uses mana to block incoming damage and deals bonus damage based on missing mana.",
  "cost": 275,
  "mythium": 0,
  "income": 0,
  "supply": 1,
  "attackType": "Magic",
  "attackTypeDescription": "75% to Arcane<br />105% to Fortified<br />125% to Natural<br />100% to Swift",
  "armorType": "Swift",
  "armorTypeDescription": "80% from Impact<br />100% from Magic<br />120% from Pierce",
  "hp": 1820,
  "mana": 8,
  "hpRegen": 0,
  "manaRegen": 0.4000000059604645,
  "attackSpeed": 0.949999988079071,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 141,
  "damage": 134,
  "range": 200,
  "totalValue": 275,
  "bounty": 0,
  "tier": "6",
  "legion": "Element",
  "legionType": "element_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.5,
  "spawnDelay": 0,
  "effectiveDps": 141.0500030517578,
  "effectiveThreat": 141.0500030517578,
  "movementSize": 0.1875,
  "backswing": 0.44999998807907104,
  "projectileSpeed": 1750,
  "mastermindGroups": "Magic, Special1, Special2",
  "flags": "Ground",
  "color": "bc6701",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Element T6",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "molten_shield_ability_id",
      "abilityClass": "",
      "name": "Molten Shield",
      "image": "Icons/MoltenShield.png",
      "description": "Absorbs 60% of incoming damage from each attack",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/MoltenShield.png' /> Molten Shield<br /><span style='color: #ffcc00'> Order String:</span> ManaShield<br /><span style='color: #ffcc00'> Mana Cost:</span> 0.80000<br /><span style='color: #ffcc00'> Percentage:</span> 0.60000<br /><span style='color: #ffcc00'>Caster Buff:</span> Molten Shield"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "overheat_ability_id",
      "abilityClass": "",
      "name": "Overheat",
      "image": "Icons/Overheat.png",
      "description": "Deals 1% bonus damage for each 2% missing mana",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Overheat.png' /> Overheat<br /><span style='color: #ffcc00'> Order String:</span> Overheat<br /><span style='color: #ffcc00'> Percentage:</span> 0.0050<br /><span style='color: #ffcc00'>Target Buff:</span> Overheat"
    }
  ],
  "upgradesTo": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "unitType": "fenix_unit_id",
      "name": "Fenix",
      "image": "Icons/Fenix.png"
    }
  ],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_fire_lord_unit_id: RawUnit = {
  name: "Fire Lord",
  sketchfabUrl: "https://sketchfab.com/models/8f1a0a0779a34abcba0b6cdbd96e8f5e",
  image: require("#LTD2/Icons/FireLord.png"),
  splash: require("#LTD2/splashes/FireLord.png"),
  description: "Uses mana to block incoming damage and deals bonus damage based on missing mana.",

  damage: Damage.Magic,
  atkSpeed: 1.0526315921561542,
  onHitDmg: 134,

  armor: Armor.Swift,
  hp: 1820,
};

export const fire_lord_unit_id: PlayableUnit = {
  ...raw_fire_lord_unit_id,
  price: 275,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "fenix_unit_id",
  "name": "Fenix",
  "image": "Icons/Fenix.png",
  "splash": "splashes/Fenix.png",
  "sketchfabUrl": "https://sketchfab.com/models/ae4745f03afc436d8fb50299e8f32c20",
  "description": "Flying. A mythical creature, now with an even more powerful mana shield.",
  "cost": 430,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Magic",
  "attackTypeDescription": "75% to Arcane<br />105% to Fortified<br />125% to Natural<br />100% to Swift",
  "armorType": "Swift",
  "armorTypeDescription": "80% from Impact<br />100% from Magic<br />120% from Pierce",
  "hp": 4650,
  "mana": 8,
  "hpRegen": 0,
  "manaRegen": 0.4000000059604645,
  "attackSpeed": 0.949999988079071,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 311,
  "damage": 295,
  "range": 200,
  "totalValue": 705,
  "bounty": 0,
  "tier": "6",
  "legion": "Element",
  "legionType": "element_legion_id",
  "movementType": "Air",
  "spawnBias": 0.5,
  "spawnDelay": 0,
  "effectiveDps": 310.5299987792969,
  "effectiveThreat": 310.5299987792969,
  "movementSize": 0.1875,
  "backswing": 0.30000001192092896,
  "projectileSpeed": 1750,
  "mastermindGroups": "None",
  "flags": "Air",
  "color": "bc6701",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Element T6U",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "enhanced_molten_shield_ability_id",
      "abilityClass": "",
      "name": "Prismatic Shield",
      "image": "Icons/PrismaticShield.png",
      "description": "Absorbs 80% of incoming damage from each attack",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/PrismaticShield.png' /> Prismatic Shield<br /><span style='color: #ffcc00'> Order String:</span> ManaShield<br /><span style='color: #ffcc00'> Mana Cost:</span> 0.80000<br /><span style='color: #ffcc00'> Percentage:</span> 0.80000<br /><span style='color: #ffcc00'>Caster Buff:</span> Prismatic Shield"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "overheat_ability_id",
      "abilityClass": "",
      "name": "Overheat",
      "image": "Icons/Overheat.png",
      "description": "Deals 1% bonus damage for each 2% missing mana",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Overheat.png' /> Overheat<br /><span style='color: #ffcc00'> Order String:</span> Overheat<br /><span style='color: #ffcc00'> Percentage:</span> 0.0050<br /><span style='color: #ffcc00'>Target Buff:</span> Overheat"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "unitType": "fire_lord_unit_id",
      "name": "Fire Lord",
      "image": "Icons/FireLord.png"
    }
  ],
  "amountSpawned": 0
}
*/

export const raw_fenix_unit_id: RawUnit = {
  name: "Fenix",
  sketchfabUrl: "https://sketchfab.com/models/ae4745f03afc436d8fb50299e8f32c20",
  image: require("#LTD2/Icons/Fenix.png"),
  splash: require("#LTD2/splashes/Fenix.png"),
  description: "Flying. A mythical creature, now with an even more powerful mana shield.",

  damage: Damage.Magic,
  atkSpeed: 1.0526315921561542,
  onHitDmg: 295,

  armor: Armor.Swift,
  hp: 4650,
};

export const fenix_unit_id: PlayableUnit = {
  ...raw_fenix_unit_id,
  price: 430,
  evolFrom: fire_lord_unit_id,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "chaos_hound_unit_id",
  "name": "Chaos Hound",
  "image": "Icons/ChaosHound.png",
  "splash": "splashes/ChaosHound.png",
  "sketchfabUrl": "https://sketchfab.com/models/2d419573bcdb4086b9eb97f2f3ebc911",
  "description": "High damage melee pet",
  "cost": 0,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Impact",
  "attackTypeDescription": "115% to Arcane<br />115% to Fortified<br />90% to Natural<br />80% to Swift",
  "armorType": "Fortified",
  "armorTypeDescription": "115% from Impact<br />105% from Magic<br />80% from Pierce",
  "hp": 300,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.949999988079071,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 28,
  "damage": 27,
  "range": 100,
  "totalValue": 0,
  "bounty": 0,
  "tier": "0",
  "legion": "Forsaken",
  "legionType": "forsaken_legion_id",
  "movementType": "Hover",
  "spawnBias": 0.4000000059604645,
  "spawnDelay": 0,
  "effectiveDps": 28.420000076293945,
  "effectiveThreat": 28.420000076293945,
  "movementSize": 0.1875,
  "backswing": 0.30000001192092896,
  "projectileSpeed": 1200,
  "mastermindGroups": "None",
  "flags": "Ground, Organic, Summoned",
  "color": "9b2330",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Forsaken ",
  "suffix": "(Special)",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "degeneration_ability_id",
      "abilityClass": "",
      "name": "Pet",
      "image": "Icons/Pet.png",
      "description": "Pets degenerate 2% max health per second",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Pet.png' /> Pet<br /><span style='color: #ffcc00'>Caster Buff:</span> Pet<br /><span style='color: #efe1a7'>--- Damage Per Second (% of Max):</span> 0.0200 (Ability Damage)"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_chaos_hound_unit_id: RawUnit = {
  name: "Chaos Hound",
  sketchfabUrl: "https://sketchfab.com/models/2d419573bcdb4086b9eb97f2f3ebc911",
  image: require("#LTD2/Icons/ChaosHound.png"),
  splash: require("#LTD2/splashes/ChaosHound.png"),
  description: "High damage melee pet",

  damage: Damage.Impact,
  atkSpeed: 1.0526315921561542,
  onHitDmg: 27,

  armor: Armor.Fortified,
  hp: 300,
};

export const chaos_hound_unit_id: PlayableUnit = {
  ...raw_chaos_hound_unit_id,
  price: 0,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "undead_dragon_unit_id",
  "name": "Undead Dragon",
  "image": "Icons/UndeadDragon.png",
  "splash": "splashes/UndeadDragon.png",
  "sketchfabUrl": "https://sketchfab.com/models/79267c0e3e314b39b5615281ae405839",
  "description": "Ranged summon that heals itself when it kills a unit",
  "cost": 0,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Magic",
  "attackTypeDescription": "75% to Arcane<br />105% to Fortified<br />125% to Natural<br />100% to Swift",
  "armorType": "Arcane",
  "armorTypeDescription": "115% from Impact<br />75% from Magic<br />115% from Pierce",
  "hp": 800,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.2699999809265137,
  "attackSpeedDescriptor": "Slow",
  "mspd": 300,
  "dps": 79,
  "damage": 100,
  "range": 350,
  "totalValue": 0,
  "bounty": 0,
  "tier": "0",
  "legion": "Forsaken",
  "legionType": "forsaken_legion_id",
  "movementType": "Air",
  "spawnBias": 0.800000011920929,
  "spawnDelay": 0,
  "effectiveDps": 78.73999786376953,
  "effectiveThreat": 78.73999786376953,
  "movementSize": 0.1875,
  "backswing": 0.3499999940395355,
  "projectileSpeed": 1500,
  "mastermindGroups": "None",
  "flags": "Air, Organic, Summoned",
  "color": "9b2330",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Forsaken ",
  "suffix": "(Special)",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "degeneration_ability_id",
      "abilityClass": "",
      "name": "Pet",
      "image": "Icons/Pet.png",
      "description": "Pets degenerate 2% max health per second",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Pet.png' /> Pet<br /><span style='color: #ffcc00'>Caster Buff:</span> Pet<br /><span style='color: #efe1a7'>--- Damage Per Second (% of Max):</span> 0.0200 (Ability Damage)"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "devour_ability_id",
      "abilityClass": "",
      "name": "Devour",
      "image": "Icons/Devour.png",
      "description": "When this unit lands a killing blow, it heals itself for 20% of its max health.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Devour.png' /> Devour<br /><span style='color: #ffcc00'> Order String:</span> BigGameHunter"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_undead_dragon_unit_id: RawUnit = {
  name: "Undead Dragon",
  sketchfabUrl: "https://sketchfab.com/models/79267c0e3e314b39b5615281ae405839",
  image: require("#LTD2/Icons/UndeadDragon.png"),
  splash: require("#LTD2/splashes/UndeadDragon.png"),
  description: "Ranged summon that heals itself when it kills a unit",

  damage: Damage.Magic,
  atkSpeed: 0.7874015866287349,
  onHitDmg: 100,

  armor: Armor.Arcane,
  hp: 800,
};

export const undead_dragon_unit_id: PlayableUnit = {
  ...raw_undead_dragon_unit_id,
  price: 0,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "chaos_hound_harbinger_unit_id",
  "name": "Cerberus",
  "image": "Icons/Cerberus.png",
  "splash": "splashes/Cerberus.png",
  "sketchfabUrl": "https://sketchfab.com/models/2d419573bcdb4086b9eb97f2f3ebc911",
  "description": "High damage melee pet",
  "cost": 0,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Impact",
  "attackTypeDescription": "115% to Arcane<br />115% to Fortified<br />90% to Natural<br />80% to Swift",
  "armorType": "Fortified",
  "armorTypeDescription": "115% from Impact<br />105% from Magic<br />80% from Pierce",
  "hp": 860,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.949999988079071,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 81,
  "damage": 77,
  "range": 100,
  "totalValue": 0,
  "bounty": 0,
  "tier": "0",
  "legion": "Forsaken",
  "legionType": "forsaken_legion_id",
  "movementType": "Hover",
  "spawnBias": 0.4000000059604645,
  "spawnDelay": 0,
  "effectiveDps": 81.05000305175781,
  "effectiveThreat": 81.05000305175781,
  "movementSize": 0.1875,
  "backswing": 0.30000001192092896,
  "projectileSpeed": 1200,
  "mastermindGroups": "None",
  "flags": "Ground, Organic, Summoned",
  "color": "9b2330",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Forsaken ",
  "suffix": "(Special)",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "degeneration_ability_id",
      "abilityClass": "",
      "name": "Pet",
      "image": "Icons/Pet.png",
      "description": "Pets degenerate 2% max health per second",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Pet.png' /> Pet<br /><span style='color: #ffcc00'>Caster Buff:</span> Pet<br /><span style='color: #efe1a7'>--- Damage Per Second (% of Max):</span> 0.0200 (Ability Damage)"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_chaos_hound_harbinger_unit_id: RawUnit = {
  name: "Cerberus",
  sketchfabUrl: "https://sketchfab.com/models/2d419573bcdb4086b9eb97f2f3ebc911",
  image: require("#LTD2/Icons/Cerberus.png"),
  splash: require("#LTD2/splashes/Cerberus.png"),
  description: "High damage melee pet",

  damage: Damage.Impact,
  atkSpeed: 1.0526315921561542,
  onHitDmg: 77,

  armor: Armor.Fortified,
  hp: 860,
};

export const chaos_hound_harbinger_unit_id: PlayableUnit = {
  ...raw_chaos_hound_harbinger_unit_id,
  price: 0,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "thanatos_unit_id",
  "name": "Thanatos",
  "image": "Icons/Thanatos.png",
  "splash": "splashes/Thanatos.png",
  "sketchfabUrl": "https://sketchfab.com/models/79267c0e3e314b39b5615281ae405839",
  "description": "Ranged summon that heals itself when it kills a unit",
  "cost": 0,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Magic",
  "attackTypeDescription": "75% to Arcane<br />105% to Fortified<br />125% to Natural<br />100% to Swift",
  "armorType": "Arcane",
  "armorTypeDescription": "115% from Impact<br />75% from Magic<br />115% from Pierce",
  "hp": 2000,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.2699999809265137,
  "attackSpeedDescriptor": "Slow",
  "mspd": 300,
  "dps": 197,
  "damage": 250,
  "range": 350,
  "totalValue": 0,
  "bounty": 0,
  "tier": "0",
  "legion": "Forsaken",
  "legionType": "forsaken_legion_id",
  "movementType": "Air",
  "spawnBias": 0.800000011920929,
  "spawnDelay": 0,
  "effectiveDps": 196.85000610351562,
  "effectiveThreat": 196.85000610351562,
  "movementSize": 0.1875,
  "backswing": 0.3499999940395355,
  "projectileSpeed": 1500,
  "mastermindGroups": "None",
  "flags": "Air, Organic, Summoned",
  "color": "9b2330",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Forsaken ",
  "suffix": "(Special)",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "degeneration_ability_id",
      "abilityClass": "",
      "name": "Pet",
      "image": "Icons/Pet.png",
      "description": "Pets degenerate 2% max health per second",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Pet.png' /> Pet<br /><span style='color: #ffcc00'>Caster Buff:</span> Pet<br /><span style='color: #efe1a7'>--- Damage Per Second (% of Max):</span> 0.0200 (Ability Damage)"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "devour_ability_id",
      "abilityClass": "",
      "name": "Devour",
      "image": "Icons/Devour.png",
      "description": "When this unit lands a killing blow, it heals itself for 20% of its max health.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Devour.png' /> Devour<br /><span style='color: #ffcc00'> Order String:</span> BigGameHunter"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_thanatos_unit_id: RawUnit = {
  name: "Thanatos",
  sketchfabUrl: "https://sketchfab.com/models/79267c0e3e314b39b5615281ae405839",
  image: require("#LTD2/Icons/Thanatos.png"),
  splash: require("#LTD2/splashes/Thanatos.png"),
  description: "Ranged summon that heals itself when it kills a unit",

  damage: Damage.Magic,
  atkSpeed: 0.7874015866287349,
  onHitDmg: 250,

  armor: Armor.Arcane,
  hp: 2000,
};

export const thanatos_unit_id: PlayableUnit = {
  ...raw_thanatos_unit_id,
  price: 0,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "bone_warrior_unit_id",
  "name": "Bone Warrior",
  "image": "Icons/BoneWarrior.png",
  "splash": "splashes/BoneWarrior.png",
  "sketchfabUrl": "https://sketchfab.com/models/08390ff61dcc4b98a38c6f998f2505e7",
  "description": "Passively regenerates health. Has three upgrade paths, making it incredibly versatile.",
  "cost": 15,
  "mythium": 0,
  "income": 0,
  "supply": 1,
  "attackType": "Impact",
  "attackTypeDescription": "115% to Arcane<br />115% to Fortified<br />90% to Natural<br />80% to Swift",
  "armorType": "Swift",
  "armorTypeDescription": "80% from Impact<br />100% from Magic<br />120% from Pierce",
  "hp": 100,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.2999999523162842,
  "attackSpeedDescriptor": "Slow",
  "mspd": 300,
  "dps": 4,
  "damage": 5,
  "range": 100,
  "totalValue": 15,
  "bounty": 0,
  "tier": "1",
  "legion": "Forsaken",
  "legionType": "forsaken_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.4000000059604645,
  "spawnDelay": 0,
  "effectiveDps": 3.8499999046325684,
  "effectiveThreat": 3.8499999046325684,
  "movementSize": 0.1875,
  "backswing": 0.20000000298023224,
  "projectileSpeed": 1200,
  "mastermindGroups": "Special1, Special2, Special3",
  "flags": "Ground, Organic",
  "color": "9b2330",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Forsaken T1",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "frenzy_ability_id",
      "abilityClass": "",
      "name": "Skeletal Regeneration",
      "image": "Icons/SkeletalRegeneration.png",
      "description": "Regenerates 3 + 3% missing health per second",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/SkeletalRegeneration.png' /> Skeletal Regeneration<br /><span style='color: #ffcc00'>Caster Buff:</span> Skeletal Regeneration<br /><span style='color: #efe1a7'>--- Stats - HP Regeneration Bonus (Flat):</span> 3.00000<br /><span style='color: #efe1a7'>--- Stats - HP Regeneration Bonus (% of Missing HP):</span> 0.03000"
    }
  ],
  "upgradesTo": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "unitType": "fire_archer_unit_id",
      "name": "Fire Archer",
      "image": "Icons/FireArcher.png"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "unitType": "dark_mage_unit_id",
      "name": "Dark Mage",
      "image": "Icons/DarkMage.png"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 3,
      "unitType": "bone_crusher_unit_id",
      "name": "Bone Crusher",
      "image": "Icons/BoneCrusher.png"
    }
  ],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_bone_warrior_unit_id: RawUnit = {
  name: "Bone Warrior",
  sketchfabUrl: "https://sketchfab.com/models/08390ff61dcc4b98a38c6f998f2505e7",
  image: require("#LTD2/Icons/BoneWarrior.png"),
  splash: require("#LTD2/splashes/BoneWarrior.png"),
  description: "Passively regenerates health. Has three upgrade paths, making it incredibly versatile.",

  damage: Damage.Impact,
  atkSpeed: 0.7692307974459868,
  onHitDmg: 5,

  armor: Armor.Swift,
  hp: 100,
};

export const bone_warrior_unit_id: PlayableUnit = {
  ...raw_bone_warrior_unit_id,
  price: 15,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "dark_mage_unit_id",
  "name": "Dark Mage",
  "image": "Icons/DarkMage.png",
  "splash": "splashes/DarkMage.png",
  "sketchfabUrl": "https://sketchfab.com/models/e38e736ee1ec46458c90ed9eb7adb427",
  "description": "Gives an ally a huge attack-speed boost",
  "cost": 80,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Magic",
  "attackTypeDescription": "75% to Arcane<br />105% to Fortified<br />125% to Natural<br />100% to Swift",
  "armorType": "Swift",
  "armorTypeDescription": "80% from Impact<br />100% from Magic<br />120% from Pierce",
  "hp": 500,
  "mana": 8,
  "hpRegen": 0,
  "manaRegen": 0.875,
  "attackSpeed": 1.1200000047683716,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 32,
  "damage": 36,
  "range": 400,
  "totalValue": 95,
  "bounty": 0,
  "tier": "1",
  "legion": "Forsaken",
  "legionType": "forsaken_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.699999988079071,
  "spawnDelay": 0,
  "effectiveDps": 32.13999938964844,
  "effectiveThreat": 32.13999938964844,
  "movementSize": 0.1875,
  "backswing": 0.2199999988079071,
  "projectileSpeed": 1750,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "9b2330",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Forsaken T1U",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "mindwarp_ability_id",
      "abilityClass": "",
      "name": "Mind Warp",
      "image": "Icons/MindWarp.png",
      "description": "Increase the attack speed of the highest DPS nearby unit by 30% for 5 seconds",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/MindWarp.png' /> Mind Warp<br /><span style='color: #ffcc00'> Order String:</span> Mindwarp<br /><span style='color: #ffcc00'> Projectile - Speed:</span> 3000<br /><span style='color: #ffcc00'> Mana Cost:</span> 7.00000<br /><span style='color: #ffcc00'> Duration:</span> 5.00000<br /><span style='color: #ffcc00'> Duration (Boss):</span> 5.00000<br /><span style='color: #ffcc00'> Targets Considered:</span> Allied, NonBoss<br /><span style='color: #ffcc00'>Target Buff:</span> Mind Warp<br /><span style='color: #efe1a7'>--- Stats - Attack Rate Bonus (%):</span> 0.30000"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "frenzy_ability_id",
      "abilityClass": "",
      "name": "Skeletal Regeneration",
      "image": "Icons/SkeletalRegeneration.png",
      "description": "Regenerates 3 + 3% missing health per second",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/SkeletalRegeneration.png' /> Skeletal Regeneration<br /><span style='color: #ffcc00'>Caster Buff:</span> Skeletal Regeneration<br /><span style='color: #efe1a7'>--- Stats - HP Regeneration Bonus (Flat):</span> 3.00000<br /><span style='color: #efe1a7'>--- Stats - HP Regeneration Bonus (% of Missing HP):</span> 0.03000"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "abilityType": "start_with_0_mana_ability_id",
      "abilityClass": "hidden",
      "name": "Start With 0 Mana",
      "image": "Icons/StartWith0Mana.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Start With 0 Mana<br /><span style='color: #ffcc00'> Order String:</span> Generator<br /><span style='color: #ffcc00'> Percentage:</span> 0.0000<br /><span style='color: #ffcc00'> Targets Excluded:</span> Building"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 3,
      "unitType": "bone_warrior_unit_id",
      "name": "Bone Warrior",
      "image": "Icons/BoneWarrior.png"
    }
  ],
  "amountSpawned": 0
}
*/

export const raw_dark_mage_unit_id: RawUnit = {
  name: "Dark Mage",
  sketchfabUrl: "https://sketchfab.com/models/e38e736ee1ec46458c90ed9eb7adb427",
  image: require("#LTD2/Icons/DarkMage.png"),
  splash: require("#LTD2/splashes/DarkMage.png"),
  description: "Gives an ally a huge attack-speed boost",

  damage: Damage.Magic,
  atkSpeed: 0.8928571390558262,
  onHitDmg: 36,

  armor: Armor.Swift,
  hp: 500,
};

export const dark_mage_unit_id: PlayableUnit = {
  ...raw_dark_mage_unit_id,
  price: 80,
  evolFrom: bone_warrior_unit_id,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "fire_archer_unit_id",
  "name": "Fire Archer",
  "image": "Icons/FireArcher.png",
  "splash": "splashes/FireArcher.png",
  "sketchfabUrl": "https://sketchfab.com/models/e7922b03b8be474e8d6adbcfc2f8ef9a",
  "description": "Long-ranged. In addition to passively regenerating health, it uses mana to ignite its arrows, dealing bonus damage.",
  "cost": 65,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Pierce",
  "attackTypeDescription": "115% to Arcane<br />80% to Fortified<br />85% to Natural<br />120% to Swift",
  "armorType": "Swift",
  "armorTypeDescription": "80% from Impact<br />100% from Magic<br />120% from Pierce",
  "hp": 460,
  "mana": 6,
  "hpRegen": 0,
  "manaRegen": 0.30000001192092896,
  "attackSpeed": 1.1200000047683716,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 25,
  "damage": 28,
  "range": 550,
  "totalValue": 80,
  "bounty": 0,
  "tier": "1",
  "legion": "Forsaken",
  "legionType": "forsaken_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.699999988079071,
  "spawnDelay": 0,
  "effectiveDps": 25,
  "effectiveThreat": 25,
  "movementSize": 0.1875,
  "backswing": 0.20000000298023224,
  "projectileSpeed": 2500,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "9b2330",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Forsaken T1U",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "flaming_arrows_ability_id",
      "abilityClass": "",
      "name": "Flaming Arrows",
      "image": "Icons/FlamingArrows.png",
      "description": "Each attack deals 40 bonus magic damage. Ability damage.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/FlamingArrows.png' /> Flaming Arrows<br /><span style='color: #ffcc00'> Order String:</span> SearingArrows<br /><span style='color: #ffcc00'> Projectile - Speed:</span> 2500<br /><span style='color: #ffcc00'> Attack Type:</span> <img class='tooltip-icon' src='hud/img/icons/Magic.png' /> Magic<br /><span style='color: #ffcc00'> Mana Cost:</span> 1.20000<br /><span style='color: #ffcc00'> Damage Base:</span> 40.000 (Ability Damage)<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "frenzy_ability_id",
      "abilityClass": "",
      "name": "Skeletal Regeneration",
      "image": "Icons/SkeletalRegeneration.png",
      "description": "Regenerates 3 + 3% missing health per second",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/SkeletalRegeneration.png' /> Skeletal Regeneration<br /><span style='color: #ffcc00'>Caster Buff:</span> Skeletal Regeneration<br /><span style='color: #efe1a7'>--- Stats - HP Regeneration Bonus (Flat):</span> 3.00000<br /><span style='color: #efe1a7'>--- Stats - HP Regeneration Bonus (% of Missing HP):</span> 0.03000"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "unitType": "bone_warrior_unit_id",
      "name": "Bone Warrior",
      "image": "Icons/BoneWarrior.png"
    }
  ],
  "amountSpawned": 0
}
*/

export const raw_fire_archer_unit_id: RawUnit = {
  name: "Fire Archer",
  sketchfabUrl: "https://sketchfab.com/models/e7922b03b8be474e8d6adbcfc2f8ef9a",
  image: require("#LTD2/Icons/FireArcher.png"),
  splash: require("#LTD2/splashes/FireArcher.png"),
  description: "Long-ranged. In addition to passively regenerating health, it uses mana to ignite its arrows, dealing bonus damage.",

  damage: Damage.Pierce,
  atkSpeed: 0.8928571390558262,
  onHitDmg: 28,

  armor: Armor.Swift,
  hp: 460,
};

export const fire_archer_unit_id: PlayableUnit = {
  ...raw_fire_archer_unit_id,
  price: 65,
  evolFrom: bone_warrior_unit_id,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "bone_crusher_unit_id",
  "name": "Bone Crusher",
  "image": "Icons/BoneCrusher.png",
  "splash": "splashes/BoneCrusher.png",
  "sketchfabUrl": "https://sketchfab.com/models/df040ece365d4e9085ce68df324d0ea5",
  "description": "The only tanky skeleton. The weaker it is, the faster it regenerates.",
  "cost": 95,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Impact",
  "attackTypeDescription": "115% to Arcane<br />115% to Fortified<br />90% to Natural<br />80% to Swift",
  "armorType": "Fortified",
  "armorTypeDescription": "115% from Impact<br />105% from Magic<br />80% from Pierce",
  "hp": 1140,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.949999988079071,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 37,
  "damage": 35,
  "range": 100,
  "totalValue": 110,
  "bounty": 0,
  "tier": "1",
  "legion": "Forsaken",
  "legionType": "forsaken_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.20000000298023224,
  "spawnDelay": 0,
  "effectiveDps": 36.84000015258789,
  "effectiveThreat": 36.84000015258789,
  "movementSize": 0.1875,
  "backswing": 0.18000000715255737,
  "projectileSpeed": 1200,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "9b2330",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Forsaken T1U",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "frenzy_ability_id",
      "abilityClass": "",
      "name": "Skeletal Regeneration",
      "image": "Icons/SkeletalRegeneration.png",
      "description": "Regenerates 3 + 3% missing health per second",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/SkeletalRegeneration.png' /> Skeletal Regeneration<br /><span style='color: #ffcc00'>Caster Buff:</span> Skeletal Regeneration<br /><span style='color: #efe1a7'>--- Stats - HP Regeneration Bonus (Flat):</span> 3.00000<br /><span style='color: #efe1a7'>--- Stats - HP Regeneration Bonus (% of Missing HP):</span> 0.03000"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "unitType": "bone_warrior_unit_id",
      "name": "Bone Warrior",
      "image": "Icons/BoneWarrior.png"
    }
  ],
  "amountSpawned": 0
}
*/

export const raw_bone_crusher_unit_id: RawUnit = {
  name: "Bone Crusher",
  sketchfabUrl: "https://sketchfab.com/models/df040ece365d4e9085ce68df324d0ea5",
  image: require("#LTD2/Icons/BoneCrusher.png"),
  splash: require("#LTD2/splashes/BoneCrusher.png"),
  description: "The only tanky skeleton. The weaker it is, the faster it regenerates.",

  damage: Damage.Impact,
  atkSpeed: 1.0526315921561542,
  onHitDmg: 35,

  armor: Armor.Fortified,
  hp: 1140,
};

export const bone_crusher_unit_id: PlayableUnit = {
  ...raw_bone_crusher_unit_id,
  price: 95,
  evolFrom: bone_warrior_unit_id,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "gargoyle_unit_id",
  "name": "Gargoyle",
  "image": "Icons/Gargoyle.png",
  "splash": "splashes/Gargoyle.png",
  "sketchfabUrl": "https://sketchfab.com/models/d5b3c55e2de6495da0232748c4aad509",
  "description": "Flying. Takes reduced damage.",
  "cost": 40,
  "mythium": 0,
  "income": 0,
  "supply": 1,
  "attackType": "Magic",
  "attackTypeDescription": "75% to Arcane<br />105% to Fortified<br />125% to Natural<br />100% to Swift",
  "armorType": "Arcane",
  "armorTypeDescription": "115% from Impact<br />75% from Magic<br />115% from Pierce",
  "hp": 290,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.100000023841858,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 15,
  "damage": 17,
  "range": 100,
  "totalValue": 40,
  "bounty": 0,
  "tier": "2",
  "legion": "Forsaken",
  "legionType": "forsaken_legion_id",
  "movementType": "Air",
  "spawnBias": 0.20000000298023224,
  "spawnDelay": 0,
  "effectiveDps": 15.449999809265137,
  "effectiveThreat": 15.449999809265137,
  "movementSize": 0.1875,
  "backswing": 0.4000000059604645,
  "projectileSpeed": 1500,
  "mastermindGroups": "Arcane, Magic",
  "flags": "Air, Organic",
  "color": "9b2330",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Forsaken T2",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "stone_form_new_ability_id",
      "abilityClass": "",
      "name": "Stone Skin",
      "image": "Icons/StoneSkin.png",
      "description": "Takes 1 + 30% reduced damage",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/StoneSkin.png' /> Stone Skin<br /><span style='color: #ffcc00'>Caster Buff:</span> Stone Skin<br /><span style='color: #efe1a7'>--- Stats - Damage Reduction (% Reduction):</span> 0.30000<br /><span style='color: #efe1a7'>--- Stats - Damage Reduction (Flat):</span> 1.00000"
    }
  ],
  "upgradesTo": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "unitType": "green_devil_unit_id",
      "name": "Green Devil",
      "image": "Icons/GreenDevil.png"
    }
  ],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_gargoyle_unit_id: RawUnit = {
  name: "Gargoyle",
  sketchfabUrl: "https://sketchfab.com/models/d5b3c55e2de6495da0232748c4aad509",
  image: require("#LTD2/Icons/Gargoyle.png"),
  splash: require("#LTD2/splashes/Gargoyle.png"),
  description: "Flying. Takes reduced damage.",

  damage: Damage.Magic,
  atkSpeed: 0.9090908893868948,
  onHitDmg: 17,

  armor: Armor.Arcane,
  hp: 290,
};

export const gargoyle_unit_id: PlayableUnit = {
  ...raw_gargoyle_unit_id,
  price: 40,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "green_devil_unit_id",
  "name": "Green Devil",
  "image": "Icons/GreenDevil.png",
  "splash": "splashes/GreenDevil.png",
  "sketchfabUrl": "https://sketchfab.com/models/8cef6b6f92d84a94a49052d1cd5f3362",
  "description": "No longer flying. Takes further reduced damage.",
  "cost": 120,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Magic",
  "attackTypeDescription": "75% to Arcane<br />105% to Fortified<br />125% to Natural<br />100% to Swift",
  "armorType": "Arcane",
  "armorTypeDescription": "115% from Impact<br />75% from Magic<br />115% from Pierce",
  "hp": 1050,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.7599999904632568,
  "attackSpeedDescriptor": "Fast",
  "mspd": 300,
  "dps": 63,
  "damage": 48,
  "range": 100,
  "totalValue": 160,
  "bounty": 0,
  "tier": "2",
  "legion": "Forsaken",
  "legionType": "forsaken_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.20000000298023224,
  "spawnDelay": 0,
  "effectiveDps": 63.15999984741211,
  "effectiveThreat": 63.15999984741211,
  "movementSize": 0.1875,
  "backswing": 0.17000000178813934,
  "projectileSpeed": 1200,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "9b2330",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Forsaken T2U",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "granite_skin_ability_id",
      "abilityClass": "",
      "name": "Granite Skin",
      "image": "Icons/GraniteSkin.png",
      "description": "Takes 1 + 35% reduced damage",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/GraniteSkin.png' /> Granite Skin<br /><span style='color: #ffcc00'>Caster Buff:</span> Granite Skin<br /><span style='color: #efe1a7'>--- Stats - Damage Reduction (% Reduction):</span> 0.35000<br /><span style='color: #efe1a7'>--- Stats - Damage Reduction (Flat):</span> 1.00000"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "unitType": "gargoyle_unit_id",
      "name": "Gargoyle",
      "image": "Icons/Gargoyle.png"
    }
  ],
  "amountSpawned": 0
}
*/

export const raw_green_devil_unit_id: RawUnit = {
  name: "Green Devil",
  sketchfabUrl: "https://sketchfab.com/models/8cef6b6f92d84a94a49052d1cd5f3362",
  image: require("#LTD2/Icons/GreenDevil.png"),
  splash: require("#LTD2/splashes/GreenDevil.png"),
  description: "No longer flying. Takes further reduced damage.",

  damage: Damage.Magic,
  atkSpeed: 1.3157894901951928,
  onHitDmg: 48,

  armor: Armor.Arcane,
  hp: 1050,
};

export const green_devil_unit_id: PlayableUnit = {
  ...raw_green_devil_unit_id,
  price: 120,
  evolFrom: gargoyle_unit_id,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "gateguard_unit_id",
  "name": "Gateguard",
  "image": "Icons/Gateguard.png",
  "splash": "splashes/Gateguard.png",
  "sketchfabUrl": "https://sketchfab.com/models/cd2d8d30d78c45d693715d046f7a07f8",
  "description": "Summons a Chaos Hound to do its bidding.",
  "cost": 100,
  "mythium": 0,
  "income": 0,
  "supply": 2,
  "attackType": "Impact",
  "attackTypeDescription": "115% to Arcane<br />115% to Fortified<br />90% to Natural<br />80% to Swift",
  "armorType": "Fortified",
  "armorTypeDescription": "115% from Impact<br />105% from Magic<br />80% from Pierce",
  "hp": 620,
  "mana": 14,
  "hpRegen": 0,
  "manaRegen": 0.7200000286102295,
  "attackSpeed": 0.949999988079071,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 28,
  "damage": 27,
  "range": 100,
  "totalValue": 100,
  "bounty": 0,
  "tier": "3",
  "legion": "Forsaken",
  "legionType": "forsaken_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.5,
  "spawnDelay": 0,
  "effectiveDps": 28.420000076293945,
  "effectiveThreat": 28.420000076293945,
  "movementSize": 0.1875,
  "backswing": 0.2199999988079071,
  "projectileSpeed": 1200,
  "mastermindGroups": "Impact, Special1, Special2, Special3, Special4",
  "flags": "Ground, Organic",
  "color": "9b2330",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Forsaken T3",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "hound_ability_id",
      "abilityClass": "",
      "name": "Summon Chaos Hound",
      "image": "Icons/SummonHound.png",
      "description": "Summons a Chaos Hound",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/SummonHound.png' /> Summon Chaos Hound<br /><span style='color: #ffcc00'> Order String:</span> SummonChaosHound<br /><span style='color: #ffcc00'> Mana Cost:</span> 13.00000<br /><span style='color: #ffcc00'> Duration:</span> 180.0000<br /><span style='color: #ffcc00'> Unit:</span> <img class='tooltip-icon' src='hud/img/Icons/ChaosHound.png' />Chaos Hound<br /><span style='color: #ffcc00'>Target Buff:</span> Pet<br /><span style='color: #efe1a7'>--- Damage Per Second (% of Max):</span> 0.0200 (Ability Damage)"
    }
  ],
  "upgradesTo": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "unitType": "harbinger_unit_id",
      "name": "Harbinger",
      "image": "Icons/Harbinger.png"
    }
  ],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_gateguard_unit_id: RawUnit = {
  name: "Gateguard",
  sketchfabUrl: "https://sketchfab.com/models/cd2d8d30d78c45d693715d046f7a07f8",
  image: require("#LTD2/Icons/Gateguard.png"),
  splash: require("#LTD2/splashes/Gateguard.png"),
  description: "Summons a Chaos Hound to do its bidding.",

  damage: Damage.Impact,
  atkSpeed: 1.0526315921561542,
  onHitDmg: 27,

  armor: Armor.Fortified,
  hp: 620,
};

export const gateguard_unit_id: PlayableUnit = {
  ...raw_gateguard_unit_id,
  price: 100,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "harbinger_unit_id",
  "name": "Harbinger",
  "image": "Icons/Harbinger.png",
  "splash": "splashes/Harbinger.png",
  "sketchfabUrl": "https://sketchfab.com/models/b9f8c8e8175348589d1ddbced6f2a125",
  "description": "Now summons a fearsome Cerberus.",
  "cost": 190,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Impact",
  "attackTypeDescription": "115% to Arcane<br />115% to Fortified<br />90% to Natural<br />80% to Swift",
  "armorType": "Fortified",
  "armorTypeDescription": "115% from Impact<br />105% from Magic<br />80% from Pierce",
  "hp": 1770,
  "mana": 28,
  "hpRegen": 0,
  "manaRegen": 1.4500000476837158,
  "attackSpeed": 0.949999988079071,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 81,
  "damage": 77,
  "range": 100,
  "totalValue": 290,
  "bounty": 0,
  "tier": "3",
  "legion": "Forsaken",
  "legionType": "forsaken_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.5,
  "spawnDelay": 0,
  "effectiveDps": 81.05000305175781,
  "effectiveThreat": 81.05000305175781,
  "movementSize": 0.1875,
  "backswing": 0.5199999809265137,
  "projectileSpeed": 1200,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "9b2330",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Forsaken T3U",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "summon_cerberus_ability_id",
      "abilityClass": "",
      "name": "Summon Cerberus",
      "image": "Icons/SummonCerberus.png",
      "description": "Summons a Cerberus",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/SummonCerberus.png' /> Summon Cerberus<br /><span style='color: #ffcc00'> Order String:</span> SummonChaosHound<br /><span style='color: #ffcc00'> Mana Cost:</span> 26.55000<br /><span style='color: #ffcc00'> Duration:</span> 180.0000<br /><span style='color: #ffcc00'> Unit:</span> <img class='tooltip-icon' src='hud/img/Icons/Cerberus.png' />Cerberus<br /><span style='color: #ffcc00'>Target Buff:</span> Pet<br /><span style='color: #efe1a7'>--- Damage Per Second (% of Max):</span> 0.0200 (Ability Damage)"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "unitType": "gateguard_unit_id",
      "name": "Gateguard",
      "image": "Icons/Gateguard.png"
    }
  ],
  "amountSpawned": 0
}
*/

export const raw_harbinger_unit_id: RawUnit = {
  name: "Harbinger",
  sketchfabUrl: "https://sketchfab.com/models/b9f8c8e8175348589d1ddbced6f2a125",
  image: require("#LTD2/Icons/Harbinger.png"),
  splash: require("#LTD2/splashes/Harbinger.png"),
  description: "Now summons a fearsome Cerberus.",

  damage: Damage.Impact,
  atkSpeed: 1.0526315921561542,
  onHitDmg: 77,

  armor: Armor.Fortified,
  hp: 1770,
};

export const harbinger_unit_id: PlayableUnit = {
  ...raw_harbinger_unit_id,
  price: 190,
  evolFrom: gateguard_unit_id,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "butcher_unit_id",
  "name": "Butcher",
  "image": "Icons/Butcher.png",
  "splash": "splashes/Butcher.png",
  "sketchfabUrl": "https://sketchfab.com/models/e56f8cccab0e4e97a14d1f8a28ca7c1a",
  "description": "Aura. Grants adjacently-built units lifesteal and ability vamp",
  "cost": 150,
  "mythium": 0,
  "income": 0,
  "supply": 1,
  "attackType": "Impact",
  "attackTypeDescription": "115% to Arcane<br />115% to Fortified<br />90% to Natural<br />80% to Swift",
  "armorType": "Natural",
  "armorTypeDescription": "90% from Impact<br />125% from Magic<br />85% from Pierce",
  "hp": 1330,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 45,
  "damage": 45,
  "range": 100,
  "totalValue": 150,
  "bounty": 0,
  "tier": "4",
  "legion": "Forsaken",
  "legionType": "forsaken_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.20000000298023224,
  "spawnDelay": 0,
  "effectiveDps": 45,
  "effectiveThreat": 45,
  "movementSize": 0.1875,
  "backswing": 0.4000000059604645,
  "projectileSpeed": 1500,
  "mastermindGroups": "Special1, Special2, Special3, Special4",
  "flags": "Ground, Organic",
  "color": "9b2330",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Forsaken T4",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "leech_oracle_ability_id",
      "abilityClass": "",
      "name": "Leech",
      "image": "Icons/Leech.png",
      "description": "Grants 20% lifesteal and ability vamp to adjacently-built units. Does not stack.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Leech.png' /> Leech<br /><span style='color: #ffcc00'> Order String:</span> Oracle<br /><span style='color: #ffcc00'> Area of Effect:</span> 120<br /><span style='color: #ffcc00'> Targets Considered:</span> Allied, Invulnerable<br /><span style='color: #ffcc00'>Target Buff:</span> Leech<br /><span style='color: #efe1a7'>--- Stats - Lifesteal Bonus (%):</span> 0.20000"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "leech_ability_id",
      "abilityClass": "hidden",
      "name": "Leech",
      "image": "Icons/Leech.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Leech<br /><span style='color: #ffcc00'> Order String:</span> SimpleAura<br /><span style='color: #ffcc00'> Area of Effect:</span> 120<br /><span style='color: #ffcc00'> Targets Considered:</span> Allied<br /><span style='color: #ffcc00'> Targets Excluded:</span> Boss<br /><span style='color: #ffcc00'>Target Buff:</span> Leech (Buff)<br /><span style='color: #efe1a7'>--- Stats - Lifesteal Bonus (%):</span> 0.20000<br /><span style='color: #ffcc00'>Caster Buff:</span> Leech (Caster Buff)"
    }
  ],
  "upgradesTo": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "unitType": "head_chef_unit_id",
      "name": "Head Chef",
      "image": "Icons/HeadChef.png"
    }
  ],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_butcher_unit_id: RawUnit = {
  name: "Butcher",
  sketchfabUrl: "https://sketchfab.com/models/e56f8cccab0e4e97a14d1f8a28ca7c1a",
  image: require("#LTD2/Icons/Butcher.png"),
  splash: require("#LTD2/splashes/Butcher.png"),
  description: "Aura. Grants adjacently-built units lifesteal and ability vamp",

  damage: Damage.Impact,
  atkSpeed: 1,
  onHitDmg: 45,

  armor: Armor.Natural,
  hp: 1330,
};

export const butcher_unit_id: PlayableUnit = {
  ...raw_butcher_unit_id,
  price: 150,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "head_chef_unit_id",
  "name": "Head Chef",
  "image": "Icons/HeadChef.png",
  "splash": "splashes/HeadChef.png",
  "sketchfabUrl": "https://sketchfab.com/models/f37a9f2d6c8e4db689c8d9f9f6ff6f7a",
  "description": "Aura. Grants adjacently-built units lifesteal and ability vamp, plus an active heal.",
  "cost": 265,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Impact",
  "attackTypeDescription": "115% to Arcane<br />115% to Fortified<br />90% to Natural<br />80% to Swift",
  "armorType": "Natural",
  "armorTypeDescription": "90% from Impact<br />125% from Magic<br />85% from Pierce",
  "hp": 3670,
  "mana": 4,
  "hpRegen": 0,
  "manaRegen": 0.4000000059604645,
  "attackSpeed": 1,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 124,
  "damage": 124,
  "range": 100,
  "totalValue": 415,
  "bounty": 0,
  "tier": "4",
  "legion": "Forsaken",
  "legionType": "forsaken_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.20000000298023224,
  "spawnDelay": 0,
  "effectiveDps": 124,
  "effectiveThreat": 124,
  "movementSize": 0.1875,
  "backswing": 0.4000000059604645,
  "projectileSpeed": 1500,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "9b2330",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Forsaken T4U",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "cannibalism_oracle_ability_id",
      "abilityClass": "",
      "name": "Cannibalism",
      "image": "Icons/Cannibalism.png",
      "description": "Grants 30% lifesteal and ability vamp to adjacently-built units. Does not stack.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Cannibalism.png' /> Cannibalism<br /><span style='color: #ffcc00'> Order String:</span> Oracle<br /><span style='color: #ffcc00'> Area of Effect:</span> 120<br /><span style='color: #ffcc00'> Targets Considered:</span> Allied, Invulnerable<br /><span style='color: #ffcc00'>Target Buff:</span> Cannibalism<br /><span style='color: #efe1a7'>--- Stats - Lifesteal Bonus (%):</span> 0.30000"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "cannibalism_ability_id",
      "abilityClass": "hidden",
      "name": "Cannibalism",
      "image": "Icons/Cannibalism.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Cannibalism<br /><span style='color: #ffcc00'> Order String:</span> SimpleAura<br /><span style='color: #ffcc00'> Area of Effect:</span> 120<br /><span style='color: #ffcc00'> Targets Considered:</span> Allied<br /><span style='color: #ffcc00'> Targets Excluded:</span> Boss<br /><span style='color: #ffcc00'>Target Buff:</span> Cannibalism (Buff)<br /><span style='color: #efe1a7'>--- Stats - Lifesteal Bonus (%):</span> 0.30000<br /><span style='color: #ffcc00'>Caster Buff:</span> Cannibalism (Caster Buff)"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "abilityType": "skull_stew_ability_id",
      "abilityClass": "",
      "name": "Skull Stew",
      "image": "Icons/SkullStew.png",
      "description": "Heals a nearby ally for 200 health over 1 seconds",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/SkullStew.png' /> Skull Stew<br /><span style='color: #ffcc00'> Order String:</span> SkullStew<br /><span style='color: #ffcc00'> Projectile - Speed:</span> 2000<br /><span style='color: #ffcc00'> Mana Cost:</span> 3.50000<br /><span style='color: #ffcc00'> Duration:</span> 1.0000<br /><span style='color: #ffcc00'> Duration (Boss):</span> 1.0000<br /><span style='color: #ffcc00'> Targets Considered:</span> Allied, Invulnerable<br /><span style='color: #ffcc00'> Targets Excluded:</span> Boss, CannotBeHealed<br /><span style='color: #ffcc00'>Target Buff:</span> Skull Stew<br /><span style='color: #efe1a7'>--- Max Stacks:</span> 9<br /><span style='color: #efe1a7'>--- Stats - HP Regeneration Bonus (Flat):</span> 200.0000"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 3,
      "unitType": "butcher_unit_id",
      "name": "Butcher",
      "image": "Icons/Butcher.png"
    }
  ],
  "amountSpawned": 0
}
*/

export const raw_head_chef_unit_id: RawUnit = {
  name: "Head Chef",
  sketchfabUrl: "https://sketchfab.com/models/f37a9f2d6c8e4db689c8d9f9f6ff6f7a",
  image: require("#LTD2/Icons/HeadChef.png"),
  splash: require("#LTD2/splashes/HeadChef.png"),
  description: "Aura. Grants adjacently-built units lifesteal and ability vamp, plus an active heal.",

  damage: Damage.Impact,
  atkSpeed: 1,
  onHitDmg: 124,

  armor: Armor.Natural,
  hp: 3670,
};

export const head_chef_unit_id: PlayableUnit = {
  ...raw_head_chef_unit_id,
  price: 265,
  evolFrom: butcher_unit_id,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "nightmare_unit_id",
  "name": "Nightmare",
  "image": "Icons/Nightmare.png",
  "splash": "splashes/Nightmare.png",
  "sketchfabUrl": "https://sketchfab.com/models/20a8cdba3e9e4215a0e1229b59da0b4f",
  "description": "Deals tons of damage, but lacks defense.",
  "cost": 185,
  "mythium": 0,
  "income": 0,
  "supply": 1,
  "attackType": "Pierce",
  "attackTypeDescription": "115% to Arcane<br />80% to Fortified<br />85% to Natural<br />120% to Swift",
  "armorType": "Swift",
  "armorTypeDescription": "80% from Impact<br />100% from Magic<br />120% from Pierce",
  "hp": 1100,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.5699999928474426,
  "attackSpeedDescriptor": "Fast",
  "mspd": 300,
  "dps": 123,
  "damage": 70,
  "range": 100,
  "totalValue": 185,
  "bounty": 0,
  "tier": "5",
  "legion": "Forsaken",
  "legionType": "forsaken_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.800000011920929,
  "spawnDelay": 0,
  "effectiveDps": 122.80999755859375,
  "effectiveThreat": 122.80999755859375,
  "movementSize": 0.1875,
  "backswing": 0.17000000178813934,
  "projectileSpeed": 1200,
  "mastermindGroups": "Pierce, Special1, Special2, Special3, Special4",
  "flags": "Ground, Organic",
  "color": "9b2330",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Forsaken T5",
  "suffix": "",
  "abilities": [],
  "upgradesTo": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "unitType": "doppelganger_unit_id",
      "name": "Doppelganger",
      "image": "Icons/Doppelganger.png"
    }
  ],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_nightmare_unit_id: RawUnit = {
  name: "Nightmare",
  sketchfabUrl: "https://sketchfab.com/models/20a8cdba3e9e4215a0e1229b59da0b4f",
  image: require("#LTD2/Icons/Nightmare.png"),
  splash: require("#LTD2/splashes/Nightmare.png"),
  description: "Deals tons of damage, but lacks defense.",

  damage: Damage.Pierce,
  atkSpeed: 1.7543859869269236,
  onHitDmg: 70,

  armor: Armor.Swift,
  hp: 1100,
};

export const nightmare_unit_id: PlayableUnit = {
  ...raw_nightmare_unit_id,
  price: 185,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "doppelganger_unit_id",
  "name": "Doppelganger",
  "image": "Icons/Doppelganger.png",
  "splash": "splashes/Doppelganger.png",
  "sketchfabUrl": "https://sketchfab.com/models/caddff9d134a450995f3b2010dcb1c54",
  "description": "Every time it attacks, its reaper's scythe grows stronger.",
  "cost": 340,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Pierce",
  "attackTypeDescription": "115% to Arcane<br />80% to Fortified<br />85% to Natural<br />120% to Swift",
  "armorType": "Swift",
  "armorTypeDescription": "80% from Impact<br />100% from Magic<br />120% from Pierce",
  "hp": 3300,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.5199999809265137,
  "attackSpeedDescriptor": "Fast",
  "mspd": 300,
  "dps": 250,
  "damage": 130,
  "range": 100,
  "totalValue": 525,
  "bounty": 0,
  "tier": "5",
  "legion": "Forsaken",
  "legionType": "forsaken_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.800000011920929,
  "spawnDelay": 0,
  "effectiveDps": 250,
  "effectiveThreat": 250,
  "movementSize": 0.1875,
  "backswing": 0.15000000596046448,
  "projectileSpeed": 1200,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "9b2330",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Forsaken T5U",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "shadow_walker_ability_id",
      "abilityClass": "",
      "name": "Fatality",
      "image": "Icons/Fatality.png",
      "description": "Each attack increases attack damage by 2",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Fatality.png' /> Fatality<br /><span style='color: #ffcc00'> Order String:</span> Fatality<br /><span style='color: #ffcc00'> Custom Value 1:</span> 2.00000<br /><span style='color: #ffcc00'>Target Buff:</span> Fatality"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "unitType": "nightmare_unit_id",
      "name": "Nightmare",
      "image": "Icons/Nightmare.png"
    }
  ],
  "amountSpawned": 0
}
*/

export const raw_doppelganger_unit_id: RawUnit = {
  name: "Doppelganger",
  sketchfabUrl: "https://sketchfab.com/models/caddff9d134a450995f3b2010dcb1c54",
  image: require("#LTD2/Icons/Doppelganger.png"),
  splash: require("#LTD2/splashes/Doppelganger.png"),
  description: "Every time it attacks, its reaper's scythe grows stronger.",

  damage: Damage.Pierce,
  atkSpeed: 1.9230769936149668,
  onHitDmg: 130,

  armor: Armor.Swift,
  hp: 3300,
};

export const doppelganger_unit_id: PlayableUnit = {
  ...raw_doppelganger_unit_id,
  price: 340,
  evolFrom: nightmare_unit_id,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "lord_of_death_unit_id",
  "name": "Lord Of Death",
  "image": "Icons/LordOfDeath.png",
  "splash": "splashes/LordOfDeath.png",
  "sketchfabUrl": "https://sketchfab.com/models/2050523e127c4c6192498456f4d90480",
  "description": "Conjures a dragon that devours its foes.",
  "cost": 265,
  "mythium": 0,
  "income": 0,
  "supply": 2,
  "attackType": "Magic",
  "attackTypeDescription": "75% to Arcane<br />105% to Fortified<br />125% to Natural<br />100% to Swift",
  "armorType": "Arcane",
  "armorTypeDescription": "115% from Impact<br />75% from Magic<br />115% from Pierce",
  "hp": 1240,
  "mana": 40,
  "hpRegen": 0,
  "manaRegen": 1.25,
  "attackSpeed": 1,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 63,
  "damage": 63,
  "range": 500,
  "totalValue": 265,
  "bounty": 0,
  "tier": "6",
  "legion": "Forsaken",
  "legionType": "forsaken_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.8999999761581421,
  "spawnDelay": 0,
  "effectiveDps": 63,
  "effectiveThreat": 63,
  "movementSize": 0.1875,
  "backswing": 0.20000000298023224,
  "projectileSpeed": 1750,
  "mastermindGroups": "Magic, Special1, Special2",
  "flags": "Ground, Organic",
  "color": "9b2330",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Forsaken T6",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "raise_juggernaut_ability_id",
      "abilityClass": "",
      "name": "Conjure Dragon",
      "image": "Icons/ConjureDragon.png",
      "description": "Summons an Undead Dragon (magic/arcane)",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/ConjureDragon.png' /> Conjure Dragon<br /><span style='color: #ffcc00'> Order String:</span> ConjureDragon<br /><span style='color: #ffcc00'> Mana Cost:</span> 38.50000<br /><span style='color: #ffcc00'> Duration:</span> 180.0000<br /><span style='color: #ffcc00'> Unit:</span> <img class='tooltip-icon' src='hud/img/Icons/UndeadDragon.png' />Undead Dragon<br /><span style='color: #ffcc00'>Target Buff:</span> Pet<br /><span style='color: #efe1a7'>--- Damage Per Second (% of Max):</span> 0.0200 (Ability Damage)"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "intellect_ability_id",
      "abilityClass": "",
      "name": "Intellect",
      "image": "Icons/Intellect.png",
      "description": "Killing a unit restores 15% of max mana",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Intellect.png' /> Intellect<br /><span style='color: #ffcc00'> Order String:</span> Intellect<br /><span style='color: #ffcc00'> Percentage:</span> 0.15000<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy"
    }
  ],
  "upgradesTo": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "unitType": "hades_unit_id",
      "name": "Hades",
      "image": "Icons/Hades.png"
    }
  ],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_lord_of_death_unit_id: RawUnit = {
  name: "Lord Of Death",
  sketchfabUrl: "https://sketchfab.com/models/2050523e127c4c6192498456f4d90480",
  image: require("#LTD2/Icons/LordOfDeath.png"),
  splash: require("#LTD2/splashes/LordOfDeath.png"),
  description: "Conjures a dragon that devours its foes.",

  damage: Damage.Magic,
  atkSpeed: 1,
  onHitDmg: 63,

  armor: Armor.Arcane,
  hp: 1240,
};

export const lord_of_death_unit_id: PlayableUnit = {
  ...raw_lord_of_death_unit_id,
  price: 265,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "hades_unit_id",
  "name": "Hades",
  "image": "Icons/Hades.png",
  "splash": "splashes/Hades.png",
  "sketchfabUrl": "https://sketchfab.com/models/a9d9468f17104aac8534965d3f9bf83a",
  "description": "Summons a dragon of unrivaled power. Gains mana when enemy units die.",
  "cost": 410,
  "mythium": 0,
  "income": 0,
  "supply": 1,
  "attackType": "Magic",
  "attackTypeDescription": "75% to Arcane<br />105% to Fortified<br />125% to Natural<br />100% to Swift",
  "armorType": "Arcane",
  "armorTypeDescription": "115% from Impact<br />75% from Magic<br />115% from Pierce",
  "hp": 3100,
  "mana": 70,
  "hpRegen": 0,
  "manaRegen": 1.2000000476837158,
  "attackSpeed": 1,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 158,
  "damage": 158,
  "range": 500,
  "totalValue": 675,
  "bounty": 0,
  "tier": "6",
  "legion": "Forsaken",
  "legionType": "forsaken_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.8999999761581421,
  "spawnDelay": 0,
  "effectiveDps": 158,
  "effectiveThreat": 158,
  "movementSize": 0.1875,
  "backswing": 0.20000000298023224,
  "projectileSpeed": 1750,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "9b2330",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Forsaken T6U",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "conjure_thanatos_ability_id",
      "abilityClass": "",
      "name": "Conjure Thanatos",
      "image": "Icons/ConjureThanatos.png",
      "description": "Summons Thanatos (Magic/Arcane)",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/ConjureThanatos.png' /> Conjure Thanatos<br /><span style='color: #ffcc00'> Order String:</span> ConjureDragon<br /><span style='color: #ffcc00'> Mana Cost:</span> 68.00000<br /><span style='color: #ffcc00'> Duration:</span> 180.0000<br /><span style='color: #ffcc00'> Unit:</span> <img class='tooltip-icon' src='hud/img/Icons/Thanatos.png' />Thanatos<br /><span style='color: #ffcc00'>Target Buff:</span> Pet<br /><span style='color: #efe1a7'>--- Damage Per Second (% of Max):</span> 0.0200 (Ability Damage)"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "necromancy_ability_id",
      "abilityClass": "",
      "name": "Necromancy",
      "image": "Icons/Necromancy.png",
      "description": "Restores 3 mana when a nearby enemy dies, excluding Froggos.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Necromancy.png' /> Necromancy<br /><span style='color: #ffcc00'> Order String:</span> Necromancy<br /><span style='color: #ffcc00'> Mana Cost:</span> 3.00000<br /><span style='color: #ffcc00'> Area of Effect:</span> 800<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy<br /><span style='color: #ffcc00'> Targets Excluded:</span> Undead"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "abilityType": "intellect_ability_id",
      "abilityClass": "",
      "name": "Intellect",
      "image": "Icons/Intellect.png",
      "description": "Killing a unit restores 15% of max mana",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Intellect.png' /> Intellect<br /><span style='color: #ffcc00'> Order String:</span> Intellect<br /><span style='color: #ffcc00'> Percentage:</span> 0.15000<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 3,
      "unitType": "lord_of_death_unit_id",
      "name": "Lord Of Death",
      "image": "Icons/LordOfDeath.png"
    }
  ],
  "amountSpawned": 0
}
*/

export const raw_hades_unit_id: RawUnit = {
  name: "Hades",
  sketchfabUrl: "https://sketchfab.com/models/a9d9468f17104aac8534965d3f9bf83a",
  image: require("#LTD2/Icons/Hades.png"),
  splash: require("#LTD2/splashes/Hades.png"),
  description: "Summons a dragon of unrivaled power. Gains mana when enemy units die.",

  damage: Damage.Magic,
  atkSpeed: 1,
  onHitDmg: 158,

  armor: Armor.Arcane,
  hp: 3100,
};

export const hades_unit_id: PlayableUnit = {
  ...raw_hades_unit_id,
  price: 410,
  evolFrom: lord_of_death_unit_id,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "buzz_unit_id",
  "name": "Buzz",
  "image": "Icons/Buzz.png",
  "splash": "splashes/Buzz.png",
  "sketchfabUrl": "https://sketchfab.com/models/973036d942304ca68ebc16f627e79d50",
  "description": "Flying. Poisons enemies when it dies.",
  "cost": 20,
  "mythium": 0,
  "income": 0,
  "supply": 1,
  "attackType": "Pierce",
  "attackTypeDescription": "115% to Arcane<br />80% to Fortified<br />85% to Natural<br />120% to Swift",
  "armorType": "Natural",
  "armorTypeDescription": "90% from Impact<br />125% from Magic<br />85% from Pierce",
  "hp": 150,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.9900000095367432,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 9,
  "damage": 9,
  "range": 100,
  "totalValue": 20,
  "bounty": 0,
  "tier": "1",
  "legion": "Grove",
  "legionType": "grove_legion_id",
  "movementType": "Air",
  "spawnBias": 0.4000000059604645,
  "spawnDelay": 0,
  "effectiveDps": 9.09000015258789,
  "effectiveThreat": 9.09000015258789,
  "movementSize": 0.1875,
  "backswing": 0.3499999940395355,
  "projectileSpeed": 1200,
  "mastermindGroups": "Pierce, Special1",
  "flags": "Air, Organic",
  "color": "98b605",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Grove T1",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "anaphylaxis_ability_id",
      "abilityClass": "",
      "name": "Anaphylaxis",
      "image": "Icons/Anaphylaxis.png",
      "description": "Deals 50 magic damage over 2 seconds to its killer. Ability damage.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Anaphylaxis.png' /> Anaphylaxis<br /><span style='color: #ffcc00'> Order String:</span> Anaphylaxis<br /><span style='color: #ffcc00'> Attack Type:</span> <img class='tooltip-icon' src='hud/img/icons/Magic.png' /> Magic<br /><span style='color: #ffcc00'> Duration:</span> 2.0000<br /><span style='color: #ffcc00'> Duration (Boss):</span> 2.0000<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy<br /><span style='color: #ffcc00'>Target Buff:</span> Anaphylaxis<br /><span style='color: #efe1a7'>--- Attack Type:</span> <img class='tooltip-icon' src='hud/img/icons/Magic.png' />Magic<br /><span style='color: #efe1a7'>--- Damage Per Second:</span> 25.00000 (Ability Damage)<br /><span style='color: #efe1a7'>--- Max Stacks:</span> 30"
    }
  ],
  "upgradesTo": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "unitType": "consort_unit_id",
      "name": "Consort",
      "image": "Icons/Consort.png"
    }
  ],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_buzz_unit_id: RawUnit = {
  name: "Buzz",
  sketchfabUrl: "https://sketchfab.com/models/973036d942304ca68ebc16f627e79d50",
  image: require("#LTD2/Icons/Buzz.png"),
  splash: require("#LTD2/splashes/Buzz.png"),
  description: "Flying. Poisons enemies when it dies.",

  damage: Damage.Pierce,
  atkSpeed: 1.0101010003706326,
  onHitDmg: 9,

  armor: Armor.Natural,
  hp: 150,
};

export const buzz_unit_id: PlayableUnit = {
  ...raw_buzz_unit_id,
  price: 20,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "consort_unit_id",
  "name": "Consort",
  "image": "Icons/Consort.png",
  "splash": "splashes/Consort.png",
  "sketchfabUrl": "https://sketchfab.com/models/53fc534157e9490ba594245d5c0b0dea",
  "description": "Flying. Its claws and carapace make it much more formidable. It also posseses a more potent poison.",
  "cost": 85,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Pierce",
  "attackTypeDescription": "115% to Arcane<br />80% to Fortified<br />85% to Natural<br />120% to Swift",
  "armorType": "Natural",
  "armorTypeDescription": "90% from Impact<br />125% from Magic<br />85% from Pierce",
  "hp": 900,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.9900000095367432,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 51,
  "damage": 50,
  "range": 100,
  "totalValue": 105,
  "bounty": 0,
  "tier": "1",
  "legion": "Grove",
  "legionType": "grove_legion_id",
  "movementType": "Air",
  "spawnBias": 0.4000000059604645,
  "spawnDelay": 0,
  "effectiveDps": 50.5099983215332,
  "effectiveThreat": 50.5099983215332,
  "movementSize": 0.1875,
  "backswing": 0.30000001192092896,
  "projectileSpeed": 1200,
  "mastermindGroups": "None",
  "flags": "Air, Organic",
  "color": "98b605",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Grove T1U",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "paraphylaxis_ability_id",
      "abilityClass": "",
      "name": "Paraphylaxis",
      "image": "Icons/Paraphylaxis.png",
      "description": "Deals 280 magic damage over 2 seconds to its killer. Ability damage.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Paraphylaxis.png' /> Paraphylaxis<br /><span style='color: #ffcc00'> Order String:</span> Anaphylaxis<br /><span style='color: #ffcc00'> Attack Type:</span> <img class='tooltip-icon' src='hud/img/icons/Magic.png' /> Magic<br /><span style='color: #ffcc00'> Duration:</span> 2.0000<br /><span style='color: #ffcc00'> Duration (Boss):</span> 2.0000<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy<br /><span style='color: #ffcc00'>Target Buff:</span> Anaphylaxis<br /><span style='color: #efe1a7'>--- Attack Type:</span> <img class='tooltip-icon' src='hud/img/icons/Magic.png' />Magic<br /><span style='color: #efe1a7'>--- Damage Per Second:</span> 140.00000 (Ability Damage)<br /><span style='color: #efe1a7'>--- Max Stacks:</span> 30"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "unitType": "buzz_unit_id",
      "name": "Buzz",
      "image": "Icons/Buzz.png"
    }
  ],
  "amountSpawned": 0
}
*/

export const raw_consort_unit_id: RawUnit = {
  name: "Consort",
  sketchfabUrl: "https://sketchfab.com/models/53fc534157e9490ba594245d5c0b0dea",
  image: require("#LTD2/Icons/Consort.png"),
  splash: require("#LTD2/splashes/Consort.png"),
  description: "Flying. Its claws and carapace make it much more formidable. It also posseses a more potent poison.",

  damage: Damage.Pierce,
  atkSpeed: 1.0101010003706326,
  onHitDmg: 50,

  armor: Armor.Natural,
  hp: 900,
};

export const consort_unit_id: PlayableUnit = {
  ...raw_consort_unit_id,
  price: 85,
  evolFrom: buzz_unit_id,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "ranger_unit_id",
  "name": "Ranger",
  "image": "Icons/Ranger.png",
  "splash": "splashes/Ranger.png",
  "sketchfabUrl": "https://sketchfab.com/models/5c322c3e6d2749f2932f2db11d9eee9e",
  "description": "Long-ranged. Deals extra damage to bosses.",
  "cost": 55,
  "mythium": 0,
  "income": 0,
  "supply": 1,
  "attackType": "Pierce",
  "attackTypeDescription": "115% to Arcane<br />80% to Fortified<br />85% to Natural<br />120% to Swift",
  "armorType": "Swift",
  "armorTypeDescription": "80% from Impact<br />100% from Magic<br />120% from Pierce",
  "hp": 330,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.8700000047683716,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 31,
  "damage": 27,
  "range": 700,
  "totalValue": 55,
  "bounty": 0,
  "tier": "2",
  "legion": "Grove",
  "legionType": "grove_legion_id",
  "movementType": "Ground",
  "spawnBias": 1,
  "spawnDelay": 0,
  "effectiveDps": 31.030000686645508,
  "effectiveThreat": 31.030000686645508,
  "movementSize": 0.1875,
  "backswing": 0.2199999988079071,
  "projectileSpeed": 3000,
  "mastermindGroups": "Pierce, Special1, Special2, Special3, Special4",
  "flags": "Ground, Organic",
  "color": "98b605",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Grove T2",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "accuracy_ability_id",
      "abilityClass": "",
      "name": "Accuracy",
      "image": "Icons/Accuracy.png",
      "description": "Deals 5 bonus pure damage to bosses. Ability damage.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Accuracy.png' /> Accuracy<br /><span style='color: #ffcc00'> Order String:</span> Bash<br /><span style='color: #ffcc00'> Damage Base:</span> 5.00000 (Ability Damage)<br /><span style='color: #ffcc00'> Percentage:</span> 1.00000<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy<br /><span style='color: #ffcc00'> Targets Excluded:</span> NonBoss"
    }
  ],
  "upgradesTo": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "unitType": "daphne_unit_id",
      "name": "Daphne",
      "image": "Icons/Daphne.png"
    }
  ],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_ranger_unit_id: RawUnit = {
  name: "Ranger",
  sketchfabUrl: "https://sketchfab.com/models/5c322c3e6d2749f2932f2db11d9eee9e",
  image: require("#LTD2/Icons/Ranger.png"),
  splash: require("#LTD2/splashes/Ranger.png"),
  description: "Long-ranged. Deals extra damage to bosses.",

  damage: Damage.Pierce,
  atkSpeed: 1.1494252810564518,
  onHitDmg: 27,

  armor: Armor.Swift,
  hp: 330,
};

export const ranger_unit_id: PlayableUnit = {
  ...raw_ranger_unit_id,
  price: 55,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "daphne_unit_id",
  "name": "Daphne",
  "image": "Icons/Daphne.png",
  "splash": "splashes/Daphne.png",
  "sketchfabUrl": "https://sketchfab.com/models/12ce9bed4c164d568e36a5a693de95f5",
  "description": "Synergizes with many units and fast attackers.",
  "cost": 125,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Pierce",
  "attackTypeDescription": "115% to Arcane<br />80% to Fortified<br />85% to Natural<br />120% to Swift",
  "armorType": "Swift",
  "armorTypeDescription": "80% from Impact<br />100% from Magic<br />120% from Pierce",
  "hp": 1050,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.800000011920929,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 94,
  "damage": 75,
  "range": 700,
  "totalValue": 180,
  "bounty": 0,
  "tier": "2",
  "legion": "Grove",
  "legionType": "grove_legion_id",
  "movementType": "Ground",
  "spawnBias": 1,
  "spawnDelay": 0,
  "effectiveDps": 93.75,
  "effectiveThreat": 93.75,
  "movementSize": 0.1875,
  "backswing": 0.30000001192092896,
  "projectileSpeed": 3000,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "98b605",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Grove T2U",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "precision_ability_id",
      "abilityClass": "",
      "name": "Precision",
      "image": "Icons/Precision.png",
      "description": "Deals 15 bonus pure damage to bosses. Ability damage.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Precision.png' /> Precision<br /><span style='color: #ffcc00'> Order String:</span> Bash<br /><span style='color: #ffcc00'> Damage Base:</span> 15.00000 (Ability Damage)<br /><span style='color: #ffcc00'> Percentage:</span> 1.00000<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy<br /><span style='color: #ffcc00'> Targets Excluded:</span> NonBoss"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "mark_target_ability_id",
      "abilityClass": "",
      "name": "Mark Target",
      "image": "Icons/MarkTarget.png",
      "description": "Each attack amplifies all damage taken by 1 (0.5 to bosses), stacking up to 10 times.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/MarkTarget.png' /> Mark Target<br /><span style='color: #ffcc00'>Damage Type:</span> Ability Damage<br /><span style='color: #ffcc00'> Order String:</span> BounceAttack<br /><span style='color: #ffcc00'> Duration:</span> 3.000<br /><span style='color: #ffcc00'> Duration (Boss):</span> 3.000<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy<br /><span style='color: #ffcc00'> Targets Excluded:</span> Boss<br /><span style='color: #ffcc00'>Target Buff:</span> Mark Target<br /><span style='color: #efe1a7'>--- Max Stacks:</span> 10<br /><span style='color: #efe1a7'>--- Stats - Damage Reduction (Flat):</span> -1.0000"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "abilityType": "mark_target_boss_ability_id",
      "abilityClass": "hidden",
      "name": "Mark Target (Boss)",
      "image": "Icons/MarkTarget.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Mark Target (Boss)<br /><span style='color: #ffcc00'>Damage Type:</span> Ability Damage<br /><span style='color: #ffcc00'> Order String:</span> BounceAttack<br /><span style='color: #ffcc00'> Duration:</span> 3.000<br /><span style='color: #ffcc00'> Duration (Boss):</span> 3.000<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy<br /><span style='color: #ffcc00'> Targets Excluded:</span> NonBoss<br /><span style='color: #ffcc00'>Target Buff:</span> Mark Target (Boss Buff)<br /><span style='color: #efe1a7'>--- Max Stacks:</span> 10<br /><span style='color: #efe1a7'>--- Stats - Damage Reduction (Flat):</span> -0.50000"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 3,
      "unitType": "ranger_unit_id",
      "name": "Ranger",
      "image": "Icons/Ranger.png"
    }
  ],
  "amountSpawned": 0
}
*/

export const raw_daphne_unit_id: RawUnit = {
  name: "Daphne",
  sketchfabUrl: "https://sketchfab.com/models/12ce9bed4c164d568e36a5a693de95f5",
  image: require("#LTD2/Icons/Daphne.png"),
  splash: require("#LTD2/splashes/Daphne.png"),
  description: "Synergizes with many units and fast attackers.",

  damage: Damage.Pierce,
  atkSpeed: 1.2499999813735487,
  onHitDmg: 75,

  armor: Armor.Swift,
  hp: 1050,
};

export const daphne_unit_id: PlayableUnit = {
  ...raw_daphne_unit_id,
  price: 125,
  evolFrom: ranger_unit_id,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "wileshroom_unit_id",
  "name": "Wileshroom",
  "image": "Icons/Wileshroom.png",
  "splash": "splashes/Wileshroom.png",
  "sketchfabUrl": "https://sketchfab.com/models/8118ff0c5e9c40c299e85166d3471ca1",
  "description": "Starts with half health, but rapidly regenerates.",
  "cost": 95,
  "mythium": 0,
  "income": 0,
  "supply": 1,
  "attackType": "Impact",
  "attackTypeDescription": "115% to Arcane<br />115% to Fortified<br />90% to Natural<br />80% to Swift",
  "armorType": "Arcane",
  "armorTypeDescription": "115% from Impact<br />75% from Magic<br />115% from Pierce",
  "hp": 1740,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.1699999570846558,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 45,
  "damage": 53,
  "range": 100,
  "totalValue": 95,
  "bounty": 0,
  "tier": "3",
  "legion": "Grove",
  "legionType": "grove_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.30000001192092896,
  "spawnDelay": 0,
  "effectiveDps": 45.29999923706055,
  "effectiveThreat": 45.29999923706055,
  "movementSize": 0.1875,
  "backswing": 0.4000000059604645,
  "projectileSpeed": 1200,
  "mastermindGroups": "Arcane, Impact",
  "flags": "Ground, Organic",
  "color": "98b605",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Grove T3",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "cell_regrowth_ability_id",
      "abilityClass": "",
      "name": "Cell Regrowth",
      "image": "Icons/CellRegrowth.png",
      "description": "Starts with 50% health, but regenerates 0.5% of missing health per second",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/CellRegrowth.png' /> Cell Regrowth<br /><span style='color: #ffcc00'> Order String:</span> CellRegrowth<br /><span style='color: #ffcc00'> Percentage:</span> 0.50000<br /><span style='color: #ffcc00'> Targets Excluded:</span> Building<br /><span style='color: #ffcc00'>Target Buff:</span> Cell Regrowth<br /><span style='color: #efe1a7'>--- Stats - HP Regeneration Bonus (% of Missing HP):</span> 0.00500"
    }
  ],
  "upgradesTo": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "unitType": "canopie_unit_id",
      "name": "Canopie",
      "image": "Icons/Canopie.png"
    }
  ],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_wileshroom_unit_id: RawUnit = {
  name: "Wileshroom",
  sketchfabUrl: "https://sketchfab.com/models/8118ff0c5e9c40c299e85166d3471ca1",
  image: require("#LTD2/Icons/Wileshroom.png"),
  splash: require("#LTD2/splashes/Wileshroom.png"),
  description: "Starts with half health, but rapidly regenerates.",

  damage: Damage.Impact,
  atkSpeed: 0.8547008860510964,
  onHitDmg: 53,

  armor: Armor.Arcane,
  hp: 1740,
};

export const wileshroom_unit_id: PlayableUnit = {
  ...raw_wileshroom_unit_id,
  price: 95,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "canopie_unit_id",
  "name": "Canopie",
  "image": "Icons/Canopie.png",
  "splash": "splashes/Canopie.png",
  "sketchfabUrl": "https://sketchfab.com/models/f2eb15ae3b3a47869c937bd54ffba77d",
  "description": "Flying. Now a fully grown adult, it's tankier and hits harder.",
  "cost": 180,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Impact",
  "attackTypeDescription": "115% to Arcane<br />115% to Fortified<br />90% to Natural<br />80% to Swift",
  "armorType": "Arcane",
  "armorTypeDescription": "115% from Impact<br />75% from Magic<br />115% from Pierce",
  "hp": 4800,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.149999976158142,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 130,
  "damage": 150,
  "range": 100,
  "totalValue": 275,
  "bounty": 0,
  "tier": "3",
  "legion": "Grove",
  "legionType": "grove_legion_id",
  "movementType": "Air",
  "spawnBias": 0.30000001192092896,
  "spawnDelay": 0,
  "effectiveDps": 130.42999267578125,
  "effectiveThreat": 130.42999267578125,
  "movementSize": 0.1875,
  "backswing": 0.4000000059604645,
  "projectileSpeed": 1200,
  "mastermindGroups": "None",
  "flags": "Air, Organic",
  "color": "98b605",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Grove T3U",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "cell_regrowth_canopie_ability_id",
      "abilityClass": "",
      "name": "Cell Regrowth",
      "image": "Icons/CellRegrowth.png",
      "description": "Starts with 50% health, but regenerates 0.5% of missing health per second",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/CellRegrowth.png' /> Cell Regrowth<br /><span style='color: #ffcc00'> Order String:</span> CellRegrowth<br /><span style='color: #ffcc00'> Percentage:</span> 0.50000<br /><span style='color: #ffcc00'> Targets Excluded:</span> Building<br /><span style='color: #ffcc00'>Target Buff:</span> Cell Regrowth<br /><span style='color: #efe1a7'>--- Stats - HP Regeneration Bonus (% of Missing HP):</span> 0.00500"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "unitType": "wileshroom_unit_id",
      "name": "Wileshroom",
      "image": "Icons/Wileshroom.png"
    }
  ],
  "amountSpawned": 0
}
*/

export const raw_canopie_unit_id: RawUnit = {
  name: "Canopie",
  sketchfabUrl: "https://sketchfab.com/models/f2eb15ae3b3a47869c937bd54ffba77d",
  image: require("#LTD2/Icons/Canopie.png"),
  splash: require("#LTD2/splashes/Canopie.png"),
  description: "Flying. Now a fully grown adult, it's tankier and hits harder.",

  damage: Damage.Impact,
  atkSpeed: 0.8695652354191746,
  onHitDmg: 150,

  armor: Armor.Arcane,
  hp: 4800,
};

export const canopie_unit_id: PlayableUnit = {
  ...raw_canopie_unit_id,
  price: 180,
  evolFrom: wileshroom_unit_id,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "honeyflower_unit_id",
  "name": "Honeyflower",
  "image": "Icons/Honeyflower.png",
  "splash": "splashes/Honeyflower.png",
  "sketchfabUrl": "https://sketchfab.com/models/0ab57438b4f646b0ab7ff93ef49a915c",
  "description": "Passively damages all nearby enemies with its sickly sweet scent.",
  "cost": 130,
  "mythium": 0,
  "income": 0,
  "supply": 1,
  "attackType": "Magic",
  "attackTypeDescription": "75% to Arcane<br />105% to Fortified<br />125% to Natural<br />100% to Swift",
  "armorType": "Natural",
  "armorTypeDescription": "90% from Impact<br />125% from Magic<br />85% from Pierce",
  "hp": 1400,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 1,
  "damage": 1,
  "range": 100,
  "totalValue": 130,
  "bounty": 0,
  "tier": "4",
  "legion": "Grove",
  "legionType": "grove_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.30000001192092896,
  "spawnDelay": 0,
  "effectiveDps": 1,
  "effectiveThreat": 1,
  "movementSize": 0.1875,
  "backswing": 0.3499999940395355,
  "projectileSpeed": 1000,
  "mastermindGroups": "Magic, Special1, Special2, Special3, Special4",
  "flags": "Ground, Organic",
  "color": "98b605",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Grove T4",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "fragrance_ability_id",
      "abilityClass": "",
      "name": "Fragrance",
      "image": "Icons/Fragrance.png",
      "description": "Deals 10 + 0.03% max health magic damage per second to nearby enemies. Stacks with itself. Ability damage. ",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Fragrance.png' /> Fragrance<br /><span style='color: #ffcc00'> Order String:</span> SimpleAura<br /><span style='color: #ffcc00'> Area of Effect:</span> 250<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy<br /><span style='color: #ffcc00'> Targets Excluded:</span> Boss<br /><span style='color: #ffcc00'>Target Buff:</span> Fragrance<br /><span style='color: #efe1a7'>--- Attack Type:</span> <img class='tooltip-icon' src='hud/img/icons/Magic.png' />Magic<br /><span style='color: #efe1a7'>--- Damage Per Second:</span> 10.00000 (Ability Damage)<br /><span style='color: #efe1a7'>--- Damage Per Second (% of Max):</span> 0.00030 (Ability Damage)<br /><span style='color: #efe1a7'>--- Max Stacks:</span> 30<br /><span style='color: #ffcc00'>Caster Buff:</span> Fragrance<br /><span style='color: #efe1a7'>--- Attack Type:</span> <img class='tooltip-icon' src='hud/img/icons/Magic.png' />Magic"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "fragrance_boss_ability_id",
      "abilityClass": "hidden",
      "name": "Fragrance (Boss)",
      "image": "Icons/Fragrance.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Fragrance (Boss)<br /><span style='color: #ffcc00'> Order String:</span> SimpleAura<br /><span style='color: #ffcc00'> Area of Effect:</span> 250<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy<br /><span style='color: #ffcc00'> Targets Excluded:</span> NonBoss<br /><span style='color: #ffcc00'>Target Buff:</span> Fragrance (Boss Buff)<br /><span style='color: #efe1a7'>--- Attack Type:</span> <img class='tooltip-icon' src='hud/img/icons/Magic.png' />Magic<br /><span style='color: #efe1a7'>--- Damage Per Second:</span> 10.00000 (Ability Damage)<br /><span style='color: #efe1a7'>--- Damage Per Second (% of Max):</span> 0.00030 (Ability Damage)<br /><span style='color: #efe1a7'>--- Max Stacks:</span> 30"
    }
  ],
  "upgradesTo": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "unitType": "deathcap_unit_id",
      "name": "Deathcap",
      "image": "Icons/Deathcap.png"
    }
  ],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_honeyflower_unit_id: RawUnit = {
  name: "Honeyflower",
  sketchfabUrl: "https://sketchfab.com/models/0ab57438b4f646b0ab7ff93ef49a915c",
  image: require("#LTD2/Icons/Honeyflower.png"),
  splash: require("#LTD2/splashes/Honeyflower.png"),
  description: "Passively damages all nearby enemies with its sickly sweet scent.",

  damage: Damage.Magic,
  atkSpeed: 1,
  onHitDmg: 1,

  armor: Armor.Natural,
  hp: 1400,
};

export const honeyflower_unit_id: PlayableUnit = {
  ...raw_honeyflower_unit_id,
  price: 130,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "deathcap_unit_id",
  "name": "Deathcap",
  "image": "Icons/Deathcap.png",
  "splash": "splashes/Deathcap.png",
  "sketchfabUrl": "https://sketchfab.com/models/96c7c60f48f14deaa801a2585d8bf5b9",
  "description": "Now even more toxic.",
  "cost": 265,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Magic",
  "attackTypeDescription": "75% to Arcane<br />105% to Fortified<br />125% to Natural<br />100% to Swift",
  "armorType": "Natural",
  "armorTypeDescription": "90% from Impact<br />125% from Magic<br />85% from Pierce",
  "hp": 4250,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.0499999523162842,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 1,
  "damage": 1,
  "range": 100,
  "totalValue": 395,
  "bounty": 0,
  "tier": "4",
  "legion": "Grove",
  "legionType": "grove_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.30000001192092896,
  "spawnDelay": 0,
  "effectiveDps": 0.949999988079071,
  "effectiveThreat": 0.949999988079071,
  "movementSize": 0.1875,
  "backswing": 0.30000001192092896,
  "projectileSpeed": 1200,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "98b605",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Grove T4U",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "noxious_scent_ability_id",
      "abilityClass": "",
      "name": "Noxious Scent",
      "image": "Icons/NoxiousScent.png",
      "description": "Deals 30 + 0.1% max health magic damage per second to nearby enemies. Stacks with itself. Ability damage. ",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/NoxiousScent.png' /> Noxious Scent<br /><span style='color: #ffcc00'> Order String:</span> SimpleAura<br /><span style='color: #ffcc00'> Area of Effect:</span> 250<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy<br /><span style='color: #ffcc00'> Targets Excluded:</span> Boss<br /><span style='color: #ffcc00'>Target Buff:</span> Noxious Scent<br /><span style='color: #efe1a7'>--- Attack Type:</span> <img class='tooltip-icon' src='hud/img/icons/Magic.png' />Magic<br /><span style='color: #efe1a7'>--- Damage Per Second:</span> 30.0000 (Ability Damage)<br /><span style='color: #efe1a7'>--- Damage Per Second (% of Max):</span> 0.00100 (Ability Damage)<br /><span style='color: #efe1a7'>--- Max Stacks:</span> 40<br /><span style='color: #ffcc00'>Caster Buff:</span> Noxious Scent<br /><span style='color: #efe1a7'>--- Attack Type:</span> <img class='tooltip-icon' src='hud/img/icons/Magic.png' />Magic"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "noxious_scent_boss_ability_id",
      "abilityClass": "hidden",
      "name": "Noxious Scent (Boss)",
      "image": "Icons/NoxiousScent.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Noxious Scent (Boss)<br /><span style='color: #ffcc00'> Order String:</span> SimpleAura<br /><span style='color: #ffcc00'> Area of Effect:</span> 250<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy<br /><span style='color: #ffcc00'> Targets Excluded:</span> NonBoss<br /><span style='color: #ffcc00'>Target Buff:</span> Noxious Scent (Boss Buff)<br /><span style='color: #efe1a7'>--- Attack Type:</span> <img class='tooltip-icon' src='hud/img/icons/Magic.png' />Magic<br /><span style='color: #efe1a7'>--- Damage Per Second:</span> 30.0000 (Ability Damage)<br /><span style='color: #efe1a7'>--- Damage Per Second (% of Max):</span> 0.00100 (Ability Damage)<br /><span style='color: #efe1a7'>--- Max Stacks:</span> 40"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "unitType": "honeyflower_unit_id",
      "name": "Honeyflower",
      "image": "Icons/Honeyflower.png"
    }
  ],
  "amountSpawned": 0
}
*/

export const raw_deathcap_unit_id: RawUnit = {
  name: "Deathcap",
  sketchfabUrl: "https://sketchfab.com/models/96c7c60f48f14deaa801a2585d8bf5b9",
  image: require("#LTD2/Icons/Deathcap.png"),
  splash: require("#LTD2/splashes/Deathcap.png"),
  description: "Now even more toxic.",

  damage: Damage.Magic,
  atkSpeed: 0.9523809956314903,
  onHitDmg: 1,

  armor: Armor.Natural,
  hp: 4250,
};

export const deathcap_unit_id: PlayableUnit = {
  ...raw_deathcap_unit_id,
  price: 265,
  evolFrom: honeyflower_unit_id,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "antler_unit_id",
  "name": "Antler",
  "image": "Icons/Antler.png",
  "splash": "splashes/Antler.png",
  "sketchfabUrl": "https://sketchfab.com/models/3b44d3fb6e8e4d6287caa08fc1fa90a1",
  "description": "Tanky. Strong when paired with Whitemane.",
  "cost": 200,
  "mythium": 0,
  "income": 0,
  "supply": 1,
  "attackType": "Impact",
  "attackTypeDescription": "115% to Arcane<br />115% to Fortified<br />90% to Natural<br />80% to Swift",
  "armorType": "Natural",
  "armorTypeDescription": "90% from Impact<br />125% from Magic<br />85% from Pierce",
  "hp": 2350,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.0499999523162842,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 69,
  "damage": 72,
  "range": 100,
  "totalValue": 200,
  "bounty": 0,
  "tier": "5",
  "legion": "Grove",
  "legionType": "grove_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.10000000149011612,
  "spawnDelay": 0,
  "effectiveDps": 68.56999969482422,
  "effectiveThreat": 68.56999969482422,
  "movementSize": 0.1875,
  "backswing": 0.44999998807907104,
  "projectileSpeed": 1200,
  "mastermindGroups": "FortOrNat, SwiftOrNat, Impact",
  "flags": "Ground, Organic",
  "color": "98b605",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Grove T5",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "thick_hide_ability_id",
      "abilityClass": "",
      "name": "Thick Hide",
      "image": "Icons/ThickHide.png",
      "description": "Gains an additional 10% damage reduction when built next to a Whitemane.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/ThickHide.png' /> Thick Hide<br /><span style='color: #ffcc00'>Caster Buff:</span> Thick Hide"
    }
  ],
  "upgradesTo": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "unitType": "whitemane_unit_id",
      "name": "Whitemane",
      "image": "Icons/Whitemane.png"
    }
  ],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_antler_unit_id: RawUnit = {
  name: "Antler",
  sketchfabUrl: "https://sketchfab.com/models/3b44d3fb6e8e4d6287caa08fc1fa90a1",
  image: require("#LTD2/Icons/Antler.png"),
  splash: require("#LTD2/splashes/Antler.png"),
  description: "Tanky. Strong when paired with Whitemane.",

  damage: Damage.Impact,
  atkSpeed: 0.9523809956314903,
  onHitDmg: 72,

  armor: Armor.Natural,
  hp: 2350,
};

export const antler_unit_id: PlayableUnit = {
  ...raw_antler_unit_id,
  price: 200,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "whitemane_unit_id",
  "name": "Whitemane",
  "image": "Icons/Whitemane.png",
  "splash": "splashes/Whitemane.png",
  "sketchfabUrl": "https://sketchfab.com/models/aae58d98825f483a9310fd9f0b2ff369",
  "description": "Aura. Adjacently-built units take less damage.",
  "cost": 355,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Impact",
  "attackTypeDescription": "115% to Arcane<br />115% to Fortified<br />90% to Natural<br />80% to Swift",
  "armorType": "Natural",
  "armorTypeDescription": "90% from Impact<br />125% from Magic<br />85% from Pierce",
  "hp": 4600,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.350000023841858,
  "attackSpeedDescriptor": "Slow",
  "mspd": 300,
  "dps": 134,
  "damage": 181,
  "range": 100,
  "totalValue": 555,
  "bounty": 0,
  "tier": "5",
  "legion": "Grove",
  "legionType": "grove_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.10000000149011612,
  "spawnDelay": 0,
  "effectiveDps": 134.07000732421875,
  "effectiveThreat": 134.07000732421875,
  "movementSize": 0.1875,
  "backswing": 0.25,
  "projectileSpeed": 1200,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "98b605",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Grove T5U",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "council_oracle_ability_id",
      "abilityClass": "hidden",
      "name": "Council (Oracle version)",
      "image": "Icons/Council.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Council (Oracle version)<br /><span style='color: #ffcc00'> Order String:</span> Oracle<br /><span style='color: #ffcc00'> Area of Effect:</span> 120<br /><span style='color: #ffcc00'> Targets Considered:</span> Allied, Invulnerable<br /><span style='color: #ffcc00'>Target Buff:</span> Council (Buff)<br /><span style='color: #efe1a7'>--- Stats - Damage Reduction (% Reduction):</span> 0.10000<br /><span style='color: #efe1a7'>--- Stats - Damage Reduction (Flat):</span> 5"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "council_ability_id",
      "abilityClass": "hidden",
      "name": "Council",
      "image": "Icons/Council.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Council<br /><span style='color: #ffcc00'> Order String:</span> SimpleAura<br /><span style='color: #ffcc00'> Area of Effect:</span> 120<br /><span style='color: #ffcc00'> Targets Considered:</span> Allied<br /><span style='color: #ffcc00'>Target Buff:</span> Council (Buff)<br /><span style='color: #efe1a7'>--- Stats - Damage Reduction (% Reduction):</span> 0.10000<br /><span style='color: #efe1a7'>--- Stats - Damage Reduction (Flat):</span> 5<br /><span style='color: #ffcc00'>Caster Buff:</span> Council (Caster Buff)"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "abilityType": "council_antler_ability_id",
      "abilityClass": "hidden",
      "name": "Council (Antler)",
      "image": "Icons/Council.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Council (Antler)<br /><span style='color: #ffcc00'> Order String:</span> SimpleAura<br /><span style='color: #ffcc00'> Area of Effect:</span> 120<br /><span style='color: #ffcc00'> Targets Considered:</span> Allied, NotSelf<br /><span style='color: #ffcc00'> Unit:</span> <img class='tooltip-icon' src='hud/img/Icons/Antler.png' />Antler<br /><span style='color: #ffcc00'>Target Buff:</span> Council (Antler Buff)<br /><span style='color: #efe1a7'>--- Stats - Damage Reduction (% Reduction):</span> 0.11110"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 3,
      "abilityType": "council_antler_oracle_ability_id",
      "abilityClass": "",
      "name": "Council",
      "image": "Icons/Council.png",
      "description": "Adjacently-built units take 5 + 10% (20% for Antlers) reduced damage from all sources.  Does not stack.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Council.png' /> Council<br /><span style='color: #ffcc00'> Order String:</span> Oracle<br /><span style='color: #ffcc00'> Area of Effect:</span> 120<br /><span style='color: #ffcc00'> Targets Considered:</span> Allied, NotSelf, Invulnerable<br /><span style='color: #ffcc00'> Unit:</span> <img class='tooltip-icon' src='hud/img/Icons/Antler.png' />Antler<br /><span style='color: #ffcc00'>Target Buff:</span> Council<br /><span style='color: #efe1a7'>--- Stats - Damage Reduction (% Reduction):</span> 0.11110"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 4,
      "unitType": "antler_unit_id",
      "name": "Antler",
      "image": "Icons/Antler.png"
    }
  ],
  "amountSpawned": 0
}
*/

export const raw_whitemane_unit_id: RawUnit = {
  name: "Whitemane",
  sketchfabUrl: "https://sketchfab.com/models/aae58d98825f483a9310fd9f0b2ff369",
  image: require("#LTD2/Icons/Whitemane.png"),
  splash: require("#LTD2/splashes/Whitemane.png"),
  description: "Aura. Adjacently-built units take less damage.",

  damage: Damage.Impact,
  atkSpeed: 0.7407407276587887,
  onHitDmg: 181,

  armor: Armor.Natural,
  hp: 4600,
};

export const whitemane_unit_id: PlayableUnit = {
  ...raw_whitemane_unit_id,
  price: 355,
  evolFrom: antler_unit_id,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "banana_bunk_unit_id",
  "name": "Banana Bunk",
  "image": "Icons/BananaBunk.png",
  "splash": "splashes/BananaBunk.png",
  "sketchfabUrl": "https://sketchfab.com/models/6603b3635d134b8f9970a97d1060c94f",
  "description": "Ranged, but very tanky. Reflects damage whenever it is attacked.",
  "cost": 280,
  "mythium": 0,
  "income": 0,
  "supply": 1,
  "attackType": "Pierce",
  "attackTypeDescription": "115% to Arcane<br />80% to Fortified<br />85% to Natural<br />120% to Swift",
  "armorType": "Fortified",
  "armorTypeDescription": "115% from Impact<br />105% from Magic<br />80% from Pierce",
  "hp": 3050,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.800000011920929,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 88,
  "damage": 70,
  "range": 200,
  "totalValue": 280,
  "bounty": 0,
  "tier": "6",
  "legion": "Grove",
  "legionType": "grove_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.10000000149011612,
  "spawnDelay": 0,
  "effectiveDps": 87.5,
  "effectiveThreat": 87.5,
  "movementSize": 0.1875,
  "backswing": 0.20000000298023224,
  "projectileSpeed": 1750,
  "mastermindGroups": "FortOrNat, Pierce",
  "flags": "Ground, Organic",
  "color": "98b605",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Grove T6",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "barbs_ability_id",
      "abilityClass": "",
      "name": "Barbs",
      "image": "Icons/Barbs.png",
      "description": "Whenever this unit is attacked, it reflects 15 + 10% damage taken back as pierce damage. Ability damage.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Barbs.png' /> Barbs<br /><span style='color: #ffcc00'> Order String:</span> Thorns<br /><span style='color: #ffcc00'> Attack Type:</span> <img class='tooltip-icon' src='hud/img/icons/Pierce.png' /> Pierce<br /><span style='color: #ffcc00'> Damage Base:</span> 15.00000 (Ability Damage)<br /><span style='color: #ffcc00'> Percentage:</span> 0.1000"
    }
  ],
  "upgradesTo": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "unitType": "banana_haven_unit_id",
      "name": "Banana Haven",
      "image": "Icons/BananaHaven.png"
    }
  ],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_banana_bunk_unit_id: RawUnit = {
  name: "Banana Bunk",
  sketchfabUrl: "https://sketchfab.com/models/6603b3635d134b8f9970a97d1060c94f",
  image: require("#LTD2/Icons/BananaBunk.png"),
  splash: require("#LTD2/splashes/BananaBunk.png"),
  description: "Ranged, but very tanky. Reflects damage whenever it is attacked.",

  damage: Damage.Pierce,
  atkSpeed: 1.2499999813735487,
  onHitDmg: 70,

  armor: Armor.Fortified,
  hp: 3050,
};

export const banana_bunk_unit_id: PlayableUnit = {
  ...raw_banana_bunk_unit_id,
  price: 280,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "banana_haven_unit_id",
  "name": "Banana Haven",
  "image": "Icons/BananaHaven.png",
  "splash": "splashes/BananaHaven.png",
  "sketchfabUrl": "https://sketchfab.com/models/dec85651c0be44218140c0faed9c112c",
  "description": "Now throws 3 bananas per attack. Plus, its thorns reflect tons more damage.",
  "cost": 420,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Pierce",
  "attackTypeDescription": "115% to Arcane<br />80% to Fortified<br />85% to Natural<br />120% to Swift",
  "armorType": "Fortified",
  "armorTypeDescription": "115% from Impact<br />105% from Magic<br />80% from Pierce",
  "hp": 7600,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.800000011920929,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 75,
  "damage": 60,
  "range": 200,
  "totalValue": 700,
  "bounty": 0,
  "tier": "6",
  "legion": "Grove",
  "legionType": "grove_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.10000000149011612,
  "spawnDelay": 0,
  "effectiveDps": 75,
  "effectiveThreat": 75,
  "movementSize": 0.1875,
  "backswing": 0.20000000298023224,
  "projectileSpeed": 1750,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "98b605",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Grove T6U",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "thorns_ability_id",
      "abilityClass": "",
      "name": "Thorns",
      "image": "Icons/Thorns.png",
      "description": "Whenever this unit is attacked, it reflects 20 + 15% damage taken back as pierce damage. Ability damage.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Thorns.png' /> Thorns<br /><span style='color: #ffcc00'> Order String:</span> Thorns<br /><span style='color: #ffcc00'> Attack Type:</span> <img class='tooltip-icon' src='hud/img/icons/Pierce.png' /> Pierce<br /><span style='color: #ffcc00'> Damage Base:</span> 20.00000 (Ability Damage)<br /><span style='color: #ffcc00'> Percentage:</span> 0.1500"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "tribal_warfare_ability_id",
      "abilityClass": "",
      "name": "Tribal Warfare",
      "image": "Icons/TribalWarfare.png",
      "description": "Attacks up to 3 units at once but can still strike the same unit with multiple bananas. Ability damage.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/TribalWarfare.png' /> Tribal Warfare<br /><span style='color: #ffcc00'>Damage Type:</span> Ability Damage<br /><span style='color: #ffcc00'> Order String:</span> BounceAttack<br /><span style='color: #ffcc00'> Attack Type:</span> <img class='tooltip-icon' src='hud/img/icons/Pierce.png' /> Pierce<br /><span style='color: #ffcc00'> Percentage:</span> 1.000<br /><span style='color: #ffcc00'> Max Targets:</span> 2"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "unitType": "banana_bunk_unit_id",
      "name": "Banana Bunk",
      "image": "Icons/BananaBunk.png"
    }
  ],
  "amountSpawned": 0
}
*/

export const raw_banana_haven_unit_id: RawUnit = {
  name: "Banana Haven",
  sketchfabUrl: "https://sketchfab.com/models/dec85651c0be44218140c0faed9c112c",
  image: require("#LTD2/Icons/BananaHaven.png"),
  splash: require("#LTD2/splashes/BananaHaven.png"),
  description: "Now throws 3 bananas per attack. Plus, its thorns reflect tons more damage.",

  damage: Damage.Pierce,
  atkSpeed: 1.2499999813735487,
  onHitDmg: 60,

  armor: Armor.Fortified,
  hp: 7600,
};

export const banana_haven_unit_id: PlayableUnit = {
  ...raw_banana_haven_unit_id,
  price: 420,
  evolFrom: banana_bunk_unit_id,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "peewee_unit_id",
  "name": "Peewee",
  "image": "Icons/Peewee.png",
  "splash": "splashes/Peewee.png",
  "sketchfabUrl": "https://sketchfab.com/models/d43c5e37cd924111be76ef20262a08a4",
  "description": "Flying. You can pay gold to activate Booster, which powers it up for the next wave only.",
  "cost": 25,
  "mythium": 0,
  "income": 0,
  "supply": 1,
  "attackType": "Pierce",
  "attackTypeDescription": "115% to Arcane<br />80% to Fortified<br />85% to Natural<br />120% to Swift",
  "armorType": "Arcane",
  "armorTypeDescription": "115% from Impact<br />75% from Magic<br />115% from Pierce",
  "hp": 250,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.9900000095367432,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 8,
  "damage": 8,
  "range": 100,
  "totalValue": 25,
  "bounty": 0,
  "tier": "1",
  "legion": "Mech",
  "legionType": "mech_legion_id",
  "movementType": "Air",
  "spawnBias": 0.20000000298023224,
  "spawnDelay": 0,
  "effectiveDps": 8.079999923706055,
  "effectiveThreat": 8.079999923706055,
  "movementSize": 0.1875,
  "backswing": 0.30000001192092896,
  "projectileSpeed": 2800,
  "mastermindGroups": "Arcane",
  "flags": "Air, Organic",
  "color": "2b6fce",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Mech T1",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "booster_ability_id",
      "abilityClass": "",
      "name": "Booster",
      "image": "Icons/Booster.png",
      "description": "Boosts this unit's attack speed by 120% and increases range to 500 for the next wave only",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Booster.png' /> Booster<br /><span style='color: #ffcc00'> Linked Abilities:</span> <img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Booster (Active), <img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Undo<br /><span style='color: #ffcc00'> Order String:</span> JuiceUp<br /><span style='color: #ffcc00'> Gold Cost:</span> 5"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "booster_active_ability_id",
      "abilityClass": "linked",
      "name": "Booster (Active)",
      "image": "Icons/Booster.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Booster (Active)<br /><span style='color: #ffcc00'>Caster Buff:</span> Booster (Buff)<br /><span style='color: #efe1a7'>--- Stats - Attack Rate Bonus (%):</span> 1.20000<br /><span style='color: #efe1a7'>--- Stats - Bonus Attack Range (Flat):</span> 400"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "abilityType": "booster_undo_ability_id",
      "abilityClass": "linked",
      "name": "Undo",
      "image": "Icons/UndoBooster.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Undo<br /><span style='color: #ffcc00'> Linked Abilities:</span> <img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Booster<br /><span style='color: #ffcc00'> Order String:</span> JuiceUp<br /><span style='color: #ffcc00'> Gold Cost:</span> -5"
    }
  ],
  "upgradesTo": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 3,
      "unitType": "veteran_unit_id",
      "name": "Veteran",
      "image": "Icons/Veteran.png"
    }
  ],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_peewee_unit_id: RawUnit = {
  name: "Peewee",
  sketchfabUrl: "https://sketchfab.com/models/d43c5e37cd924111be76ef20262a08a4",
  image: require("#LTD2/Icons/Peewee.png"),
  splash: require("#LTD2/splashes/Peewee.png"),
  description: "Flying. You can pay gold to activate Booster, which powers it up for the next wave only.",

  damage: Damage.Pierce,
  atkSpeed: 1.0101010003706326,
  onHitDmg: 8,

  armor: Armor.Arcane,
  hp: 250,
};

export const peewee_unit_id: PlayableUnit = {
  ...raw_peewee_unit_id,
  price: 25,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "veteran_unit_id",
  "name": "Veteran",
  "image": "Icons/Veteran.png",
  "splash": "splashes/Veteran.png",
  "sketchfabUrl": "https://sketchfab.com/models/209812df4f2f4a6d9922834cb775789d",
  "description": "No longer flying. A combat suit provides all-around better stats.",
  "cost": 90,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Pierce",
  "attackTypeDescription": "115% to Arcane<br />80% to Fortified<br />85% to Natural<br />120% to Swift",
  "armorType": "Arcane",
  "armorTypeDescription": "115% from Impact<br />75% from Magic<br />115% from Pierce",
  "hp": 1400,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.9200000166893005,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 39,
  "damage": 36,
  "range": 100,
  "totalValue": 115,
  "bounty": 0,
  "tier": "1",
  "legion": "Mech",
  "legionType": "mech_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.20000000298023224,
  "spawnDelay": 0,
  "effectiveDps": 39.130001068115234,
  "effectiveThreat": 39.130001068115234,
  "movementSize": 0.1875,
  "backswing": 0.4000000059604645,
  "projectileSpeed": 2800,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "2b6fce",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Mech T1U",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "stimpack_ability_id",
      "abilityClass": "",
      "name": "Stimpack",
      "image": "Icons/Stimpack.png",
      "description": "Boosts this unit's attack speed by 120% and increases range to 500 for the next wave only",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Stimpack.png' /> Stimpack<br /><span style='color: #ffcc00'> Linked Abilities:</span> <img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Stimpack (Active), <img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Undo<br /><span style='color: #ffcc00'> Order String:</span> JuiceUp<br /><span style='color: #ffcc00'> Gold Cost:</span> 20"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "stimpack_active_ability_id",
      "abilityClass": "linked",
      "name": "Stimpack (Active)",
      "image": "Icons/Stimpack.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Stimpack (Active)<br /><span style='color: #ffcc00'>Caster Buff:</span> Stimpack (Buff)<br /><span style='color: #efe1a7'>--- Stats - Attack Rate Bonus (%):</span> 1.20000<br /><span style='color: #efe1a7'>--- Stats - Bonus Attack Range (Flat):</span> 400"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "abilityType": "undo_stimpack_ability_id",
      "abilityClass": "linked",
      "name": "Undo",
      "image": "Icons/UndoStimpack.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Undo<br /><span style='color: #ffcc00'> Linked Abilities:</span> <img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Stimpack<br /><span style='color: #ffcc00'> Order String:</span> JuiceUp<br /><span style='color: #ffcc00'> Gold Cost:</span> -20"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 3,
      "unitType": "peewee_unit_id",
      "name": "Peewee",
      "image": "Icons/Peewee.png"
    }
  ],
  "amountSpawned": 0
}
*/

export const raw_veteran_unit_id: RawUnit = {
  name: "Veteran",
  sketchfabUrl: "https://sketchfab.com/models/209812df4f2f4a6d9922834cb775789d",
  image: require("#LTD2/Icons/Veteran.png"),
  splash: require("#LTD2/splashes/Veteran.png"),
  description: "No longer flying. A combat suit provides all-around better stats.",

  damage: Damage.Pierce,
  atkSpeed: 1.086956502021148,
  onHitDmg: 36,

  armor: Armor.Arcane,
  hp: 1400,
};

export const veteran_unit_id: PlayableUnit = {
  ...raw_veteran_unit_id,
  price: 90,
  evolFrom: peewee_unit_id,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "bazooka_unit_id",
  "name": "Bazooka",
  "image": "Icons/Bazooka.png",
  "splash": "splashes/Bazooka.png",
  "sketchfabUrl": "https://sketchfab.com/models/aa7f5578adb84c1ca557a751f820cc4e",
  "description": "Long-ranged. Has two very different upgrade paths.",
  "cost": 45,
  "mythium": 0,
  "income": 0,
  "supply": 1,
  "attackType": "Impact",
  "attackTypeDescription": "115% to Arcane<br />115% to Fortified<br />90% to Natural<br />80% to Swift",
  "armorType": "Swift",
  "armorTypeDescription": "80% from Impact<br />100% from Magic<br />120% from Pierce",
  "hp": 320,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.3300000429153442,
  "attackSpeedDescriptor": "Slow",
  "mspd": 300,
  "dps": 25,
  "damage": 33,
  "range": 600,
  "totalValue": 45,
  "bounty": 0,
  "tier": "2",
  "legion": "Mech",
  "legionType": "mech_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.8999999761581421,
  "spawnDelay": 0,
  "effectiveDps": 24.809999465942383,
  "effectiveThreat": 24.809999465942383,
  "movementSize": 0.1875,
  "backswing": 0.23000000417232513,
  "projectileSpeed": 2800,
  "mastermindGroups": "Magic, Impact, Special1, Special2, Special3",
  "flags": "Ground, Organic",
  "color": "2b6fce",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Mech T2",
  "suffix": "",
  "abilities": [],
  "upgradesTo": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "unitType": "pyro_unit_id",
      "name": "Pyro",
      "image": "Icons/Pyro.png"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "unitType": "zeus_unit_id",
      "name": "Zeus",
      "image": "Icons/Zeus.png"
    }
  ],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_bazooka_unit_id: RawUnit = {
  name: "Bazooka",
  sketchfabUrl: "https://sketchfab.com/models/aa7f5578adb84c1ca557a751f820cc4e",
  image: require("#LTD2/Icons/Bazooka.png"),
  splash: require("#LTD2/splashes/Bazooka.png"),
  description: "Long-ranged. Has two very different upgrade paths.",

  damage: Damage.Impact,
  atkSpeed: 0.7518796749870864,
  onHitDmg: 33,

  armor: Armor.Swift,
  hp: 320,
};

export const bazooka_unit_id: PlayableUnit = {
  ...raw_bazooka_unit_id,
  price: 45,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "pyro_unit_id",
  "name": "Pyro",
  "image": "Icons/Pyro.png",
  "splash": "splashes/Pyro.png",
  "sketchfabUrl": "https://sketchfab.com/models/0d7449afec5249d08b0807b1283ad625",
  "description": "Wields a flamethrower that deals splash damage.",
  "cost": 175,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Magic",
  "attackTypeDescription": "75% to Arcane<br />105% to Fortified<br />125% to Natural<br />100% to Swift",
  "armorType": "Fortified",
  "armorTypeDescription": "115% from Impact<br />105% from Magic<br />80% from Pierce",
  "hp": 1750,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.6200000047683716,
  "attackSpeedDescriptor": "Fast",
  "mspd": 300,
  "dps": 48,
  "damage": 30,
  "range": 300,
  "totalValue": 220,
  "bounty": 0,
  "tier": "2",
  "legion": "Mech",
  "legionType": "mech_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.800000011920929,
  "spawnDelay": 0,
  "effectiveDps": 48.38999938964844,
  "effectiveThreat": 48.38999938964844,
  "movementSize": 0.1875,
  "backswing": 0.15000000596046448,
  "projectileSpeed": 1750,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "2b6fce",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Mech T2U",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "pyro_splash_ability_id",
      "abilityClass": "",
      "name": "Pyromancy",
      "image": "Icons/Pyromancy.png",
      "description": "Each attack deals 50% (30% to flying) damage to nearby enemies. Ability damage.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Pyromancy.png' /> Pyromancy<br /><span style='color: #ffcc00'>Damage Type:</span> Ability Damage<br /><span style='color: #ffcc00'> Order String:</span> SplashAttack<br /><span style='color: #ffcc00'> Percentage:</span> 0.5000<br /><span style='color: #ffcc00'> Area of Effect:</span> 150<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy<br /><span style='color: #ffcc00'> Targets Excluded:</span> Air"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "pyromancy_air_ability_id",
      "abilityClass": "hidden",
      "name": "Pyromancy (Flying)",
      "image": "Icons/Pyromancy.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Pyromancy (Flying)<br /><span style='color: #ffcc00'>Damage Type:</span> Ability Damage<br /><span style='color: #ffcc00'> Order String:</span> SplashAttack<br /><span style='color: #ffcc00'> Percentage:</span> 0.3000<br /><span style='color: #ffcc00'> Area of Effect:</span> 150<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy, Air"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "unitType": "bazooka_unit_id",
      "name": "Bazooka",
      "image": "Icons/Bazooka.png"
    }
  ],
  "amountSpawned": 0
}
*/

export const raw_pyro_unit_id: RawUnit = {
  name: "Pyro",
  sketchfabUrl: "https://sketchfab.com/models/0d7449afec5249d08b0807b1283ad625",
  image: require("#LTD2/Icons/Pyro.png"),
  splash: require("#LTD2/splashes/Pyro.png"),
  description: "Wields a flamethrower that deals splash damage.",

  damage: Damage.Magic,
  atkSpeed: 1.612903213401739,
  onHitDmg: 30,

  armor: Armor.Fortified,
  hp: 1750,
};

export const pyro_unit_id: PlayableUnit = {
  ...raw_pyro_unit_id,
  price: 175,
  evolFrom: bazooka_unit_id,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "zeus_unit_id",
  "name": "Zeus",
  "image": "Icons/Zeus.png",
  "splash": "splashes/Zeus.png",
  "sketchfabUrl": "https://sketchfab.com/models/ed50a19c704e441f8c82195d6b1400b8",
  "description": "Ultra long-ranged sniper. Unleashes lightning bolts every few attacks.",
  "cost": 130,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Impact",
  "attackTypeDescription": "115% to Arcane<br />115% to Fortified<br />90% to Natural<br />80% to Swift",
  "armorType": "Swift",
  "armorTypeDescription": "80% from Impact<br />100% from Magic<br />120% from Pierce",
  "hp": 940,
  "mana": 4,
  "hpRegen": 0,
  "manaRegen": 0.25,
  "attackSpeed": 0.42500001192092896,
  "attackSpeedDescriptor": "Very Fast",
  "mspd": 300,
  "dps": 47,
  "damage": 20,
  "range": 800,
  "totalValue": 175,
  "bounty": 0,
  "tier": "2",
  "legion": "Mech",
  "legionType": "mech_legion_id",
  "movementType": "Ground",
  "spawnBias": 1,
  "spawnDelay": 0,
  "effectiveDps": 47.060001373291016,
  "effectiveThreat": 47.060001373291016,
  "movementSize": 0.1875,
  "backswing": 0.20000000298023224,
  "projectileSpeed": 4000,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "2b6fce",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Mech T2U",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "power_surge_ability_id",
      "abilityClass": "",
      "name": "Power Surge",
      "image": "Icons/PowerSurge.png",
      "description": "Deal 160 bonus magic damage to a single target. Ability damage.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/PowerSurge.png' /> Power Surge<br /><span style='color: #ffcc00'> Order String:</span> EmpoweredAttack<br /><span style='color: #ffcc00'> Projectile - Speed:</span> 5000<br /><span style='color: #ffcc00'> Attack Type:</span> <img class='tooltip-icon' src='hud/img/icons/Magic.png' /> Magic<br /><span style='color: #ffcc00'> Mana Cost:</span> 3.00000<br /><span style='color: #ffcc00'> Damage Base:</span> 160.00000 (Ability Damage)<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "high_voltage_ability_id",
      "abilityClass": "",
      "name": "Magnetic Accelerator",
      "image": "Icons/MagneticAccelerator.png",
      "description": "Restores 0.25 mana with each attack",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/MagneticAccelerator.png' /> Magnetic Accelerator<br /><span style='color: #ffcc00'> Order String:</span> MagneticAccelerator<br /><span style='color: #ffcc00'> Mana Cost:</span> 0.25000"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "unitType": "bazooka_unit_id",
      "name": "Bazooka",
      "image": "Icons/Bazooka.png"
    }
  ],
  "amountSpawned": 0
}
*/

export const raw_zeus_unit_id: RawUnit = {
  name: "Zeus",
  sketchfabUrl: "https://sketchfab.com/models/ed50a19c704e441f8c82195d6b1400b8",
  image: require("#LTD2/Icons/Zeus.png"),
  splash: require("#LTD2/splashes/Zeus.png"),
  description: "Ultra long-ranged sniper. Unleashes lightning bolts every few attacks.",

  damage: Damage.Impact,
  atkSpeed: 2.3529411104723676,
  onHitDmg: 20,

  armor: Armor.Swift,
  hp: 940,
};

export const zeus_unit_id: PlayableUnit = {
  ...raw_zeus_unit_id,
  price: 130,
  evolFrom: bazooka_unit_id,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "tempest_unit_id",
  "name": "Tempest",
  "image": "Icons/Tempest.png",
  "splash": "splashes/Tempest.png",
  "sketchfabUrl": "https://sketchfab.com/models/ab5883b57a5947ddb96a381c37884bc4",
  "description": "Flying. Has a machine-gunner on-board that randomly hits ground enemies.",
  "cost": 90,
  "mythium": 0,
  "income": 0,
  "supply": 1,
  "attackType": "Pierce",
  "attackTypeDescription": "115% to Arcane<br />80% to Fortified<br />85% to Natural<br />120% to Swift",
  "armorType": "Fortified",
  "armorTypeDescription": "115% from Impact<br />105% from Magic<br />80% from Pierce",
  "hp": 750,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.949999988079071,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 33,
  "damage": 31,
  "range": 300,
  "totalValue": 90,
  "bounty": 0,
  "tier": "3",
  "legion": "Mech",
  "legionType": "mech_legion_id",
  "movementType": "Air",
  "spawnBias": 0.699999988079071,
  "spawnDelay": 0,
  "effectiveDps": 32.630001068115234,
  "effectiveThreat": 32.630001068115234,
  "movementSize": 0.1875,
  "backswing": 0.15000000596046448,
  "projectileSpeed": 1750,
  "mastermindGroups": "Pierce, Special1, Special2, Special3, Special4",
  "flags": "Air, Mechanical",
  "color": "2b6fce",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Mech T3",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "anti_air_turret_ability_id",
      "abilityClass": "",
      "name": "Machine Gunner",
      "image": "Icons/MachineGunner.png",
      "description": "Shoots a nearby ground enemy for 14 pierce damage every 0.5 seconds. Ability damage.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/MachineGunner.png' /> Machine Gunner<br /><span style='color: #ffcc00'> Order String:</span> Icewind<br /><span style='color: #ffcc00'> Projectile - Speed:</span> 1400<br /><span style='color: #ffcc00'> Attack Type:</span> <img class='tooltip-icon' src='hud/img/icons/Pierce.png' /> Pierce<br /><span style='color: #ffcc00'> Damage Base:</span> 14.00000 (Ability Damage)<br /><span style='color: #ffcc00'> Area of Effect:</span> 350<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy, Ground"
    }
  ],
  "upgradesTo": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "unitType": "leviathan_unit_id",
      "name": "Leviathan",
      "image": "Icons/Leviathan.png"
    }
  ],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_tempest_unit_id: RawUnit = {
  name: "Tempest",
  sketchfabUrl: "https://sketchfab.com/models/ab5883b57a5947ddb96a381c37884bc4",
  image: require("#LTD2/Icons/Tempest.png"),
  splash: require("#LTD2/splashes/Tempest.png"),
  description: "Flying. Has a machine-gunner on-board that randomly hits ground enemies.",

  damage: Damage.Pierce,
  atkSpeed: 1.0526315921561542,
  onHitDmg: 31,

  armor: Armor.Fortified,
  hp: 750,
};

export const tempest_unit_id: PlayableUnit = {
  ...raw_tempest_unit_id,
  price: 90,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "leviathan_unit_id",
  "name": "Leviathan",
  "image": "Icons/Leviathan.png",
  "splash": "splashes/Leviathan.png",
  "sketchfabUrl": "https://sketchfab.com/models/6c3627111838424386b96159d30d3f39",
  "description": "Flying aura. Boosts damage of adjacently-built flying units. Also, it has a bombardier on-board.",
  "cost": 190,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Pierce",
  "attackTypeDescription": "115% to Arcane<br />80% to Fortified<br />85% to Natural<br />120% to Swift",
  "armorType": "Fortified",
  "armorTypeDescription": "115% from Impact<br />105% from Magic<br />80% from Pierce",
  "hp": 1830,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 72,
  "damage": 72,
  "range": 300,
  "totalValue": 280,
  "bounty": 0,
  "tier": "3",
  "legion": "Mech",
  "legionType": "mech_legion_id",
  "movementType": "Air",
  "spawnBias": 0.699999988079071,
  "spawnDelay": 0,
  "effectiveDps": 72,
  "effectiveThreat": 72,
  "movementSize": 0.1875,
  "backswing": 0.20000000298023224,
  "projectileSpeed": 1750,
  "mastermindGroups": "None",
  "flags": "Air, Mechanical",
  "color": "2b6fce",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Mech T3U",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "aerial_command_oracle_ability_id",
      "abilityClass": "",
      "name": "Aerial Command",
      "image": "Icons/AerialCommand.png",
      "description": "Increases the damage and ability damage of adjacently-built flying units by 18%, stacking up to 2 times.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/AerialCommand.png' /> Aerial Command<br /><span style='color: #ffcc00'> Order String:</span> Oracle<br /><span style='color: #ffcc00'> Area of Effect:</span> 120<br /><span style='color: #ffcc00'> Targets Considered:</span> Allied, Air, Invulnerable<br /><span style='color: #ffcc00'>Target Buff:</span> Aerial Command<br /><span style='color: #efe1a7'>--- Max Stacks:</span> 2<br /><span style='color: #efe1a7'>--- Stats - Attack Damage Bonus (% Additive Post Bonuses):</span> 0.18000<br /><span style='color: #efe1a7'>--- Stats - Spell Damage Bonus (%):</span> 0.18000"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "bombardier_ability_id",
      "abilityClass": "",
      "name": "Bombardier",
      "image": "Icons/Bombardier.png",
      "description": "Shoots a nearby non-flying enemy for 85 pierce damage every 1.5 seconds. Ability damage.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Bombardier.png' /> Bombardier<br /><span style='color: #ffcc00'> Order String:</span> Icewind<br /><span style='color: #ffcc00'> Projectile - Speed:</span> 2000<br /><span style='color: #ffcc00'> Attack Type:</span> <img class='tooltip-icon' src='hud/img/icons/Pierce.png' /> Pierce<br /><span style='color: #ffcc00'> Damage Base:</span> 85.00000 (Ability Damage)<br /><span style='color: #ffcc00'> Area of Effect:</span> 350<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy<br /><span style='color: #ffcc00'> Targets Excluded:</span> Air"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "abilityType": "aerial_command_ability_id",
      "abilityClass": "hidden",
      "name": "Aerial Command",
      "image": "Icons/AerialCommand.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Aerial Command<br /><span style='color: #ffcc00'> Order String:</span> SimpleAura<br /><span style='color: #ffcc00'> Area of Effect:</span> 120<br /><span style='color: #ffcc00'> Targets Considered:</span> Allied, Air<br /><span style='color: #ffcc00'>Target Buff:</span> Aerial Command (Buff)<br /><span style='color: #efe1a7'>--- Max Stacks:</span> 2<br /><span style='color: #efe1a7'>--- Stats - Attack Damage Bonus (% Additive Post Bonuses):</span> 0.18000<br /><span style='color: #efe1a7'>--- Stats - Spell Damage Bonus (%):</span> 0.18000<br /><span style='color: #ffcc00'>Caster Buff:</span> Aerial Command (Caster Buff)"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 3,
      "unitType": "tempest_unit_id",
      "name": "Tempest",
      "image": "Icons/Tempest.png"
    }
  ],
  "amountSpawned": 0
}
*/

export const raw_leviathan_unit_id: RawUnit = {
  name: "Leviathan",
  sketchfabUrl: "https://sketchfab.com/models/6c3627111838424386b96159d30d3f39",
  image: require("#LTD2/Icons/Leviathan.png"),
  splash: require("#LTD2/splashes/Leviathan.png"),
  description: "Flying aura. Boosts damage of adjacently-built flying units. Also, it has a bombardier on-board.",

  damage: Damage.Pierce,
  atkSpeed: 1,
  onHitDmg: 72,

  armor: Armor.Fortified,
  hp: 1830,
};

export const leviathan_unit_id: PlayableUnit = {
  ...raw_leviathan_unit_id,
  price: 190,
  evolFrom: tempest_unit_id,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "aps_unit_id",
  "name": "APS",
  "image": "Icons/APS.png",
  "splash": "splashes/APS.png",
  "sketchfabUrl": "https://sketchfab.com/models/3ea341bb2d0c49e4a0ea643a0a3c2fdf",
  "description": "Aura. Provides an attack speed aura to adjacently-built units",
  "cost": 150,
  "mythium": 0,
  "income": 0,
  "supply": 1,
  "attackType": "Pierce",
  "attackTypeDescription": "115% to Arcane<br />80% to Fortified<br />85% to Natural<br />120% to Swift",
  "armorType": "Fortified",
  "armorTypeDescription": "115% from Impact<br />105% from Magic<br />80% from Pierce",
  "hp": 870,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.7099999785423279,
  "attackSpeedDescriptor": "Fast",
  "mspd": 300,
  "dps": 54,
  "damage": 38,
  "range": 400,
  "totalValue": 150,
  "bounty": 0,
  "tier": "4",
  "legion": "Mech",
  "legionType": "mech_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.699999988079071,
  "spawnDelay": 0,
  "effectiveDps": 53.52000045776367,
  "effectiveThreat": 53.52000045776367,
  "movementSize": 0.1875,
  "backswing": 0.20000000298023224,
  "projectileSpeed": 2000,
  "mastermindGroups": "Special1, Special2, Special3, Special4",
  "flags": "Ground, Mechanical",
  "color": "2b6fce",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Mech T4",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "energize_oracle_ability_id",
      "abilityClass": "",
      "name": "Energize",
      "image": "Icons/Energize.png",
      "description": "Grants adjacently-built units +12% attack speed. Does not stack.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Energize.png' /> Energize<br /><span style='color: #ffcc00'> Order String:</span> Oracle<br /><span style='color: #ffcc00'> Area of Effect:</span> 120<br /><span style='color: #ffcc00'> Targets Considered:</span> Allied, Invulnerable<br /><span style='color: #ffcc00'>Target Buff:</span> Energize<br /><span style='color: #efe1a7'>--- Stats - Attack Rate Bonus (%):</span> 0.12000"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "energize_ability_id",
      "abilityClass": "hidden",
      "name": "Energize",
      "image": "Icons/Energize.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Energize<br /><span style='color: #ffcc00'> Order String:</span> SimpleAura<br /><span style='color: #ffcc00'> Area of Effect:</span> 120<br /><span style='color: #ffcc00'> Targets Considered:</span> Allied<br /><span style='color: #ffcc00'>Target Buff:</span> Energize (Buff)<br /><span style='color: #efe1a7'>--- Stats - Attack Rate Bonus (%):</span> 0.12000<br /><span style='color: #ffcc00'>Caster Buff:</span> Energize (Caster Buff)"
    }
  ],
  "upgradesTo": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "unitType": "mps_unit_id",
      "name": "MPS",
      "image": "Icons/MPS.png"
    }
  ],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_aps_unit_id: RawUnit = {
  name: "APS",
  sketchfabUrl: "https://sketchfab.com/models/3ea341bb2d0c49e4a0ea643a0a3c2fdf",
  image: require("#LTD2/Icons/APS.png"),
  splash: require("#LTD2/splashes/APS.png"),
  description: "Aura. Provides an attack speed aura to adjacently-built units",

  damage: Damage.Pierce,
  atkSpeed: 1.408450746791654,
  onHitDmg: 38,

  armor: Armor.Fortified,
  hp: 870,
};

export const aps_unit_id: PlayableUnit = {
  ...raw_aps_unit_id,
  price: 150,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "mps_unit_id",
  "name": "MPS",
  "image": "Icons/MPS.png",
  "splash": "splashes/MPS.png",
  "sketchfabUrl": "https://sketchfab.com/models/6d511a0ced024da1ae509010250d838a",
  "description": "Aura. Provides an even more powerful attack speed aura to adjacently-built units",
  "cost": 250,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Pierce",
  "attackTypeDescription": "115% to Arcane<br />80% to Fortified<br />85% to Natural<br />120% to Swift",
  "armorType": "Fortified",
  "armorTypeDescription": "115% from Impact<br />105% from Magic<br />80% from Pierce",
  "hp": 2320,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.7099999785423279,
  "attackSpeedDescriptor": "Fast",
  "mspd": 300,
  "dps": 142,
  "damage": 101,
  "range": 400,
  "totalValue": 400,
  "bounty": 0,
  "tier": "4",
  "legion": "Mech",
  "legionType": "mech_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.699999988079071,
  "spawnDelay": 0,
  "effectiveDps": 142.25,
  "effectiveThreat": 142.25,
  "movementSize": 0.1875,
  "backswing": 0.20000000298023224,
  "projectileSpeed": 2000,
  "mastermindGroups": "None",
  "flags": "Ground, Mechanical",
  "color": "2b6fce",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Mech T4U",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "mythium_core_oracle_ability_id",
      "abilityClass": "",
      "name": "Mythium Core",
      "image": "Icons/MythiumCore.png",
      "description": "Grants adjacently-built units +18% attack speed. Does not stack.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/MythiumCore.png' /> Mythium Core<br /><span style='color: #ffcc00'> Order String:</span> Oracle<br /><span style='color: #ffcc00'> Area of Effect:</span> 120<br /><span style='color: #ffcc00'> Targets Considered:</span> Allied, Invulnerable<br /><span style='color: #ffcc00'>Target Buff:</span> Mythium Core<br /><span style='color: #efe1a7'>--- Stats - Attack Rate Bonus (%):</span> 0.18000"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "overcharge_ability_id",
      "abilityClass": "hidden",
      "name": "Mythium Core",
      "image": "Icons/MythiumCore.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Mythium Core<br /><span style='color: #ffcc00'> Order String:</span> SimpleAura<br /><span style='color: #ffcc00'> Area of Effect:</span> 120<br /><span style='color: #ffcc00'> Targets Considered:</span> Allied<br /><span style='color: #ffcc00'>Target Buff:</span> Mythium Core (Buff)<br /><span style='color: #efe1a7'>--- Stats - Attack Rate Bonus (%):</span> 0.18000<br /><span style='color: #ffcc00'>Caster Buff:</span> Mythium Core (Caster Buff)"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "unitType": "aps_unit_id",
      "name": "APS",
      "image": "Icons/APS.png"
    }
  ],
  "amountSpawned": 0
}
*/

export const raw_mps_unit_id: RawUnit = {
  name: "MPS",
  sketchfabUrl: "https://sketchfab.com/models/6d511a0ced024da1ae509010250d838a",
  image: require("#LTD2/Icons/MPS.png"),
  splash: require("#LTD2/splashes/MPS.png"),
  description: "Aura. Provides an even more powerful attack speed aura to adjacently-built units",

  damage: Damage.Pierce,
  atkSpeed: 1.408450746791654,
  onHitDmg: 101,

  armor: Armor.Fortified,
  hp: 2320,
};

export const mps_unit_id: PlayableUnit = {
  ...raw_mps_unit_id,
  price: 250,
  evolFrom: aps_unit_id,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "berserker_unit_id",
  "name": "Berserker",
  "image": "Icons/Berserker.png",
  "splash": "splashes/Berserker.png",
  "sketchfabUrl": "https://sketchfab.com/models/d0f1de4f249f4c898c7a0744cbf2b3f9",
  "description": "Tanky, but also an excellent boss-killer. Metal claws deal ramping damage when attacking the same target.",
  "cost": 190,
  "mythium": 0,
  "income": 0,
  "supply": 1,
  "attackType": "Pierce",
  "attackTypeDescription": "115% to Arcane<br />80% to Fortified<br />85% to Natural<br />120% to Swift",
  "armorType": "Swift",
  "armorTypeDescription": "80% from Impact<br />100% from Magic<br />120% from Pierce",
  "hp": 2130,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.75,
  "attackSpeedDescriptor": "Fast",
  "mspd": 300,
  "dps": 40,
  "damage": 30,
  "range": 100,
  "totalValue": 190,
  "bounty": 0,
  "tier": "5",
  "legion": "Mech",
  "legionType": "mech_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.20000000298023224,
  "spawnDelay": 0,
  "effectiveDps": 40,
  "effectiveThreat": 40,
  "movementSize": 0.1875,
  "backswing": 0.28999999165534973,
  "projectileSpeed": 1200,
  "mastermindGroups": "SwiftOrNat, Special1, Special2, Special3, Special4",
  "flags": "Ground, Organic",
  "color": "2b6fce",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Mech T5",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "brawler_ramping_ability_id",
      "abilityClass": "",
      "name": "Brawler",
      "image": "Icons/Brawler.png",
      "description": "Deals 9 bonus magic damage for each consecutive attack against the same target. Ability damage.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Brawler.png' /> Brawler<br /><span style='color: #ffcc00'> Order String:</span> RampingDamage<br /><span style='color: #ffcc00'> Attack Type:</span> <img class='tooltip-icon' src='hud/img/icons/Magic.png' /> Magic<br /><span style='color: #ffcc00'> Damage Base:</span> 9.00000 (Ability Damage)<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy"
    }
  ],
  "upgradesTo": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "unitType": "fatalizer_unit_id",
      "name": "Fatalizer",
      "image": "Icons/Fatalizer.png"
    }
  ],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_berserker_unit_id: RawUnit = {
  name: "Berserker",
  sketchfabUrl: "https://sketchfab.com/models/d0f1de4f249f4c898c7a0744cbf2b3f9",
  image: require("#LTD2/Icons/Berserker.png"),
  splash: require("#LTD2/splashes/Berserker.png"),
  description: "Tanky, but also an excellent boss-killer. Metal claws deal ramping damage when attacking the same target.",

  damage: Damage.Pierce,
  atkSpeed: 1.3333333333333333,
  onHitDmg: 30,

  armor: Armor.Swift,
  hp: 2130,
};

export const berserker_unit_id: PlayableUnit = {
  ...raw_berserker_unit_id,
  price: 190,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "fatalizer_unit_id",
  "name": "Fatalizer",
  "image": "Icons/Fatalizer.png",
  "splash": "splashes/Fatalizer.png",
  "sketchfabUrl": "https://sketchfab.com/models/840bf76e637e4f0a99f9418b772efdfd",
  "description": "Now wields energy blades, making it deal massive ramping damage.",
  "cost": 355,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Pierce",
  "attackTypeDescription": "115% to Arcane<br />80% to Fortified<br />85% to Natural<br />120% to Swift",
  "armorType": "Swift",
  "armorTypeDescription": "80% from Impact<br />100% from Magic<br />120% from Pierce",
  "hp": 6100,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.75,
  "attackSpeedDescriptor": "Fast",
  "mspd": 300,
  "dps": 115,
  "damage": 86,
  "range": 100,
  "totalValue": 545,
  "bounty": 0,
  "tier": "5",
  "legion": "Mech",
  "legionType": "mech_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.20000000298023224,
  "spawnDelay": 0,
  "effectiveDps": 114.66999816894531,
  "effectiveThreat": 114.66999816894531,
  "movementSize": 0.1875,
  "backswing": 0.15000000596046448,
  "projectileSpeed": 1200,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "2b6fce",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Mech T5U",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "death_stare_ramping_ability_id",
      "abilityClass": "",
      "name": "Duelist",
      "image": "Icons/Duelist.png",
      "description": "Deals 25 bonus magic damage for each consecutive attack against the same target. Ability damage.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Duelist.png' /> Duelist<br /><span style='color: #ffcc00'> Order String:</span> RampingDamage<br /><span style='color: #ffcc00'> Attack Type:</span> <img class='tooltip-icon' src='hud/img/icons/Magic.png' /> Magic<br /><span style='color: #ffcc00'> Damage Base:</span> 25.00000 (Ability Damage)<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "unitType": "berserker_unit_id",
      "name": "Berserker",
      "image": "Icons/Berserker.png"
    }
  ],
  "amountSpawned": 0
}
*/

export const raw_fatalizer_unit_id: RawUnit = {
  name: "Fatalizer",
  sketchfabUrl: "https://sketchfab.com/models/840bf76e637e4f0a99f9418b772efdfd",
  image: require("#LTD2/Icons/Fatalizer.png"),
  splash: require("#LTD2/splashes/Fatalizer.png"),
  description: "Now wields energy blades, making it deal massive ramping damage.",

  damage: Damage.Pierce,
  atkSpeed: 1.3333333333333333,
  onHitDmg: 86,

  armor: Armor.Swift,
  hp: 6100,
};

export const fatalizer_unit_id: PlayableUnit = {
  ...raw_fatalizer_unit_id,
  price: 355,
  evolFrom: berserker_unit_id,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "millennium_unit_id",
  "name": "Millennium",
  "image": "Icons/Millennium.png",
  "splash": "splashes/Millennium.png",
  "sketchfabUrl": "https://sketchfab.com/models/88e4f8bee2ea418bbf3f5dd3b838788c",
  "description": "Creates explosions whenever it lands a killing blow.",
  "cost": 290,
  "mythium": 0,
  "income": 0,
  "supply": 1,
  "attackType": "Impact",
  "attackTypeDescription": "115% to Arcane<br />115% to Fortified<br />90% to Natural<br />80% to Swift",
  "armorType": "Arcane",
  "armorTypeDescription": "115% from Impact<br />75% from Magic<br />115% from Pierce",
  "hp": 2100,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.6200000047683716,
  "attackSpeedDescriptor": "Very Slow",
  "mspd": 300,
  "dps": 151,
  "damage": 245,
  "range": 550,
  "totalValue": 290,
  "bounty": 0,
  "tier": "6",
  "legion": "Mech",
  "legionType": "mech_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.800000011920929,
  "spawnDelay": 0,
  "effectiveDps": 151.22999572753906,
  "effectiveThreat": 151.22999572753906,
  "movementSize": 0.1875,
  "backswing": 0.30000001192092896,
  "projectileSpeed": 3000,
  "mastermindGroups": "Impact, Special1, Special2, Special3",
  "flags": "Ground, Mechanical",
  "color": "2b6fce",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Mech T6",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "explosive_shells_ability_id",
      "abilityClass": "",
      "name": "Explosive Shells",
      "image": "Icons/ExplosiveShells.png",
      "description": "When dealing the killing blow, deals 35 impact damage to enemies in an area around the dying unit. Ability damage.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/ExplosiveShells.png' /> Explosive Shells<br /><span style='color: #ffcc00'> Order String:</span> ExplodeOnAttackTargetDeath<br /><span style='color: #ffcc00'> Attack Type:</span> <img class='tooltip-icon' src='hud/img/icons/Impact.png' /> Impact<br /><span style='color: #ffcc00'> Damage Base:</span> 35.00000 (Ability Damage)<br /><span style='color: #ffcc00'> Area of Effect:</span> 150<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy<br /><span style='color: #ffcc00'> Targets Excluded:</span> Air"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "explosive_shells_air_ability_id",
      "abilityClass": "hidden",
      "name": "Explosive Shells (Flying)",
      "image": "Icons/ExplosiveShells.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Explosive Shells (Flying)<br /><span style='color: #ffcc00'> Order String:</span> ExplodeOnAttackTargetDeath<br /><span style='color: #ffcc00'> Attack Type:</span> <img class='tooltip-icon' src='hud/img/icons/Impact.png' /> Impact<br /><span style='color: #ffcc00'> Damage Base:</span> 35.00000 (Ability Damage)<br /><span style='color: #ffcc00'> Area of Effect:</span> 150<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy, Air"
    }
  ],
  "upgradesTo": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "unitType": "doomsday_machine_unit_id",
      "name": "Doomsday Machine",
      "image": "Icons/DoomsdayMachine.png"
    }
  ],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_millennium_unit_id: RawUnit = {
  name: "Millennium",
  sketchfabUrl: "https://sketchfab.com/models/88e4f8bee2ea418bbf3f5dd3b838788c",
  image: require("#LTD2/Icons/Millennium.png"),
  splash: require("#LTD2/splashes/Millennium.png"),
  description: "Creates explosions whenever it lands a killing blow.",

  damage: Damage.Impact,
  atkSpeed: 0.6172839488003461,
  onHitDmg: 245,

  armor: Armor.Arcane,
  hp: 2100,
};

export const millennium_unit_id: PlayableUnit = {
  ...raw_millennium_unit_id,
  price: 290,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "doomsday_machine_unit_id",
  "name": "Doomsday Machine",
  "image": "Icons/DoomsdayMachine.png",
  "splash": "splashes/DoomsdayMachine.png",
  "sketchfabUrl": "https://sketchfab.com/models/913241d7d1134219a6eec6c7b1177adf",
  "description": "Unmatched power, unmatched price tag.",
  "cost": 425,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Impact",
  "attackTypeDescription": "115% to Arcane<br />115% to Fortified<br />90% to Natural<br />80% to Swift",
  "armorType": "Arcane",
  "armorTypeDescription": "115% from Impact<br />75% from Magic<br />115% from Pierce",
  "hp": 5140,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.6200000047683716,
  "attackSpeedDescriptor": "Very Slow",
  "mspd": 300,
  "dps": 370,
  "damage": 600,
  "range": 550,
  "totalValue": 715,
  "bounty": 0,
  "tier": "6",
  "legion": "Mech",
  "legionType": "mech_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.800000011920929,
  "spawnDelay": 0,
  "effectiveDps": 370.3699951171875,
  "effectiveThreat": 370.3699951171875,
  "movementSize": 0.1875,
  "backswing": 0.30000001192092896,
  "projectileSpeed": 3000,
  "mastermindGroups": "None",
  "flags": "Ground, Mechanical",
  "color": "2b6fce",
  "turnRate": 2,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Mech T6U",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "detonation_ability_id",
      "abilityClass": "",
      "name": "Detonation",
      "image": "Icons/Detonation.png",
      "description": "When dealing the killing blow, deals 85 impact damage to enemies in an area around the dying unit. Ability damage.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Detonation.png' /> Detonation<br /><span style='color: #ffcc00'> Order String:</span> ExplodeOnAttackTargetDeath<br /><span style='color: #ffcc00'> Attack Type:</span> <img class='tooltip-icon' src='hud/img/icons/Impact.png' /> Impact<br /><span style='color: #ffcc00'> Damage Base:</span> 85.00000 (Ability Damage)<br /><span style='color: #ffcc00'> Area of Effect:</span> 150<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy<br /><span style='color: #ffcc00'> Targets Excluded:</span> Air"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "detonation_flying_ability_id",
      "abilityClass": "hidden",
      "name": "Detonation (Flying)",
      "image": "Icons/Detonation.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Detonation (Flying)<br /><span style='color: #ffcc00'> Order String:</span> ExplodeOnAttackTargetDeath<br /><span style='color: #ffcc00'> Attack Type:</span> <img class='tooltip-icon' src='hud/img/icons/Impact.png' /> Impact<br /><span style='color: #ffcc00'> Damage Base:</span> 85.00000 (Ability Damage)<br /><span style='color: #ffcc00'> Area of Effect:</span> 150<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy, Air"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "unitType": "millennium_unit_id",
      "name": "Millennium",
      "image": "Icons/Millennium.png"
    }
  ],
  "amountSpawned": 0
}
*/

export const raw_doomsday_machine_unit_id: RawUnit = {
  name: "Doomsday Machine",
  sketchfabUrl: "https://sketchfab.com/models/913241d7d1134219a6eec6c7b1177adf",
  image: require("#LTD2/Icons/DoomsdayMachine.png"),
  splash: require("#LTD2/splashes/DoomsdayMachine.png"),
  description: "Unmatched power, unmatched price tag.",

  damage: Damage.Impact,
  atkSpeed: 0.6172839488003461,
  onHitDmg: 600,

  armor: Armor.Arcane,
  hp: 5140,
};

export const doomsday_machine_unit_id: PlayableUnit = {
  ...raw_doomsday_machine_unit_id,
  price: 425,
  evolFrom: millennium_unit_id,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "snail_unit_id",
  "name": "Snail",
  "image": "Icons/Snail.png",
  "splash": "splashes/Snail.png",
  "sketchfabUrl": "https://sketchfab.com/models/5c4b21d55495488480676b1ef1670a55",
  "description": "Weak. Mainly used as a source of income.",
  "cost": 0,
  "mythium": 20,
  "income": 6,
  "supply": 0,
  "attackType": "Impact",
  "attackTypeDescription": "115% to Arcane<br />115% to Fortified<br />90% to Natural<br />80% to Swift",
  "armorType": "Fortified",
  "armorTypeDescription": "115% from Impact<br />105% from Magic<br />80% from Pierce",
  "hp": 280,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.100000023841858,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 11,
  "damage": 12,
  "range": 100,
  "totalValue": 0,
  "bounty": 6,
  "tier": "0",
  "legion": "Mercenary",
  "legionType": "nether_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.25,
  "spawnDelay": 0.25,
  "effectiveDps": 10.90999984741211,
  "effectiveThreat": 10.90999984741211,
  "movementSize": 0.1875,
  "backswing": 1,
  "projectileSpeed": 1200,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "8c3c10",
  "turnRate": 0.5,
  "stockMax": 8,
  "stockTime": 9999,
  "prefix": "Mercenary 01",
  "suffix": "",
  "abilities": [],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_snail_unit_id: RawUnit = {
  name: "Snail",
  sketchfabUrl: "https://sketchfab.com/models/5c4b21d55495488480676b1ef1670a55",
  image: require("#LTD2/Icons/Snail.png"),
  splash: require("#LTD2/splashes/Snail.png"),
  description: "Weak. Mainly used as a source of income.",

  damage: Damage.Impact,
  atkSpeed: 0.9090908893868948,
  onHitDmg: 12,

  armor: Armor.Fortified,
  hp: 280,
};

export const snail_unit_id: PlayableUnit = {
  ...raw_snail_unit_id,
  price: 0,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "giant_snail_unit_id",
  "name": "Giant Snail",
  "image": "Icons/GiantSnail.png",
  "splash": "splashes/GiantSnail.png",
  "sketchfabUrl": "https://sketchfab.com/models/5c4b21d55495488480676b1ef1670a55",
  "description": "Melee tank (Available when the Legion Spell 'Giant Snail' is used)",
  "cost": 0,
  "mythium": 20,
  "income": 6,
  "supply": 0,
  "attackType": "Impact",
  "attackTypeDescription": "115% to Arcane<br />115% to Fortified<br />90% to Natural<br />80% to Swift",
  "armorType": "Fortified",
  "armorTypeDescription": "115% from Impact<br />105% from Magic<br />80% from Pierce",
  "hp": 490,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.100000023841858,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 19,
  "damage": 21,
  "range": 100,
  "totalValue": 0,
  "bounty": 6,
  "tier": "0",
  "legion": "Mercenary",
  "legionType": "nether_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.25,
  "spawnDelay": 0.25,
  "effectiveDps": 19.09000015258789,
  "effectiveThreat": 19.09000015258789,
  "movementSize": 0.1875,
  "backswing": 1,
  "projectileSpeed": 1200,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "8c3c10",
  "turnRate": 0.5,
  "stockMax": 8,
  "stockTime": 9999,
  "prefix": "Mercenary 01",
  "suffix": "",
  "abilities": [],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_giant_snail_unit_id: RawUnit = {
  name: "Giant Snail",
  sketchfabUrl: "https://sketchfab.com/models/5c4b21d55495488480676b1ef1670a55",
  image: require("#LTD2/Icons/GiantSnail.png"),
  splash: require("#LTD2/splashes/GiantSnail.png"),
  description: "Melee tank (Available when the Legion Spell 'Giant Snail' is used)",

  damage: Damage.Impact,
  atkSpeed: 0.9090908893868948,
  onHitDmg: 21,

  armor: Armor.Fortified,
  hp: 490,
};

export const giant_snail_unit_id: PlayableUnit = {
  ...raw_giant_snail_unit_id,
  price: 0,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "lizard_unit_id",
  "name": "Lizard",
  "image": "Icons/Lizard.png",
  "splash": "splashes/Lizard.png",
  "sketchfabUrl": "https://sketchfab.com/models/8ea04b40a4854114a21b1cc4f3098f00",
  "description": "Long-ranged. Throws spears at its enemies from afar.",
  "cost": 0,
  "mythium": 40,
  "income": 12,
  "supply": 0,
  "attackType": "Pierce",
  "attackTypeDescription": "115% to Arcane<br />80% to Fortified<br />85% to Natural<br />120% to Swift",
  "armorType": "Natural",
  "armorTypeDescription": "90% from Impact<br />125% from Magic<br />85% from Pierce",
  "hp": 320,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.340000033378601,
  "attackSpeedDescriptor": "Slow",
  "mspd": 300,
  "dps": 25,
  "damage": 34,
  "range": 550,
  "totalValue": 0,
  "bounty": 12,
  "tier": "0",
  "legion": "Mercenary",
  "legionType": "nether_legion_id",
  "movementType": "Ground",
  "spawnBias": 1,
  "spawnDelay": 1,
  "effectiveDps": 25.3700008392334,
  "effectiveThreat": 25.3700008392334,
  "movementSize": 0.1875,
  "backswing": 0.15000000596046448,
  "projectileSpeed": 2500,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "8c3c10",
  "turnRate": 0.5,
  "stockMax": 8,
  "stockTime": 9999,
  "prefix": "Mercenary 02",
  "suffix": "",
  "abilities": [],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_lizard_unit_id: RawUnit = {
  name: "Lizard",
  sketchfabUrl: "https://sketchfab.com/models/8ea04b40a4854114a21b1cc4f3098f00",
  image: require("#LTD2/Icons/Lizard.png"),
  splash: require("#LTD2/splashes/Lizard.png"),
  description: "Long-ranged. Throws spears at its enemies from afar.",

  damage: Damage.Pierce,
  atkSpeed: 0.746268638127311,
  onHitDmg: 34,

  armor: Armor.Natural,
  hp: 320,
};

export const lizard_unit_id: PlayableUnit = {
  ...raw_lizard_unit_id,
  price: 0,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "dragon_turtle_unit_id",
  "name": "Dragon Turtle",
  "image": "Icons/DragonTurtle.png",
  "splash": "splashes/DragonTurtle.png",
  "sketchfabUrl": "https://sketchfab.com/models/b8915249f5ff402fb40c9615cddf2d5f",
  "description": "Its hard shell makes it a great tank.",
  "cost": 0,
  "mythium": 40,
  "income": 12,
  "supply": 0,
  "attackType": "Magic",
  "attackTypeDescription": "75% to Arcane<br />105% to Fortified<br />125% to Natural<br />100% to Swift",
  "armorType": "Arcane",
  "armorTypeDescription": "115% from Impact<br />75% from Magic<br />115% from Pierce",
  "hp": 600,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.0499999523162842,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 24,
  "damage": 25,
  "range": 100,
  "totalValue": 0,
  "bounty": 12,
  "tier": "0",
  "legion": "Mercenary",
  "legionType": "nether_legion_id",
  "movementType": "Air",
  "spawnBias": 0.25,
  "spawnDelay": 0.25,
  "effectiveDps": 23.809999465942383,
  "effectiveThreat": 23.809999465942383,
  "movementSize": 0.1875,
  "backswing": 0.15000000596046448,
  "projectileSpeed": 1200,
  "mastermindGroups": "None",
  "flags": "Air, Organic",
  "color": "8c3c10",
  "turnRate": 0.5,
  "stockMax": 8,
  "stockTime": 9999,
  "prefix": "Mercenary 03",
  "suffix": "",
  "abilities": [],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_dragon_turtle_unit_id: RawUnit = {
  name: "Dragon Turtle",
  sketchfabUrl: "https://sketchfab.com/models/b8915249f5ff402fb40c9615cddf2d5f",
  image: require("#LTD2/Icons/DragonTurtle.png"),
  splash: require("#LTD2/splashes/DragonTurtle.png"),
  description: "Its hard shell makes it a great tank.",

  damage: Damage.Magic,
  atkSpeed: 0.9523809956314903,
  onHitDmg: 25,

  armor: Armor.Arcane,
  hp: 600,
};

export const dragon_turtle_unit_id: PlayableUnit = {
  ...raw_dragon_turtle_unit_id,
  price: 0,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "brute_unit_id",
  "name": "Brute",
  "image": "Icons/Brute.png",
  "splash": "splashes/Brute.png",
  "sketchfabUrl": "https://sketchfab.com/models/536e09057c7648ef90e9ff5b8de42d75",
  "description": "Mauls enemies, slowing their attack speed.",
  "cost": 0,
  "mythium": 60,
  "income": 15,
  "supply": 0,
  "attackType": "Impact",
  "attackTypeDescription": "115% to Arcane<br />115% to Fortified<br />90% to Natural<br />80% to Swift",
  "armorType": "Natural",
  "armorTypeDescription": "90% from Impact<br />125% from Magic<br />85% from Pierce",
  "hp": 870,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.1100000143051147,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 50,
  "damage": 55,
  "range": 100,
  "totalValue": 0,
  "bounty": 15,
  "tier": "0",
  "legion": "Mercenary",
  "legionType": "nether_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.8500000238418579,
  "spawnDelay": 0.75,
  "effectiveDps": 60,
  "effectiveThreat": 75,
  "movementSize": 0.1875,
  "backswing": 0.15000000596046448,
  "projectileSpeed": 1200,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "8c3c10",
  "turnRate": 0.5,
  "stockMax": 8,
  "stockTime": 9999,
  "prefix": "Mercenary 04",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "maul_ability_id",
      "abilityClass": "",
      "name": "Maul",
      "image": "Icons/Maul.png",
      "description": "Each attack slows attack speed by 4% (1% against the king), stacking up to 6 times",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Maul.png' /> Maul<br /><span style='color: #ffcc00'>Damage Type:</span> Ability Damage<br /><span style='color: #ffcc00'> Order String:</span> BounceAttack<br /><span style='color: #ffcc00'> Duration:</span> 3.000<br /><span style='color: #ffcc00'> Duration (Boss):</span> 3.000<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy<br /><span style='color: #ffcc00'> Targets Excluded:</span> Boss<br /><span style='color: #ffcc00'>Target Buff:</span> Maul<br /><span style='color: #efe1a7'>--- Max Stacks:</span> 6<br /><span style='color: #efe1a7'>--- Stats - Attack Rate Bonus (%):</span> -0.04000"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "maul_boss_ability_id",
      "abilityClass": "hidden",
      "name": "Maul (Boss)",
      "image": "Icons/Maul.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Maul (Boss)<br /><span style='color: #ffcc00'>Damage Type:</span> Ability Damage<br /><span style='color: #ffcc00'> Order String:</span> BounceAttack<br /><span style='color: #ffcc00'> Duration:</span> 3.000<br /><span style='color: #ffcc00'> Duration (Boss):</span> 3.000<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy<br /><span style='color: #ffcc00'> Targets Excluded:</span> NonBoss<br /><span style='color: #ffcc00'>Target Buff:</span> Maul (Boss Buff)<br /><span style='color: #efe1a7'>--- Max Stacks:</span> 6<br /><span style='color: #efe1a7'>--- Stats - Attack Rate Bonus (%):</span> -0.01000"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_brute_unit_id: RawUnit = {
  name: "Brute",
  sketchfabUrl: "https://sketchfab.com/models/536e09057c7648ef90e9ff5b8de42d75",
  image: require("#LTD2/Icons/Brute.png"),
  splash: require("#LTD2/splashes/Brute.png"),
  description: "Mauls enemies, slowing their attack speed.",

  damage: Damage.Impact,
  atkSpeed: 0.900900889290549,
  onHitDmg: 55,

  armor: Armor.Natural,
  hp: 870,
};

export const brute_unit_id: PlayableUnit = {
  ...raw_brute_unit_id,
  price: 0,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "fiend_unit_id",
  "name": "Fiend",
  "image": "Icons/Fiend.png",
  "splash": "splashes/Fiend.png",
  "sketchfabUrl": "https://sketchfab.com/models/4eba6c7b34ac416685e25c9d1db12e7a",
  "description": "Tanky, despite its thin limbs.",
  "cost": 0,
  "mythium": 60,
  "income": 18,
  "supply": 0,
  "attackType": "Pierce",
  "attackTypeDescription": "115% to Arcane<br />80% to Fortified<br />85% to Natural<br />120% to Swift",
  "armorType": "Swift",
  "armorTypeDescription": "80% from Impact<br />100% from Magic<br />120% from Pierce",
  "hp": 920,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.75,
  "attackSpeedDescriptor": "Fast",
  "mspd": 300,
  "dps": 36,
  "damage": 27,
  "range": 100,
  "totalValue": 0,
  "bounty": 18,
  "tier": "0",
  "legion": "Mercenary",
  "legionType": "nether_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.25,
  "spawnDelay": 0.25,
  "effectiveDps": 36,
  "effectiveThreat": 36,
  "movementSize": 0.1875,
  "backswing": 0.15000000596046448,
  "projectileSpeed": 1200,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "8c3c10",
  "turnRate": 0.5,
  "stockMax": 8,
  "stockTime": 9999,
  "prefix": "Mercenary 05",
  "suffix": "",
  "abilities": [],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_fiend_unit_id: RawUnit = {
  name: "Fiend",
  sketchfabUrl: "https://sketchfab.com/models/4eba6c7b34ac416685e25c9d1db12e7a",
  image: require("#LTD2/Icons/Fiend.png"),
  splash: require("#LTD2/splashes/Fiend.png"),
  description: "Tanky, despite its thin limbs.",

  damage: Damage.Pierce,
  atkSpeed: 1.3333333333333333,
  onHitDmg: 27,

  armor: Armor.Swift,
  hp: 920,
};

export const fiend_unit_id: PlayableUnit = {
  ...raw_fiend_unit_id,
  price: 0,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "hermit_unit_id",
  "name": "Hermit",
  "image": "Icons/Hermit.png",
  "splash": "splashes/Hermit.png",
  "sketchfabUrl": "https://sketchfab.com/models/828043c7c62d4ce7b7ae521cd510e433",
  "description": "Its strange seeds passively heal allies.",
  "cost": 0,
  "mythium": 80,
  "income": 20,
  "supply": 0,
  "attackType": "Magic",
  "attackTypeDescription": "75% to Arcane<br />105% to Fortified<br />125% to Natural<br />100% to Swift",
  "armorType": "Natural",
  "armorTypeDescription": "90% from Impact<br />125% from Magic<br />85% from Pierce",
  "hp": 700,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 38,
  "damage": 38,
  "range": 550,
  "totalValue": 0,
  "bounty": 20,
  "tier": "0",
  "legion": "Mercenary",
  "legionType": "nether_legion_id",
  "movementType": "Ground",
  "spawnBias": 1,
  "spawnDelay": 1,
  "effectiveDps": 38,
  "effectiveThreat": 55,
  "movementSize": 0.1875,
  "backswing": 0.15000000596046448,
  "projectileSpeed": 2500,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "8c3c10",
  "turnRate": 0.5,
  "stockMax": 8,
  "stockTime": 9999,
  "prefix": "Mercenary 06",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "healing_aura_ability_id",
      "abilityClass": "",
      "name": "Healing Aura",
      "image": "Icons/HealingAura.png",
      "description": "Nearby allies recover 7 (14 for bosses) health per second. Does not stack.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/HealingAura.png' /> Healing Aura<br /><span style='color: #ffcc00'> Order String:</span> SimpleAura<br /><span style='color: #ffcc00'> Area of Effect:</span> 1000<br /><span style='color: #ffcc00'> Targets Considered:</span> Allied<br /><span style='color: #ffcc00'> Targets Excluded:</span> Boss, Building<br /><span style='color: #ffcc00'>Target Buff:</span> Healing Aura<br /><span style='color: #efe1a7'>--- Stats - HP Regeneration Bonus (Flat):</span> 7.00000<br /><span style='color: #ffcc00'>Caster Buff:</span> Healing Aura"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "healing_aura_boss_ability_id",
      "abilityClass": "hidden",
      "name": "Healing Aura (Boss)",
      "image": "Icons/HealingAura.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Healing Aura (Boss)<br /><span style='color: #ffcc00'> Order String:</span> SimpleAura<br /><span style='color: #ffcc00'> Area of Effect:</span> 1000<br /><span style='color: #ffcc00'> Targets Considered:</span> Allied<br /><span style='color: #ffcc00'> Targets Excluded:</span> NonBoss, Building<br /><span style='color: #ffcc00'>Target Buff:</span> Healing Aura (Boss Buff)<br /><span style='color: #efe1a7'>--- Stats - HP Regeneration Bonus (Flat):</span> 14.00000<br /><span style='color: #ffcc00'>Caster Buff:</span> Healing Aura (Caster Buff)"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_hermit_unit_id: RawUnit = {
  name: "Hermit",
  sketchfabUrl: "https://sketchfab.com/models/828043c7c62d4ce7b7ae521cd510e433",
  image: require("#LTD2/Icons/Hermit.png"),
  splash: require("#LTD2/splashes/Hermit.png"),
  description: "Its strange seeds passively heal allies.",

  damage: Damage.Magic,
  atkSpeed: 1,
  onHitDmg: 38,

  armor: Armor.Natural,
  hp: 700,
};

export const hermit_unit_id: PlayableUnit = {
  ...raw_hermit_unit_id,
  price: 0,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "dino_unit_id",
  "name": "Dino",
  "image": "Icons/Dino.png",
  "splash": "splashes/Dino.png",
  "sketchfabUrl": "https://sketchfab.com/models/3a2b3d0597534e88a63c9d1f4a620951",
  "description": "Not too smart, but very tanky.",
  "cost": 0,
  "mythium": 80,
  "income": 24,
  "supply": 0,
  "attackType": "Pierce",
  "attackTypeDescription": "115% to Arcane<br />80% to Fortified<br />85% to Natural<br />120% to Swift",
  "armorType": "Natural",
  "armorTypeDescription": "90% from Impact<br />125% from Magic<br />85% from Pierce",
  "hp": 1260,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.350000023841858,
  "attackSpeedDescriptor": "Slow",
  "mspd": 300,
  "dps": 46,
  "damage": 62,
  "range": 100,
  "totalValue": 0,
  "bounty": 24,
  "tier": "0",
  "legion": "Mercenary",
  "legionType": "nether_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.25,
  "spawnDelay": 0.25,
  "effectiveDps": 45.93000030517578,
  "effectiveThreat": 45.93000030517578,
  "movementSize": 0.1875,
  "backswing": 0.15000000596046448,
  "projectileSpeed": 1200,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "8c3c10",
  "turnRate": 0.5,
  "stockMax": 8,
  "stockTime": 9999,
  "prefix": "Mercenary 07",
  "suffix": "",
  "abilities": [],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_dino_unit_id: RawUnit = {
  name: "Dino",
  sketchfabUrl: "https://sketchfab.com/models/3a2b3d0597534e88a63c9d1f4a620951",
  image: require("#LTD2/Icons/Dino.png"),
  splash: require("#LTD2/splashes/Dino.png"),
  description: "Not too smart, but very tanky.",

  damage: Damage.Pierce,
  atkSpeed: 0.7407407276587887,
  onHitDmg: 62,

  armor: Armor.Natural,
  hp: 1260,
};

export const dino_unit_id: PlayableUnit = {
  ...raw_dino_unit_id,
  price: 0,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "imp_mercenary_unit_id",
  "name": "Imp",
  "image": "Icons/Imp.png",
  "splash": "splashes/Imp.png",
  "sketchfabUrl": "https://sketchfab.com/models/f7ed35aa5c2646aab39713a5cf6df260",
  "description": "Spawns an extra Imp when hired. Deals high damage.",
  "cost": 0,
  "mythium": 100,
  "income": 25,
  "supply": 0,
  "attackType": "Magic",
  "attackTypeDescription": "75% to Arcane<br />105% to Fortified<br />125% to Natural<br />100% to Swift",
  "armorType": "Natural",
  "armorTypeDescription": "90% from Impact<br />125% from Magic<br />85% from Pierce",
  "hp": 780,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.8199999928474426,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 46,
  "damage": 38,
  "range": 100,
  "totalValue": 0,
  "bounty": 13,
  "tier": "0",
  "legion": "Mercenary",
  "legionType": "nether_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.8500000238418579,
  "spawnDelay": 0.75,
  "effectiveDps": 46.34000015258789,
  "effectiveThreat": 46.34000015258789,
  "movementSize": 0.1875,
  "backswing": 0.15000000596046448,
  "projectileSpeed": 1200,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "8c3c10",
  "turnRate": 0.5,
  "stockMax": 8,
  "stockTime": 9999,
  "prefix": "Mercenary 08",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "diabolic_duo_ability_id",
      "abilityClass": "",
      "name": "Diabolic Duo",
      "image": "Icons/DiabolicDuo.png",
      "description": "Imps spawn in pairs",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/DiabolicDuo.png' /> Diabolic Duo"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_imp_mercenary_unit_id: RawUnit = {
  name: "Imp",
  sketchfabUrl: "https://sketchfab.com/models/f7ed35aa5c2646aab39713a5cf6df260",
  image: require("#LTD2/Icons/Imp.png"),
  splash: require("#LTD2/splashes/Imp.png"),
  description: "Spawns an extra Imp when hired. Deals high damage.",

  damage: Damage.Magic,
  atkSpeed: 1.2195122057593062,
  onHitDmg: 38,

  armor: Armor.Natural,
  hp: 780,
};

export const imp_mercenary_unit_id: PlayableUnit = {
  ...raw_imp_mercenary_unit_id,
  price: 0,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "cannoneer_unit_id",
  "name": "Cannoneer",
  "image": "Icons/Cannoneer.png",
  "splash": "splashes/Cannoneer.png",
  "sketchfabUrl": "https://sketchfab.com/models/23156ab520844c8d94885e486a80c2da",
  "description": "Her long-range cannon packs a punch.",
  "cost": 0,
  "mythium": 100,
  "income": 30,
  "supply": 0,
  "attackType": "Impact",
  "attackTypeDescription": "115% to Arcane<br />115% to Fortified<br />90% to Natural<br />80% to Swift",
  "armorType": "Swift",
  "armorTypeDescription": "80% from Impact<br />100% from Magic<br />120% from Pierce",
  "hp": 950,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.5199999809265137,
  "attackSpeedDescriptor": "Very Slow",
  "mspd": 300,
  "dps": 67,
  "damage": 102,
  "range": 550,
  "totalValue": 0,
  "bounty": 30,
  "tier": "0",
  "legion": "Mercenary",
  "legionType": "nether_legion_id",
  "movementType": "Ground",
  "spawnBias": 1,
  "spawnDelay": 1,
  "effectiveDps": 67.11000061035156,
  "effectiveThreat": 67.11000061035156,
  "movementSize": 0.1875,
  "backswing": 0.2199999988079071,
  "projectileSpeed": 3200,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "8c3c10",
  "turnRate": 0.5,
  "stockMax": 8,
  "stockTime": 9999,
  "prefix": "Mercenary 09",
  "suffix": "",
  "abilities": [],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_cannoneer_unit_id: RawUnit = {
  name: "Cannoneer",
  sketchfabUrl: "https://sketchfab.com/models/23156ab520844c8d94885e486a80c2da",
  image: require("#LTD2/Icons/Cannoneer.png"),
  splash: require("#LTD2/splashes/Cannoneer.png"),
  description: "Her long-range cannon packs a punch.",

  damage: Damage.Impact,
  atkSpeed: 0.6578947450975964,
  onHitDmg: 102,

  armor: Armor.Swift,
  hp: 950,
};

export const cannoneer_unit_id: PlayableUnit = {
  ...raw_cannoneer_unit_id,
  price: 0,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "safety_mole_unit_id",
  "name": "Safety Mole",
  "image": "Icons/SafetyMole.png",
  "splash": "splashes/SafetyMole.png",
  "sketchfabUrl": "https://sketchfab.com/models/d0e69dc6d8544c6cb2ed08e488e1d7a9",
  "description": "Safety first! Passively reduces incoming damage for nearby allies.",
  "cost": 0,
  "mythium": 120,
  "income": 30,
  "supply": 0,
  "attackType": "Impact",
  "attackTypeDescription": "115% to Arcane<br />115% to Fortified<br />90% to Natural<br />80% to Swift",
  "armorType": "Natural",
  "armorTypeDescription": "90% from Impact<br />125% from Magic<br />85% from Pierce",
  "hp": 1680,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.149999976158142,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 77,
  "damage": 88,
  "range": 100,
  "totalValue": 0,
  "bounty": 30,
  "tier": "0",
  "legion": "Mercenary",
  "legionType": "nether_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.8500000238418579,
  "spawnDelay": 0.75,
  "effectiveDps": 76.5199966430664,
  "effectiveThreat": 76.5199966430664,
  "movementSize": 0.1875,
  "backswing": 0.15000000596046448,
  "projectileSpeed": 1200,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "8c3c10",
  "turnRate": 0.5,
  "stockMax": 8,
  "stockTime": 9999,
  "prefix": "Mercenary 10",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "protection_aura_ability_id",
      "abilityClass": "",
      "name": "Safety Aura",
      "image": "Icons/SafetyAura.png",
      "description": "Nearby allies take 4 reduced damage from all sources. Does not stack.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/SafetyAura.png' /> Safety Aura<br /><span style='color: #ffcc00'> Order String:</span> SimpleAura<br /><span style='color: #ffcc00'> Area of Effect:</span> 1000<br /><span style='color: #ffcc00'> Targets Considered:</span> Allied<br /><span style='color: #ffcc00'> Targets Excluded:</span> Building<br /><span style='color: #ffcc00'>Target Buff:</span> Safety Aura<br /><span style='color: #efe1a7'>--- Stats - Damage Reduction (Flat):</span> 4.00000<br /><span style='color: #ffcc00'>Caster Buff:</span> Safety Aura"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_safety_mole_unit_id: RawUnit = {
  name: "Safety Mole",
  sketchfabUrl: "https://sketchfab.com/models/d0e69dc6d8544c6cb2ed08e488e1d7a9",
  image: require("#LTD2/Icons/SafetyMole.png"),
  splash: require("#LTD2/splashes/SafetyMole.png"),
  description: "Safety first! Passively reduces incoming damage for nearby allies.",

  damage: Damage.Impact,
  atkSpeed: 0.8695652354191746,
  onHitDmg: 88,

  armor: Armor.Natural,
  hp: 1680,
};

export const safety_mole_unit_id: PlayableUnit = {
  ...raw_safety_mole_unit_id,
  price: 0,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "drake_unit_id",
  "name": "Drake",
  "image": "Icons/Drake.png",
  "splash": "splashes/Drake.png",
  "sketchfabUrl": "https://sketchfab.com/models/cc74601b17b549e3bdf05658c9f4cb82",
  "description": "Though young, its fiery breath deals high damage.",
  "cost": 0,
  "mythium": 120,
  "income": 36,
  "supply": 0,
  "attackType": "Magic",
  "attackTypeDescription": "75% to Arcane<br />105% to Fortified<br />125% to Natural<br />100% to Swift",
  "armorType": "Swift",
  "armorTypeDescription": "80% from Impact<br />100% from Magic<br />120% from Pierce",
  "hp": 1320,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.0299999713897705,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 82,
  "damage": 84,
  "range": 550,
  "totalValue": 0,
  "bounty": 36,
  "tier": "0",
  "legion": "Mercenary",
  "legionType": "nether_legion_id",
  "movementType": "Air",
  "spawnBias": 1,
  "spawnDelay": 1,
  "effectiveDps": 81.55000305175781,
  "effectiveThreat": 81.55000305175781,
  "movementSize": 0.1875,
  "backswing": 0.15000000596046448,
  "projectileSpeed": 2500,
  "mastermindGroups": "None",
  "flags": "Air, Organic",
  "color": "8c3c10",
  "turnRate": 0.5,
  "stockMax": 8,
  "stockTime": 9999,
  "prefix": "Mercenary 11",
  "suffix": "",
  "abilities": [],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_drake_unit_id: RawUnit = {
  name: "Drake",
  sketchfabUrl: "https://sketchfab.com/models/cc74601b17b549e3bdf05658c9f4cb82",
  image: require("#LTD2/Icons/Drake.png"),
  splash: require("#LTD2/splashes/Drake.png"),
  description: "Though young, its fiery breath deals high damage.",

  damage: Damage.Magic,
  atkSpeed: 0.970873813375653,
  onHitDmg: 84,

  armor: Armor.Swift,
  hp: 1320,
};

export const drake_unit_id: PlayableUnit = {
  ...raw_drake_unit_id,
  price: 0,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "pack_leader_unit_id",
  "name": "Pack Leader",
  "image": "Icons/PackLeader.png",
  "splash": "splashes/PackLeader.png",
  "sketchfabUrl": "https://sketchfab.com/models/84ca6bc7639f4a168f58b160b415c6d5",
  "description": "War drums passively boost the damage of allies.",
  "cost": 0,
  "mythium": 160,
  "income": 40,
  "supply": 0,
  "attackType": "Impact",
  "attackTypeDescription": "115% to Arcane<br />115% to Fortified<br />90% to Natural<br />80% to Swift",
  "armorType": "Fortified",
  "armorTypeDescription": "115% from Impact<br />105% from Magic<br />80% from Pierce",
  "hp": 2000,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.25,
  "attackSpeedDescriptor": "Slow",
  "mspd": 300,
  "dps": 88,
  "damage": 110,
  "range": 100,
  "totalValue": 0,
  "bounty": 40,
  "tier": "0",
  "legion": "Mercenary",
  "legionType": "nether_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.8500000238418579,
  "spawnDelay": 0.75,
  "effectiveDps": 88,
  "effectiveThreat": 150,
  "movementSize": 0.1875,
  "backswing": 0.15000000596046448,
  "projectileSpeed": 1200,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "8c3c10",
  "turnRate": 0.5,
  "stockMax": 8,
  "stockTime": 9999,
  "prefix": "Mercenary 12",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "leadership_ability_id",
      "abilityClass": "",
      "name": "Leadership Aura",
      "image": "Icons/LeadershipAura.png",
      "description": "Increases the attack damage of nearby allies by 7 (14 for bosses). Does not stack.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/LeadershipAura.png' /> Leadership Aura<br /><span style='color: #ffcc00'> Order String:</span> SimpleAura<br /><span style='color: #ffcc00'> Area of Effect:</span> 1000<br /><span style='color: #ffcc00'> Targets Considered:</span> Allied<br /><span style='color: #ffcc00'> Targets Excluded:</span> Boss, Building<br /><span style='color: #ffcc00'>Target Buff:</span> Leadership Aura<br /><span style='color: #efe1a7'>--- Stats - Attack Damage Bonus (Flat):</span> 7<br /><span style='color: #ffcc00'>Caster Buff:</span> Leadership Aura"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "leadership_boss_ability_id",
      "abilityClass": "hidden",
      "name": "Leadership Aura (Boss)",
      "image": "Icons/LeadershipAura.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Leadership Aura (Boss)<br /><span style='color: #ffcc00'> Order String:</span> SimpleAura<br /><span style='color: #ffcc00'> Area of Effect:</span> 1000<br /><span style='color: #ffcc00'> Targets Considered:</span> Allied<br /><span style='color: #ffcc00'> Targets Excluded:</span> NonBoss, Building<br /><span style='color: #ffcc00'>Target Buff:</span> Leadership Aura (Boss Buff)<br /><span style='color: #efe1a7'>--- Stats - Attack Damage Bonus (Flat):</span> 14<br /><span style='color: #ffcc00'>Caster Buff:</span> Leadership Aura (Caster Buff)"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_pack_leader_unit_id: RawUnit = {
  name: "Pack Leader",
  sketchfabUrl: "https://sketchfab.com/models/84ca6bc7639f4a168f58b160b415c6d5",
  image: require("#LTD2/Icons/PackLeader.png"),
  splash: require("#LTD2/splashes/PackLeader.png"),
  description: "War drums passively boost the damage of allies.",

  damage: Damage.Impact,
  atkSpeed: 0.8,
  onHitDmg: 110,

  armor: Armor.Fortified,
  hp: 2000,
};

export const pack_leader_unit_id: PlayableUnit = {
  ...raw_pack_leader_unit_id,
  price: 0,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "froggo_unit_id",
  "name": "Froggo",
  "image": "Icons/Froggo.png",
  "splash": "splashes/Froggo.png",
  "sketchfabUrl": "https://sketchfab.com/models/2f7ddc30cf8b48e1a2bd9ffd15da8175",
  "description": "High damage ranged pet",
  "cost": 0,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Pierce",
  "attackTypeDescription": "115% to Arcane<br />80% to Fortified<br />85% to Natural<br />120% to Swift",
  "armorType": "Arcane",
  "armorTypeDescription": "115% from Impact<br />75% from Magic<br />115% from Pierce",
  "hp": 500,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.8500000238418579,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 42,
  "damage": 36,
  "range": 400,
  "totalValue": 0,
  "bounty": 3,
  "tier": "0",
  "legion": "Mercenary",
  "legionType": "nether_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.5,
  "spawnDelay": 0,
  "effectiveDps": 42.349998474121094,
  "effectiveThreat": 5,
  "movementSize": 0.1875,
  "backswing": 0.17000000178813934,
  "projectileSpeed": 2500,
  "mastermindGroups": "None",
  "flags": "Ground, Organic, Summoned, Undead",
  "color": "8c3c10",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Mercenary 12",
  "suffix": "(Special)",
  "abilities": [],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_froggo_unit_id: RawUnit = {
  name: "Froggo",
  sketchfabUrl: "https://sketchfab.com/models/2f7ddc30cf8b48e1a2bd9ffd15da8175",
  image: require("#LTD2/Icons/Froggo.png"),
  splash: require("#LTD2/splashes/Froggo.png"),
  description: "High damage ranged pet",

  damage: Damage.Pierce,
  atkSpeed: 1.1764705552361838,
  onHitDmg: 36,

  armor: Armor.Arcane,
  hp: 500,
};

export const froggo_unit_id: PlayableUnit = {
  ...raw_froggo_unit_id,
  price: 0,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "mimic_unit_id",
  "name": "Mimic",
  "image": "Icons/Mimic.png",
  "splash": "splashes/Mimic.png",
  "sketchfabUrl": "https://sketchfab.com/models/efb9549cc2374fa89a8a49f35a4a0066",
  "description": "Deals high damage and gains gold on kill.",
  "cost": 0,
  "mythium": 160,
  "income": 40,
  "supply": 0,
  "attackType": "Pierce",
  "attackTypeDescription": "115% to Arcane<br />80% to Fortified<br />85% to Natural<br />120% to Swift",
  "armorType": "Arcane",
  "armorTypeDescription": "115% from Impact<br />75% from Magic<br />115% from Pierce",
  "hp": 2350,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.8600000143051147,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 145,
  "damage": 125,
  "range": 100,
  "totalValue": 0,
  "bounty": 40,
  "tier": "0",
  "legion": "Mercenary",
  "legionType": "nether_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.8500000238418579,
  "spawnDelay": 0.75,
  "effectiveDps": 145.35000610351562,
  "effectiveThreat": 145.35000610351562,
  "movementSize": 0.1875,
  "backswing": 0.15000000596046448,
  "projectileSpeed": 1200,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "8c3c10",
  "turnRate": 0.5,
  "stockMax": 8,
  "stockTime": 9999,
  "prefix": "Mercenary 13",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "plunder_ability_id",
      "abilityClass": "",
      "name": "Plunder",
      "image": "Icons/Plunder.png",
      "description": "Gains +3 gold each time it kills a unit.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Plunder.png' /> Plunder<br /><span style='color: #ffcc00'> Order String:</span> Plunder<br /><span style='color: #ffcc00'> Gold Cost:</span> 3<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_mimic_unit_id: RawUnit = {
  name: "Mimic",
  sketchfabUrl: "https://sketchfab.com/models/efb9549cc2374fa89a8a49f35a4a0066",
  image: require("#LTD2/Icons/Mimic.png"),
  splash: require("#LTD2/splashes/Mimic.png"),
  description: "Deals high damage and gains gold on kill.",

  damage: Damage.Pierce,
  atkSpeed: 1.1627906783327278,
  onHitDmg: 125,

  armor: Armor.Arcane,
  hp: 2350,
};

export const mimic_unit_id: PlayableUnit = {
  ...raw_mimic_unit_id,
  price: 0,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "witch_unit_id",
  "name": "Witch",
  "image": "Icons/Witch.png",
  "splash": "splashes/Witch.png",
  "sketchfabUrl": "https://sketchfab.com/models/9b593ab3be7742bda7ac9b651b0e6054",
  "description": "Summons an endless supply of Froggos.",
  "cost": 0,
  "mythium": 200,
  "income": 50,
  "supply": 0,
  "attackType": "Magic",
  "attackTypeDescription": "75% to Arcane<br />105% to Fortified<br />125% to Natural<br />100% to Swift",
  "armorType": "Arcane",
  "armorTypeDescription": "115% from Impact<br />75% from Magic<br />115% from Pierce",
  "hp": 1800,
  "mana": 60,
  "hpRegen": 0,
  "manaRegen": 1,
  "attackSpeed": 0.6299999952316284,
  "attackSpeedDescriptor": "Fast",
  "mspd": 300,
  "dps": 127,
  "damage": 80,
  "range": 550,
  "totalValue": 0,
  "bounty": 40,
  "tier": "0",
  "legion": "Mercenary",
  "legionType": "nether_legion_id",
  "movementType": "Ground",
  "spawnBias": 1,
  "spawnDelay": 1,
  "effectiveDps": 126.9800033569336,
  "effectiveThreat": 175,
  "movementSize": 0.1875,
  "backswing": 0.2199999988079071,
  "projectileSpeed": 2500,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "8c3c10",
  "turnRate": 0.5,
  "stockMax": 8,
  "stockTime": 9999,
  "prefix": "Mercenary 14",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "summon_froggo_ability_id",
      "abilityClass": "",
      "name": "Summon Froggo",
      "image": "Icons/SummonFroggo.png",
      "description": "Summons a Froggo (pierce/arcane)",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/SummonFroggo.png' /> Summon Froggo<br /><span style='color: #ffcc00'> Order String:</span> RaiseImp<br /><span style='color: #ffcc00'> Mana Cost:</span> 59.00000<br /><span style='color: #ffcc00'> Duration:</span> 180.0000<br /><span style='color: #ffcc00'> Unit:</span> <img class='tooltip-icon' src='hud/img/Icons/Froggo.png' />Froggo"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "advanced_necromancy_ability_id",
      "abilityClass": "",
      "name": "Witchcraft",
      "image": "Icons/Witchcraft.png",
      "description": "Restores 3 mana when a nearby unit dies, excluding Froggos",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Witchcraft.png' /> Witchcraft<br /><span style='color: #ffcc00'> Order String:</span> Necromancy<br /><span style='color: #ffcc00'> Mana Cost:</span> 3.00000<br /><span style='color: #ffcc00'> Area of Effect:</span> 800<br /><span style='color: #ffcc00'> Targets Considered:</span> Allied, Enemy<br /><span style='color: #ffcc00'> Targets Excluded:</span> Undead"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_witch_unit_id: RawUnit = {
  name: "Witch",
  sketchfabUrl: "https://sketchfab.com/models/9b593ab3be7742bda7ac9b651b0e6054",
  image: require("#LTD2/Icons/Witch.png"),
  splash: require("#LTD2/splashes/Witch.png"),
  description: "Summons an endless supply of Froggos.",

  damage: Damage.Magic,
  atkSpeed: 1.5873015993156252,
  onHitDmg: 80,

  armor: Armor.Arcane,
  hp: 1800,
};

export const witch_unit_id: PlayableUnit = {
  ...raw_witch_unit_id,
  price: 0,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "ogre_unit_id",
  "name": "Ogre",
  "image": "Icons/Ogre.png",
  "splash": "splashes/Ogre.png",
  "sketchfabUrl": "https://sketchfab.com/models/4489f2369df6495f8d071d13805f32cc",
  "description": "Every third attack, deals bonus damage. Refreshes when it gets a kill.",
  "cost": 0,
  "mythium": 200,
  "income": 50,
  "supply": 0,
  "attackType": "Impact",
  "attackTypeDescription": "115% to Arcane<br />115% to Fortified<br />90% to Natural<br />80% to Swift",
  "armorType": "Fortified",
  "armorTypeDescription": "115% from Impact<br />105% from Magic<br />80% from Pierce",
  "hp": 2950,
  "mana": 3,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.7200000286102295,
  "attackSpeedDescriptor": "Fast",
  "mspd": 300,
  "dps": 139,
  "damage": 100,
  "range": 100,
  "totalValue": 0,
  "bounty": 50,
  "tier": "0",
  "legion": "Mercenary",
  "legionType": "nether_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.8500000238418579,
  "spawnDelay": 0.75,
  "effectiveDps": 225,
  "effectiveThreat": 200,
  "movementSize": 0.1875,
  "backswing": 0.44999998807907104,
  "projectileSpeed": 1200,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "8c3c10",
  "turnRate": 0.5,
  "stockMax": 8,
  "stockTime": 9999,
  "prefix": "Mercenary 15",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "knock_out_new_ability_id",
      "abilityClass": "",
      "name": "Knock Out",
      "image": "Icons/KnockOut.png",
      "description": "Deals 125 Impact damage every third attack. Refreshes on kill.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/KnockOut.png' /> Knock Out<br /><span style='color: #ffcc00'> Order String:</span> SearingArrows<br /><span style='color: #ffcc00'> Attack Type:</span> <img class='tooltip-icon' src='hud/img/icons/Impact.png' /> Impact<br /><span style='color: #ffcc00'> Mana Cost:</span> 3.00000<br /><span style='color: #ffcc00'> Damage Base:</span> 125.00000 (Ability Damage)<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "mana_on_attack_and_on_kill_ability_id",
      "abilityClass": "hidden",
      "name": "Mana On Attack And Kill",
      "image": "Icons/ManaOnAttackAndKill.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Mana On Attack And Kill<br /><span style='color: #ffcc00'> Order String:</span> MagneticAccelerator<br /><span style='color: #ffcc00'> Mana Cost:</span> 1.0000"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "abilityType": "start_with_0_mana_ability_id",
      "abilityClass": "hidden",
      "name": "Start With 0 Mana",
      "image": "Icons/StartWith0Mana.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Start With 0 Mana<br /><span style='color: #ffcc00'> Order String:</span> Generator<br /><span style='color: #ffcc00'> Percentage:</span> 0.0000<br /><span style='color: #ffcc00'> Targets Excluded:</span> Building"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_ogre_unit_id: RawUnit = {
  name: "Ogre",
  sketchfabUrl: "https://sketchfab.com/models/4489f2369df6495f8d071d13805f32cc",
  image: require("#LTD2/Icons/Ogre.png"),
  splash: require("#LTD2/splashes/Ogre.png"),
  description: "Every third attack, deals bonus damage. Refreshes when it gets a kill.",

  damage: Damage.Impact,
  atkSpeed: 1.3888888336994052,
  onHitDmg: 100,

  armor: Armor.Fortified,
  hp: 2950,
};

export const ogre_unit_id: PlayableUnit = {
  ...raw_ogre_unit_id,
  price: 0,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "ghost_knight_unit_id",
  "name": "Ghost Knight",
  "image": "Icons/GhostKnight.png",
  "splash": "splashes/GhostKnight.png",
  "sketchfabUrl": "https://sketchfab.com/models/6b42d15f543b4daab8cd70405e9316da",
  "description": "Takes reduced damage from all autoattacks (non-ability damage).",
  "cost": 0,
  "mythium": 240,
  "income": 60,
  "supply": 0,
  "attackType": "Impact",
  "attackTypeDescription": "115% to Arcane<br />115% to Fortified<br />90% to Natural<br />80% to Swift",
  "armorType": "Swift",
  "armorTypeDescription": "80% from Impact<br />100% from Magic<br />120% from Pierce",
  "hp": 3500,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.100000023841858,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 168,
  "damage": 185,
  "range": 100,
  "totalValue": 0,
  "bounty": 60,
  "tier": "0",
  "legion": "Mercenary",
  "legionType": "nether_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.25,
  "spawnDelay": 0.25,
  "effectiveDps": 168.17999267578125,
  "effectiveThreat": 168.17999267578125,
  "movementSize": 0.1875,
  "backswing": 0.20000000298023224,
  "projectileSpeed": 1200,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "8c3c10",
  "turnRate": 0.5,
  "stockMax": 8,
  "stockTime": 9999,
  "prefix": "Mercenary 16",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "evasion_ability_id",
      "abilityClass": "",
      "name": "Ghost Walker",
      "image": "Icons/GhostWalker.png",
      "description": "Reduces damage taken from autoattacks (non-ability damage) by 30%",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/GhostWalker.png' /> Ghost Walker<br /><span style='color: #ffcc00'> Order String:</span> AutoAttackDamageReduction<br /><span style='color: #ffcc00'> Percentage:</span> 0.30000"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_ghost_knight_unit_id: RawUnit = {
  name: "Ghost Knight",
  sketchfabUrl: "https://sketchfab.com/models/6b42d15f543b4daab8cd70405e9316da",
  image: require("#LTD2/Icons/GhostKnight.png"),
  splash: require("#LTD2/splashes/GhostKnight.png"),
  description: "Takes reduced damage from all autoattacks (non-ability damage).",

  damage: Damage.Impact,
  atkSpeed: 0.9090908893868948,
  onHitDmg: 185,

  armor: Armor.Swift,
  hp: 3500,
};

export const ghost_knight_unit_id: PlayableUnit = {
  ...raw_ghost_knight_unit_id,
  price: 0,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "four_eyes_unit_id",
  "name": "Four Eyes",
  "image": "Icons/FourEyes.png",
  "splash": "splashes/FourEyes.png",
  "sketchfabUrl": "https://sketchfab.com/models/6875502fb74848b9beed1902c4e55823",
  "description": "Deals more damage the more it attacks the same target.",
  "cost": 0,
  "mythium": 240,
  "income": 60,
  "supply": 0,
  "attackType": "Magic",
  "attackTypeDescription": "75% to Arcane<br />105% to Fortified<br />125% to Natural<br />100% to Swift",
  "armorType": "Natural",
  "armorTypeDescription": "90% from Impact<br />125% from Magic<br />85% from Pierce",
  "hp": 2500,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.4300000071525574,
  "attackSpeedDescriptor": "Very Fast",
  "mspd": 300,
  "dps": 151,
  "damage": 65,
  "range": 550,
  "totalValue": 0,
  "bounty": 60,
  "tier": "0",
  "legion": "Mercenary",
  "legionType": "nether_legion_id",
  "movementType": "Ground",
  "spawnBias": 1,
  "spawnDelay": 1,
  "effectiveDps": 400,
  "effectiveThreat": 450,
  "movementSize": 0.1875,
  "backswing": 0.20000000298023224,
  "projectileSpeed": 3000,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "8c3c10",
  "turnRate": 0.5,
  "stockMax": 8,
  "stockTime": 9999,
  "prefix": "Mercenary 17",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "duelist_ramping_ability_id",
      "abilityClass": "",
      "name": "Death Stare",
      "image": "Icons/DeathStare.png",
      "description": "Deals 5 bonus magic damage for each consecutive attack against the same target. Ability damage.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/DeathStare.png' /> Death Stare<br /><span style='color: #ffcc00'> Order String:</span> RampingDamage<br /><span style='color: #ffcc00'> Attack Type:</span> <img class='tooltip-icon' src='hud/img/icons/Magic.png' /> Magic<br /><span style='color: #ffcc00'> Damage Base:</span> 5.0000 (Ability Damage)<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_four_eyes_unit_id: RawUnit = {
  name: "Four Eyes",
  sketchfabUrl: "https://sketchfab.com/models/6875502fb74848b9beed1902c4e55823",
  image: require("#LTD2/Icons/FourEyes.png"),
  splash: require("#LTD2/splashes/FourEyes.png"),
  description: "Deals more damage the more it attacks the same target.",

  damage: Damage.Magic,
  atkSpeed: 2.3255813566654555,
  onHitDmg: 65,

  armor: Armor.Natural,
  hp: 2500,
};

export const four_eyes_unit_id: PlayableUnit = {
  ...raw_four_eyes_unit_id,
  price: 0,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "centaur_unit_id",
  "name": "Centaur",
  "image": "Icons/Centaur.png",
  "splash": "splashes/Centaur.png",
  "sketchfabUrl": "https://sketchfab.com/models/a5295203cccd4787a360a167d85ba618",
  "description": "High damage dealer. Cleaves up to 5 enemies.",
  "cost": 0,
  "mythium": 280,
  "income": 70,
  "supply": 0,
  "attackType": "Pierce",
  "attackTypeDescription": "115% to Arcane<br />80% to Fortified<br />85% to Natural<br />120% to Swift",
  "armorType": "Natural",
  "armorTypeDescription": "90% from Impact<br />125% from Magic<br />85% from Pierce",
  "hp": 4200,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.5099999904632568,
  "attackSpeedDescriptor": "Fast",
  "mspd": 300,
  "dps": 180,
  "damage": 92,
  "range": 100,
  "totalValue": 0,
  "bounty": 70,
  "tier": "0",
  "legion": "Mercenary",
  "legionType": "nether_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.8500000238418579,
  "spawnDelay": 0.75,
  "effectiveDps": 300,
  "effectiveThreat": 180,
  "movementSize": 0.1875,
  "backswing": 0.15000000596046448,
  "projectileSpeed": 1200,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "8c3c10",
  "turnRate": 0.5,
  "stockMax": 8,
  "stockTime": 9999,
  "prefix": "Mercenary 18",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "cleave_ability_id",
      "abilityClass": "",
      "name": "Cleave",
      "image": "Icons/Cleave.png",
      "description": "Each attack deals 20% damage to up to 5 nearby enemies",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Cleave.png' /> Cleave<br /><span style='color: #ffcc00'>Damage Type:</span> Ability Damage<br /><span style='color: #ffcc00'> Order String:</span> SplashAttack<br /><span style='color: #ffcc00'> Percentage:</span> 0.20000<br /><span style='color: #ffcc00'> Area of Effect:</span> 150<br /><span style='color: #ffcc00'> Max Targets:</span> 5<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_centaur_unit_id: RawUnit = {
  name: "Centaur",
  sketchfabUrl: "https://sketchfab.com/models/a5295203cccd4787a360a167d85ba618",
  image: require("#LTD2/Icons/Centaur.png"),
  splash: require("#LTD2/splashes/Centaur.png"),
  description: "High damage dealer. Cleaves up to 5 enemies.",

  damage: Damage.Pierce,
  atkSpeed: 1.9607843503911702,
  onHitDmg: 92,

  armor: Armor.Natural,
  hp: 4200,
};

export const centaur_unit_id: PlayableUnit = {
  ...raw_centaur_unit_id,
  price: 0,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "shaman_unit_id",
  "name": "Shaman",
  "image": "Icons/Shaman.png",
  "splash": "splashes/Shaman.png",
  "sketchfabUrl": "https://sketchfab.com/models/0b100a0b17a441bdbcaba14f32c62542",
  "description": "Boosts the damage and attack speed of nearby allies.",
  "cost": 0,
  "mythium": 320,
  "income": 80,
  "supply": 0,
  "attackType": "Magic",
  "attackTypeDescription": "75% to Arcane<br />105% to Fortified<br />125% to Natural<br />100% to Swift",
  "armorType": "Swift",
  "armorTypeDescription": "80% from Impact<br />100% from Magic<br />120% from Pierce",
  "hp": 3000,
  "mana": 3,
  "hpRegen": 0,
  "manaRegen": 1.2000000476837158,
  "attackSpeed": 0.7699999809265137,
  "attackSpeedDescriptor": "Fast",
  "mspd": 300,
  "dps": 143,
  "damage": 110,
  "range": 550,
  "totalValue": 0,
  "bounty": 80,
  "tier": "0",
  "legion": "Mercenary",
  "legionType": "nether_legion_id",
  "movementType": "Ground",
  "spawnBias": 1,
  "spawnDelay": 1,
  "effectiveDps": 142.86000061035156,
  "effectiveThreat": 300,
  "movementSize": 0.1875,
  "backswing": 0.2199999988079071,
  "projectileSpeed": 2500,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "8c3c10",
  "turnRate": 0.5,
  "stockMax": 8,
  "stockTime": 9999,
  "prefix": "Mercenary 19",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "blood_rush_ability_id",
      "abilityClass": "",
      "name": "Blood Rush",
      "image": "Icons/BloodRush.png",
      "description": "Increases attack speed of an ally by 10% and damage by 40 for 5 seconds",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/BloodRush.png' /> Blood Rush<br /><span style='color: #ffcc00'> Order String:</span> BloodRush<br /><span style='color: #ffcc00'> Projectile - Speed:</span> 1750<br /><span style='color: #ffcc00'> Mana Cost:</span> 3.00000<br /><span style='color: #ffcc00'> Duration:</span> 5.00000<br /><span style='color: #ffcc00'> Duration (Boss):</span> 5.00000<br /><span style='color: #ffcc00'> Targets Considered:</span> Allied<br /><span style='color: #ffcc00'> Targets Excluded:</span> Building<br /><span style='color: #ffcc00'>Target Buff:</span> Blood Rush<br /><span style='color: #efe1a7'>--- Stats - Attack Damage Bonus (Flat):</span> 40<br /><span style='color: #efe1a7'>--- Stats - Attack Rate Bonus (%):</span> 0.1000"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_shaman_unit_id: RawUnit = {
  name: "Shaman",
  sketchfabUrl: "https://sketchfab.com/models/0b100a0b17a441bdbcaba14f32c62542",
  image: require("#LTD2/Icons/Shaman.png"),
  splash: require("#LTD2/splashes/Shaman.png"),
  description: "Boosts the damage and attack speed of nearby allies.",

  damage: Damage.Magic,
  atkSpeed: 1.2987013308711195,
  onHitDmg: 110,

  armor: Armor.Swift,
  hp: 3000,
};

export const shaman_unit_id: PlayableUnit = {
  ...raw_shaman_unit_id,
  price: 0,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "siege_ram_unit_id",
  "name": "Siege Ram",
  "image": "Icons/SiegeRam.png",
  "splash": "splashes/SiegeRam.png",
  "sketchfabUrl": "https://sketchfab.com/models/d867b3c250c9461e8f7211851b2e5875",
  "description": "Takes reduced damage from ranged units.",
  "cost": 0,
  "mythium": 320,
  "income": 80,
  "supply": 0,
  "attackType": "Pierce",
  "attackTypeDescription": "115% to Arcane<br />80% to Fortified<br />85% to Natural<br />120% to Swift",
  "armorType": "Fortified",
  "armorTypeDescription": "115% from Impact<br />105% from Magic<br />80% from Pierce",
  "hp": 5550,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.2999999523162842,
  "attackSpeedDescriptor": "Slow",
  "mspd": 300,
  "dps": 225,
  "damage": 292,
  "range": 100,
  "totalValue": 0,
  "bounty": 80,
  "tier": "0",
  "legion": "Mercenary",
  "legionType": "nether_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.25,
  "spawnDelay": 0.25,
  "effectiveDps": 224.6199951171875,
  "effectiveThreat": 224.6199951171875,
  "movementSize": 0.1875,
  "backswing": 0.4000000059604645,
  "projectileSpeed": 0,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "8c3c10",
  "turnRate": 0.5,
  "stockMax": 8,
  "stockTime": 9999,
  "prefix": "Mercenary 20",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "deflection_ability_id",
      "abilityClass": "",
      "name": "Deflection",
      "image": "Icons/Deflection.png",
      "description": "Reduces damage taken from ranged units by 15%",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Deflection.png' /> Deflection<br /><span style='color: #ffcc00'> Order String:</span> RangedDamageReduction<br /><span style='color: #ffcc00'> Percentage:</span> 0.150"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_siege_ram_unit_id: RawUnit = {
  name: "Siege Ram",
  sketchfabUrl: "https://sketchfab.com/models/d867b3c250c9461e8f7211851b2e5875",
  image: require("#LTD2/Icons/SiegeRam.png"),
  splash: require("#LTD2/splashes/SiegeRam.png"),
  description: "Takes reduced damage from ranged units.",

  damage: Damage.Pierce,
  atkSpeed: 0.7692307974459868,
  onHitDmg: 292,

  armor: Armor.Fortified,
  hp: 5550,
};

export const siege_ram_unit_id: PlayableUnit = {
  ...raw_siege_ram_unit_id,
  price: 0,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "kraken_unit_id",
  "name": "Kraken",
  "image": "Icons/Kraken.png",
  "splash": "splashes/Kraken.png",
  "sketchfabUrl": "https://sketchfab.com/models/31c8bee6fa5546bfa9cd77f469f4fde0",
  "description": "Tanky. Grows stronger as the game progresses.",
  "cost": 0,
  "mythium": 400,
  "income": 100,
  "supply": 0,
  "attackType": "Impact",
  "attackTypeDescription": "115% to Arcane<br />115% to Fortified<br />90% to Natural<br />80% to Swift",
  "armorType": "Arcane",
  "armorTypeDescription": "115% from Impact<br />75% from Magic<br />115% from Pierce",
  "hp": 6950,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.149999976158142,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 253,
  "damage": 291,
  "range": 100,
  "totalValue": 0,
  "bounty": 100,
  "tier": "0",
  "legion": "Mercenary",
  "legionType": "nether_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.25,
  "spawnDelay": 0.25,
  "effectiveDps": 253.0399932861328,
  "effectiveThreat": 253.0399932861328,
  "movementSize": 0.1875,
  "backswing": 0.44999998807907104,
  "projectileSpeed": 1200,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "8c3c10",
  "turnRate": 0.5,
  "stockMax": 8,
  "stockTime": 9999,
  "prefix": "Mercenary 21",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "ancient_power_ability_id",
      "abilityClass": "",
      "name": "Ancient Power",
      "image": "Icons/AncientPower.png",
      "description": "Gains +0.75% damage and +0.75% damage reduction based on the current wave number",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/AncientPower.png' /> Ancient Power<br /><span style='color: #ffcc00'> Order String:</span> AncientPower<br /><span style='color: #ffcc00'> Attack Type:</span> <img class='tooltip-icon' src='hud/img/icons/Magic.png' /> Magic<br /><span style='color: #ffcc00'> Area of Effect:</span> 150<br /><span style='color: #ffcc00'> Custom Value 1:</span> 1.00000<br /><span style='color: #ffcc00'> Custom Value 2:</span> 1.00000<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy<br /><span style='color: #ffcc00'>Target Buff:</span> Ancient Power<br /><span style='color: #efe1a7'>--- Max Stacks:</span> 99<br /><span style='color: #efe1a7'>--- Stats - Attack Damage Bonus (%):</span> 0.00750<br /><span style='color: #efe1a7'>--- Stats - Damage Reduction (% Reduction):</span> 0.00750"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_kraken_unit_id: RawUnit = {
  name: "Kraken",
  sketchfabUrl: "https://sketchfab.com/models/31c8bee6fa5546bfa9cd77f469f4fde0",
  image: require("#LTD2/Icons/Kraken.png"),
  splash: require("#LTD2/splashes/Kraken.png"),
  description: "Tanky. Grows stronger as the game progresses.",

  damage: Damage.Impact,
  atkSpeed: 0.8695652354191746,
  onHitDmg: 291,

  armor: Armor.Arcane,
  hp: 6950,
};

export const kraken_unit_id: PlayableUnit = {
  ...raw_kraken_unit_id,
  price: 0,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "looter_unit_id",
  "name": "Looter",
  "image": "Icons/Looter.png",
  "splash": "splashes/Looter.png",
  "sketchfabUrl": "https://sketchfab.com/models/1702879bdc1c4ce58a066d83b560501d",
  "description": "High damage, but incredibly weak.",
  "cost": 10,
  "mythium": 0,
  "income": 0,
  "supply": 1,
  "attackType": "Impact",
  "attackTypeDescription": "115% to Arcane<br />115% to Fortified<br />90% to Natural<br />80% to Swift",
  "armorType": "Swift",
  "armorTypeDescription": "80% from Impact<br />100% from Magic<br />120% from Pierce",
  "hp": 80,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.2999999523162842,
  "attackSpeedDescriptor": "Slow",
  "mspd": 300,
  "dps": 5,
  "damage": 7,
  "range": 100,
  "totalValue": 10,
  "bounty": 0,
  "tier": "1",
  "legion": "Nomad",
  "legionType": "nomad_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.6000000238418579,
  "spawnDelay": 0,
  "effectiveDps": 5.380000114440918,
  "effectiveThreat": 5.380000114440918,
  "movementSize": 0.1875,
  "backswing": 0.6499999761581421,
  "projectileSpeed": 1200,
  "mastermindGroups": "Impact, Special1",
  "flags": "Ground, Organic",
  "color": "9c742d",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Nomad T1",
  "suffix": "",
  "abilities": [],
  "upgradesTo": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "unitType": "pack_rat_unit_id",
      "name": "Pack Rat",
      "image": "Icons/PackRat.png"
    }
  ],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_looter_unit_id: RawUnit = {
  name: "Looter",
  sketchfabUrl: "https://sketchfab.com/models/1702879bdc1c4ce58a066d83b560501d",
  image: require("#LTD2/Icons/Looter.png"),
  splash: require("#LTD2/splashes/Looter.png"),
  description: "High damage, but incredibly weak.",

  damage: Damage.Impact,
  atkSpeed: 0.7692307974459868,
  onHitDmg: 7,

  armor: Armor.Swift,
  hp: 80,
};

export const looter_unit_id: PlayableUnit = {
  ...raw_looter_unit_id,
  price: 10,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "pack_rat_unit_id",
  "name": "Pack Rat",
  "image": "Icons/PackRat.png",
  "splash": "splashes/PackRat.png",
  "sketchfabUrl": "https://sketchfab.com/models/8a046f536ef948d7ab2d8d723d87e1a0",
  "description": "You can activate Treasure Hunt, which will send it away to gather mythium.",
  "cost": 65,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Impact",
  "attackTypeDescription": "115% to Arcane<br />115% to Fortified<br />90% to Natural<br />80% to Swift",
  "armorType": "Swift",
  "armorTypeDescription": "80% from Impact<br />100% from Magic<br />120% from Pierce",
  "hp": 620,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.2999999523162842,
  "attackSpeedDescriptor": "Slow",
  "mspd": 300,
  "dps": 45,
  "damage": 59,
  "range": 100,
  "totalValue": 75,
  "bounty": 0,
  "tier": "1",
  "legion": "Nomad",
  "legionType": "nomad_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.6000000238418579,
  "spawnDelay": 0,
  "effectiveDps": 45.380001068115234,
  "effectiveThreat": 45.380001068115234,
  "movementSize": 0.1875,
  "backswing": 0.800000011920929,
  "projectileSpeed": 1200,
  "mastermindGroups": "SwiftOrNat, Impact",
  "flags": "Ground, Organic",
  "color": "9c742d",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Nomad T1U",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "treasure_hunt_ability_id",
      "abilityClass": "",
      "name": "Treasure Hunt",
      "image": "Icons/TreasureHunt.png",
      "description": "Your Pack Rat abandons this wave's battle. When the wave ends, it returns and you gain +10 mythium.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/TreasureHunt.png' /> Treasure Hunt<br /><span style='color: #ffcc00'> Order String:</span> SearchForTreasure<br /><span style='color: #ffcc00'> Unit:</span> <img class='tooltip-icon' src='hud/img/Icons/PackRat(Footprints).png' />Treasure Hunt"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "unitType": "looter_unit_id",
      "name": "Looter",
      "image": "Icons/Looter.png"
    }
  ],
  "amountSpawned": 0
}
*/

export const raw_pack_rat_unit_id: RawUnit = {
  name: "Pack Rat",
  sketchfabUrl: "https://sketchfab.com/models/8a046f536ef948d7ab2d8d723d87e1a0",
  image: require("#LTD2/Icons/PackRat.png"),
  splash: require("#LTD2/splashes/PackRat.png"),
  description: "You can activate Treasure Hunt, which will send it away to gather mythium.",

  damage: Damage.Impact,
  atkSpeed: 0.7692307974459868,
  onHitDmg: 59,

  armor: Armor.Swift,
  hp: 620,
};

export const pack_rat_unit_id: PlayableUnit = {
  ...raw_pack_rat_unit_id,
  price: 65,
  evolFrom: looter_unit_id,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "harpy_unit_id",
  "name": "Harpy",
  "image": "Icons/Harpy.png",
  "splash": "splashes/Harpy.png",
  "sketchfabUrl": "https://sketchfab.com/models/9c12dbe858e647cebf0e6dcd2286f56b",
  "description": "Flying. The longer it flies before entering battle, the faster it will attack.",
  "cost": 35,
  "mythium": 0,
  "income": 0,
  "supply": 1,
  "attackType": "Pierce",
  "attackTypeDescription": "115% to Arcane<br />80% to Fortified<br />85% to Natural<br />120% to Swift",
  "armorType": "Natural",
  "armorTypeDescription": "90% from Impact<br />125% from Magic<br />85% from Pierce",
  "hp": 220,
  "mana": 20,
  "hpRegen": 0,
  "manaRegen": 0.0010000000474974513,
  "attackSpeed": 0.7400000095367432,
  "attackSpeedDescriptor": "Fast",
  "mspd": 300,
  "dps": 22,
  "damage": 16,
  "range": 450,
  "totalValue": 35,
  "bounty": 0,
  "tier": "2",
  "legion": "Nomad",
  "legionType": "nomad_legion_id",
  "movementType": "Air",
  "spawnBias": 1,
  "spawnDelay": 0,
  "effectiveDps": 21.6200008392334,
  "effectiveThreat": 21.6200008392334,
  "movementSize": 0.1875,
  "backswing": 0.5299999713897705,
  "projectileSpeed": 2500,
  "mastermindGroups": "Pierce, Special1, Special2, Special3, Special4",
  "flags": "Air",
  "color": "9c742d",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Nomad T2",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "acrobatics_harpy_ability_id",
      "abilityClass": "",
      "name": "Acrobatics",
      "image": "Icons/Acrobatics.png",
      "description": "Starts with 0 mana, but gains mana when it moves before entering battle. Each 2 mana increases attack speed by 10%.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Acrobatics.png' /> Acrobatics<br /><span style='color: #ffcc00'> Order String:</span> Acrobatics<br /><span style='color: #ffcc00'> Mana Cost:</span> 2.00000<br /><span style='color: #ffcc00'> Percentage:</span> 0.0000<br /><span style='color: #ffcc00'> Custom Value 1:</span> 1.50000<br /><span style='color: #ffcc00'> Targets Excluded:</span> Building<br /><span style='color: #ffcc00'>Target Buff:</span> Acrobatics<br /><span style='color: #efe1a7'>--- Max Stacks:</span> 100<br /><span style='color: #efe1a7'>--- Stats - Attack Rate Bonus (%):</span> 0.1000"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "start_with_0_mana_ability_id",
      "abilityClass": "hidden",
      "name": "Start With 0 Mana",
      "image": "Icons/StartWith0Mana.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Start With 0 Mana<br /><span style='color: #ffcc00'> Order String:</span> Generator<br /><span style='color: #ffcc00'> Percentage:</span> 0.0000<br /><span style='color: #ffcc00'> Targets Excluded:</span> Building"
    }
  ],
  "upgradesTo": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "unitType": "sky_queen_unit_id",
      "name": "Sky Queen",
      "image": "Icons/SkyQueen.png"
    }
  ],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_harpy_unit_id: RawUnit = {
  name: "Harpy",
  sketchfabUrl: "https://sketchfab.com/models/9c12dbe858e647cebf0e6dcd2286f56b",
  image: require("#LTD2/Icons/Harpy.png"),
  splash: require("#LTD2/splashes/Harpy.png"),
  description: "Flying. The longer it flies before entering battle, the faster it will attack.",

  damage: Damage.Pierce,
  atkSpeed: 1.3513513339358236,
  onHitDmg: 16,

  armor: Armor.Natural,
  hp: 220,
};

export const harpy_unit_id: PlayableUnit = {
  ...raw_harpy_unit_id,
  price: 35,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "sky_queen_unit_id",
  "name": "Sky Queen",
  "image": "Icons/SkyQueen.png",
  "splash": "splashes/SkyQueen.png",
  "sketchfabUrl": "https://sketchfab.com/models/be690a4fe4ef4b07ac7f36b8d31b4df9",
  "description": "Flying. Now gains attack speed whenever it attacks.",
  "cost": 165,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Pierce",
  "attackTypeDescription": "115% to Arcane<br />80% to Fortified<br />85% to Natural<br />120% to Swift",
  "armorType": "Natural",
  "armorTypeDescription": "90% from Impact<br />125% from Magic<br />85% from Pierce",
  "hp": 1250,
  "mana": 120,
  "hpRegen": 0,
  "manaRegen": 0.0010000000474974513,
  "attackSpeed": 0.7400000095367432,
  "attackSpeedDescriptor": "Fast",
  "mspd": 300,
  "dps": 97,
  "damage": 72,
  "range": 450,
  "totalValue": 200,
  "bounty": 0,
  "tier": "2",
  "legion": "Nomad",
  "legionType": "nomad_legion_id",
  "movementType": "Air",
  "spawnBias": 1,
  "spawnDelay": 0,
  "effectiveDps": 97.30000305175781,
  "effectiveThreat": 97.30000305175781,
  "movementSize": 0.1875,
  "backswing": 0.5,
  "projectileSpeed": 3000,
  "mastermindGroups": "None",
  "flags": "Air",
  "color": "9c742d",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Nomad T2U",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "acrobatics_ability_id",
      "abilityClass": "",
      "name": "Acrobatics",
      "image": "Icons/Acrobatics.png",
      "description": "Starts with 0 mana, but gains mana when it moves before entering battle. Each 8 mana increases attack speed by 10%.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Acrobatics.png' /> Acrobatics<br /><span style='color: #ffcc00'> Order String:</span> Acrobatics<br /><span style='color: #ffcc00'> Mana Cost:</span> 8.00000<br /><span style='color: #ffcc00'> Percentage:</span> 0.0000<br /><span style='color: #ffcc00'> Custom Value 1:</span> 6.00000<br /><span style='color: #ffcc00'> Targets Excluded:</span> Building<br /><span style='color: #ffcc00'>Target Buff:</span> Acrobatics<br /><span style='color: #efe1a7'>--- Max Stacks:</span> 100<br /><span style='color: #efe1a7'>--- Stats - Attack Rate Bonus (%):</span> 0.1000"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "crimson_claws_ability_id",
      "abilityClass": "",
      "name": "Crimson Claws",
      "image": "Icons/CrimsonClaws.png",
      "description": "Restores 0.8 mana with each attack",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/CrimsonClaws.png' /> Crimson Claws<br /><span style='color: #ffcc00'> Order String:</span> MagneticAccelerator<br /><span style='color: #ffcc00'> Mana Cost:</span> 0.80000"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "abilityType": "start_with_0_mana_ability_id",
      "abilityClass": "hidden",
      "name": "Start With 0 Mana",
      "image": "Icons/StartWith0Mana.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Start With 0 Mana<br /><span style='color: #ffcc00'> Order String:</span> Generator<br /><span style='color: #ffcc00'> Percentage:</span> 0.0000<br /><span style='color: #ffcc00'> Targets Excluded:</span> Building"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 3,
      "unitType": "harpy_unit_id",
      "name": "Harpy",
      "image": "Icons/Harpy.png"
    }
  ],
  "amountSpawned": 0
}
*/

export const raw_sky_queen_unit_id: RawUnit = {
  name: "Sky Queen",
  sketchfabUrl: "https://sketchfab.com/models/be690a4fe4ef4b07ac7f36b8d31b4df9",
  image: require("#LTD2/Icons/SkyQueen.png"),
  splash: require("#LTD2/splashes/SkyQueen.png"),
  description: "Flying. Now gains attack speed whenever it attacks.",

  damage: Damage.Pierce,
  atkSpeed: 1.3513513339358236,
  onHitDmg: 72,

  armor: Armor.Natural,
  hp: 1250,
};

export const sky_queen_unit_id: PlayableUnit = {
  ...raw_sky_queen_unit_id,
  price: 165,
  evolFrom: harpy_unit_id,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "warg_unit_id",
  "name": "Warg",
  "image": "Icons/Warg.png",
  "splash": "splashes/Warg.png",
  "sketchfabUrl": "https://sketchfab.com/models/accfce1e31fd44198a27b82aed96b42a",
  "description": "Depending on its upgrade path, it will prefer to be near cheap fighters or expensive fighters.",
  "cost": 85,
  "mythium": 0,
  "income": 0,
  "supply": 1,
  "attackType": "Pierce",
  "attackTypeDescription": "115% to Arcane<br />80% to Fortified<br />85% to Natural<br />120% to Swift",
  "armorType": "Natural",
  "armorTypeDescription": "90% from Impact<br />125% from Magic<br />85% from Pierce",
  "hp": 960,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.9200000166893005,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 34,
  "damage": 31,
  "range": 100,
  "totalValue": 85,
  "bounty": 0,
  "tier": "3",
  "legion": "Nomad",
  "legionType": "nomad_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.30000001192092896,
  "spawnDelay": 0,
  "effectiveDps": 33.70000076293945,
  "effectiveThreat": 33.70000076293945,
  "movementSize": 0.1875,
  "backswing": 0.6200000047683716,
  "projectileSpeed": 1200,
  "mastermindGroups": "FortOrNat, SwiftOrNat, Pierce",
  "flags": "Ground, Organic",
  "color": "9c742d",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Nomad T3",
  "suffix": "",
  "abilities": [],
  "upgradesTo": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "unitType": "lioness_unit_id",
      "name": "Lioness",
      "image": "Icons/Lioness.png"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "unitType": "alpha_male_unit_id",
      "name": "Alpha Male",
      "image": "Icons/AlphaMale.png"
    }
  ],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_warg_unit_id: RawUnit = {
  name: "Warg",
  sketchfabUrl: "https://sketchfab.com/models/accfce1e31fd44198a27b82aed96b42a",
  image: require("#LTD2/Icons/Warg.png"),
  splash: require("#LTD2/splashes/Warg.png"),
  description: "Depending on its upgrade path, it will prefer to be near cheap fighters or expensive fighters.",

  damage: Damage.Pierce,
  atkSpeed: 1.086956502021148,
  onHitDmg: 31,

  armor: Armor.Natural,
  hp: 960,
};

export const warg_unit_id: PlayableUnit = {
  ...raw_warg_unit_id,
  price: 85,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "alpha_male_unit_id",
  "name": "Alpha Male",
  "image": "Icons/AlphaMale.png",
  "splash": "splashes/AlphaMale.png",
  "sketchfabUrl": "https://sketchfab.com/models/aeb9bfe1159b4e2b924373c573dd1eea",
  "description": "Powerful, but gets weaker when built next to expensive units.",
  "cost": 200,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Pierce",
  "attackTypeDescription": "115% to Arcane<br />80% to Fortified<br />85% to Natural<br />120% to Swift",
  "armorType": "Natural",
  "armorTypeDescription": "90% from Impact<br />125% from Magic<br />85% from Pierce",
  "hp": 3360,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.9200000166893005,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 112,
  "damage": 103,
  "range": 100,
  "totalValue": 285,
  "bounty": 0,
  "tier": "3",
  "legion": "Nomad",
  "legionType": "nomad_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.20000000298023224,
  "spawnDelay": 0,
  "effectiveDps": 111.95999908447266,
  "effectiveThreat": 111.95999908447266,
  "movementSize": 0.1875,
  "backswing": 0.550000011920929,
  "projectileSpeed": 1200,
  "mastermindGroups": "SwiftOrNat, Pierce",
  "flags": "Ground, Organic",
  "color": "9c742d",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Nomad T3U",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "dominion_ability_id",
      "abilityClass": "",
      "name": "Dominion",
      "image": "Icons/Dominion.png",
      "description": "Has powerful combat stats but damage and damage reduction are decreased by 5% for each adjacently-built unit that is equal or higher total fighter value than Alpha Male (285 value)",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Dominion.png' /> Dominion<br /><span style='color: #ffcc00'> Order String:</span> Dominion<br /><span style='color: #ffcc00'> Area of Effect:</span> 120<br /><span style='color: #ffcc00'> Custom Value 1:</span> 285.0000<br /><span style='color: #ffcc00'> Custom Value 2:</span> 9999.0000<br /><span style='color: #ffcc00'> Targets Considered:</span> Allied, Invulnerable<br /><span style='color: #ffcc00'>Target Buff:</span> Dominion<br /><span style='color: #efe1a7'>--- Max Stacks:</span> 6<br /><span style='color: #efe1a7'>--- Stats - Attack Damage Bonus (%):</span> -0.0500<br /><span style='color: #efe1a7'>--- Stats - Damage Reduction (% Reduction):</span> -0.0500"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "unitType": "warg_unit_id",
      "name": "Warg",
      "image": "Icons/Warg.png"
    }
  ],
  "amountSpawned": 0
}
*/

export const raw_alpha_male_unit_id: RawUnit = {
  name: "Alpha Male",
  sketchfabUrl: "https://sketchfab.com/models/aeb9bfe1159b4e2b924373c573dd1eea",
  image: require("#LTD2/Icons/AlphaMale.png"),
  splash: require("#LTD2/splashes/AlphaMale.png"),
  description: "Powerful, but gets weaker when built next to expensive units.",

  damage: Damage.Pierce,
  atkSpeed: 1.086956502021148,
  onHitDmg: 103,

  armor: Armor.Natural,
  hp: 3360,
};

export const alpha_male_unit_id: PlayableUnit = {
  ...raw_alpha_male_unit_id,
  price: 200,
  evolFrom: warg_unit_id,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "lioness_unit_id",
  "name": "Lioness",
  "image": "Icons/Lioness.png",
  "splash": "splashes/Lioness.png",
  "sketchfabUrl": "https://sketchfab.com/models/c1bb6f81db3947d6a4448564777ede93",
  "description": "Gets stronger when built next to expensive units.",
  "cost": 175,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Pierce",
  "attackTypeDescription": "115% to Arcane<br />80% to Fortified<br />85% to Natural<br />120% to Swift",
  "armorType": "Natural",
  "armorTypeDescription": "90% from Impact<br />125% from Magic<br />85% from Pierce",
  "hp": 2300,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.8999999761581421,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 108,
  "damage": 97,
  "range": 100,
  "totalValue": 260,
  "bounty": 0,
  "tier": "3",
  "legion": "Nomad",
  "legionType": "nomad_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.5,
  "spawnDelay": 0,
  "effectiveDps": 107.77999877929688,
  "effectiveThreat": 107.77999877929688,
  "movementSize": 0.1875,
  "backswing": 0.4000000059604645,
  "projectileSpeed": 1200,
  "mastermindGroups": "SwiftOrNat, Pierce",
  "flags": "Ground, Organic",
  "color": "9c742d",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Nomad T3U",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "entourage_ability_id",
      "abilityClass": "",
      "name": "Entourage",
      "image": "Icons/Entourage.png",
      "description": "Damage and damage reduction are increased by 8% for each adjacently-built unit that is higher total fighter value than Lioness (260 value)",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Entourage.png' /> Entourage<br /><span style='color: #ffcc00'> Order String:</span> Dominion<br /><span style='color: #ffcc00'> Area of Effect:</span> 120<br /><span style='color: #ffcc00'> Custom Value 1:</span> 261.0000<br /><span style='color: #ffcc00'> Custom Value 2:</span> 9999.0000<br /><span style='color: #ffcc00'> Targets Considered:</span> Allied, Invulnerable<br /><span style='color: #ffcc00'>Target Buff:</span> Entourage<br /><span style='color: #efe1a7'>--- Max Stacks:</span> 6<br /><span style='color: #efe1a7'>--- Stats - Attack Damage Bonus (%):</span> 0.0800<br /><span style='color: #efe1a7'>--- Stats - Damage Reduction (% Reduction):</span> 0.0800"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "unitType": "warg_unit_id",
      "name": "Warg",
      "image": "Icons/Warg.png"
    }
  ],
  "amountSpawned": 0
}
*/

export const raw_lioness_unit_id: RawUnit = {
  name: "Lioness",
  sketchfabUrl: "https://sketchfab.com/models/c1bb6f81db3947d6a4448564777ede93",
  image: require("#LTD2/Icons/Lioness.png"),
  splash: require("#LTD2/splashes/Lioness.png"),
  description: "Gets stronger when built next to expensive units.",

  damage: Damage.Pierce,
  atkSpeed: 1.1111111405455043,
  onHitDmg: 97,

  armor: Armor.Natural,
  hp: 2300,
};

export const lioness_unit_id: PlayableUnit = {
  ...raw_lioness_unit_id,
  price: 175,
  evolFrom: warg_unit_id,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "desert_pilgrim_unit_id",
  "name": "Desert Pilgrim",
  "image": "Icons/DesertPilgrim.png",
  "splash": "splashes/DesertPilgrim.png",
  "sketchfabUrl": "https://sketchfab.com/models/70989422232749ca8fa682c43c64fb49",
  "description": "Has a bouncing heal.",
  "cost": 135,
  "mythium": 0,
  "income": 0,
  "supply": 1,
  "attackType": "Magic",
  "attackTypeDescription": "75% to Arcane<br />105% to Fortified<br />125% to Natural<br />100% to Swift",
  "armorType": "Fortified",
  "armorTypeDescription": "115% from Impact<br />105% from Magic<br />80% from Pierce",
  "hp": 870,
  "mana": 9,
  "hpRegen": 0,
  "manaRegen": 0.8399999737739563,
  "attackSpeed": 1.2000000476837158,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 50,
  "damage": 60,
  "range": 350,
  "totalValue": 135,
  "bounty": 0,
  "tier": "4",
  "legion": "Nomad",
  "legionType": "nomad_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.800000011920929,
  "spawnDelay": 0,
  "effectiveDps": 50,
  "effectiveThreat": 50,
  "movementSize": 0.1875,
  "backswing": 0.4000000059604645,
  "projectileSpeed": 2300,
  "mastermindGroups": "Magic, Special1, Special2, Special3, Special4",
  "flags": "Ground, Organic",
  "color": "9c742d",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Nomad T4",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "chain_heal_ability_id",
      "abilityClass": "",
      "name": "Chain Heal",
      "image": "Icons/ChainHeal.png",
      "description": "Heals a nearby ally for 120 health, bouncing to 3 nearby allies",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/ChainHeal.png' /> Chain Heal<br /><span style='color: #ffcc00'> Order String:</span> ChainHeal<br /><span style='color: #ffcc00'> Projectile - Speed:</span> 5000<br /><span style='color: #ffcc00'> Mana Cost:</span> 7.95000<br /><span style='color: #ffcc00'> Damage Healed:</span> 120.00000<br /><span style='color: #ffcc00'> Percentage:</span> 1.0000<br /><span style='color: #ffcc00'> Area of Effect:</span> 1000<br /><span style='color: #ffcc00'> Bounces:</span> 3<br /><span style='color: #ffcc00'> Targets Considered:</span> Allied, NonBoss, Invulnerable<br /><span style='color: #ffcc00'> Targets Excluded:</span> CannotBeHealed"
    }
  ],
  "upgradesTo": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "unitType": "lost_chieftain_unit_id",
      "name": "Lost Chieftain",
      "image": "Icons/LostChieftain.png"
    }
  ],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_desert_pilgrim_unit_id: RawUnit = {
  name: "Desert Pilgrim",
  sketchfabUrl: "https://sketchfab.com/models/70989422232749ca8fa682c43c64fb49",
  image: require("#LTD2/Icons/DesertPilgrim.png"),
  splash: require("#LTD2/splashes/DesertPilgrim.png"),
  description: "Has a bouncing heal.",

  damage: Damage.Magic,
  atkSpeed: 0.8333333002196431,
  onHitDmg: 60,

  armor: Armor.Fortified,
  hp: 870,
};

export const desert_pilgrim_unit_id: PlayableUnit = {
  ...raw_desert_pilgrim_unit_id,
  price: 135,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "lost_chieftain_unit_id",
  "name": "Lost Chieftain",
  "image": "Icons/LostChieftain.png",
  "splash": "splashes/LostChieftain.png",
  "sketchfabUrl": "https://sketchfab.com/models/fde8175054de4692be8ee6f0cd38bd9d",
  "description": "Blesses an ally with the spirit of earth, increasing its max health.",
  "cost": 255,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Magic",
  "attackTypeDescription": "75% to Arcane<br />105% to Fortified<br />125% to Natural<br />100% to Swift",
  "armorType": "Fortified",
  "armorTypeDescription": "115% from Impact<br />105% from Magic<br />80% from Pierce",
  "hp": 2350,
  "mana": 1,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.2000000476837158,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 169,
  "damage": 203,
  "range": 350,
  "totalValue": 390,
  "bounty": 0,
  "tier": "4",
  "legion": "Nomad",
  "legionType": "nomad_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.800000011920929,
  "spawnDelay": 0,
  "effectiveDps": 169.1699981689453,
  "effectiveThreat": 169.1699981689453,
  "movementSize": 0.1875,
  "backswing": 0.4000000059604645,
  "projectileSpeed": 2300,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "9c742d",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Nomad T4U",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "gaia_shield_ability_id",
      "abilityClass": "",
      "name": "Gaia Shield",
      "image": "Icons/GaiaShield.png",
      "description": "Increases the max health of the forward-most positioned fighter by 1500. Casts once. Stacks with itself.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/GaiaShield.png' /> Gaia Shield<br /><span style='color: #ffcc00'> Order String:</span> GaiaShield<br /><span style='color: #ffcc00'> Projectile - Speed:</span> 2000<br /><span style='color: #ffcc00'> Mana Cost:</span> 1.00000<br /><span style='color: #ffcc00'> Duration:</span> 9999.0000<br /><span style='color: #ffcc00'> Duration (Boss):</span> 9999.0000<br /><span style='color: #ffcc00'> Targets Considered:</span> Allied, NonBoss<br /><span style='color: #ffcc00'>Target Buff:</span> Gaia Shield<br /><span style='color: #efe1a7'>--- Max Stacks:</span> 100<br /><span style='color: #efe1a7'>--- Stats - Max HP Bonus (Flat):</span> 1500.00000"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "unitType": "desert_pilgrim_unit_id",
      "name": "Desert Pilgrim",
      "image": "Icons/DesertPilgrim.png"
    }
  ],
  "amountSpawned": 0
}
*/

export const raw_lost_chieftain_unit_id: RawUnit = {
  name: "Lost Chieftain",
  sketchfabUrl: "https://sketchfab.com/models/fde8175054de4692be8ee6f0cd38bd9d",
  image: require("#LTD2/Icons/LostChieftain.png"),
  splash: require("#LTD2/splashes/LostChieftain.png"),
  description: "Blesses an ally with the spirit of earth, increasing its max health.",

  damage: Damage.Magic,
  atkSpeed: 0.8333333002196431,
  onHitDmg: 203,

  armor: Armor.Fortified,
  hp: 2350,
};

export const lost_chieftain_unit_id: PlayableUnit = {
  ...raw_lost_chieftain_unit_id,
  price: 255,
  evolFrom: desert_pilgrim_unit_id,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "sand_badger_unit_id",
  "name": "Sand Badger",
  "image": "Icons/SandBadger.png",
  "splash": "splashes/SandBadger.png",
  "sketchfabUrl": "https://sketchfab.com/models/dcc148e3497941e386101378ee34d4a5",
  "description": "Tanky. Not known for its offense.",
  "cost": 195,
  "mythium": 0,
  "income": 0,
  "supply": 1,
  "attackType": "Impact",
  "attackTypeDescription": "115% to Arcane<br />115% to Fortified<br />90% to Natural<br />80% to Swift",
  "armorType": "Arcane",
  "armorTypeDescription": "115% from Impact<br />75% from Magic<br />115% from Pierce",
  "hp": 2460,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.2999999523162842,
  "attackSpeedDescriptor": "Slow",
  "mspd": 300,
  "dps": 57,
  "damage": 74,
  "range": 100,
  "totalValue": 195,
  "bounty": 0,
  "tier": "5",
  "legion": "Nomad",
  "legionType": "nomad_legion_id",
  "movementType": "Ground",
  "spawnBias": 0,
  "spawnDelay": 0,
  "effectiveDps": 56.91999816894531,
  "effectiveThreat": 56.91999816894531,
  "movementSize": 0.1875,
  "backswing": 0.6000000238418579,
  "projectileSpeed": 1200,
  "mastermindGroups": "Arcane, Impact",
  "flags": "Ground, Organic",
  "color": "9c742d",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Nomad T5",
  "suffix": "",
  "abilities": [],
  "upgradesTo": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "unitType": "iron_scales_unit_id",
      "name": "Iron Scales",
      "image": "Icons/IronScales.png"
    }
  ],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_sand_badger_unit_id: RawUnit = {
  name: "Sand Badger",
  sketchfabUrl: "https://sketchfab.com/models/dcc148e3497941e386101378ee34d4a5",
  image: require("#LTD2/Icons/SandBadger.png"),
  splash: require("#LTD2/splashes/SandBadger.png"),
  description: "Tanky. Not known for its offense.",

  damage: Damage.Impact,
  atkSpeed: 0.7692307974459868,
  onHitDmg: 74,

  armor: Armor.Arcane,
  hp: 2460,
};

export const sand_badger_unit_id: PlayableUnit = {
  ...raw_sand_badger_unit_id,
  price: 195,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "iron_scales_unit_id",
  "name": "Iron Scales",
  "image": "Icons/IronScales.png",
  "splash": "splashes/IronScales.png",
  "sketchfabUrl": "https://sketchfab.com/models/3085d73e7f164d91a706753f8759adf9",
  "description": "The ultimate arcane tank.",
  "cost": 335,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Impact",
  "attackTypeDescription": "115% to Arcane<br />115% to Fortified<br />90% to Natural<br />80% to Swift",
  "armorType": "Arcane",
  "armorTypeDescription": "115% from Impact<br />75% from Magic<br />115% from Pierce",
  "hp": 6680,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.2999999523162842,
  "attackSpeedDescriptor": "Slow",
  "mspd": 300,
  "dps": 155,
  "damage": 201,
  "range": 100,
  "totalValue": 530,
  "bounty": 0,
  "tier": "5",
  "legion": "Nomad",
  "legionType": "nomad_legion_id",
  "movementType": "Ground",
  "spawnBias": 0,
  "spawnDelay": 0,
  "effectiveDps": 154.6199951171875,
  "effectiveThreat": 154.6199951171875,
  "movementSize": 0.1875,
  "backswing": 0.550000011920929,
  "projectileSpeed": 1200,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "9c742d",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Nomad T5U",
  "suffix": "",
  "abilities": [],
  "upgradesTo": [],
  "upgradesFrom": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "unitType": "sand_badger_unit_id",
      "name": "Sand Badger",
      "image": "Icons/SandBadger.png"
    }
  ],
  "amountSpawned": 0
}
*/

export const raw_iron_scales_unit_id: RawUnit = {
  name: "Iron Scales",
  sketchfabUrl: "https://sketchfab.com/models/3085d73e7f164d91a706753f8759adf9",
  image: require("#LTD2/Icons/IronScales.png"),
  splash: require("#LTD2/splashes/IronScales.png"),
  description: "The ultimate arcane tank.",

  damage: Damage.Impact,
  atkSpeed: 0.7692307974459868,
  onHitDmg: 201,

  armor: Armor.Arcane,
  hp: 6680,
};

export const iron_scales_unit_id: PlayableUnit = {
  ...raw_iron_scales_unit_id,
  price: 335,
  evolFrom: sand_badger_unit_id,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "great_boar_unit_id",
  "name": "Great Boar",
  "image": "Icons/GreatBoar.png",
  "splash": "splashes/GreatBoar.png",
  "sketchfabUrl": "https://sketchfab.com/models/37c862665fea46908b00cb64e058a881",
  "description": "Tanky. Charges into battle, accelerating with each step. Deals splash damage when it hits.",
  "cost": 270,
  "mythium": 0,
  "income": 0,
  "supply": 1,
  "attackType": "Pierce",
  "attackTypeDescription": "115% to Arcane<br />80% to Fortified<br />85% to Natural<br />120% to Swift",
  "armorType": "Fortified",
  "armorTypeDescription": "115% from Impact<br />105% from Magic<br />80% from Pierce",
  "hp": 3200,
  "mana": 25,
  "hpRegen": 0,
  "manaRegen": 0.0010000000474974513,
  "attackSpeed": 1,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 63,
  "damage": 63,
  "range": 100,
  "totalValue": 270,
  "bounty": 0,
  "tier": "6",
  "legion": "Nomad",
  "legionType": "nomad_legion_id",
  "movementType": "Ground",
  "spawnBias": 0,
  "spawnDelay": 0,
  "effectiveDps": 63,
  "effectiveThreat": 63,
  "movementSize": 0.1875,
  "backswing": 0.3499999940395355,
  "projectileSpeed": 1200,
  "mastermindGroups": "FortOrNat, Special1",
  "flags": "Ground, Organic",
  "color": "9c742d",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Nomad T6",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "stampede_ability_id",
      "abilityClass": "",
      "name": "Stampede",
      "image": "Icons/Stampede.png",
      "description": "Charges into battle as soon as the wave spawns. After reaching the enemy, deals Impact damage and slows targets' attack speed by 6%. The further the charge, the more damage it deals.  Ability damage.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Stampede.png' /> Stampede<br /><span style='color: #ffcc00'> Order String:</span> Stampede<br /><span style='color: #ffcc00'> Attack Type:</span> <img class='tooltip-icon' src='hud/img/icons/Impact.png' /> Impact<br /><span style='color: #ffcc00'> Mana Cost:</span> 1.66660<br /><span style='color: #ffcc00'> Damage Base:</span> 9.00000 (Ability Damage)<br /><span style='color: #ffcc00'> Percentage:</span> 0.00000<br /><span style='color: #ffcc00'> Area of Effect:</span> 400<br /><span style='color: #ffcc00'> Duration:</span> 3.00000<br /><span style='color: #ffcc00'> Duration (Boss):</span> 3.00000<br /><span style='color: #ffcc00'> Custom Value 1:</span> 6.00000<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy<br /><span style='color: #ffcc00'>Target Buff:</span> Stampede<br /><span style='color: #efe1a7'>--- Max Stacks:</span> 200<br /><span style='color: #efe1a7'>--- Stats - Attack Rate Bonus (%):</span> -0.06000"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "start_with_0_mana_ability_id",
      "abilityClass": "hidden",
      "name": "Start With 0 Mana",
      "image": "Icons/StartWith0Mana.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Start With 0 Mana<br /><span style='color: #ffcc00'> Order String:</span> Generator<br /><span style='color: #ffcc00'> Percentage:</span> 0.0000<br /><span style='color: #ffcc00'> Targets Excluded:</span> Building"
    }
  ],
  "upgradesTo": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "unitType": "red_eyes_unit_id",
      "name": "Red Eyes",
      "image": "Icons/RedEyes.png"
    }
  ],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_great_boar_unit_id: RawUnit = {
  name: "Great Boar",
  sketchfabUrl: "https://sketchfab.com/models/37c862665fea46908b00cb64e058a881",
  image: require("#LTD2/Icons/GreatBoar.png"),
  splash: require("#LTD2/splashes/GreatBoar.png"),
  description: "Tanky. Charges into battle, accelerating with each step. Deals splash damage when it hits.",

  damage: Damage.Pierce,
  atkSpeed: 1,
  onHitDmg: 63,

  armor: Armor.Fortified,
  hp: 3200,
};

export const great_boar_unit_id: PlayableUnit = {
  ...raw_great_boar_unit_id,
  price: 270,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "red_eyes_unit_id",
  "name": "Red Eyes",
  "image": "Icons/RedEyes.png",
  "splash": "splashes/RedEyes.png",
  "sketchfabUrl": "https://sketchfab.com/models/90499707037f404dbb71c2ff0bceb58a",
  "description": "Even tankier and charges with even greater ferocity.",
  "cost": 420,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Pierce",
  "attackTypeDescription": "115% to Arcane<br />80% to Fortified<br />85% to Natural<br />120% to Swift",
  "armorType": "Fortified",
  "armorTypeDescription": "115% from Impact<br />105% from Magic<br />80% from Pierce",
  "hp": 8100,
  "mana": 40,
  "hpRegen": 0,
  "manaRegen": 0.0010000000474974513,
  "attackSpeed": 1,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 159,
  "damage": 159,
  "range": 100,
  "totalValue": 690,
  "bounty": 0,
  "tier": "6",
  "legion": "Nomad",
  "legionType": "nomad_legion_id",
  "movementType": "Ground",
  "spawnBias": 0,
  "spawnDelay": 0,
  "effectiveDps": 159,
  "effectiveThreat": 159,
  "movementSize": 0.1875,
  "backswing": 0.4000000059604645,
  "projectileSpeed": 1200,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "9c742d",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Nomad T6U",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "thunderous_charge_ability_id",
      "abilityClass": "",
      "name": "Thunderous Charge",
      "image": "Icons/ThunderousCharge.png",
      "description": "Charges into battle as soon as the wave spawns. After reaching the enemy, deals Impact damage and slows targets' attack speed by 15%. The further the charge, the more damage it deals.  Ability damage.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/ThunderousCharge.png' /> Thunderous Charge<br /><span style='color: #ffcc00'> Order String:</span> Stampede<br /><span style='color: #ffcc00'> Attack Type:</span> <img class='tooltip-icon' src='hud/img/icons/Impact.png' /> Impact<br /><span style='color: #ffcc00'> Mana Cost:</span> 2.66660<br /><span style='color: #ffcc00'> Damage Base:</span> 23.00000 (Ability Damage)<br /><span style='color: #ffcc00'> Percentage:</span> 0.00000<br /><span style='color: #ffcc00'> Area of Effect:</span> 400<br /><span style='color: #ffcc00'> Duration:</span> 3.00000<br /><span style='color: #ffcc00'> Duration (Boss):</span> 3.00000<br /><span style='color: #ffcc00'> Custom Value 1:</span> 9.60000<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy<br /><span style='color: #ffcc00'>Target Buff:</span> Thunderous Charge<br /><span style='color: #efe1a7'>--- Max Stacks:</span> 200<br /><span style='color: #efe1a7'>--- Stats - Attack Rate Bonus (%):</span> -0.15000"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "start_with_0_mana_ability_id",
      "abilityClass": "hidden",
      "name": "Start With 0 Mana",
      "image": "Icons/StartWith0Mana.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Start With 0 Mana<br /><span style='color: #ffcc00'> Order String:</span> Generator<br /><span style='color: #ffcc00'> Percentage:</span> 0.0000<br /><span style='color: #ffcc00'> Targets Excluded:</span> Building"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "unitType": "great_boar_unit_id",
      "name": "Great Boar",
      "image": "Icons/GreatBoar.png"
    }
  ],
  "amountSpawned": 0
}
*/

export const raw_red_eyes_unit_id: RawUnit = {
  name: "Red Eyes",
  sketchfabUrl: "https://sketchfab.com/models/90499707037f404dbb71c2ff0bceb58a",
  image: require("#LTD2/Icons/RedEyes.png"),
  splash: require("#LTD2/splashes/RedEyes.png"),
  description: "Even tankier and charges with even greater ferocity.",

  damage: Damage.Pierce,
  atkSpeed: 1,
  onHitDmg: 159,

  armor: Armor.Fortified,
  hp: 8100,
};

export const red_eyes_unit_id: PlayableUnit = {
  ...raw_red_eyes_unit_id,
  price: 420,
  evolFrom: great_boar_unit_id,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "hellion_unit_id",
  "name": "Hellion",
  "image": "Icons/Hellion.png",
  "splash": "splashes/Hellion.png",
  "sketchfabUrl": "",
  "description": "A melee minion summoned by Soul Gate",
  "cost": 0,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Impact",
  "attackTypeDescription": "115% to Arcane<br />115% to Fortified<br />90% to Natural<br />80% to Swift",
  "armorType": "Natural",
  "armorTypeDescription": "90% from Impact<br />125% from Magic<br />85% from Pierce",
  "hp": 1060,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.6899999976158142,
  "attackSpeedDescriptor": "Fast",
  "mspd": 300,
  "dps": 100,
  "damage": 69,
  "range": 100,
  "totalValue": 0,
  "bounty": 0,
  "tier": "0",
  "legion": "Shrine",
  "legionType": "shrine_legion_id",
  "movementType": "Hover",
  "spawnBias": 0.5,
  "spawnDelay": 0,
  "effectiveDps": 100,
  "effectiveThreat": 100,
  "movementSize": 0.1875,
  "backswing": 0.15000000596046448,
  "projectileSpeed": 1200,
  "mastermindGroups": "None",
  "flags": "Ground, Organic, Summoned",
  "color": "b21a49",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Shrine ",
  "suffix": "(Special)",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "degeneration_ability_id",
      "abilityClass": "",
      "name": "Pet",
      "image": "Icons/Pet.png",
      "description": "Pets degenerate 2% max health per second",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Pet.png' /> Pet<br /><span style='color: #ffcc00'>Caster Buff:</span> Pet<br /><span style='color: #efe1a7'>--- Damage Per Second (% of Max):</span> 0.0200 (Ability Damage)"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_hellion_unit_id: RawUnit = {
  name: "Hellion",
  sketchfabUrl: "",
  image: require("#LTD2/Icons/Hellion.png"),
  splash: require("#LTD2/splashes/Hellion.png"),
  description: "A melee minion summoned by Soul Gate",

  damage: Damage.Impact,
  atkSpeed: 1.4492753673265821,
  onHitDmg: 69,

  armor: Armor.Natural,
  hp: 1060,
};

export const hellion_unit_id: PlayableUnit = {
  ...raw_hellion_unit_id,
  price: 0,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "elite_hellion_unit_id",
  "name": "Elite Hellion",
  "image": "Icons/EliteHellion.png",
  "splash": "splashes/EliteHellion.png",
  "sketchfabUrl": "",
  "description": "A melee minion summoned by Hell Gate",
  "cost": 0,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Impact",
  "attackTypeDescription": "115% to Arcane<br />115% to Fortified<br />90% to Natural<br />80% to Swift",
  "armorType": "Natural",
  "armorTypeDescription": "90% from Impact<br />125% from Magic<br />85% from Pierce",
  "hp": 2660,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.550000011920929,
  "attackSpeedDescriptor": "Fast",
  "mspd": 300,
  "dps": 251,
  "damage": 138,
  "range": 100,
  "totalValue": 0,
  "bounty": 0,
  "tier": "0",
  "legion": "Shrine",
  "legionType": "shrine_legion_id",
  "movementType": "Hover",
  "spawnBias": 0.5,
  "spawnDelay": 0,
  "effectiveDps": 250.91000366210938,
  "effectiveThreat": 250.91000366210938,
  "movementSize": 0.1875,
  "backswing": 0.15000000596046448,
  "projectileSpeed": 1200,
  "mastermindGroups": "None",
  "flags": "Ground, Organic, Summoned",
  "color": "b21a49",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Shrine ",
  "suffix": "(Special)",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "degeneration_ability_id",
      "abilityClass": "",
      "name": "Pet",
      "image": "Icons/Pet.png",
      "description": "Pets degenerate 2% max health per second",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Pet.png' /> Pet<br /><span style='color: #ffcc00'>Caster Buff:</span> Pet<br /><span style='color: #efe1a7'>--- Damage Per Second (% of Max):</span> 0.0200 (Ability Damage)"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_elite_hellion_unit_id: RawUnit = {
  name: "Elite Hellion",
  sketchfabUrl: "",
  image: require("#LTD2/Icons/EliteHellion.png"),
  splash: require("#LTD2/splashes/EliteHellion.png"),
  description: "A melee minion summoned by Hell Gate",

  damage: Damage.Impact,
  atkSpeed: 1.8181817787737895,
  onHitDmg: 138,

  armor: Armor.Natural,
  hp: 2660,
};

export const elite_hellion_unit_id: PlayableUnit = {
  ...raw_elite_hellion_unit_id,
  price: 0,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "nightcrawler_unit_id",
  "name": "Nightcrawler",
  "image": "Icons/Nightcrawler.png",
  "splash": "splashes/Nightcrawler.png",
  "sketchfabUrl": "",
  "description": "A ranged minion summoned by Soul Gate",
  "cost": 0,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Magic",
  "attackTypeDescription": "75% to Arcane<br />105% to Fortified<br />125% to Natural<br />100% to Swift",
  "armorType": "Natural",
  "armorTypeDescription": "90% from Impact<br />125% from Magic<br />85% from Pierce",
  "hp": 530,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.8399999737739563,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 82,
  "damage": 69,
  "range": 400,
  "totalValue": 0,
  "bounty": 0,
  "tier": "0",
  "legion": "Shrine",
  "legionType": "shrine_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.800000011920929,
  "spawnDelay": 0,
  "effectiveDps": 82.13999938964844,
  "effectiveThreat": 82.13999938964844,
  "movementSize": 0.1875,
  "backswing": 0.15000000596046448,
  "projectileSpeed": 1800,
  "mastermindGroups": "None",
  "flags": "Ground, Organic, Summoned",
  "color": "b21a49",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Shrine ",
  "suffix": "(Special)",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "degeneration_ability_id",
      "abilityClass": "",
      "name": "Pet",
      "image": "Icons/Pet.png",
      "description": "Pets degenerate 2% max health per second",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Pet.png' /> Pet<br /><span style='color: #ffcc00'>Caster Buff:</span> Pet<br /><span style='color: #efe1a7'>--- Damage Per Second (% of Max):</span> 0.0200 (Ability Damage)"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_nightcrawler_unit_id: RawUnit = {
  name: "Nightcrawler",
  sketchfabUrl: "",
  image: require("#LTD2/Icons/Nightcrawler.png"),
  splash: require("#LTD2/splashes/Nightcrawler.png"),
  description: "A ranged minion summoned by Soul Gate",

  damage: Damage.Magic,
  atkSpeed: 1.190476227644621,
  onHitDmg: 69,

  armor: Armor.Natural,
  hp: 530,
};

export const nightcrawler_unit_id: PlayableUnit = {
  ...raw_nightcrawler_unit_id,
  price: 0,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "elite_nightcrawler_unit_id",
  "name": "Elite Nightcrawler",
  "image": "Icons/EliteNightcrawler.png",
  "splash": "splashes/EliteNightcrawler.png",
  "sketchfabUrl": "",
  "description": "A ranged minion summoned by Hell Gate",
  "cost": 0,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Magic",
  "attackTypeDescription": "75% to Arcane<br />105% to Fortified<br />125% to Natural<br />100% to Swift",
  "armorType": "Natural",
  "armorTypeDescription": "90% from Impact<br />125% from Magic<br />85% from Pierce",
  "hp": 1330,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.6700000166893005,
  "attackSpeedDescriptor": "Fast",
  "mspd": 300,
  "dps": 206,
  "damage": 138,
  "range": 400,
  "totalValue": 0,
  "bounty": 0,
  "tier": "0",
  "legion": "Shrine",
  "legionType": "shrine_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.800000011920929,
  "spawnDelay": 0,
  "effectiveDps": 205.97000122070312,
  "effectiveThreat": 205.97000122070312,
  "movementSize": 0.1875,
  "backswing": 0.15000000596046448,
  "projectileSpeed": 1800,
  "mastermindGroups": "None",
  "flags": "Ground, Organic, Summoned",
  "color": "b21a49",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Shrine ",
  "suffix": "(Special)",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "degeneration_ability_id",
      "abilityClass": "",
      "name": "Pet",
      "image": "Icons/Pet.png",
      "description": "Pets degenerate 2% max health per second",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Pet.png' /> Pet<br /><span style='color: #ffcc00'>Caster Buff:</span> Pet<br /><span style='color: #efe1a7'>--- Damage Per Second (% of Max):</span> 0.0200 (Ability Damage)"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_elite_nightcrawler_unit_id: RawUnit = {
  name: "Elite Nightcrawler",
  sketchfabUrl: "",
  image: require("#LTD2/Icons/EliteNightcrawler.png"),
  splash: require("#LTD2/splashes/EliteNightcrawler.png"),
  description: "A ranged minion summoned by Hell Gate",

  damage: Damage.Magic,
  atkSpeed: 1.492537276254622,
  onHitDmg: 138,

  armor: Armor.Natural,
  hp: 1330,
};

export const elite_nightcrawler_unit_id: PlayableUnit = {
  ...raw_elite_nightcrawler_unit_id,
  price: 0,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "masked_spirit_unit_id",
  "name": "Masked Spirit",
  "image": "Icons/MaskedSpirit.png",
  "splash": "splashes/MaskedSpirit.png",
  "sketchfabUrl": "https://sketchfab.com/models/67b29899e7724d618a037d31d2bb5d1d",
  "description": "Long-ranged. Each attack hits up to 3 random targets.",
  "cost": 25,
  "mythium": 0,
  "income": 0,
  "supply": 1,
  "attackType": "Pierce",
  "attackTypeDescription": "115% to Arcane<br />80% to Fortified<br />85% to Natural<br />120% to Swift",
  "armorType": "Swift",
  "armorTypeDescription": "80% from Impact<br />100% from Magic<br />120% from Pierce",
  "hp": 130,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.9700000286102295,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 7,
  "damage": 7,
  "range": 600,
  "totalValue": 25,
  "bounty": 0,
  "tier": "1",
  "legion": "Shrine",
  "legionType": "shrine_legion_id",
  "movementType": "Hover",
  "spawnBias": 0.8999999761581421,
  "spawnDelay": 0,
  "effectiveDps": 7.21999979019165,
  "effectiveThreat": 7.21999979019165,
  "movementSize": 0.1875,
  "backswing": 0.27000001072883606,
  "projectileSpeed": 2000,
  "mastermindGroups": "Pierce, Special1, Special2",
  "flags": "Ground",
  "color": "b21a49",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Shrine T1",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "spectral_scream_ability_id",
      "abilityClass": "",
      "name": "Spectral Scream",
      "image": "Icons/SpectralScream.png",
      "description": "Each attack hits up to 3 nearby units. Cannot target the same unit twice. Ability damage.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/SpectralScream.png' /> Spectral Scream<br /><span style='color: #ffcc00'>Damage Type:</span> Ability Damage<br /><span style='color: #ffcc00'> Order String:</span> SilentScream<br /><span style='color: #ffcc00'> Projectile - Speed:</span> 0<br /><span style='color: #ffcc00'> Damage Base:</span> 0.00000 (Ability Damage)<br /><span style='color: #ffcc00'> Max Targets:</span> 3<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy"
    }
  ],
  "upgradesTo": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "unitType": "false_maiden_unit_id",
      "name": "False Maiden",
      "image": "Icons/FalseMaiden.png"
    }
  ],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_masked_spirit_unit_id: RawUnit = {
  name: "Masked Spirit",
  sketchfabUrl: "https://sketchfab.com/models/67b29899e7724d618a037d31d2bb5d1d",
  image: require("#LTD2/Icons/MaskedSpirit.png"),
  splash: require("#LTD2/splashes/MaskedSpirit.png"),
  description: "Long-ranged. Each attack hits up to 3 random targets.",

  damage: Damage.Pierce,
  atkSpeed: 1.0309278046442463,
  onHitDmg: 7,

  armor: Armor.Swift,
  hp: 130,
};

export const masked_spirit_unit_id: PlayableUnit = {
  ...raw_masked_spirit_unit_id,
  price: 25,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "false_maiden_unit_id",
  "name": "False Maiden",
  "image": "Icons/FalseMaiden.png",
  "splash": "splashes/FalseMaiden.png",
  "sketchfabUrl": "https://sketchfab.com/models/d1c218ca592547ea8c7622999f330bab",
  "description": "A new mask gives it a stronger attack.",
  "cost": 70,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Pierce",
  "attackTypeDescription": "115% to Arcane<br />80% to Fortified<br />85% to Natural<br />120% to Swift",
  "armorType": "Swift",
  "armorTypeDescription": "80% from Impact<br />100% from Magic<br />120% from Pierce",
  "hp": 670,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.9700000286102295,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 27,
  "damage": 26,
  "range": 600,
  "totalValue": 95,
  "bounty": 0,
  "tier": "1",
  "legion": "Shrine",
  "legionType": "shrine_legion_id",
  "movementType": "Hover",
  "spawnBias": 0.8999999761581421,
  "spawnDelay": 0,
  "effectiveDps": 26.799999237060547,
  "effectiveThreat": 26.799999237060547,
  "movementSize": 0.1875,
  "backswing": 0.27000001072883606,
  "projectileSpeed": 2000,
  "mastermindGroups": "None",
  "flags": "Ground",
  "color": "b21a49",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Shrine T1U",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "spectral_scream_ability_id",
      "abilityClass": "",
      "name": "Spectral Scream",
      "image": "Icons/SpectralScream.png",
      "description": "Each attack hits up to 3 nearby units. Cannot target the same unit twice. Ability damage.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/SpectralScream.png' /> Spectral Scream<br /><span style='color: #ffcc00'>Damage Type:</span> Ability Damage<br /><span style='color: #ffcc00'> Order String:</span> SilentScream<br /><span style='color: #ffcc00'> Projectile - Speed:</span> 0<br /><span style='color: #ffcc00'> Damage Base:</span> 0.00000 (Ability Damage)<br /><span style='color: #ffcc00'> Max Targets:</span> 3<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy"
    }
  ],
  "upgradesTo": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "unitType": "hell_raiser_unit_id",
      "name": "Hell Raiser",
      "image": "Icons/HellRaiser.png"
    }
  ],
  "upgradesFrom": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "unitType": "masked_spirit_unit_id",
      "name": "Masked Spirit",
      "image": "Icons/MaskedSpirit.png"
    }
  ],
  "amountSpawned": 0
}
*/

export const raw_false_maiden_unit_id: RawUnit = {
  name: "False Maiden",
  sketchfabUrl: "https://sketchfab.com/models/d1c218ca592547ea8c7622999f330bab",
  image: require("#LTD2/Icons/FalseMaiden.png"),
  splash: require("#LTD2/splashes/FalseMaiden.png"),
  description: "A new mask gives it a stronger attack.",

  damage: Damage.Pierce,
  atkSpeed: 1.0309278046442463,
  onHitDmg: 26,

  armor: Armor.Swift,
  hp: 670,
};

export const false_maiden_unit_id: PlayableUnit = {
  ...raw_false_maiden_unit_id,
  price: 70,
  evolFrom: masked_spirit_unit_id,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "hell_raiser_unit_id",
  "name": "Hell Raiser",
  "image": "Icons/HellRaiser.png",
  "splash": "splashes/HellRaiser.png",
  "sketchfabUrl": "https://sketchfab.com/models/8cd31783386546e9982d385dd867c5a8",
  "description": "Has a fiery temper. Alternates between stronger and weaker forms.",
  "cost": 120,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Pierce",
  "attackTypeDescription": "115% to Arcane<br />80% to Fortified<br />85% to Natural<br />120% to Swift",
  "armorType": "Swift",
  "armorTypeDescription": "80% from Impact<br />100% from Magic<br />120% from Pierce",
  "hp": 1500,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.9700000286102295,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 53,
  "damage": 51,
  "range": 600,
  "totalValue": 215,
  "bounty": 0,
  "tier": "1",
  "legion": "Shrine",
  "legionType": "shrine_legion_id",
  "movementType": "Hover",
  "spawnBias": 0.8999999761581421,
  "spawnDelay": 0,
  "effectiveDps": 52.58000183105469,
  "effectiveThreat": 52.58000183105469,
  "movementSize": 0.1875,
  "backswing": 0.27000001072883606,
  "projectileSpeed": 2000,
  "mastermindGroups": "None",
  "flags": "Ground",
  "color": "b21a49",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Shrine T1UU",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "tantrum_ability_id",
      "abilityClass": "",
      "name": "Tantrum",
      "image": "Icons/Tantrum.png",
      "description": "Every other wave, it has a tantrum, making it attack 20% faster.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Tantrum.png' /> Tantrum<br /><span style='color: #ffcc00'> Linked Abilities:</span> <img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Tantrum<br /><span style='color: #ffcc00'> Order String:</span> Bipolar<br /><span style='color: #ffcc00'> Unit:</span> <img class='tooltip-icon' src='hud/img/Icons/HellRaiser.png' />Hell Raiser<br /><span style='color: #ffcc00'> Unit 2:</span> <img class='tooltip-icon' src='hud/img/Icons/HellRaiser.png' />Hell Raiser"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "tantrum_effect_ability_id",
      "abilityClass": "linked",
      "name": "Tantrum",
      "image": "Icons/TantrumEffect.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Tantrum<br /><span style='color: #ffcc00'>Caster Buff:</span> Tantrum (Buff)<br /><span style='color: #efe1a7'>--- Stats - Attack Rate Bonus (%):</span> 0.20000"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "abilityType": "spectral_scream_ability_id",
      "abilityClass": "",
      "name": "Spectral Scream",
      "image": "Icons/SpectralScream.png",
      "description": "Each attack hits up to 3 nearby units. Cannot target the same unit twice. Ability damage.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/SpectralScream.png' /> Spectral Scream<br /><span style='color: #ffcc00'>Damage Type:</span> Ability Damage<br /><span style='color: #ffcc00'> Order String:</span> SilentScream<br /><span style='color: #ffcc00'> Projectile - Speed:</span> 0<br /><span style='color: #ffcc00'> Damage Base:</span> 0.00000 (Ability Damage)<br /><span style='color: #ffcc00'> Max Targets:</span> 3<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 3,
      "unitType": "false_maiden_unit_id",
      "name": "False Maiden",
      "image": "Icons/FalseMaiden.png"
    }
  ],
  "amountSpawned": 0
}
*/

export const raw_hell_raiser_unit_id: RawUnit = {
  name: "Hell Raiser",
  sketchfabUrl: "https://sketchfab.com/models/8cd31783386546e9982d385dd867c5a8",
  image: require("#LTD2/Icons/HellRaiser.png"),
  splash: require("#LTD2/splashes/HellRaiser.png"),
  description: "Has a fiery temper. Alternates between stronger and weaker forms.",

  damage: Damage.Pierce,
  atkSpeed: 1.0309278046442463,
  onHitDmg: 51,

  armor: Armor.Swift,
  hp: 1500,
};

export const hell_raiser_unit_id: PlayableUnit = {
  ...raw_hell_raiser_unit_id,
  price: 120,
  evolFrom: false_maiden_unit_id,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "nekomata_unit_id",
  "name": "Nekomata",
  "image": "Icons/Nekomata.png",
  "splash": "splashes/Nekomata.png",
  "sketchfabUrl": "https://sketchfab.com/models/06bb70ef39bb4751bb84bb9808b8fece",
  "description": "Can activate Spirit Offering to permanently upgrade its stats.",
  "cost": 60,
  "mythium": 0,
  "income": 0,
  "supply": 1,
  "attackType": "Pierce",
  "attackTypeDescription": "115% to Arcane<br />80% to Fortified<br />85% to Natural<br />120% to Swift",
  "armorType": "Arcane",
  "armorTypeDescription": "115% from Impact<br />75% from Magic<br />115% from Pierce",
  "hp": 600,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.090000033378601,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 28,
  "damage": 31,
  "range": 100,
  "totalValue": 60,
  "bounty": 0,
  "tier": "2",
  "legion": "Shrine",
  "legionType": "shrine_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.5,
  "spawnDelay": 0,
  "effectiveDps": 28.440000534057617,
  "effectiveThreat": 28.440000534057617,
  "movementSize": 0.1875,
  "backswing": 0.4000000059604645,
  "projectileSpeed": 1200,
  "mastermindGroups": "Arcane, Pierce",
  "flags": "Ground, Organic",
  "color": "b21a49",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Shrine T2",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "spirit_offering_ability_id",
      "abilityClass": "",
      "name": "Spirit Offering",
      "image": "Icons/SpiritOffering.png",
      "description": "Eat a tasty fish to permanently increase its health by 280 and damage by 15. At max (7) stacks, gains Devour, healing for 1% of its max health each time it gets a kill.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/SpiritOffering.png' /> Spirit Offering<br /><span style='color: #ffcc00'> Linked Abilities:</span> <img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Devour (Nekomata)<br /><span style='color: #ffcc00'> Order String:</span> SpiritOffering<br /><span style='color: #ffcc00'> Gold Cost:</span> 30<br /><span style='color: #ffcc00'> Custom Value 1:</span> 7.00000<br /><span style='color: #ffcc00'>Target Buff:</span> Spirit Offering<br /><span style='color: #efe1a7'>--- Max Stacks:</span> 7<br /><span style='color: #efe1a7'>--- Stats - Attack Damage Bonus (Flat):</span> 15<br /><span style='color: #efe1a7'>--- Stats - Max HP Bonus (Flat):</span> 280.00000"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "devour_nekomata_ability_id",
      "abilityClass": "linked",
      "name": "Devour (Nekomata)",
      "image": "Icons/Devour.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Devour (Nekomata)<br /><span style='color: #ffcc00'> Order String:</span> BigGameHunter<br /><span style='color: #ffcc00'>Caster Buff:</span> Nekomata Max Stacks (Texture Swap)"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_nekomata_unit_id: RawUnit = {
  name: "Nekomata",
  sketchfabUrl: "https://sketchfab.com/models/06bb70ef39bb4751bb84bb9808b8fece",
  image: require("#LTD2/Icons/Nekomata.png"),
  splash: require("#LTD2/splashes/Nekomata.png"),
  description: "Can activate Spirit Offering to permanently upgrade its stats.",

  damage: Damage.Pierce,
  atkSpeed: 0.9174311645664506,
  onHitDmg: 31,

  armor: Armor.Arcane,
  hp: 600,
};

export const nekomata_unit_id: PlayableUnit = {
  ...raw_nekomata_unit_id,
  price: 60,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "infiltrator_unit_id",
  "name": "Infiltrator",
  "image": "Icons/Infiltrator.png",
  "splash": "splashes/Infiltrator.png",
  "sketchfabUrl": "https://sketchfab.com/models/4620dab62fa84038ba2ac99f78e522fa",
  "description": "You can pay gold to activate Arm With Shuriken, which powers it up for the next wave only.",
  "cost": 80,
  "mythium": 0,
  "income": 0,
  "supply": 1,
  "attackType": "Magic",
  "attackTypeDescription": "75% to Arcane<br />105% to Fortified<br />125% to Natural<br />100% to Swift",
  "armorType": "Swift",
  "armorTypeDescription": "80% from Impact<br />100% from Magic<br />120% from Pierce",
  "hp": 730,
  "mana": 4,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.9599999785423279,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 41,
  "damage": 39,
  "range": 100,
  "totalValue": 80,
  "bounty": 0,
  "tier": "3",
  "legion": "Shrine",
  "legionType": "shrine_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.5,
  "spawnDelay": 0,
  "effectiveDps": 40.630001068115234,
  "effectiveThreat": 40.630001068115234,
  "movementSize": 0.1875,
  "backswing": 0.5299999713897705,
  "projectileSpeed": 5000,
  "mastermindGroups": "Magic, Special1, Special2, Special3, Special4",
  "flags": "Ground, Organic",
  "color": "b21a49",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Shrine T3",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "arm_with_shuriken_ability_id",
      "abilityClass": "",
      "name": "Arm With Shuriken",
      "image": "Icons/ArmWithShuriken.png",
      "description": "Pay gold to equip with a shuriken for the next wave only. Each shuriken deals 33 bonus damage from range. Ability damage.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/ArmWithShuriken.png' /> Arm With Shuriken<br /><span style='color: #ffcc00'> Order String:</span> ArmWithShuriken<br /><span style='color: #ffcc00'> Gold Cost:</span> 1<br /><span style='color: #ffcc00'> Custom Value 1:</span> 1.00000<br /><span style='color: #ffcc00'>Target Buff:</span> Arm With Shuriken<br /><span style='color: #efe1a7'>--- Stats - Bonus Attack Range (Flat):</span> 400"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "throw_shuriken_ability_id",
      "abilityClass": "hidden",
      "name": "Throw Shuriken",
      "image": "Icons/ThrowShuriken.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Throw Shuriken<br /><span style='color: #ffcc00'> Order String:</span> ThrowShuriken<br /><span style='color: #ffcc00'> Projectile - Speed:</span> 2500<br /><span style='color: #ffcc00'> Attack Type:</span> <img class='tooltip-icon' src='hud/img/icons/Magic.png' /> Magic<br /><span style='color: #ffcc00'> Mana Cost:</span> 1.00000<br /><span style='color: #ffcc00'> Damage Base:</span> 33.00000 (Ability Damage)<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy<br /><span style='color: #ffcc00'>Target Buff:</span> Arm With Shuriken (Buff)<br /><span style='color: #efe1a7'>--- Stats - Bonus Attack Range (Flat):</span> 400"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "abilityType": "mana_transfer_on_becomes_fighter_ability_id",
      "abilityClass": "hidden",
      "name": "Mana Transfer On Becomes Fighter",
      "image": "Icons/ManaTransferOnBecomesFighter.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Mana Transfer On Becomes Fighter<br /><span style='color: #ffcc00'> Order String:</span> ManaTransferOnBecomesFighter<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 3,
      "abilityType": "reset_mana_on_becomes_tower_ability_id",
      "abilityClass": "hidden",
      "name": "Reset Mana On Becomes Tower",
      "image": "Icons/ResetManaOnBecomesTower.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Reset Mana On Becomes Tower<br /><span style='color: #ffcc00'> Order String:</span> ResetManaOnBecomesTower<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 4,
      "abilityType": "start_with_0_mana_ability_id",
      "abilityClass": "hidden",
      "name": "Start With 0 Mana",
      "image": "Icons/StartWith0Mana.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Start With 0 Mana<br /><span style='color: #ffcc00'> Order String:</span> Generator<br /><span style='color: #ffcc00'> Percentage:</span> 0.0000<br /><span style='color: #ffcc00'> Targets Excluded:</span> Building"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 5,
      "abilityType": "disable_mana_regen_ability_id",
      "abilityClass": "hidden",
      "name": "Disable Mana Regen",
      "image": "Icons/DisableManaRegen.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Disable Mana Regen<br /><span style='color: #ffcc00'> Order String:</span> DisableManaRegen"
    }
  ],
  "upgradesTo": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 6,
      "unitType": "orchid_unit_id",
      "name": "Orchid",
      "image": "Icons/Orchid.png"
    }
  ],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_infiltrator_unit_id: RawUnit = {
  name: "Infiltrator",
  sketchfabUrl: "https://sketchfab.com/models/4620dab62fa84038ba2ac99f78e522fa",
  image: require("#LTD2/Icons/Infiltrator.png"),
  splash: require("#LTD2/splashes/Infiltrator.png"),
  description: "You can pay gold to activate Arm With Shuriken, which powers it up for the next wave only.",

  damage: Damage.Magic,
  atkSpeed: 1.0416666899497316,
  onHitDmg: 39,

  armor: Armor.Swift,
  hp: 730,
};

export const infiltrator_unit_id: PlayableUnit = {
  ...raw_infiltrator_unit_id,
  price: 80,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "orchid_unit_id",
  "name": "Orchid",
  "image": "Icons/Orchid.png",
  "splash": "splashes/Orchid.png",
  "sketchfabUrl": "https://sketchfab.com/models/0c99ca43b3134b0ca6c061d3ce43f13a",
  "description": "Now throws three times as many shurikens in the same amount of time.",
  "cost": 200,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Magic",
  "attackTypeDescription": "75% to Arcane<br />105% to Fortified<br />125% to Natural<br />100% to Swift",
  "armorType": "Swift",
  "armorTypeDescription": "80% from Impact<br />100% from Magic<br />120% from Pierce",
  "hp": 2530,
  "mana": 12,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.949999988079071,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 141,
  "damage": 134,
  "range": 100,
  "totalValue": 280,
  "bounty": 0,
  "tier": "3",
  "legion": "Shrine",
  "legionType": "shrine_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.5,
  "spawnDelay": 0,
  "effectiveDps": 141.0500030517578,
  "effectiveThreat": 141.0500030517578,
  "movementSize": 0.1875,
  "backswing": 0.5299999713897705,
  "projectileSpeed": 5000,
  "mastermindGroups": "Magic, Special1, Special2, Special3",
  "flags": "Ground, Organic",
  "color": "b21a49",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Shrine T3U",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "shuriken_cyclone_ability_id",
      "abilityClass": "",
      "name": "Shuriken Cyclone",
      "image": "Icons/ShurikenCyclone.png",
      "description": "Pay gold to equip with 3 shurikens for next wave only. Each shuriken deals 33 bonus damage from range and attacks 1.8x as fast. Ability damage.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/ShurikenCyclone.png' /> Shuriken Cyclone<br /><span style='color: #ffcc00'> Order String:</span> ArmWithShuriken<br /><span style='color: #ffcc00'> Gold Cost:</span> 5<br /><span style='color: #ffcc00'> Custom Value 1:</span> 3.00000<br /><span style='color: #ffcc00'>Target Buff:</span> Shuriken Cyclone<br /><span style='color: #efe1a7'>--- Stats - Attack Rate Bonus (%):</span> 0.80000<br /><span style='color: #efe1a7'>--- Stats - Bonus Attack Range (Flat):</span> 400"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "throw_shuriken_cyclone_ability_id",
      "abilityClass": "hidden",
      "name": "Throw Shuriken Cyclone",
      "image": "Icons/ThrowShurikenCyclone.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Throw Shuriken Cyclone<br /><span style='color: #ffcc00'> Order String:</span> ThrowShuriken<br /><span style='color: #ffcc00'> Projectile - Speed:</span> 2500<br /><span style='color: #ffcc00'> Attack Type:</span> <img class='tooltip-icon' src='hud/img/icons/Magic.png' /> Magic<br /><span style='color: #ffcc00'> Mana Cost:</span> 1.00000<br /><span style='color: #ffcc00'> Damage Base:</span> 33.00000 (Ability Damage)<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy<br /><span style='color: #ffcc00'>Target Buff:</span> Shuriken Cyclone (Buff)<br /><span style='color: #efe1a7'>--- Stats - Attack Rate Bonus (%):</span> 0.80000<br /><span style='color: #efe1a7'>--- Stats - Bonus Attack Range (Flat):</span> 400"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "abilityType": "mana_transfer_on_becomes_fighter_ability_id",
      "abilityClass": "hidden",
      "name": "Mana Transfer On Becomes Fighter",
      "image": "Icons/ManaTransferOnBecomesFighter.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Mana Transfer On Becomes Fighter<br /><span style='color: #ffcc00'> Order String:</span> ManaTransferOnBecomesFighter<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 3,
      "abilityType": "reset_mana_on_becomes_tower_ability_id",
      "abilityClass": "hidden",
      "name": "Reset Mana On Becomes Tower",
      "image": "Icons/ResetManaOnBecomesTower.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Reset Mana On Becomes Tower<br /><span style='color: #ffcc00'> Order String:</span> ResetManaOnBecomesTower<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 4,
      "abilityType": "start_with_0_mana_ability_id",
      "abilityClass": "hidden",
      "name": "Start With 0 Mana",
      "image": "Icons/StartWith0Mana.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Start With 0 Mana<br /><span style='color: #ffcc00'> Order String:</span> Generator<br /><span style='color: #ffcc00'> Percentage:</span> 0.0000<br /><span style='color: #ffcc00'> Targets Excluded:</span> Building"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 5,
      "abilityType": "disable_mana_regen_ability_id",
      "abilityClass": "hidden",
      "name": "Disable Mana Regen",
      "image": "Icons/DisableManaRegen.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Disable Mana Regen<br /><span style='color: #ffcc00'> Order String:</span> DisableManaRegen"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 6,
      "unitType": "infiltrator_unit_id",
      "name": "Infiltrator",
      "image": "Icons/Infiltrator.png"
    }
  ],
  "amountSpawned": 0
}
*/

export const raw_orchid_unit_id: RawUnit = {
  name: "Orchid",
  sketchfabUrl: "https://sketchfab.com/models/0c99ca43b3134b0ca6c061d3ce43f13a",
  image: require("#LTD2/Icons/Orchid.png"),
  splash: require("#LTD2/splashes/Orchid.png"),
  description: "Now throws three times as many shurikens in the same amount of time.",

  damage: Damage.Magic,
  atkSpeed: 1.0526315921561542,
  onHitDmg: 134,

  armor: Armor.Swift,
  hp: 2530,
};

export const orchid_unit_id: PlayableUnit = {
  ...raw_orchid_unit_id,
  price: 200,
  evolFrom: infiltrator_unit_id,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "eternal_wanderer_unit_id",
  "name": "Eternal Wanderer",
  "image": "Icons/EternalWanderer.png",
  "splash": "splashes/EternalWanderer.png",
  "sketchfabUrl": "https://sketchfab.com/models/78b60b3379f94e5a88ce09e1b9826b5d",
  "description": "Comes back to life after it dies.",
  "cost": 125,
  "mythium": 0,
  "income": 0,
  "supply": 1,
  "attackType": "Impact",
  "attackTypeDescription": "115% to Arcane<br />115% to Fortified<br />90% to Natural<br />80% to Swift",
  "armorType": "Fortified",
  "armorTypeDescription": "115% from Impact<br />105% from Magic<br />80% from Pierce",
  "hp": 770,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.2000000476837158,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 30,
  "damage": 36,
  "range": 100,
  "totalValue": 125,
  "bounty": 0,
  "tier": "4",
  "legion": "Shrine",
  "legionType": "shrine_legion_id",
  "movementType": "Hover",
  "spawnBias": 0.20000000298023224,
  "spawnDelay": 0,
  "effectiveDps": 30,
  "effectiveThreat": 30,
  "movementSize": 0.1875,
  "backswing": 0.6000000238418579,
  "projectileSpeed": 0,
  "mastermindGroups": "FortOrNat, Impact, Special1, Special2",
  "flags": "Ground, Organic",
  "color": "b21a49",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Shrine T4",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "shallow_grave_ability_id",
      "abilityClass": "",
      "name": "Shallow Grave",
      "image": "Icons/ShallowGrave.png",
      "description": "Comes back to life 2 seconds after dying and gains 50% attack speed.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/ShallowGrave.png' /> Shallow Grave<br /><span style='color: #ffcc00'> Order String:</span> SplitOnDeath<br /><span style='color: #ffcc00'> Duration:</span> 2.00000<br /><span style='color: #ffcc00'>Caster Buff:</span> Shallow Grave"
    }
  ],
  "upgradesTo": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "unitType": "samurai_soul_unit_id",
      "name": "Samurai Soul",
      "image": "Icons/SamuraiSoul.png"
    }
  ],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_eternal_wanderer_unit_id: RawUnit = {
  name: "Eternal Wanderer",
  sketchfabUrl: "https://sketchfab.com/models/78b60b3379f94e5a88ce09e1b9826b5d",
  image: require("#LTD2/Icons/EternalWanderer.png"),
  splash: require("#LTD2/splashes/EternalWanderer.png"),
  description: "Comes back to life after it dies.",

  damage: Damage.Impact,
  atkSpeed: 0.8333333002196431,
  onHitDmg: 36,

  armor: Armor.Fortified,
  hp: 770,
};

export const eternal_wanderer_unit_id: PlayableUnit = {
  ...raw_eternal_wanderer_unit_id,
  price: 125,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "samurai_soul_unit_id",
  "name": "Samurai Soul",
  "image": "Icons/SamuraiSoul.png",
  "splash": "splashes/SamuraiSoul.png",
  "sketchfabUrl": "https://sketchfab.com/models/a8a07dbfec8c460bb3ddbaa5d560be16",
  "description": "Has red plate armor and a legendary sword. Comes back to life after it dies.",
  "cost": 275,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Impact",
  "attackTypeDescription": "115% to Arcane<br />115% to Fortified<br />90% to Natural<br />80% to Swift",
  "armorType": "Fortified",
  "armorTypeDescription": "115% from Impact<br />105% from Magic<br />80% from Pierce",
  "hp": 2500,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.2000000476837158,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 96,
  "damage": 115,
  "range": 100,
  "totalValue": 400,
  "bounty": 0,
  "tier": "4",
  "legion": "Shrine",
  "legionType": "shrine_legion_id",
  "movementType": "Hover",
  "spawnBias": 0.20000000298023224,
  "spawnDelay": 0,
  "effectiveDps": 95.83000183105469,
  "effectiveThreat": 95.83000183105469,
  "movementSize": 0.1875,
  "backswing": 0.6000000238418579,
  "projectileSpeed": 0,
  "mastermindGroups": "FortOrNat, Impact, Special1, Special2",
  "flags": "Ground, Organic",
  "color": "b21a49",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Shrine T4U",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "shallow_grave_red_ability_id",
      "abilityClass": "",
      "name": "Shallow Grave",
      "image": "Icons/ShallowGrave.png",
      "description": "Comes back to life 2 seconds after dying and gains 50% attack speed.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/ShallowGrave.png' /> Shallow Grave<br /><span style='color: #ffcc00'> Order String:</span> SplitOnDeath<br /><span style='color: #ffcc00'> Duration:</span> 2.00000<br /><span style='color: #ffcc00'>Caster Buff:</span> Shallow Grave"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "unitType": "eternal_wanderer_unit_id",
      "name": "Eternal Wanderer",
      "image": "Icons/EternalWanderer.png"
    }
  ],
  "amountSpawned": 0
}
*/

export const raw_samurai_soul_unit_id: RawUnit = {
  name: "Samurai Soul",
  sketchfabUrl: "https://sketchfab.com/models/a8a07dbfec8c460bb3ddbaa5d560be16",
  image: require("#LTD2/Icons/SamuraiSoul.png"),
  splash: require("#LTD2/splashes/SamuraiSoul.png"),
  description: "Has red plate armor and a legendary sword. Comes back to life after it dies.",

  damage: Damage.Impact,
  atkSpeed: 0.8333333002196431,
  onHitDmg: 115,

  armor: Armor.Fortified,
  hp: 2500,
};

export const samurai_soul_unit_id: PlayableUnit = {
  ...raw_samurai_soul_unit_id,
  price: 275,
  evolFrom: eternal_wanderer_unit_id,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "yozora_unit_id",
  "name": "Yozora",
  "image": "Icons/Yozora.png",
  "splash": "splashes/Yozora.png",
  "sketchfabUrl": "https://sketchfab.com/models/a7487681d3b44f1182a84e135cf18f3d",
  "description": "Deceptively tanky and chills nearby enemies.",
  "cost": 190,
  "mythium": 0,
  "income": 0,
  "supply": 1,
  "attackType": "Pure",
  "attackTypeDescription": "100% to all defense types.",
  "armorType": "Swift",
  "armorTypeDescription": "80% from Impact<br />100% from Magic<br />120% from Pierce",
  "hp": 1530,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.8199999928474426,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 57,
  "damage": 47,
  "range": 100,
  "totalValue": 190,
  "bounty": 0,
  "tier": "5",
  "legion": "Shrine",
  "legionType": "shrine_legion_id",
  "movementType": "Hover",
  "spawnBias": 0.20000000298023224,
  "spawnDelay": 0,
  "effectiveDps": 57.31999969482422,
  "effectiveThreat": 57.31999969482422,
  "movementSize": 0.1875,
  "backswing": 0.47999998927116394,
  "projectileSpeed": 0,
  "mastermindGroups": "SwiftOrNat, Special1, Special2, Special3",
  "flags": "Ground, Organic",
  "color": "b21a49",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Shrine T5",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "wintry_touch_ability_id",
      "abilityClass": "",
      "name": "Wintry Touch",
      "image": "Icons/WintryTouch.png",
      "description": "Deals 3 pure damage per second and reduces the attack speed of nearby enemies by 0.75% in a large area. Stacks with itself.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/WintryTouch.png' /> Wintry Touch<br /><span style='color: #ffcc00'> Order String:</span> SimpleAura<br /><span style='color: #ffcc00'> Area of Effect:</span> 500<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy<br /><span style='color: #ffcc00'> Targets Excluded:</span> Boss<br /><span style='color: #ffcc00'>Target Buff:</span> Wintry Touch<br /><span style='color: #efe1a7'>--- Damage Per Second:</span> 3.00000 (Ability Damage)<br /><span style='color: #efe1a7'>--- Max Stacks:</span> 50<br /><span style='color: #efe1a7'>--- Stats - Attack Rate Bonus (%):</span> -0.00750<br /><span style='color: #ffcc00'>Caster Buff:</span> Wintry Touch"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "wintry_touch_boss_ability_id",
      "abilityClass": "hidden",
      "name": "Wintry Touch (Boss)",
      "image": "Icons/WintryTouch.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Wintry Touch (Boss)<br /><span style='color: #ffcc00'> Order String:</span> SimpleAura<br /><span style='color: #ffcc00'> Area of Effect:</span> 500<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy<br /><span style='color: #ffcc00'> Targets Excluded:</span> NonBoss<br /><span style='color: #ffcc00'>Target Buff:</span> Wintry Touch (Boss Buff)<br /><span style='color: #efe1a7'>--- Damage Per Second:</span> 3.00000 (Ability Damage)<br /><span style='color: #efe1a7'>--- Max Stacks:</span> 50<br /><span style='color: #efe1a7'>--- Stats - Attack Rate Bonus (%):</span> -0.00750"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "abilityType": "nimble_feet_ability_id",
      "abilityClass": "",
      "name": "Nimble Feet",
      "image": "Icons/NimbleFeet.png",
      "description": "Has a 25% chance to dodge each incoming attack.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/NimbleFeet.png' /> Nimble Feet<br /><span style='color: #ffcc00'> Order String:</span> Evasion<br /><span style='color: #ffcc00'> Percentage:</span> 0.25000<br /><span style='color: #ffcc00'>Caster Buff:</span> Nimble Feet"
    }
  ],
  "upgradesTo": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 3,
      "unitType": "arctaire_unit_id",
      "name": "Arctaire",
      "image": "Icons/Arctaire.png"
    }
  ],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_yozora_unit_id: RawUnit = {
  name: "Yozora",
  sketchfabUrl: "https://sketchfab.com/models/a7487681d3b44f1182a84e135cf18f3d",
  image: require("#LTD2/Icons/Yozora.png"),
  splash: require("#LTD2/splashes/Yozora.png"),
  description: "Deceptively tanky and chills nearby enemies.",

  damage: Damage.Pure,
  atkSpeed: 1.2195122057593062,
  onHitDmg: 47,

  armor: Armor.Swift,
  hp: 1530,
};

export const yozora_unit_id: PlayableUnit = {
  ...raw_yozora_unit_id,
  price: 190,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "arctaire_unit_id",
  "name": "Arctaire",
  "image": "Icons/Arctaire.png",
  "splash": "splashes/Arctaire.png",
  "sketchfabUrl": "https://sketchfab.com/models/53ccba2cc5cd4d9ea246cafeed0f0871",
  "description": "Deceptively tanky and chills nearby enemies.",
  "cost": 360,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Pure",
  "attackTypeDescription": "100% to all defense types.",
  "armorType": "Swift",
  "armorTypeDescription": "80% from Impact<br />100% from Magic<br />120% from Pierce",
  "hp": 4380,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.8199999928474426,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 163,
  "damage": 134,
  "range": 100,
  "totalValue": 550,
  "bounty": 0,
  "tier": "5",
  "legion": "Shrine",
  "legionType": "shrine_legion_id",
  "movementType": "Hover",
  "spawnBias": 0.20000000298023224,
  "spawnDelay": 0,
  "effectiveDps": 163.41000366210938,
  "effectiveThreat": 163.41000366210938,
  "movementSize": 0.1875,
  "backswing": 0.4000000059604645,
  "projectileSpeed": 0,
  "mastermindGroups": "SwiftOrNat, Special1, Special2, Special3",
  "flags": "Ground, Organic",
  "color": "b21a49",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Shrine T5U",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "frozen_veil_ability_id",
      "abilityClass": "",
      "name": "Frozen Veil",
      "image": "Icons/FrozenVeil.png",
      "description": "Deals 8 pure damage per second and reduces the attack speed of nearby enemies by 2% in a large area. Stacks with itself.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/FrozenVeil.png' /> Frozen Veil<br /><span style='color: #ffcc00'> Order String:</span> SimpleAura<br /><span style='color: #ffcc00'> Area of Effect:</span> 500<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy<br /><span style='color: #ffcc00'> Targets Excluded:</span> Boss<br /><span style='color: #ffcc00'>Target Buff:</span> Frozen Veil<br /><span style='color: #efe1a7'>--- Damage Per Second:</span> 8.00000 (Ability Damage)<br /><span style='color: #efe1a7'>--- Max Stacks:</span> 50<br /><span style='color: #efe1a7'>--- Stats - Attack Rate Bonus (%):</span> -0.02000<br /><span style='color: #ffcc00'>Caster Buff:</span> Frozen Veil"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "frozen_veil_boss_ability_id",
      "abilityClass": "hidden",
      "name": "Frozen Veil (Boss)",
      "image": "Icons/FrozenVeil.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Frozen Veil (Boss)<br /><span style='color: #ffcc00'> Order String:</span> SimpleAura<br /><span style='color: #ffcc00'> Area of Effect:</span> 500<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy<br /><span style='color: #ffcc00'> Targets Excluded:</span> NonBoss<br /><span style='color: #ffcc00'>Target Buff:</span> Frozen Veil (Boss Buff)<br /><span style='color: #efe1a7'>--- Damage Per Second:</span> 8.00000 (Ability Damage)<br /><span style='color: #efe1a7'>--- Max Stacks:</span> 50<br /><span style='color: #efe1a7'>--- Stats - Attack Rate Bonus (%):</span> -0.02000<br /><span style='color: #ffcc00'>Caster Buff:</span> Frozen Veil (Caster Buff)"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "abilityType": "nimble_feet_ability_id",
      "abilityClass": "",
      "name": "Nimble Feet",
      "image": "Icons/NimbleFeet.png",
      "description": "Has a 25% chance to dodge each incoming attack.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/NimbleFeet.png' /> Nimble Feet<br /><span style='color: #ffcc00'> Order String:</span> Evasion<br /><span style='color: #ffcc00'> Percentage:</span> 0.25000<br /><span style='color: #ffcc00'>Caster Buff:</span> Nimble Feet"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 3,
      "unitType": "yozora_unit_id",
      "name": "Yozora",
      "image": "Icons/Yozora.png"
    }
  ],
  "amountSpawned": 0
}
*/

export const raw_arctaire_unit_id: RawUnit = {
  name: "Arctaire",
  sketchfabUrl: "https://sketchfab.com/models/53ccba2cc5cd4d9ea246cafeed0f0871",
  image: require("#LTD2/Icons/Arctaire.png"),
  splash: require("#LTD2/splashes/Arctaire.png"),
  description: "Deceptively tanky and chills nearby enemies.",

  damage: Damage.Pure,
  atkSpeed: 1.2195122057593062,
  onHitDmg: 134,

  armor: Armor.Swift,
  hp: 4380,
};

export const arctaire_unit_id: PlayableUnit = {
  ...raw_arctaire_unit_id,
  price: 360,
  evolFrom: yozora_unit_id,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "soul_gate_unit_id",
  "name": "Soul Gate",
  "image": "Icons/SoulGate.png",
  "splash": "splashes/SoulGate.png",
  "sketchfabUrl": "https://sketchfab.com/models/9d7991cd84304168bc6b68c00a5d156f",
  "description": "Unique fighter. It can't move or attack, but summons minions.",
  "cost": 260,
  "mythium": 0,
  "income": 0,
  "supply": 3,
  "attackType": "Impact",
  "attackTypeDescription": "115% to Arcane<br />115% to Fortified<br />90% to Natural<br />80% to Swift",
  "armorType": "Natural",
  "armorTypeDescription": "90% from Impact<br />125% from Magic<br />85% from Pierce",
  "hp": 2000,
  "mana": 30,
  "hpRegen": 0,
  "manaRegen": 3.4000000953674316,
  "attackSpeed": 1.0499999523162842,
  "attackSpeedDescriptor": "Average",
  "mspd": 0,
  "dps": 0,
  "damage": 0,
  "range": 100,
  "totalValue": 260,
  "bounty": 0,
  "tier": "6",
  "legion": "Shrine",
  "legionType": "shrine_legion_id",
  "movementType": "None",
  "spawnBias": 0.6000000238418579,
  "spawnDelay": 0,
  "effectiveDps": 0,
  "effectiveThreat": 0,
  "movementSize": 0.25,
  "backswing": 0.15000000596046448,
  "projectileSpeed": 1200,
  "mastermindGroups": "Impact, Special1, Special2",
  "flags": "Ground",
  "color": "b21a49",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Shrine T6",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "dimensional_rift_ability_id",
      "abilityClass": "",
      "name": "Dimensional Rift",
      "image": "Icons/DimensionalRift.png",
      "description": "Summons a Hellion and Nightcrawler",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/DimensionalRift.png' /> Dimensional Rift<br /><span style='color: #ffcc00'> Order String:</span> DimensionalRift<br /><span style='color: #ffcc00'> Mana Cost:</span> 30.00000<br /><span style='color: #ffcc00'> Duration:</span> 180.00000<br /><span style='color: #ffcc00'> Unit:</span> <img class='tooltip-icon' src='hud/img/Icons/Hellion.png' />Hellion<br /><span style='color: #ffcc00'> Unit 2:</span> <img class='tooltip-icon' src='hud/img/Icons/Nightcrawler.png' />Nightcrawler<br /><span style='color: #ffcc00'>Target Buff:</span> Pet<br /><span style='color: #efe1a7'>--- Damage Per Second (% of Max):</span> 0.0200 (Ability Damage)"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "blood_price_ability_id",
      "abilityClass": "",
      "name": "Blood Price",
      "image": "Icons/BloodPrice.png",
      "description": "Every time it summons a unit, it loses 20% max hp (can kill itself).",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/BloodPrice.png' /> Blood Price<br /><span style='color: #ffcc00'> Order String:</span> BloodPrice<br /><span style='color: #ffcc00'> Percentage:</span> 0.20000<br /><span style='color: #ffcc00'> Custom Value 1:</span> 0.20000"
    }
  ],
  "upgradesTo": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "unitType": "hell_gate_unit_id",
      "name": "Hell Gate",
      "image": "Icons/HellGate.png"
    }
  ],
  "upgradesFrom": [],
  "amountSpawned": 0
}
*/

export const raw_soul_gate_unit_id: RawUnit = {
  name: "Soul Gate",
  sketchfabUrl: "https://sketchfab.com/models/9d7991cd84304168bc6b68c00a5d156f",
  image: require("#LTD2/Icons/SoulGate.png"),
  splash: require("#LTD2/splashes/SoulGate.png"),
  description: "Unique fighter. It can't move or attack, but summons minions.",

  damage: Damage.Impact,
  atkSpeed: 0.9523809956314903,
  onHitDmg: 0,

  armor: Armor.Natural,
  hp: 2000,
};

export const soul_gate_unit_id: PlayableUnit = {
  ...raw_soul_gate_unit_id,
  price: 260,
  evolFrom: null,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "hell_gate_unit_id",
  "name": "Hell Gate",
  "image": "Icons/HellGate.png",
  "splash": "splashes/HellGate.png",
  "sketchfabUrl": "https://sketchfab.com/models/82088696621447f2b4a505f38ee03eda",
  "description": "Summons even stronger minions.",
  "cost": 420,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Impact",
  "attackTypeDescription": "115% to Arcane<br />115% to Fortified<br />90% to Natural<br />80% to Swift",
  "armorType": "Natural",
  "armorTypeDescription": "90% from Impact<br />125% from Magic<br />85% from Pierce",
  "hp": 5000,
  "mana": 50,
  "hpRegen": 0,
  "manaRegen": 6,
  "attackSpeed": 1.0499999523162842,
  "attackSpeedDescriptor": "Average",
  "mspd": 0,
  "dps": 0,
  "damage": 0,
  "range": 100,
  "totalValue": 680,
  "bounty": 0,
  "tier": "6",
  "legion": "Shrine",
  "legionType": "shrine_legion_id",
  "movementType": "None",
  "spawnBias": 0.6000000238418579,
  "spawnDelay": 0,
  "effectiveDps": 0,
  "effectiveThreat": 0,
  "movementSize": 0.25,
  "backswing": 0.15000000596046448,
  "projectileSpeed": 1200,
  "mastermindGroups": "None",
  "flags": "Ground",
  "color": "b21a49",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Shrine T6U",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "dimensional_chasm_ability_id",
      "abilityClass": "",
      "name": "Dimensional Chasm",
      "image": "Icons/DimensionalChasm.png",
      "description": "Summons an Elite Hellion and Elite Nightcrawler",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/DimensionalChasm.png' /> Dimensional Chasm<br /><span style='color: #ffcc00'> Order String:</span> DimensionalRift<br /><span style='color: #ffcc00'> Mana Cost:</span> 50.00000<br /><span style='color: #ffcc00'> Duration:</span> 180.00000<br /><span style='color: #ffcc00'> Unit:</span> <img class='tooltip-icon' src='hud/img/Icons/EliteHellion.png' />Elite Hellion<br /><span style='color: #ffcc00'> Unit 2:</span> <img class='tooltip-icon' src='hud/img/Icons/EliteNightcrawler.png' />Elite Nightcrawler<br /><span style='color: #ffcc00'>Target Buff:</span> Pet<br /><span style='color: #efe1a7'>--- Damage Per Second (% of Max):</span> 0.0200 (Ability Damage)"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "blood_price_ability_id",
      "abilityClass": "",
      "name": "Blood Price",
      "image": "Icons/BloodPrice.png",
      "description": "Every time it summons a unit, it loses 20% max hp (can kill itself).",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/BloodPrice.png' /> Blood Price<br /><span style='color: #ffcc00'> Order String:</span> BloodPrice<br /><span style='color: #ffcc00'> Percentage:</span> 0.20000<br /><span style='color: #ffcc00'> Custom Value 1:</span> 0.20000"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexUpgradeUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "unitType": "soul_gate_unit_id",
      "name": "Soul Gate",
      "image": "Icons/SoulGate.png"
    }
  ],
  "amountSpawned": 0
}
*/

export const raw_hell_gate_unit_id: RawUnit = {
  name: "Hell Gate",
  sketchfabUrl: "https://sketchfab.com/models/82088696621447f2b4a505f38ee03eda",
  image: require("#LTD2/Icons/HellGate.png"),
  splash: require("#LTD2/splashes/HellGate.png"),
  description: "Summons even stronger minions.",

  damage: Damage.Impact,
  atkSpeed: 0.9523809956314903,
  onHitDmg: 0,

  armor: Armor.Natural,
  hp: 5000,
};

export const hell_gate_unit_id: PlayableUnit = {
  ...raw_hell_gate_unit_id,
  price: 420,
  evolFrom: soul_gate_unit_id,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "crab_unit_id",
  "name": "Crab",
  "image": "Icons/Crab.png",
  "splash": "splashes/Crab.png",
  "sketchfabUrl": "https://sketchfab.com/models/8f90cb6b5ee74180b2e5313e2ed69c82",
  "description": "Its shell is pretty soft, and its claws aren't that sharp.",
  "cost": 0,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Pierce",
  "attackTypeDescription": "115% to Arcane<br />80% to Fortified<br />85% to Natural<br />120% to Swift",
  "armorType": "Fortified",
  "armorTypeDescription": "115% from Impact<br />105% from Magic<br />80% from Pierce",
  "hp": 120,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.149999976158142,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 5,
  "damage": 6,
  "range": 100,
  "totalValue": 0,
  "bounty": 6,
  "tier": "0",
  "legion": "Wave",
  "legionType": "creature_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.5,
  "spawnDelay": 0,
  "effectiveDps": 5.21999979019165,
  "effectiveThreat": 5.21999979019165,
  "movementSize": 0.1875,
  "backswing": 0.15000000596046448,
  "projectileSpeed": 1200,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "60493d",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Wave 01",
  "suffix": "",
  "abilities": [],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 12
}
*/

export const raw_crab_unit_id: RawUnit = {
  name: "Crab",
  sketchfabUrl: "https://sketchfab.com/models/8f90cb6b5ee74180b2e5313e2ed69c82",
  image: require("#LTD2/Icons/Crab.png"),
  splash: require("#LTD2/splashes/Crab.png"),
  description: "Its shell is pretty soft, and its claws aren't that sharp.",

  damage: Damage.Pierce,
  atkSpeed: 0.8695652354191746,
  onHitDmg: 6,

  armor: Armor.Fortified,
  hp: 120,
};

export const crab_unit_id: WaveUnit = {
  ...raw_crab_unit_id,
  nb: 12,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "wale_unit_id",
  "name": "Wale",
  "image": "Icons/Wale.png",
  "splash": "splashes/Wale.png",
  "sketchfabUrl": "https://sketchfab.com/models/609bba53a24b4ea38f35d0f0644f2e81",
  "description": "Tusked creature that whacks enemies with a stick.",
  "cost": 0,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Impact",
  "attackTypeDescription": "115% to Arcane<br />115% to Fortified<br />90% to Natural<br />80% to Swift",
  "armorType": "Arcane",
  "armorTypeDescription": "115% from Impact<br />75% from Magic<br />115% from Pierce",
  "hp": 140,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.1200000047683716,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 7,
  "damage": 8,
  "range": 100,
  "totalValue": 0,
  "bounty": 7,
  "tier": "0",
  "legion": "Wave",
  "legionType": "creature_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.5,
  "spawnDelay": 0,
  "effectiveDps": 7.139999866485596,
  "effectiveThreat": 7.139999866485596,
  "movementSize": 0.1875,
  "backswing": 0.11999999731779099,
  "projectileSpeed": 1200,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "60493d",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Wave 02",
  "suffix": "",
  "abilities": [],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 12
}
*/

export const raw_wale_unit_id: RawUnit = {
  name: "Wale",
  sketchfabUrl: "https://sketchfab.com/models/609bba53a24b4ea38f35d0f0644f2e81",
  image: require("#LTD2/Icons/Wale.png"),
  splash: require("#LTD2/splashes/Wale.png"),
  description: "Tusked creature that whacks enemies with a stick.",

  damage: Damage.Impact,
  atkSpeed: 0.8928571390558262,
  onHitDmg: 8,

  armor: Armor.Arcane,
  hp: 140,
};

export const wale_unit_id: WaveUnit = {
  ...raw_wale_unit_id,
  nb: 12,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "hopper_unit_id",
  "name": "Hopper",
  "image": "Icons/Hopper.png",
  "splash": "splashes/Hopper.png",
  "sketchfabUrl": "https://sketchfab.com/models/0698ec75b44c4471b0ff2691567efd45",
  "description": "Has a tendency to attack in swarms.",
  "cost": 0,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Magic",
  "attackTypeDescription": "75% to Arcane<br />105% to Fortified<br />125% to Natural<br />100% to Swift",
  "armorType": "Natural",
  "armorTypeDescription": "90% from Impact<br />125% from Magic<br />85% from Pierce",
  "hp": 120,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.2000000476837158,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 7,
  "damage": 8,
  "range": 100,
  "totalValue": 0,
  "bounty": 5,
  "tier": "0",
  "legion": "Wave",
  "legionType": "creature_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.5,
  "spawnDelay": 0,
  "effectiveDps": 6.670000076293945,
  "effectiveThreat": 6.670000076293945,
  "movementSize": 0.1875,
  "backswing": 0.15000000596046448,
  "projectileSpeed": 1200,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "60493d",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Wave 03",
  "suffix": "",
  "abilities": [],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 18
}
*/

export const raw_hopper_unit_id: RawUnit = {
  name: "Hopper",
  sketchfabUrl: "https://sketchfab.com/models/0698ec75b44c4471b0ff2691567efd45",
  image: require("#LTD2/Icons/Hopper.png"),
  splash: require("#LTD2/splashes/Hopper.png"),
  description: "Has a tendency to attack in swarms.",

  damage: Damage.Magic,
  atkSpeed: 0.8333333002196431,
  onHitDmg: 8,

  armor: Armor.Natural,
  hp: 120,
};

export const hopper_unit_id: WaveUnit = {
  ...raw_hopper_unit_id,
  nb: 18,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "flying_chicken_unit_id",
  "name": "Flying Chicken",
  "image": "Icons/FlyingChicken.png",
  "splash": "splashes/FlyingChicken.png",
  "sketchfabUrl": "https://sketchfab.com/models/2a8a6a69e466401a99e0a0dcde2daab5",
  "description": "Flying. Its claws are blunt, but powerful.",
  "cost": 0,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Impact",
  "attackTypeDescription": "115% to Arcane<br />115% to Fortified<br />90% to Natural<br />80% to Swift",
  "armorType": "Swift",
  "armorTypeDescription": "80% from Impact<br />100% from Magic<br />120% from Pierce",
  "hp": 230,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.9300000071525574,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 13,
  "damage": 12,
  "range": 100,
  "totalValue": 0,
  "bounty": 8,
  "tier": "0",
  "legion": "Wave",
  "legionType": "creature_legion_id",
  "movementType": "Air",
  "spawnBias": 0.5,
  "spawnDelay": 0,
  "effectiveDps": 12.899999618530273,
  "effectiveThreat": 12.899999618530273,
  "movementSize": 0.33250001072883606,
  "backswing": 0.15000000596046448,
  "projectileSpeed": 1000,
  "mastermindGroups": "None",
  "flags": "Air, Organic",
  "color": "60493d",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Wave 04",
  "suffix": "",
  "abilities": [],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 12
}
*/

export const raw_flying_chicken_unit_id: RawUnit = {
  name: "Flying Chicken",
  sketchfabUrl: "https://sketchfab.com/models/2a8a6a69e466401a99e0a0dcde2daab5",
  image: require("#LTD2/Icons/FlyingChicken.png"),
  splash: require("#LTD2/splashes/FlyingChicken.png"),
  description: "Flying. Its claws are blunt, but powerful.",

  damage: Damage.Impact,
  atkSpeed: 1.0752688089344926,
  onHitDmg: 12,

  armor: Armor.Swift,
  hp: 230,
};

export const flying_chicken_unit_id: WaveUnit = {
  ...raw_flying_chicken_unit_id,
  nb: 12,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "scorpion_unit_id",
  "name": "Scorpion",
  "image": "Icons/Scorpion.png",
  "splash": "splashes/Scorpion.png",
  "sketchfabUrl": "https://sketchfab.com/models/a345a7c5c98541d389294a9f3836a975",
  "description": "Pierces enemies with its sharp tail.",
  "cost": 0,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Pierce",
  "attackTypeDescription": "115% to Arcane<br />80% to Fortified<br />85% to Natural<br />120% to Swift",
  "armorType": "Natural",
  "armorTypeDescription": "90% from Impact<br />125% from Magic<br />85% from Pierce",
  "hp": 340,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.0099999904632568,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 15,
  "damage": 15,
  "range": 100,
  "totalValue": 0,
  "bounty": 9,
  "tier": "0",
  "legion": "Wave",
  "legionType": "creature_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.5,
  "spawnDelay": 0,
  "effectiveDps": 14.850000381469727,
  "effectiveThreat": 14.850000381469727,
  "movementSize": 0.1875,
  "backswing": 0.15000000596046448,
  "projectileSpeed": 1200,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "60493d",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Wave 05",
  "suffix": "",
  "abilities": [],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 8
}
*/

export const raw_scorpion_unit_id: RawUnit = {
  name: "Scorpion",
  sketchfabUrl: "https://sketchfab.com/models/a345a7c5c98541d389294a9f3836a975",
  image: require("#LTD2/Icons/Scorpion.png"),
  splash: require("#LTD2/splashes/Scorpion.png"),
  description: "Pierces enemies with its sharp tail.",

  damage: Damage.Pierce,
  atkSpeed: 0.9900990192498218,
  onHitDmg: 15,

  armor: Armor.Natural,
  hp: 340,
};

export const scorpion_unit_id: WaveUnit = {
  ...raw_scorpion_unit_id,
  nb: 8,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "scorpion_king_unit_id",
  "name": "Scorpion King",
  "image": "Icons/ScorpionKing.png",
  "splash": "splashes/ScorpionKing.png",
  "sketchfabUrl": "https://sketchfab.com/models/b4bfb0e0319f46fea4b19f8f5e5c7078",
  "description": "Mini-boss. A giant, red scorpion.",
  "cost": 0,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Pierce",
  "attackTypeDescription": "115% to Arcane<br />80% to Fortified<br />85% to Natural<br />120% to Swift",
  "armorType": "Natural",
  "armorTypeDescription": "90% from Impact<br />125% from Magic<br />85% from Pierce",
  "hp": 1360,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.0099999904632568,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 59,
  "damage": 60,
  "range": 100,
  "totalValue": 0,
  "bounty": 36,
  "tier": "0",
  "legion": "Wave",
  "legionType": "creature_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.5,
  "spawnDelay": 0,
  "effectiveDps": 59.40999984741211,
  "effectiveThreat": 80,
  "movementSize": 0.25,
  "backswing": 0.15000000596046448,
  "projectileSpeed": 1200,
  "mastermindGroups": "None",
  "flags": "Boss, Ground, Organic",
  "color": "60493d",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Wave 05-Boss",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "boss_status_ability_id",
      "abilityClass": "",
      "name": "Boss Unit",
      "image": "Icons/BossUnit.png",
      "description": "Reduces the duration or effectiveness of most debuffs",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/BossUnit.png' /> Boss Unit<br /><span style='color: #ffcc00'>Caster Buff:</span> Boss Unit"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 1
}
*/

export const raw_scorpion_king_unit_id: RawUnit = {
  name: "Scorpion King",
  sketchfabUrl: "https://sketchfab.com/models/b4bfb0e0319f46fea4b19f8f5e5c7078",
  image: require("#LTD2/Icons/ScorpionKing.png"),
  splash: require("#LTD2/splashes/ScorpionKing.png"),
  description: "Mini-boss. A giant, red scorpion.",

  damage: Damage.Pierce,
  atkSpeed: 0.9900990192498218,
  onHitDmg: 60,

  armor: Armor.Natural,
  hp: 1360,
};

export const scorpion_king_unit_id: WaveUnit = {
  ...raw_scorpion_king_unit_id,
  nb: 1,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "rocko_unit_id",
  "name": "Rocko",
  "image": "Icons/Rocko.png",
  "splash": "splashes/Rocko.png",
  "sketchfabUrl": "https://sketchfab.com/models/47b1d38f874d437ba3b047bc54ec592e",
  "description": "Its attacks can critically strike. Strong, but few in number.",
  "cost": 0,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Impact",
  "attackTypeDescription": "115% to Arcane<br />115% to Fortified<br />90% to Natural<br />80% to Swift",
  "armorType": "Fortified",
  "armorTypeDescription": "115% from Impact<br />105% from Magic<br />80% from Pierce",
  "hp": 830,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.2699999809265137,
  "attackSpeedDescriptor": "Slow",
  "mspd": 300,
  "dps": 31,
  "damage": 40,
  "range": 100,
  "totalValue": 0,
  "bounty": 19,
  "tier": "0",
  "legion": "Wave",
  "legionType": "creature_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.5,
  "spawnDelay": 0,
  "effectiveDps": 40,
  "effectiveThreat": 40,
  "movementSize": 0.25,
  "backswing": 0.15000000596046448,
  "projectileSpeed": 1200,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "60493d",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Wave 06",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "impale_ability_id",
      "abilityClass": "",
      "name": "Impale",
      "image": "Icons/Impale.png",
      "description": "Each attack has a 25% chance to deal 150% damage",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Impale.png' /> Impale<br /><span style='color: #ffcc00'> Order String:</span> Bash<br /><span style='color: #ffcc00'> Damage Base:</span> 20.00000 (Ability Damage)<br /><span style='color: #ffcc00'> Percentage:</span> 0.25000<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 6
}
*/

export const raw_rocko_unit_id: RawUnit = {
  name: "Rocko",
  sketchfabUrl: "https://sketchfab.com/models/47b1d38f874d437ba3b047bc54ec592e",
  image: require("#LTD2/Icons/Rocko.png"),
  splash: require("#LTD2/splashes/Rocko.png"),
  description: "Its attacks can critically strike. Strong, but few in number.",

  damage: Damage.Impact,
  atkSpeed: 0.7874015866287349,
  onHitDmg: 40,

  armor: Armor.Fortified,
  hp: 830,
};

export const rocko_unit_id: WaveUnit = {
  ...raw_rocko_unit_id,
  nb: 6,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "sludge_unit_id",
  "name": "Sludge",
  "image": "Icons/Sludge.png",
  "splash": "splashes/Sludge.png",
  "sketchfabUrl": "https://sketchfab.com/models/a0ab32dffa5147a58be7a370e1022220",
  "description": "Surviving in sewers has made it resistant to magic.",
  "cost": 0,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Magic",
  "attackTypeDescription": "75% to Arcane<br />105% to Fortified<br />125% to Natural<br />100% to Swift",
  "armorType": "Arcane",
  "armorTypeDescription": "115% from Impact<br />75% from Magic<br />115% from Pierce",
  "hp": 560,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.0149999856948853,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 28,
  "damage": 28,
  "range": 100,
  "totalValue": 0,
  "bounty": 10,
  "tier": "0",
  "legion": "Wave",
  "legionType": "creature_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.5,
  "spawnDelay": 0,
  "effectiveDps": 27.59000015258789,
  "effectiveThreat": 27.59000015258789,
  "movementSize": 0.1875,
  "backswing": 0.20000000298023224,
  "projectileSpeed": 1200,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "60493d",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Wave 07",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "blobs_ability_id",
      "abilityClass": "",
      "name": "Blobs",
      "image": "Icons/Blobs.png",
      "description": "When this unit dies, it spawns a Blob.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Blobs.png' /> Blobs<br /><span style='color: #ffcc00'> Order String:</span> SplitOnDeath<br /><span style='color: #ffcc00'> Unit:</span> <img class='tooltip-icon' src='hud/img/Icons/Blob.png' />Blob"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 10
}
*/

export const raw_sludge_unit_id: RawUnit = {
  name: "Sludge",
  sketchfabUrl: "https://sketchfab.com/models/a0ab32dffa5147a58be7a370e1022220",
  image: require("#LTD2/Icons/Sludge.png"),
  splash: require("#LTD2/splashes/Sludge.png"),
  description: "Surviving in sewers has made it resistant to magic.",

  damage: Damage.Magic,
  atkSpeed: 0.9852216887622752,
  onHitDmg: 28,

  armor: Armor.Arcane,
  hp: 560,
};

export const sludge_unit_id: WaveUnit = {
  ...raw_sludge_unit_id,
  nb: 10,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "kobra_unit_id",
  "name": "Kobra",
  "image": "Icons/Kobra.png",
  "splash": "splashes/Kobra.png",
  "sketchfabUrl": "https://sketchfab.com/models/f7bf0edcb1674ccd91792d2908377274",
  "description": "Medium-ranged. Spits magical venom.",
  "cost": 0,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Magic",
  "attackTypeDescription": "75% to Arcane<br />105% to Fortified<br />125% to Natural<br />100% to Swift",
  "armorType": "Swift",
  "armorTypeDescription": "80% from Impact<br />100% from Magic<br />120% from Pierce",
  "hp": 600,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.925000011920929,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 31,
  "damage": 29,
  "range": 400,
  "totalValue": 0,
  "bounty": 11,
  "tier": "0",
  "legion": "Wave",
  "legionType": "creature_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.5,
  "spawnDelay": 0,
  "effectiveDps": 31.350000381469727,
  "effectiveThreat": 31.350000381469727,
  "movementSize": 0.1875,
  "backswing": 0.14000000059604645,
  "projectileSpeed": 2500,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "60493d",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Wave 08",
  "suffix": "",
  "abilities": [],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 12
}
*/

export const raw_kobra_unit_id: RawUnit = {
  name: "Kobra",
  sketchfabUrl: "https://sketchfab.com/models/f7bf0edcb1674ccd91792d2908377274",
  image: require("#LTD2/Icons/Kobra.png"),
  splash: require("#LTD2/splashes/Kobra.png"),
  description: "Medium-ranged. Spits magical venom.",

  damage: Damage.Magic,
  atkSpeed: 1.0810810671486588,
  onHitDmg: 29,

  armor: Armor.Swift,
  hp: 600,
};

export const kobra_unit_id: WaveUnit = {
  ...raw_kobra_unit_id,
  nb: 12,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "carapace_unit_id",
  "name": "Carapace",
  "image": "Icons/Carapace.png",
  "splash": "splashes/Carapace.png",
  "sketchfabUrl": "https://sketchfab.com/models/9382ef5c4961412081b70794dbcf5451",
  "description": "Its hard exoskeleton protects it from ranged attacks.",
  "cost": 0,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Pierce",
  "attackTypeDescription": "115% to Arcane<br />80% to Fortified<br />85% to Natural<br />120% to Swift",
  "armorType": "Fortified",
  "armorTypeDescription": "115% from Impact<br />105% from Magic<br />80% from Pierce",
  "hp": 740,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.949999988079071,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 35,
  "damage": 33,
  "range": 100,
  "totalValue": 0,
  "bounty": 12,
  "tier": "0",
  "legion": "Wave",
  "legionType": "creature_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.5,
  "spawnDelay": 0,
  "effectiveDps": 34.7400016784668,
  "effectiveThreat": 34.7400016784668,
  "movementSize": 0.1875,
  "backswing": 0.15000000596046448,
  "projectileSpeed": 1200,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "60493d",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Wave 09",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "deflection_ability_id",
      "abilityClass": "",
      "name": "Deflection",
      "image": "Icons/Deflection.png",
      "description": "Reduces damage taken from ranged units by 15%",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Deflection.png' /> Deflection<br /><span style='color: #ffcc00'> Order String:</span> RangedDamageReduction<br /><span style='color: #ffcc00'> Percentage:</span> 0.150"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 12
}
*/

export const raw_carapace_unit_id: RawUnit = {
  name: "Carapace",
  sketchfabUrl: "https://sketchfab.com/models/9382ef5c4961412081b70794dbcf5451",
  image: require("#LTD2/Icons/Carapace.png"),
  splash: require("#LTD2/splashes/Carapace.png"),
  description: "Its hard exoskeleton protects it from ranged attacks.",

  damage: Damage.Pierce,
  atkSpeed: 1.0526315921561542,
  onHitDmg: 33,

  armor: Armor.Fortified,
  hp: 740,
};

export const carapace_unit_id: WaveUnit = {
  ...raw_carapace_unit_id,
  nb: 12,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "granddaddy_unit_id",
  "name": "Granddaddy",
  "image": "Icons/Granddaddy.png",
  "splash": "splashes/Granddaddy.png",
  "sketchfabUrl": "https://sketchfab.com/models/a8e53483a56642caa8901a9fe4963fe9",
  "description": "Boss. Impales foes with its head spike.",
  "cost": 0,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Impact",
  "attackTypeDescription": "115% to Arcane<br />115% to Fortified<br />90% to Natural<br />80% to Swift",
  "armorType": "Arcane",
  "armorTypeDescription": "115% from Impact<br />75% from Magic<br />115% from Pierce",
  "hp": 10000,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.26499998569488525,
  "attackSpeedDescriptor": "Ultra Fast",
  "mspd": 300,
  "dps": 434,
  "damage": 115,
  "range": 100,
  "totalValue": 0,
  "bounty": 150,
  "tier": "0",
  "legion": "Wave",
  "legionType": "creature_legion_id",
  "movementType": "Hover",
  "spawnBias": 0.5,
  "spawnDelay": 0,
  "effectiveDps": 433.9599914550781,
  "effectiveThreat": 650,
  "movementSize": 0.5,
  "backswing": 0.06499999761581421,
  "projectileSpeed": 1200,
  "mastermindGroups": "None",
  "flags": "Boss, Ground, Organic",
  "color": "60493d",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Wave 10",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "boss_status_ability_id",
      "abilityClass": "",
      "name": "Boss Unit",
      "image": "Icons/BossUnit.png",
      "description": "Reduces the duration or effectiveness of most debuffs",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/BossUnit.png' /> Boss Unit<br /><span style='color: #ffcc00'>Caster Buff:</span> Boss Unit"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 1
}
*/

export const raw_granddaddy_unit_id: RawUnit = {
  name: "Granddaddy",
  sketchfabUrl: "https://sketchfab.com/models/a8e53483a56642caa8901a9fe4963fe9",
  image: require("#LTD2/Icons/Granddaddy.png"),
  splash: require("#LTD2/splashes/Granddaddy.png"),
  description: "Boss. Impales foes with its head spike.",

  damage: Damage.Impact,
  atkSpeed: 3.7735851093644075,
  onHitDmg: 115,

  armor: Armor.Arcane,
  hp: 10000,
};

export const granddaddy_unit_id: WaveUnit = {
  ...raw_granddaddy_unit_id,
  nb: 1,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "quill_shooter_unit_id",
  "name": "Quill Shooter",
  "image": "Icons/QuillShooter.png",
  "splash": "splashes/QuillShooter.png",
  "sketchfabUrl": "https://sketchfab.com/models/9acfcb6fba2d45bf82a5feabe1a2bd47",
  "description": "Long-ranged. Launches quills from its back.",
  "cost": 0,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Pierce",
  "attackTypeDescription": "115% to Arcane<br />80% to Fortified<br />85% to Natural<br />120% to Swift",
  "armorType": "Natural",
  "armorTypeDescription": "90% from Impact<br />125% from Magic<br />85% from Pierce",
  "hp": 1080,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.2050000429153442,
  "attackSpeedDescriptor": "Slow",
  "mspd": 300,
  "dps": 56,
  "damage": 67,
  "range": 550,
  "totalValue": 0,
  "bounty": 13,
  "tier": "0",
  "legion": "Wave",
  "legionType": "creature_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.5,
  "spawnDelay": 0,
  "effectiveDps": 55.599998474121094,
  "effectiveThreat": 55.599998474121094,
  "movementSize": 0.1875,
  "backswing": 0.28999999165534973,
  "projectileSpeed": 2500,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "60493d",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Wave 11",
  "suffix": "",
  "abilities": [],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 12
}
*/

export const raw_quill_shooter_unit_id: RawUnit = {
  name: "Quill Shooter",
  sketchfabUrl: "https://sketchfab.com/models/9acfcb6fba2d45bf82a5feabe1a2bd47",
  image: require("#LTD2/Icons/QuillShooter.png"),
  splash: require("#LTD2/splashes/QuillShooter.png"),
  description: "Long-ranged. Launches quills from its back.",

  damage: Damage.Pierce,
  atkSpeed: 0.8298754891166868,
  onHitDmg: 67,

  armor: Armor.Natural,
  hp: 1080,
};

export const quill_shooter_unit_id: WaveUnit = {
  ...raw_quill_shooter_unit_id,
  nb: 12,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "mantis_unit_id",
  "name": "Mantis",
  "image": "Icons/Mantis.png",
  "splash": "splashes/Mantis.png",
  "sketchfabUrl": "https://sketchfab.com/models/75b027d9a3cb4d3c80e389110470a43a",
  "description": "Deals high damage with sharp claws.",
  "cost": 0,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Pierce",
  "attackTypeDescription": "115% to Arcane<br />80% to Fortified<br />85% to Natural<br />120% to Swift",
  "armorType": "Swift",
  "armorTypeDescription": "80% from Impact<br />100% from Magic<br />120% from Pierce",
  "hp": 1230,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 1.034999966621399,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 81,
  "damage": 84,
  "range": 100,
  "totalValue": 0,
  "bounty": 14,
  "tier": "0",
  "legion": "Wave",
  "legionType": "creature_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.5,
  "spawnDelay": 0,
  "effectiveDps": 81.16000366210938,
  "effectiveThreat": 81.16000366210938,
  "movementSize": 0.1875,
  "backswing": 0.18000000715255737,
  "projectileSpeed": 1200,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "60493d",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Wave 12",
  "suffix": "",
  "abilities": [],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 12
}
*/

export const raw_mantis_unit_id: RawUnit = {
  name: "Mantis",
  sketchfabUrl: "https://sketchfab.com/models/75b027d9a3cb4d3c80e389110470a43a",
  image: require("#LTD2/Icons/Mantis.png"),
  splash: require("#LTD2/splashes/Mantis.png"),
  description: "Deals high damage with sharp claws.",

  damage: Damage.Pierce,
  atkSpeed: 0.9661836060385093,
  onHitDmg: 84,

  armor: Armor.Swift,
  hp: 1230,
};

export const mantis_unit_id: WaveUnit = {
  ...raw_mantis_unit_id,
  nb: 12,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "drill_golem_unit_id",
  "name": "Drill Golem",
  "image": "Icons/DrillGolem.png",
  "splash": "splashes/DrillGolem.png",
  "sketchfabUrl": "https://sketchfab.com/models/24e8af62ff324050a2bf87892fc8c918",
  "description": "Tanky and hits hard, but few in number.",
  "cost": 0,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Impact",
  "attackTypeDescription": "115% to Arcane<br />115% to Fortified<br />90% to Natural<br />80% to Swift",
  "armorType": "Fortified",
  "armorTypeDescription": "115% from Impact<br />105% from Magic<br />80% from Pierce",
  "hp": 3190,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.6299999952316284,
  "attackSpeedDescriptor": "Fast",
  "mspd": 300,
  "dps": 159,
  "damage": 100,
  "range": 100,
  "totalValue": 0,
  "bounty": 30,
  "tier": "0",
  "legion": "Wave",
  "legionType": "creature_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.5,
  "spawnDelay": 0,
  "effectiveDps": 158.72999572753906,
  "effectiveThreat": 158.72999572753906,
  "movementSize": 0.25,
  "backswing": 0.15000000596046448,
  "projectileSpeed": 1200,
  "mastermindGroups": "None",
  "flags": "Ground, Mechanical",
  "color": "60493d",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Wave 13",
  "suffix": "",
  "abilities": [],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 6
}
*/

export const raw_drill_golem_unit_id: RawUnit = {
  name: "Drill Golem",
  sketchfabUrl: "https://sketchfab.com/models/24e8af62ff324050a2bf87892fc8c918",
  image: require("#LTD2/Icons/DrillGolem.png"),
  splash: require("#LTD2/splashes/DrillGolem.png"),
  description: "Tanky and hits hard, but few in number.",

  damage: Damage.Impact,
  atkSpeed: 1.5873015993156252,
  onHitDmg: 100,

  armor: Armor.Fortified,
  hp: 3190,
};

export const drill_golem_unit_id: WaveUnit = {
  ...raw_drill_golem_unit_id,
  nb: 6,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "killer_slug_unit_id",
  "name": "Killer Slug",
  "image": "Icons/KillerSlug.png",
  "splash": "splashes/KillerSlug.png",
  "sketchfabUrl": "https://sketchfab.com/models/e3712ae480334e3abdabb4c593620653",
  "description": "Overgrown pest with a magic-resistant shell.",
  "cost": 0,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Magic",
  "attackTypeDescription": "75% to Arcane<br />105% to Fortified<br />125% to Natural<br />100% to Swift",
  "armorType": "Arcane",
  "armorTypeDescription": "115% from Impact<br />75% from Magic<br />115% from Pierce",
  "hp": 1980,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.8100000023841858,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 99,
  "damage": 80,
  "range": 100,
  "totalValue": 0,
  "bounty": 16,
  "tier": "0",
  "legion": "Wave",
  "legionType": "creature_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.5,
  "spawnDelay": 0,
  "effectiveDps": 98.7699966430664,
  "effectiveThreat": 98.7699966430664,
  "movementSize": 0.1875,
  "backswing": 0.09000000357627869,
  "projectileSpeed": 1200,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "60493d",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Wave 14",
  "suffix": "",
  "abilities": [],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 12
}
*/

export const raw_killer_slug_unit_id: RawUnit = {
  name: "Killer Slug",
  sketchfabUrl: "https://sketchfab.com/models/e3712ae480334e3abdabb4c593620653",
  image: require("#LTD2/Icons/KillerSlug.png"),
  splash: require("#LTD2/splashes/KillerSlug.png"),
  description: "Overgrown pest with a magic-resistant shell.",

  damage: Damage.Magic,
  atkSpeed: 1.2345678976006922,
  onHitDmg: 80,

  armor: Armor.Arcane,
  hp: 1980,
};

export const killer_slug_unit_id: WaveUnit = {
  ...raw_killer_slug_unit_id,
  nb: 12,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "octopus_unit_id",
  "name": "Quadrapus",
  "image": "Icons/Quadrapus.png",
  "splash": "splashes/Quadrapus.png",
  "sketchfabUrl": "https://sketchfab.com/models/7109cfcf1af044a79cde9c3eee2a2d64",
  "description": "Short-ranged. Shoots magical ink from oversized nostrils.",
  "cost": 0,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Magic",
  "attackTypeDescription": "75% to Arcane<br />105% to Fortified<br />125% to Natural<br />100% to Swift",
  "armorType": "Natural",
  "armorTypeDescription": "90% from Impact<br />125% from Magic<br />85% from Pierce",
  "hp": 2250,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.9100000262260437,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 121,
  "damage": 110,
  "range": 400,
  "totalValue": 0,
  "bounty": 17,
  "tier": "0",
  "legion": "Wave",
  "legionType": "creature_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.5,
  "spawnDelay": 0,
  "effectiveDps": 120.87999725341797,
  "effectiveThreat": 120.87999725341797,
  "movementSize": 0.1875,
  "backswing": 0.20000000298023224,
  "projectileSpeed": 2500,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "60493d",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Wave 15",
  "suffix": "",
  "abilities": [],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 8
}
*/

export const raw_octopus_unit_id: RawUnit = {
  name: "Quadrapus",
  sketchfabUrl: "https://sketchfab.com/models/7109cfcf1af044a79cde9c3eee2a2d64",
  image: require("#LTD2/Icons/Quadrapus.png"),
  splash: require("#LTD2/splashes/Quadrapus.png"),
  description: "Short-ranged. Shoots magical ink from oversized nostrils.",

  damage: Damage.Magic,
  atkSpeed: 1.098901067230959,
  onHitDmg: 110,

  armor: Armor.Natural,
  hp: 2250,
};

export const octopus_unit_id: WaveUnit = {
  ...raw_octopus_unit_id,
  nb: 8,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "giant_quadrapus_unit_id",
  "name": "Giant Quadrapus",
  "image": "Icons/GiantQuadrapus.png",
  "splash": "splashes/GiantQuadrapus.png",
  "sketchfabUrl": "https://sketchfab.com/models/747843b00d684e8faeaeca085fb4fa42",
  "description": "Mini-boss. A giant, red Quadrapus.",
  "cost": 0,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Magic",
  "attackTypeDescription": "75% to Arcane<br />105% to Fortified<br />125% to Natural<br />100% to Swift",
  "armorType": "Natural",
  "armorTypeDescription": "90% from Impact<br />125% from Magic<br />85% from Pierce",
  "hp": 9000,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.9100000262260437,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 484,
  "damage": 440,
  "range": 450,
  "totalValue": 0,
  "bounty": 68,
  "tier": "0",
  "legion": "Wave",
  "legionType": "creature_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.5,
  "spawnDelay": 0,
  "effectiveDps": 483.5199890136719,
  "effectiveThreat": 755,
  "movementSize": 0.33250001072883606,
  "backswing": 0.20000000298023224,
  "projectileSpeed": 2500,
  "mastermindGroups": "None",
  "flags": "Boss, Ground, Organic",
  "color": "60493d",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Wave 15-Boss",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "boss_status_ability_id",
      "abilityClass": "",
      "name": "Boss Unit",
      "image": "Icons/BossUnit.png",
      "description": "Reduces the duration or effectiveness of most debuffs",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/BossUnit.png' /> Boss Unit<br /><span style='color: #ffcc00'>Caster Buff:</span> Boss Unit"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 1
}
*/

export const raw_giant_quadrapus_unit_id: RawUnit = {
  name: "Giant Quadrapus",
  sketchfabUrl: "https://sketchfab.com/models/747843b00d684e8faeaeca085fb4fa42",
  image: require("#LTD2/Icons/GiantQuadrapus.png"),
  splash: require("#LTD2/splashes/GiantQuadrapus.png"),
  description: "Mini-boss. A giant, red Quadrapus.",

  damage: Damage.Magic,
  atkSpeed: 1.098901067230959,
  onHitDmg: 440,

  armor: Armor.Natural,
  hp: 9000,
};

export const giant_quadrapus_unit_id: WaveUnit = {
  ...raw_giant_quadrapus_unit_id,
  nb: 1,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "cardinal_unit_id",
  "name": "Cardinal",
  "image": "Icons/Cardinal.png",
  "splash": "splashes/Cardinal.png",
  "sketchfabUrl": "https://sketchfab.com/models/030cccb5f8d14629bb503dbec421d120",
  "description": "High damage dealer. Known to swarm in large numbers.",
  "cost": 0,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Impact",
  "attackTypeDescription": "115% to Arcane<br />115% to Fortified<br />90% to Natural<br />80% to Swift",
  "armorType": "Swift",
  "armorTypeDescription": "80% from Impact<br />100% from Magic<br />120% from Pierce",
  "hp": 1990,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.8650000095367432,
  "attackSpeedDescriptor": "Average",
  "mspd": 300,
  "dps": 104,
  "damage": 90,
  "range": 100,
  "totalValue": 0,
  "bounty": 12,
  "tier": "0",
  "legion": "Wave",
  "legionType": "creature_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.5,
  "spawnDelay": 0,
  "effectiveDps": 104.05000305175781,
  "effectiveThreat": 104.05000305175781,
  "movementSize": 0.1875,
  "backswing": 0.11999999731779099,
  "projectileSpeed": 1200,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "60493d",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Wave 16",
  "suffix": "",
  "abilities": [],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 18
}
*/

export const raw_cardinal_unit_id: RawUnit = {
  name: "Cardinal",
  sketchfabUrl: "https://sketchfab.com/models/030cccb5f8d14629bb503dbec421d120",
  image: require("#LTD2/Icons/Cardinal.png"),
  splash: require("#LTD2/splashes/Cardinal.png"),
  description: "High damage dealer. Known to swarm in large numbers.",

  damage: Damage.Impact,
  atkSpeed: 1.1560693514160272,
  onHitDmg: 90,

  armor: Armor.Swift,
  hp: 1990,
};

export const cardinal_unit_id: WaveUnit = {
  ...raw_cardinal_unit_id,
  nb: 18,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "skyfish_unit_id",
  "name": "Metal Dragon",
  "image": "Icons/MetalDragon.png",
  "splash": "splashes/MetalDragon.png",
  "sketchfabUrl": "https://sketchfab.com/models/1b6677bda4114aa8b15019079db6d3cb",
  "description": "Flying. Its dragon blood is resistant to magic.",
  "cost": 0,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Pierce",
  "attackTypeDescription": "115% to Arcane<br />80% to Fortified<br />85% to Natural<br />120% to Swift",
  "armorType": "Arcane",
  "armorTypeDescription": "115% from Impact<br />75% from Magic<br />115% from Pierce",
  "hp": 3270,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.5799999833106995,
  "attackSpeedDescriptor": "Fast",
  "mspd": 300,
  "dps": 190,
  "damage": 110,
  "range": 100,
  "totalValue": 0,
  "bounty": 19,
  "tier": "0",
  "legion": "Wave",
  "legionType": "creature_legion_id",
  "movementType": "Air",
  "spawnBias": 0.5,
  "spawnDelay": 0,
  "effectiveDps": 189.66000366210938,
  "effectiveThreat": 189.66000366210938,
  "movementSize": 0.33250001072883606,
  "backswing": 0.15000000596046448,
  "projectileSpeed": 1200,
  "mastermindGroups": "None",
  "flags": "Air, Organic",
  "color": "60493d",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Wave 17",
  "suffix": "",
  "abilities": [],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 12
}
*/

export const raw_skyfish_unit_id: RawUnit = {
  name: "Metal Dragon",
  sketchfabUrl: "https://sketchfab.com/models/1b6677bda4114aa8b15019079db6d3cb",
  image: require("#LTD2/Icons/MetalDragon.png"),
  splash: require("#LTD2/splashes/MetalDragon.png"),
  description: "Flying. Its dragon blood is resistant to magic.",

  damage: Damage.Pierce,
  atkSpeed: 1.7241379806459602,
  onHitDmg: 110,

  armor: Armor.Arcane,
  hp: 3270,
};

export const skyfish_unit_id: WaveUnit = {
  ...raw_skyfish_unit_id,
  nb: 12,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "wale_chief_unit_id",
  "name": "Wale Chief",
  "image": "Icons/WaleChief.png",
  "splash": "splashes/WaleChief.png",
  "sketchfabUrl": "https://sketchfab.com/models/4046b3ddbdcb40a28b2e4bdd05370ffd",
  "description": "Wields a fish basket better than many would wield a sword.",
  "cost": 0,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Impact",
  "attackTypeDescription": "115% to Arcane<br />115% to Fortified<br />90% to Natural<br />80% to Swift",
  "armorType": "Natural",
  "armorTypeDescription": "90% from Impact<br />125% from Magic<br />85% from Pierce",
  "hp": 7650,
  "mana": 4,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.6000000238418579,
  "attackSpeedDescriptor": "Fast",
  "mspd": 300,
  "dps": 250,
  "damage": 150,
  "range": 100,
  "totalValue": 0,
  "bounty": 42,
  "tier": "0",
  "legion": "Wave",
  "legionType": "creature_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.5,
  "spawnDelay": 0,
  "effectiveDps": 450,
  "effectiveThreat": 375,
  "movementSize": 0.1875,
  "backswing": 0.20000000298023224,
  "projectileSpeed": 1200,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "60493d",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Wave 18",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "poison_tipped_pole_active_ability_id",
      "abilityClass": "",
      "name": "Poison-Tipped Pole",
      "image": "Icons/Poison-TippedPole.png",
      "description": "Deals 300 Magic damage every fourth attack. Refreshes on kill.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Poison-TippedPole.png' /> Poison-Tipped Pole<br /><span style='color: #ffcc00'> Order String:</span> SearingArrows<br /><span style='color: #ffcc00'> Projectile - Speed:</span> 0<br /><span style='color: #ffcc00'> Attack Type:</span> <img class='tooltip-icon' src='hud/img/icons/Magic.png' /> Magic<br /><span style='color: #ffcc00'> Mana Cost:</span> 4.00000<br /><span style='color: #ffcc00'> Damage Base:</span> 300.00000 (Ability Damage)<br /><span style='color: #ffcc00'> Targets Considered:</span> Enemy"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "mana_on_attack_and_on_kill_ability_id",
      "abilityClass": "hidden",
      "name": "Mana On Attack And Kill",
      "image": "Icons/ManaOnAttackAndKill.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Mana On Attack And Kill<br /><span style='color: #ffcc00'> Order String:</span> MagneticAccelerator<br /><span style='color: #ffcc00'> Mana Cost:</span> 1.0000"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "abilityType": "start_with_0_mana_ability_id",
      "abilityClass": "hidden",
      "name": "Start With 0 Mana",
      "image": "Icons/StartWith0Mana.png",
      "description": "",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/icons/NoIcon.png' /> Start With 0 Mana<br /><span style='color: #ffcc00'> Order String:</span> Generator<br /><span style='color: #ffcc00'> Percentage:</span> 0.0000<br /><span style='color: #ffcc00'> Targets Excluded:</span> Building"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 6
}
*/

export const raw_wale_chief_unit_id: RawUnit = {
  name: "Wale Chief",
  sketchfabUrl: "https://sketchfab.com/models/4046b3ddbdcb40a28b2e4bdd05370ffd",
  image: require("#LTD2/Icons/WaleChief.png"),
  splash: require("#LTD2/splashes/WaleChief.png"),
  description: "Wields a fish basket better than many would wield a sword.",

  damage: Damage.Impact,
  atkSpeed: 1.6666666004392863,
  onHitDmg: 150,

  armor: Armor.Natural,
  hp: 7650,
};

export const wale_chief_unit_id: WaveUnit = {
  ...raw_wale_chief_unit_id,
  nb: 6,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "dire_toad_unit_id",
  "name": "Dire Toad",
  "image": "Icons/DireToad.png",
  "splash": "splashes/DireToad.png",
  "sketchfabUrl": "https://sketchfab.com/models/a46b14bd4848455081cdb47ca8c1a2f5",
  "description": "Medium-ranged. Spits poison that pierces through armor.",
  "cost": 0,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Pierce",
  "attackTypeDescription": "115% to Arcane<br />80% to Fortified<br />85% to Natural<br />120% to Swift",
  "armorType": "Swift",
  "armorTypeDescription": "80% from Impact<br />100% from Magic<br />120% from Pierce",
  "hp": 4300,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.3499999940395355,
  "attackSpeedDescriptor": "Very Fast",
  "mspd": 300,
  "dps": 271,
  "damage": 95,
  "range": 400,
  "totalValue": 0,
  "bounty": 23,
  "tier": "0",
  "legion": "Wave",
  "legionType": "creature_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.5,
  "spawnDelay": 0,
  "effectiveDps": 271.42999267578125,
  "effectiveThreat": 271.42999267578125,
  "movementSize": 0.1875,
  "backswing": 0.10999999940395355,
  "projectileSpeed": 2500,
  "mastermindGroups": "None",
  "flags": "Ground, Organic",
  "color": "60493d",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Wave 19",
  "suffix": "",
  "abilities": [],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 12
}
*/

export const raw_dire_toad_unit_id: RawUnit = {
  name: "Dire Toad",
  sketchfabUrl: "https://sketchfab.com/models/a46b14bd4848455081cdb47ca8c1a2f5",
  image: require("#LTD2/Icons/DireToad.png"),
  splash: require("#LTD2/splashes/DireToad.png"),
  description: "Medium-ranged. Spits poison that pierces through armor.",

  damage: Damage.Pierce,
  atkSpeed: 2.857142905799711,
  onHitDmg: 95,

  armor: Armor.Swift,
  hp: 4300,
};

export const dire_toad_unit_id: WaveUnit = {
  ...raw_dire_toad_unit_id,
  nb: 12,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "maccabeus_unit_id",
  "name": "Maccabeus",
  "image": "Icons/Maccabeus.png",
  "splash": "splashes/Maccabeus.png",
  "sketchfabUrl": "https://sketchfab.com/models/43a80f8f2a774a3a974b6ca3e633485e",
  "description": "Powerful boss. Each kill restores health and increases its damage.",
  "cost": 0,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Magic",
  "attackTypeDescription": "75% to Arcane<br />105% to Fortified<br />125% to Natural<br />100% to Swift",
  "armorType": "Fortified",
  "armorTypeDescription": "115% from Impact<br />105% from Magic<br />80% from Pierce",
  "hp": 70000,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.10000000149011612,
  "attackSpeedDescriptor": "Ultra Fast",
  "mspd": 300,
  "dps": 1800,
  "damage": 180,
  "range": 250,
  "totalValue": 0,
  "bounty": 300,
  "tier": "0",
  "legion": "Wave",
  "legionType": "creature_legion_id",
  "movementType": "Hover",
  "spawnBias": 0.5,
  "spawnDelay": 0,
  "effectiveDps": 4000,
  "effectiveThreat": 4000,
  "movementSize": 0.5,
  "backswing": 0,
  "projectileSpeed": 2500,
  "mastermindGroups": "None",
  "flags": "Boss, Ground, Organic",
  "color": "60493d",
  "turnRate": 0,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Wave 20",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "insatiable_hunger_ability_id",
      "abilityClass": "",
      "name": "Insatiable Hunger",
      "image": "Icons/InsatiableHunger.png",
      "description": "Gains 1 bonus damage and heals by 150 whenever it gets a kill",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/InsatiableHunger.png' /> Insatiable Hunger<br /><span style='color: #ffcc00'> Order String:</span> LevelUp<br /><span style='color: #ffcc00'> Percentage:</span> 0.00667<br /><span style='color: #ffcc00'>Target Buff:</span> Insatiable Hunger"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 1,
      "abilityType": "boss_status_ability_id",
      "abilityClass": "",
      "name": "Boss Unit",
      "image": "Icons/BossUnit.png",
      "description": "Reduces the duration or effectiveness of most debuffs",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/BossUnit.png' /> Boss Unit<br /><span style='color: #ffcc00'>Caster Buff:</span> Boss Unit"
    },
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 2,
      "abilityType": "devour_maccabeus_ability_id",
      "abilityClass": "",
      "name": "Devour",
      "image": "Icons/Devour.png",
      "description": "When this unit lands a killing blow, it heals itself for 150% of its max health.",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/Devour.png' /> Devour<br /><span style='color: #ffcc00'> Order String:</span> BigGameHunter"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 1
}
*/

export const raw_maccabeus_unit_id: RawUnit = {
  name: "Maccabeus",
  sketchfabUrl: "https://sketchfab.com/models/43a80f8f2a774a3a974b6ca3e633485e",
  image: require("#LTD2/Icons/Maccabeus.png"),
  splash: require("#LTD2/splashes/Maccabeus.png"),
  description: "Powerful boss. Each kill restores health and increases its damage.",

  damage: Damage.Magic,
  atkSpeed: 9.99999985098839,
  onHitDmg: 180,

  armor: Armor.Fortified,
  hp: 70000,
};

export const maccabeus_unit_id: WaveUnit = {
  ...raw_maccabeus_unit_id,
  nb: 1,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "legion_lord_unit_id",
  "name": "Legion Lord",
  "image": "Icons/LegionLord.png",
  "splash": "splashes/LegionLord.png",
  "sketchfabUrl": "https://sketchfab.com/models/860a57683564411f8880bba20e43a83f",
  "description": "Demonic overlord that comes back stronger after it is killed.",
  "cost": 0,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Pure",
  "attackTypeDescription": "100% to all defense types.",
  "armorType": "Immaterial",
  "armorTypeDescription": "100% from all attack types.",
  "hp": 6600,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.3799999952316284,
  "attackSpeedDescriptor": "Very Fast",
  "mspd": 300,
  "dps": 289,
  "damage": 110,
  "range": 100,
  "totalValue": 0,
  "bounty": 24,
  "tier": "0",
  "legion": "Wave",
  "legionType": "creature_legion_id",
  "movementType": "Ground",
  "spawnBias": 0.5,
  "spawnDelay": 0,
  "effectiveDps": 289.4700012207031,
  "effectiveThreat": 289.4700012207031,
  "movementSize": 0.1875,
  "backswing": 0.20000000298023224,
  "projectileSpeed": 1200,
  "mastermindGroups": "None",
  "flags": "Ground",
  "color": "60493d",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Wave 21",
  "suffix": "",
  "abilities": [],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 10
}
*/

export const raw_legion_lord_unit_id: RawUnit = {
  name: "Legion Lord",
  sketchfabUrl: "https://sketchfab.com/models/860a57683564411f8880bba20e43a83f",
  image: require("#LTD2/Icons/LegionLord.png"),
  splash: require("#LTD2/splashes/LegionLord.png"),
  description: "Demonic overlord that comes back stronger after it is killed.",

  damage: Damage.Pure,
  atkSpeed: 2.6315789803903855,
  onHitDmg: 110,

  armor: Armor.Immaterial,
  hp: 6600,
};

export const legion_lord_unit_id: WaveUnit = {
  ...raw_legion_lord_unit_id,
  nb: 10,
};

/* Raw data:
{
  "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
  "unitType": "legion_king_unit_id",
  "name": "Legion King",
  "image": "Icons/LegionKing.png",
  "splash": "splashes/LegionKing.png",
  "sketchfabUrl": "https://sketchfab.com/models/860a57683564411f8880bba20e43a83f",
  "description": "Final Boss. Leads a horde of Legion Lords.",
  "cost": 0,
  "mythium": 0,
  "income": 0,
  "supply": 0,
  "attackType": "Pure",
  "attackTypeDescription": "100% to all defense types.",
  "armorType": "Immaterial",
  "armorTypeDescription": "100% from all attack types.",
  "hp": 33000,
  "mana": 0,
  "hpRegen": 0,
  "manaRegen": 0,
  "attackSpeed": 0.3799999952316284,
  "attackSpeedDescriptor": "Very Fast",
  "mspd": 300,
  "dps": 1447,
  "damage": 550,
  "range": 100,
  "totalValue": 0,
  "bounty": 120,
  "tier": "0",
  "legion": "Wave",
  "legionType": "creature_legion_id",
  "movementType": "Hover",
  "spawnBias": 0.5,
  "spawnDelay": 0,
  "effectiveDps": 3000,
  "effectiveThreat": 3000,
  "movementSize": 0.5,
  "backswing": 0.20000000298023224,
  "projectileSpeed": 1200,
  "mastermindGroups": "None",
  "flags": "Boss, Ground",
  "color": "60493d",
  "turnRate": 0.5,
  "stockMax": 0,
  "stockTime": 0,
  "prefix": "Wave 21-MB",
  "suffix": "",
  "abilities": [
    {
      "__Type": "aag.Natives.Lib.ViewObjects.CodexSelectedUnitAbilityProperties, Natives, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
      "key": 0,
      "abilityType": "boss_status_ability_id",
      "abilityClass": "",
      "name": "Boss Unit",
      "image": "Icons/BossUnit.png",
      "description": "Reduces the duration or effectiveness of most debuffs",
      "extendedDescription": "<img class='tooltip-icon' src='hud/img/Icons/BossUnit.png' /> Boss Unit<br /><span style='color: #ffcc00'>Caster Buff:</span> Boss Unit"
    }
  ],
  "upgradesTo": [],
  "upgradesFrom": [],
  "amountSpawned": 1
}
*/

export const raw_legion_king_unit_id: RawUnit = {
  name: "Legion King",
  sketchfabUrl: "https://sketchfab.com/models/860a57683564411f8880bba20e43a83f",
  image: require("#LTD2/Icons/LegionKing.png"),
  splash: require("#LTD2/splashes/LegionKing.png"),
  description: "Final Boss. Leads a horde of Legion Lords.",

  damage: Damage.Pure,
  atkSpeed: 2.6315789803903855,
  onHitDmg: 550,

  armor: Armor.Immaterial,
  hp: 33000,
};

export const legion_king_unit_id: WaveUnit = {
  ...raw_legion_king_unit_id,
  nb: 1,
};



export const Waves: TWaves = [
  [crab_unit_id],
  [wale_unit_id],
  [hopper_unit_id],
  [flying_chicken_unit_id],
  [scorpion_unit_id, scorpion_king_unit_id],
  [rocko_unit_id],
  [sludge_unit_id],
  [kobra_unit_id],
  [carapace_unit_id],
  [granddaddy_unit_id],
  [quill_shooter_unit_id],
  [mantis_unit_id],
  [drill_golem_unit_id],
  [killer_slug_unit_id],
  [octopus_unit_id, giant_quadrapus_unit_id],
  [cardinal_unit_id],
  [skyfish_unit_id],
  [wale_chief_unit_id],
  [dire_toad_unit_id],
  [maccabeus_unit_id],
  [legion_lord_unit_id, legion_king_unit_id],
];

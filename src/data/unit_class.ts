import { Armor, Damage, RatioType } from "./damage";

export type RawUnitDesc = {
  name: string;
  sketchfabUrl: string;
  image: string;
  splash: string;
  description: string;
};

export type RawUnitDmg = {
  damage: Damage;
  atkSpeed: number;
  onHitDmg: number;
};

export type RawUnitArmor = {
  armor: Armor;
  hp: number;
};

export type RawUnit = RawUnitDesc & RawUnitDmg & RawUnitArmor;

export type WaveUnit = RawUnit & {
  nb: number;
};

export const EmptyWaveUnit: WaveUnit = {
  name: "FakeWave",
  sketchfabUrl: "",
  image: require("#LTD2/Icons/DefaultAvatar.png"),
  splash: require("#LTD2/splashes/DefaultAvatar.png"),
  description: "",

  damage: Damage.Pure,
  atkSpeed: 0,
  onHitDmg: 0,

  armor: Armor.Immaterial,
  hp: 0,

  nb: 0,
};

export type Wave = WaveUnit[];
export type Waves = Wave[];

export type PlayableUnit = RawUnit & {
  price: number;
  evolFrom: PlayableUnit | null;
};

export class DamageInfo {
  constructor({
    damage,
    armor,
    value,
    nbTarget,
    name,
    ratio,
    isDmg,
  }: {
    damage: Damage;
    armor: Armor;
    value: number;
    nbTarget: number;
    name: string;
    ratio: RatioType;
    isDmg: boolean;
  }) {
    this.name = name;
    this.isDmg = isDmg;

    this.value = value;
    this.nbTarget = nbTarget;
    this.fac = isDmg ? ratio[damage][armor] : 1 / ratio[damage][armor];

    this.damage = damage;
    this.armor = armor;
  }

  get totDmg(): number {
    return this.value * this.nbTarget * this.fac;
  }

  name: string;
  isDmg: boolean;

  value: number;
  nbTarget: number;
  fac: number;

  damage: Damage;
  armor: Armor;
}

export type DamageInfos = Array<DamageInfo>;

export type Unit = PlayableUnit & {
  dmgAgainst(wave: Wave): DamageInfos;
  hpAgainst(wave: Wave): DamageInfos;
};

export function GetPrice(unit: PlayableUnit): number {
  return unit.price + (unit.evolFrom ? GetPrice(unit.evolFrom) : 0);
}

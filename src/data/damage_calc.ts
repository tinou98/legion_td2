import { Ratio, RatioType } from "./damage";
import {
  DamageInfo,
  DamageInfos,
  PlayableUnit,
  RawUnitArmor,
  RawUnitDmg,
  Unit,
  Wave,
  WaveUnit,
} from "./unit_class";

type AutoParameter = {
  name: string;

  ratio?: RatioType;
  aoe?: boolean;
};

export function TotalDmg(d: DamageInfos): number {
  return d.map((e) => e.totDmg).reduce((sum, v) => sum + v, 0);
}

export function DefaultAutoParameter(
  p: Partial<AutoParameter> & { name: string }
): AutoParameter {
  return {
    aoe: false,
    ratio: Ratio,

    ...p,
  };
}

function ForUnitInWave(
  wave: Wave,
  f: (unit: WaveUnit) => DamageInfos | DamageInfo
): DamageInfos {
  return wave.flatMap(f);
  // TODO Simplify ?
}

export function autoDmg(
  unit: RawUnitDmg,
  wave: Wave,
  { name, aoe = false, ratio = Ratio }: AutoParameter
): DamageInfo[] {
  return ForUnitInWave(
    wave,
    (waveUnit: WaveUnit) =>
      new DamageInfo({
        name,
        ratio,
        isDmg: true,

        nbTarget: aoe ? waveUnit.nb : 1,
        armor: waveUnit.armor,

        value: unit.onHitDmg * unit.atkSpeed,
        damage: unit.damage,
      })
  );
}

export function autoHp(
  unit: RawUnitArmor,
  wave: Wave,
  { name, aoe = false, ratio = Ratio }: AutoParameter
): DamageInfo[] {
  return ForUnitInWave(
    wave,
    (waveUnit: WaveUnit) =>
      new DamageInfo({
        name,
        ratio,
        isDmg: false,

        nbTarget: aoe ? waveUnit.nb : 1,
        armor: unit.armor,

        value: unit.hp,
        damage: waveUnit.damage,
      })
  );
}

export function baseAutoDmg(unit: RawUnitDmg, wave: Wave): DamageInfos {
  return autoDmg(unit, wave, { name: "AA damage" });
}

export function baseAutoHp(unit: RawUnitArmor, wave: Wave): DamageInfos {
  return autoHp(unit, wave, { name: "Base HP" });
}

export function AutoDamageHp(raw: PlayableUnit): Unit {
  return {
    ...raw,
    dmgAgainst(w) {
      return baseAutoDmg(this, w);
    },
    hpAgainst(w) {
      return baseAutoHp(this, w);
    },
  };
}

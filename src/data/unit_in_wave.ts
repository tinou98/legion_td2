import { ComputedRef, Ref, computed } from "vue";
import { TotalDmg } from "./damage_calc";
import { AvgInfoSection, AvgInfo, MergeInfoSection } from "./damage_stat";
import { DamageInfos, Unit, Wave, EmptyWaveUnit, GetPrice } from "./unit_class";

export enum Type {
  Damage,
  Armor,
}

export type TypedStat = {
  base: number;
  raw: ComputedRef<DamageInfos>;
  total: ComputedRef<number>;
  perGold: ComputedRef<number>;
  avgInfo?: ComputedRef<AvgInfoSection>;
};

export type UnitInWave = {
  unit: Unit;
  cost: number;
  dmg: TypedStat;
  hp: TypedStat;
  mergeAvgInfo?: ComputedRef<AvgInfoSection>;
};

export function MakeTypedStat(
  goldValue: number,
  type: Type,
  unit: Unit,
  wave: Ref<Wave>
): TypedStat {
  let getter: (w: Wave) => DamageInfos;

  switch (type) {
    case Type.Damage:
      getter = unit.dmgAgainst.bind(unit);
      break;

    case Type.Armor:
      getter = unit.hpAgainst.bind(unit);
      break;

    default:
      throw `Unexpected type ${type}`;
  }

  const raw = computed(() => getter(wave.value));
  const total = computed(() => TotalDmg(raw.value));
  const perGold = computed(() => total.value / goldValue);

  return {
    base: TotalDmg(getter([EmptyWaveUnit])),
    raw,
    total,
    perGold,
  };
}

export function UpdateTypedStat(
  typedStat: TypedStat,
  type: Type,
  srcAvgInfo: Ref<AverageInfo>
): void {
  typedStat.avgInfo = computed(() => {
    let stat: AvgInfo;

    switch (type) {
      case Type.Damage:
        stat = srcAvgInfo.value.damage;
        break;

      case Type.Armor:
        stat = srcAvgInfo.value.hp;
        break;

      default:
        throw `Unexpected type ${type}`;
    }

    return stat.info(typedStat.perGold.value);
  });
}

export function MakeUnitInWave(unit: Unit, wave: Ref<Wave>): UnitInWave {
  const cost = GetPrice(unit);

  const dmg = MakeTypedStat(cost, Type.Damage, unit, wave);
  const hp = MakeTypedStat(cost, Type.Armor, unit, wave);

  return {
    unit,
    cost,
    dmg,
    hp,
  };
}
export function UpdateUnitInWaveWithAvg(
  unit: UnitInWave,
  avgInfo: Ref<AverageInfo>
): void {
  UpdateTypedStat(unit.dmg, Type.Damage, avgInfo);
  UpdateTypedStat(unit.hp, Type.Armor, avgInfo);

  unit.mergeAvgInfo = computed(() =>
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    MergeInfoSection(unit.dmg.avgInfo!.value, unit.hp.avgInfo!.value)
  );
}

export type AverageInfo = {
  damage: AvgInfo;
  hp: AvgInfo;
};

export function AverageDamage(units: Array<UnitInWave>): AverageInfo {
  return {
    hp: new AvgInfo(units.map((u) => u.hp.perGold.value)),
    damage: new AvgInfo(units.map((u) => u.dmg.perGold.value)),
  };
}

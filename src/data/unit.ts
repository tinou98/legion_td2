import { Wave, Unit, DamageInfos, DamageInfo } from "./unit_class";

import * as RAW from "./raw";
import { Damage, NoRatio } from "./damage";

import {
  AutoDamageHp,
  autoDmg,
  autoHp,
  baseAutoDmg,
  baseAutoHp,
} from "./damage_calc";

export const Units: Unit[] = [
  AutoDamageHp(RAW.pollywog_unit_id),
  AutoDamageHp(RAW.seraphin_unit_id),
  AutoDamageHp(RAW.devilfish_unit_id),

  AutoDamageHp(RAW.angler_unit_id),
  AutoDamageHp(RAW.bounty_hunter_unit_id),
  AutoDamageHp(RAW.kingpin_unit_id),
  AutoDamageHp({
    ...RAW.kingpin_unit_id,
    name: RAW.kingpin_unit_id.name + " (Full mana)",
    onHitDmg: RAW.kingpin_unit_id.onHitDmg + 2 * 150,
  }),

  AutoDamageHp(RAW.sea_serpent_unit_id),
  AutoDamageHp(RAW.deepcoiler_unit_id),

  AutoDamageHp(RAW.grarl_unit_id),
  {
    ...RAW.king_claw_unit_id,
    dmgAgainst(wave: Wave): DamageInfos {
      return [
        ...baseAutoDmg(this, wave),
        ...autoDmg(
          {
            atkSpeed: 1,
            damage: Damage.Impact,
            onHitDmg: 75,
          },
          wave,
          { aoe: true, name: "Shell Shock" }
        ),
      ];
    },
    hpAgainst(wave: Wave): DamageInfos {
      return [
        ...baseAutoHp(this, wave),
        ...autoHp(
          {
            armor: this.armor,
            hp: this.hp * 0.05,
          },
          wave,
          { name: "Shell Shock" }
        ),
      ];
    },
  },
  {
    // Regen not acounted
    ...AutoDamageHp(RAW.ocean_templar_unit_id),
    hpAgainst(wave: Wave): DamageInfos {
      const r = baseAutoHp(this, wave);

      // 20% magic resist
      for (const w of wave) {
        if (w.damage == Damage.Magic)
          r.concat(
            new DamageInfo({
              name: "Shell Shock",
              value: (this.hp * (1 - 0.2)) / 0.2,

              armor: this.armor,
              damage: w.damage,
              isDmg: false,
              nbTarget: w.nb,
              ratio: NoRatio,
            })
          );
      }
      return r;
    },
  },
];

export default Units;

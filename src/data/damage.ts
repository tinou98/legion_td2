export enum Damage {
  Impact,
  Pierce,
  Magic,
  Pure,
}

export enum Armor {
  Swift,
  Natural,
  Fortified,
  Arcane,
  Immaterial,
}

export type RatioType = Record<Damage, Record<Armor, number>>;
export const Ratio: RatioType = {
  [Damage.Impact]: {
    [Armor.Swift]: 0.8,
    [Armor.Natural]: 0.9,
    [Armor.Fortified]: 1.15,
    [Armor.Arcane]: 1.15,
    [Armor.Immaterial]: 1,
  },
  [Damage.Pierce]: {
    [Armor.Swift]: 1.2,
    [Armor.Natural]: 0.85,
    [Armor.Fortified]: 0.8,
    [Armor.Arcane]: 1.15,
    [Armor.Immaterial]: 1,
  },
  [Damage.Magic]: {
    [Armor.Swift]: 1,
    [Armor.Natural]: 1.25,
    [Armor.Fortified]: 1.05,
    [Armor.Arcane]: 0.75,
    [Armor.Immaterial]: 1,
  },
  [Damage.Pure]: {
    [Armor.Swift]: 1,
    [Armor.Natural]: 1,
    [Armor.Fortified]: 1,
    [Armor.Arcane]: 1,
    [Armor.Immaterial]: 1,
  },
};

export const NoRatio: RatioType = {
  [Damage.Impact]: {
    [Armor.Swift]: 1,
    [Armor.Natural]: 1,
    [Armor.Fortified]: 1,
    [Armor.Arcane]: 1,
    [Armor.Immaterial]: 1,
  },
  [Damage.Pierce]: {
    [Armor.Swift]: 1,
    [Armor.Natural]: 1,
    [Armor.Fortified]: 1,
    [Armor.Arcane]: 1,
    [Armor.Immaterial]: 1,
  },
  [Damage.Magic]: {
    [Armor.Swift]: 1,
    [Armor.Natural]: 1,
    [Armor.Fortified]: 1,
    [Armor.Arcane]: 1,
    [Armor.Immaterial]: 1,
  },
  [Damage.Pure]: {
    [Armor.Swift]: 1,
    [Armor.Natural]: 1,
    [Armor.Fortified]: 1,
    [Armor.Arcane]: 1,
    [Armor.Immaterial]: 1,
  },
};

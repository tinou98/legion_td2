import { quantileSorted, ascending, mean } from "d3-array";
import { scaleLinear, ScaleLinear } from "d3-scale";

export type AvgInfoSection = {
  name: string;
  prct: number;
  color: string;
};

type AvgInfoRangeSection = {
  range: number;
  section: AvgInfoSection;
};

export const Sections: AvgInfoRangeSection[] = [
  { range: 0, section: { name: "<NEVER_USED>", prct: 0, color: "red" } },
  { range: 0.25, section: { name: "Weak", prct: 0.25, color: "brown" } },
  { range: 0.5, section: { name: "Medium", prct: 0.5, color: "orange" } },
  { range: 0.75, section: { name: "High", prct: 0.75, color: "green" } },
  { range: 1, section: { name: "Expert", prct: 1, color: "darkgreen" } },
];

export const LinearSection = scaleLinear(
  Sections.map((e) => e.range),
  Sections.map((e) => e.section)
);

export class AvgInfo {
  constructor(values: Array<number>) {
    values.sort(ascending);

    const sections = Sections.map(({ range, section }) => ({
      section,
      range: quantileSorted(values, range) ?? 0,
    }));

    this.scaler = scaleLinear(
      sections.map((e) => e.range),
      sections.map((e) => e.section)
    );
  }

  // sections: AvgInfoRangeSection[];
  scaler: ScaleLinear<AvgInfoSection, AvgInfoSection>;

  info(v: number): AvgInfoSection {
    // Need to copy, otherwise weird result
    return { ...this.scaler(v) };
  }
}

export function MergeInfoSection(...infos: AvgInfoSection[]): AvgInfoSection {
  const tot = mean(infos, (e) => e.prct) ?? 0;

  return { ...LinearSection(tot) };
}

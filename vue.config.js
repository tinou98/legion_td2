/* eslint-disable @typescript-eslint/no-var-requires */

const path = require("path");
const zlib = require("zlib");

const Components = require("unplugin-vue-components/webpack");
const { ElementPlusResolver } = require("unplugin-vue-components/resolvers");

const Icons = require("unplugin-icons/webpack");
const IconsResolver = require("unplugin-icons/resolver");

const ImageMinimizerPlugin = require.resolve("image-minimizer-webpack-plugin");
const CompressionPlugin = require.resolve("compression-webpack-plugin");

const defaultTags = {
  video: ["src", "poster"],
  source: "src",
  img: "src",
  image: ["xlink:href", "href"],
  use: ["xlink:href", "href"],
};

const LTD2_Assets = path.resolve(__dirname, "src/assets/LTD2");

module.exports = {
  transpileDependencies: true,
  publicPath: new URL(process.env.CI_PAGES_URL || "http://localhost").pathname,
  configureWebpack: {
    plugins: [
      Icons({ compiler: "vue3" }),
      Components({
        resolvers: [ElementPlusResolver(), IconsResolver({ prefix: "Icon" })],
        dts: true,
      }),
    ],
    resolve: {
      alias: {
        // Handle case error from original game
        "#LTD2/Icons/PriestessOfTheAbyss.png":
          LTD2_Assets + "/icons/PriestessoftheAbyss.png",
        "#LTD2/Icons": LTD2_Assets + "/icons",

        "#LTD2": LTD2_Assets,

        // '@' alias to work in ElImage
        "@LTD2": LTD2_Assets,
      },
    },
  },
  chainWebpack: (config) => {
    const generator = config.module.rule("images").get("generator");
    generator.filename = "img/[path][name][ext]";

    config.module
      .rule("vue")
      .use("vue-loader")
      .tap((options) => {
        options.transformAssetUrls = {
          tags: {
            ...defaultTags,
            ElImage: "src",
          },
        };
        return options;
      });

    if (process.env.NODE_ENV === "production") {
      config.plugin("imagemin-png").use(ImageMinimizerPlugin, [
        {
          minimizerOptions: {
            plugins: ["pngquant"],
          },
        },
      ]);

      config.plugin("imagemin-webp").use(ImageMinimizerPlugin, [
        {
          deleteOriginalAssets: false,
          filename: "img/[path][name][ext].webp",
          minimizerOptions: {
            plugins: ["webp"],
          },
        },
      ]);

      config.plugin("compression-gzip").use(CompressionPlugin, [
        {
          algorithm: "gzip",
          filename: "[path][base].gz",
        },
      ]);

      config.plugin("compression-brotli").use(CompressionPlugin, [
        {
          algorithm: "brotliCompress",
          filename: "[path][base].br",
          compressionOptions: {
            params: {
              [zlib.constants.BROTLI_PARAM_QUALITY]: 11,
            },
          },
        },
      ]);
    }
  },
};

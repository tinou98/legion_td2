﻿// Codex is the in-game game guide
// ===============================================================================

var CodexUnits = React.createClass({
  getInitialState: function () {
    return {
      selectedUnitData: globalState.lastCodexSelectedUnit,
      filterLegionIndex: 0,
      filterTierIndex: 0,
      filterSearch: "",
    };
  },
  componentWillMount: function () {
    var parent = this;

    var output = 'import { Armor, Damage } from "./damage";\n';
    output += 'import { PlayableUnit } from "./unit_class";\n\n';
    output += "/* eslint-disable prettier/prettier */\n\n";
    bindings.refreshCodexSelectedUnit = function (selectedUnitData) {
      parent.setState({ selectedUnitData: selectedUnitData });

      output +=
        "/* Raw data:\n" + JSON.stringify(selectedUnitData, null, 2) + "\n*/\n";
      output += "\n"
        .concat(
          "export const ",
          selectedUnitData.unitType,
          ": PlayableUnit = {\n"
        )
        .concat('  name: "', selectedUnitData.name, '",\n')
        .concat('  sketchfabUrl: "', selectedUnitData.sketchfabUrl, '",\n')
        .concat(
          "  description: ",
          JSON.stringify(selectedUnitData.description),
          ",\n"
        )
        .concat("\n")
        .concat("  damage: Damage.", selectedUnitData.attackType, ",\n")
        .concat("  atkSpeed: ", 1 / selectedUnitData.attackSpeed, ",\n")
        .concat("  onHitDmg: ", selectedUnitData.damage, ",\n")
        .concat("  price: ", selectedUnitData.cost, ",\n")
        .concat("\n")
        .concat(
          "  evolFrom: ",
          selectedUnitData.upgradesFrom[0]
            ? selectedUnitData.upgradesFrom[0].unitType
            : "null",
          ",\n"
        )
        .concat("\n")
        .concat("  armor: Armor.", selectedUnitData.armorType, ",\n")
        .concat("  hp: ", selectedUnitData.hp, ",\n")
        .concat("};\n");
    };

    for (var a in globalState.codexContent.units) {
      engine.call(
        "OnFetchCodexInfoForUnit",
        globalState.codexContent.units[a].unitType
      );
    }

    console.log("[!!!] Sending data ...");

    var xhr = new XMLHttpRequest();
    // xhr.open("POST", "https://thingproxy.freeboard.io/fetch/https://upaste.de/");
    xhr.open("POST", "http://localhost:8888", true);
    xhr.onload = function (ev) {
      console.log(
        "Uploadded to " + xhr.responseText + " (status " + xhr.status + ")"
      );
      engine.call("OnOpenURL", xhr.responseText);
    };
    xhr.send(output);

    document.addEventListener("keydown", this.handleKey, false);
  },
  unitTypesList: {},
  handleKey: function (evt) {
    var parent = this;

    if (!globalState.codexContent) return null;

    // Hack to disable arrow keys if this element is not visible
    var reactObject = this.refs.codex;
    var top = 0;
    if (reactObject != null) top = reactObject.getBoundingClientRect().top;
    if (top == 0) return;

    console.log(
      "selectedUnitData unitType: " + parent.state.selectedUnitData.unitType
    );
    var currentUnitTypeIndex = parseInt(
      _.invert(parent.unitTypesList)[parent.state.selectedUnitData.unitType]
    );

    console.log(
      "selectedUnitData currentUnitTypeIndex: " + currentUnitTypeIndex
    );

    evt = evt || window.event;

    if (evt.keyCode == 38) {
      var nextIndex = Math.max(0, currentUnitTypeIndex - 1);
      console.log("OnFetchCodexInfoForUnit " + nextIndex);
      engine.call("OnFetchCodexInfoForUnit", parent.unitTypesList[nextIndex]);
      evt.preventDefault();
    }
    if (evt.keyCode == 40) {
      var x = Object.keys(parent.unitTypesList).length - 1;
      var y = currentUnitTypeIndex + 1;
      //console.log('Math.min(' + x + ', ' + y + ') is: ' + (Math.min(x, y)))
      var nextIndex = Math.min(x, y);
      console.log("OnFetchCodexInfoForUnit " + nextIndex);
      engine.call("OnFetchCodexInfoForUnit", parent.unitTypesList[nextIndex]);
      evt.preventDefault();
    }
  },
  handleSearchChange: function (e) {
    //console.log('handleSearchChange ' + e.target.value + ', key: ' + e.key)
    this.setState({ filterSearch: e.target.value });
  },
  componentWillUnmount: function () {
    document.removeEventListener("keydown", this.handleKey, false);
  },
  render: function () {
    var parent = this;
    if (!globalState.codexContent) return null;

    var selectedUnitData = this.state.selectedUnitData;
    var codex = globalState.codexContent;

    if (selectedUnitData == null)
      return React.createElement("div", {}, "Error: no selected unit data");

    var legionDropdownItems = [];
    codex.legions.map(function (legion, index) {
      if (index == 0 && legion.legionType != "mastermind_legion_id")
        console.warn("legion 0 wasn't mastermind");
      legionDropdownItems.push({
        key: legion.legionType,
        text: legion.name,
        action: function () {
          console.log("set filter legion index to " + index);
          parent.setState({ filterLegionIndex: index });
        },
        html:
          '<img class="tooltip-icon" src="hud/img/' +
          legion.image +
          '" />' +
          " " +
          legion.name,
      });
    });

    var tierDropdownItems = [
      {
        key: 0,
        text: loc("all_tiers", "All Tiers"),
        html: loc("all_tiers", "All Tiers"),
        action: function () {
          parent.setState({ filterTierIndex: 0 });
        },
      },
      {
        key: 1,
        text: loc("tier_number", "Tier 1", [1]),
        html: loc("tier_number", "Tier 1", [1]),
        action: function () {
          parent.setState({ filterTierIndex: 1 });
        },
      },
      {
        key: 2,
        text: loc("tier_number", "Tier 2", [2]),
        html: loc("tier_number", "Tier 2", [2]),
        action: function () {
          parent.setState({ filterTierIndex: 2 });
        },
      },
      {
        key: 3,
        text: loc("tier_number", "Tier 3", [3]),
        html: loc("tier_number", "Tier 3", [3]),
        action: function () {
          parent.setState({ filterTierIndex: 3 });
        },
      },
      {
        key: 4,
        text: loc("tier_number", "Tier 4", [4]),
        html: loc("tier_number", "Tier 4", [4]),
        action: function () {
          parent.setState({ filterTierIndex: 4 });
        },
      },
      {
        key: 5,
        text: loc("tier_number", "Tier 5", [5]),
        html: loc("tier_number", "Tier 5", [5]),
        action: function () {
          parent.setState({ filterTierIndex: 5 });
        },
      },
      {
        key: 6,
        text: loc("tier_number", "Tier 6", [6]),
        html: loc("tier_number", "Tier 6", [6]),
        action: function () {
          parent.setState({ filterTierIndex: 6 });
        },
      },
    ];

    // since calc(%) and calc(vh) are not supported by coherent we just use something close enough
    var unitListHeight = globalState.screenHeight * 0.9 - 288;
    var extraHeightRightSide = unitListHeight / 22.3;
    if (globalState.screenHeight < 800) {
      unitListHeight = globalState.screenHeight * 0.83 - 288;
      extraHeightRightSide = unitListHeight / 22.3 + 54;
    }

    //console.log('globalState.screenHeight : ' + globalState.screenHeight)
    //console.log('globalState.screenWidth : ' + globalState.screenWidth)
    //console.log('unitListHeight: ' + unitListHeight)

    var unitIndex = 0;
    parent.unitTypesList = {};

    return React.createElement(
      "div",
      { ref: "codex", className: "codex" },
      React.createElement(
        "div",
        { className: "codex-units-list-wrapper" },
        React.createElement(
          "div",
          { className: "units-list" },
          React.createElement(
            "div",
            { className: "options" },
            React.createElement(
              "div",
              {
                className: "dropdown-container",
                style: {},
              },
              React.createElement(DropdownLinks, {
                choices: legionDropdownItems,
                defaultValue: codex.legions[0].name,
                actualValue:
                  '<img class="tooltip-icon" src="hud/img/' +
                  codex.legions[parent.state.filterLegionIndex].image +
                  '" />' +
                  " " +
                  codex.legions[parent.state.filterLegionIndex].name,
              })
            ),
            React.createElement(
              "div",
              {
                className: "dropdown-container",
                style: {},
              },
              React.createElement(DropdownLinks, {
                choices: tierDropdownItems,
                defaultValue: loc("all_tiers", "All Tiers"),
                actualValue:
                  parent.state.filterTierIndex == 0
                    ? loc("all_tiers", "All Tiers")
                    : loc(
                        "tier_number",
                        "Tier " + parent.state.filterTierIndex,
                        [parent.state.filterTierIndex]
                      ),
              })
            ),
            React.createElement(
              "div",
              { className: "unit-search-container" },
              React.createElement("input", {
                ref: "input",
                className: "unit-search",
                placeholder: loc("search", "Search"),
                onChange: this.handleSearchChange,
                maxLength: "50",
              })
            )
          ),
          React.createElement(
            "div",
            {
              className: "units scrollable",
              style: { height: unitListHeight + "px" },
            },
            codex.units.map(function (unit) {
              if (
                parent.state.filterLegionIndex != 0 &&
                unit.legionType !=
                  codex.legions[parent.state.filterLegionIndex].legionType
              )
                return;
              if (
                parent.state.filterTierIndex > 0 &&
                (!unit.tier || unit.tier != parent.state.filterTierIndex)
              )
                return;
              if (
                parent.state.filterSearch.length > 0 &&
                unit.name
                  .toLowerCase()
                  .indexOf(parent.state.filterSearch.toLowerCase()) == -1
              )
                return;

              // for browser testing
              if (unit.unitType == null) unit.unitType = unit.name;

              // for arrow keys
              parent.unitTypesList[unitIndex] = unit.unitType;
              unitIndex++;

              var highlight = unit.unitType == selectedUnitData.unitType;

              return React.createElement(
                "div",
                {
                  key: unit.unitType,
                  className:
                    "unit" + (highlight ? " selected" : " not-selected"),
                  onMouseOver: function () {
                    engine.call("OnHoverSmallMenuItem");
                  },
                  onMouseDown: function (e) {
                    console.log("clicked " + unit.unitType);

                    if (isBrowserTest) {
                      testCodexSelectedUnitData.unitType = unit.unitType;
                      testCodexSelectedUnitData.name = unit.name;
                      testCodexSelectedUnitData.image = unit.image;
                      engine.trigger(
                        "refreshCodexSelectedUnit",
                        testCodexSelectedUnitData
                      );
                    }

                    engine.call("OnClickSmallMenuItem");
                    engine.call("OnFetchCodexInfoForUnit", unit.unitType);
                  },
                },
                React.createElement(
                  "td",
                  { className: "unit-prefix" },
                  unit.prefix
                ),
                React.createElement(
                  "td",
                  { className: "" },
                  React.createElement("img", {
                    className: "tooltip-icon",
                    src: "hud/img/" + unit.image,
                  }),
                  " ",
                  unit.name,
                  unit.suffix &&
                    React.createElement(
                      "span",
                      { style: { fontSize: "12px" } },
                      " " + unit.suffix
                    )
                )
              );
            })
          )
        ),
        React.createElement(
          "div",
          {
            className: "units-content scrollable",
            style: {
              height: 100 + unitListHeight + extraHeightRightSide + "px",
            },
          },
          selectedUnitData == null &&
            React.createElement("div", {}, loc("loading", "Loading...")),
          selectedUnitData != null &&
            React.createElement(
              "div",
              { className: "columns" },
              React.createElement(
                "div",
                { className: "unit-name-container" },
                React.createElement("img", {
                  src: "hud/img/" + selectedUnitData.image,
                  style: { margin: "auto 6px" },
                }),
                React.createElement(
                  "div",
                  { className: "unit-name" },
                  selectedUnitData.name,
                  selectedUnitData.suffix &&
                    React.createElement(
                      "span",
                      { className: "suffix" },
                      " " + selectedUnitData.suffix
                    )
                ),
                " ",
                selectedUnitData.cost > 0 &&
                  React.createElement("img", {
                    className: "tooltip-icon",
                    src: "hud/img/icons/Gold.png",
                  }),
                selectedUnitData.cost > 0 &&
                  React.createElement(
                    "span",
                    { className: "stat" },
                    " " + selectedUnitData.cost + " "
                  ),
                selectedUnitData.mythium > 0 &&
                  React.createElement("img", {
                    className: "tooltip-icon",
                    src: "hud/img/icons/Mythium.png",
                  }),
                selectedUnitData.mythium > 0 &&
                  React.createElement(
                    "span",
                    { className: "stat" },
                    " " + selectedUnitData.mythium + " "
                  ),
                selectedUnitData.income > 0 &&
                  React.createElement("img", {
                    className: "tooltip-icon",
                    src: "hud/img/icons/Income.png",
                  }),
                selectedUnitData.income > 0 &&
                  React.createElement(
                    "span",
                    { className: "stat" },
                    " " + selectedUnitData.income + " "
                  ),
                selectedUnitData.supply > 0 &&
                  React.createElement("img", {
                    className: "tooltip-icon",
                    src: "hud/img/icons/Supply.png",
                  }),
                selectedUnitData.supply > 0 &&
                  React.createElement(
                    "span",
                    { className: "stat" },
                    " " + selectedUnitData.supply + " "
                  )
              ),
              React.createElement(
                "div",
                { className: "column width-40" },
                React.createElement(
                  "div",
                  { className: "unit-stats-container" },
                  React.createElement(
                    "span",
                    {
                      className: "unit-descriptor",
                      style: { background: "#" + selectedUnitData.color },
                    },
                    selectedUnitData.prefix
                  ),
                  React.createElement(
                    "table",
                    { className: "stats-table" },
                    React.createElement(
                      "tr",
                      {},
                      React.createElement(
                        "td",
                        { className: "", style: {} },
                        React.createElement(
                          "div",
                          { className: "unit-tooltip" },
                          selectedUnitData.description
                        ),
                        selectedUnitData.upgradesFrom != null &&
                          selectedUnitData.upgradesFrom.length > 0 &&
                          selectedUnitData.upgradesFrom.map(function (unit) {
                            return React.createElement(
                              "tr",
                              { className: "stat-container" },
                              React.createElement(
                                "td",
                                { className: "label" },
                                locName("upgrades_from", "Upgrades from")
                              ),
                              React.createElement(
                                "td",
                                {
                                  className: "stat hyperlink",
                                  onMouseDown: function (e) {
                                    engine.call(
                                      "OnFetchCodexInfoForUnit",
                                      unit.unitType
                                    );
                                  },
                                },
                                React.createElement("img", {
                                  className: "tooltip-icon",
                                  src: "hud/img/" + unit.image,
                                }),
                                " ",
                                unit.name,
                                unit.suffix &&
                                  React.createElement(
                                    "span",
                                    { className: "suffix" },
                                    " " + unit.suffix
                                  )
                              )
                            );
                          }),
                        selectedUnitData.upgradesTo != null &&
                          selectedUnitData.upgradesTo.length > 0 &&
                          selectedUnitData.upgradesTo.map(function (unit) {
                            return React.createElement(
                              "tr",
                              { className: "stat-container" },
                              React.createElement(
                                "td",
                                { className: "label" },
                                locName("upgrades_to", "Upgrades to")
                              ),
                              React.createElement(
                                "td",
                                {
                                  className: "stat hyperlink",
                                  onMouseDown: function (e) {
                                    engine.call(
                                      "OnFetchCodexInfoForUnit",
                                      unit.unitType
                                    );
                                  },
                                },
                                React.createElement("img", {
                                  className: "tooltip-icon",
                                  src: "hud/img/" + unit.image,
                                }),
                                " ",
                                unit.name,
                                unit.suffix &&
                                  React.createElement(
                                    "span",
                                    { className: "suffix" },
                                    " " + unit.suffix
                                  )
                              )
                            );
                          }),
                        React.createElement(
                          "div",
                          { className: "subheader" },
                          loc("stats", "Stats")
                        ),
                        selectedUnitData.amountSpawned > 0 &&
                          React.createElement(
                            "tr",
                            { className: "stat-container" },
                            React.createElement(
                              "td",
                              { className: "label" },
                              loc("x_per_spawn", "x per spawn")
                            ),
                            React.createElement(
                              "td",
                              { className: "stat" },
                              selectedUnitData.amountSpawned
                            )
                          ),
                        React.createElement(
                          "tr",
                          { className: "stat-container" },
                          React.createElement(
                            "td",
                            { className: "label" },
                            locName("health", "Health")
                          ),
                          React.createElement(
                            "td",
                            { className: "stat" },
                            selectedUnitData.hp,
                            selectedUnitData.totalValue > 0 &&
                              " (" +
                                (
                                  selectedUnitData.hp /
                                  selectedUnitData.totalValue
                                ).toFixed(1) +
                                " " +
                                loc("per_gold", "per gold") +
                                ")"
                          )
                        ),
                        selectedUnitData.mana > 0 &&
                          React.createElement(
                            "tr",
                            { className: "stat-container" },
                            React.createElement(
                              "td",
                              { className: "label" },
                              locName("mana", "Mana")
                            ),
                            React.createElement(
                              "td",
                              { className: "stat" },
                              selectedUnitData.mana
                            )
                          ),
                        selectedUnitData.mana > 0 &&
                          React.createElement(
                            "tr",
                            { className: "stat-container" },
                            React.createElement(
                              "td",
                              { className: "label" },
                              loc("mana_regen", "Mana Regen")
                            ),
                            React.createElement(
                              "td",
                              { className: "stat" },
                              selectedUnitData.manaRegen
                            )
                          ),
                        React.createElement(
                          "tr",
                          { className: "stat-container" },
                          React.createElement(
                            "td",
                            { className: "label" },
                            loc("dps", "DPS")
                          ),
                          React.createElement(
                            "td",
                            { className: "stat" },
                            selectedUnitData.dps,
                            selectedUnitData.totalValue > 0 &&
                              " (" +
                                (
                                  selectedUnitData.dps /
                                  selectedUnitData.totalValue
                                ).toFixed(1) +
                                " " +
                                loc("per_gold", "per gold") +
                                ")"
                          )
                        ),
                        React.createElement(
                          "tr",
                          { className: "stat-container" },
                          React.createElement(
                            "td",
                            { className: "label" },
                            locName("damage", "Damage")
                          ),
                          React.createElement(
                            "td",
                            { className: "stat" },
                            selectedUnitData.damage
                          )
                        ),
                        React.createElement(
                          "tr",
                          { className: "stat-container simple-tooltip" },
                          React.createElement(
                            "td",
                            { className: "label" },
                            locName("attack_cooldown", "Attack Cooldown")
                          ),
                          React.createElement(
                            "td",
                            { className: "stat" },
                            selectedUnitData.attackSpeed.toFixed(2) +
                              "s" +
                              " (" +
                              selectedUnitData.attackSpeedDescriptor +
                              ")"
                          ),
                          React.createElement(
                            "span",
                            { className: "tooltiptext" },
                            loc(
                              "attack_cooldown",
                              "The time between attacks (in seconds). Lower value means faster attack."
                            )
                          )
                        ),
                        React.createElement(
                          "tr",
                          { className: "stat-container" },
                          React.createElement(
                            "td",
                            { className: "label" },
                            loc("attacks_per_sec", "Atk/sec")
                          ),
                          React.createElement(
                            "td",
                            { className: "stat" },
                            (1 / selectedUnitData.attackSpeed).toFixed(2)
                          )
                        ),
                        React.createElement(
                          "tr",
                          { className: "stat-container" },
                          React.createElement(
                            "td",
                            { className: "label" },
                            loc("range", "Range")
                          ),
                          React.createElement(
                            "td",
                            { className: "stat" },
                            selectedUnitData.range,
                            selectedUnitData.range == 100 &&
                              " (" + loc("melee", "Melee") + ")"
                          )
                        ),
                        selectedUnitData.totalValue > 0 &&
                          React.createElement(
                            "tr",
                            { className: "stat-container" },
                            React.createElement(
                              "td",
                              { className: "label" },
                              locName("total_value", "Total Value")
                            ),
                            React.createElement(
                              "td",
                              { className: "stat" },
                              selectedUnitData.totalValue
                            )
                          ),
                        selectedUnitData.bounty > 0 &&
                          React.createElement(
                            "tr",
                            { className: "stat-container" },
                            React.createElement(
                              "td",
                              { className: "label" },
                              locName("bounty", "Bounty")
                            ),
                            React.createElement(
                              "td",
                              { className: "stat" },
                              selectedUnitData.bounty
                            )
                          ),
                        React.createElement(
                          "tr",
                          { className: "stat-container" },
                          React.createElement(
                            "td",
                            { className: "label" },
                            loc("move_speed", "Move Speed")
                          ),
                          React.createElement(
                            "td",
                            { className: "stat" },
                            selectedUnitData.mspd +
                              " (" +
                              selectedUnitData.movementType +
                              ")"
                          )
                        ),
                        React.createElement(
                          "tr",
                          { className: "stat-container" },
                          React.createElement(
                            "td",
                            { className: "label" },
                            locName("hitbox", "Hitbox")
                          ),
                          React.createElement(
                            "td",
                            { className: "stat" },
                            selectedUnitData.movementSize.toFixed(2)
                          )
                        ),
                        React.createElement(
                          "tr",
                          { className: "stat-container" },
                          React.createElement(
                            "td",
                            { className: "label" },
                            locName("flags", "Flags")
                          ),
                          React.createElement(
                            "td",
                            { className: "stat" },
                            selectedUnitData.flags
                          )
                        ),
                        React.createElement(
                          "div",
                          { className: "subheader" },
                          "Advanced Stats"
                        ),
                        React.createElement(
                          "tr",
                          {
                            className: "stat-container advanced simple-tooltip",
                          },
                          React.createElement(
                            "td",
                            { className: "label" },
                            locName("turn_rate", "Turn Rate")
                          ),
                          React.createElement(
                            "td",
                            { className: "stat" },
                            selectedUnitData.turnRate.toFixed(2)
                          ),
                          React.createElement(
                            "span",
                            { className: "tooltiptext" },
                            loc("turn_rate", "Turn Rate")
                          )
                        ),
                        React.createElement(
                          "tr",
                          {
                            className: "stat-container advanced simple-tooltip",
                          },
                          React.createElement(
                            "td",
                            { className: "label" },
                            locName("projectile_speed", "Projectile Speed")
                          ),
                          React.createElement(
                            "td",
                            { className: "stat" },
                            selectedUnitData.projectileSpeed
                          ),
                          React.createElement(
                            "span",
                            { className: "tooltiptext" },
                            loc("projectile_speed", "Projectile Speed")
                          )
                        ),
                        React.createElement(
                          "tr",
                          {
                            className: "stat-container advanced simple-tooltip",
                          },
                          React.createElement(
                            "td",
                            { className: "label" },
                            locName("backswing", "Backswing")
                          ),
                          React.createElement(
                            "td",
                            { className: "stat" },
                            selectedUnitData.backswing.toFixed(2) + "s"
                          ),
                          React.createElement(
                            "span",
                            { className: "tooltiptext" },
                            loc("backswing", "Backswing")
                          )
                        ),
                        React.createElement(
                          "tr",
                          {
                            className: "stat-container advanced simple-tooltip",
                          },
                          React.createElement(
                            "td",
                            { className: "label" },
                            locName("effective_dps", "Effective DPS")
                          ),
                          React.createElement(
                            "td",
                            { className: "stat" },
                            selectedUnitData.effectiveDps.toFixed(0)
                          ),
                          React.createElement(
                            "span",
                            { className: "tooltiptext" },
                            loc("effective_dps", "Effective DPS")
                          )
                        ),
                        React.createElement(
                          "tr",
                          {
                            className: "stat-container advanced simple-tooltip",
                          },
                          React.createElement(
                            "td",
                            { className: "label" },
                            locName("effective_threat", "Effective Threat")
                          ),
                          React.createElement(
                            "td",
                            { className: "stat" },
                            selectedUnitData.effectiveThreat.toFixed(0)
                          ),
                          React.createElement(
                            "span",
                            { className: "tooltiptext" },
                            loc("effective_threat", "Effective Threat")
                          )
                        ),
                        React.createElement(
                          "tr",
                          {
                            className: "stat-container advanced simple-tooltip",
                          },
                          React.createElement(
                            "td",
                            { className: "label" },
                            locName("spawn_bias", "Spawn Bias")
                          ),
                          React.createElement(
                            "td",
                            { className: "stat" },
                            selectedUnitData.spawnBias.toFixed(2)
                          ),
                          React.createElement(
                            "span",
                            { className: "tooltiptext" },
                            loc("spawn_bias", "Spawn Bias")
                          )
                        ),
                        selectedUnitData.legionType == "nether_legion_id" &&
                          React.createElement(
                            "tr",
                            {
                              className:
                                "stat-container advanced simple-tooltip",
                            },
                            React.createElement(
                              "td",
                              { className: "label" },
                              locName("spawn_delay", "Spawn Delay")
                            ),
                            React.createElement(
                              "td",
                              { className: "stat" },
                              selectedUnitData.spawnDelay.toFixed(2)
                            ),
                            React.createElement(
                              "span",
                              { className: "tooltiptext" },
                              loc("spawn_delay", "Spawn Delay")
                            )
                          ),
                        selectedUnitData.legionType == "nether_legion_id" &&
                          React.createElement(
                            "tr",
                            {
                              className:
                                "stat-container advanced simple-tooltip",
                            },
                            React.createElement(
                              "td",
                              { className: "label" },
                              locName("stock_max", "Stock Max")
                            ),
                            React.createElement(
                              "td",
                              { className: "stat" },
                              selectedUnitData.stockMax
                            ),
                            React.createElement(
                              "span",
                              { className: "tooltiptext" },
                              loc("stock_max", "Stock Max")
                            )
                          ),
                        selectedUnitData.legionType == "nether_legion_id" &&
                          React.createElement(
                            "tr",
                            {
                              className:
                                "stat-container advanced simple-tooltip",
                            },
                            React.createElement(
                              "td",
                              { className: "label" },
                              locName("stock_time", "Stock Time")
                            ),
                            React.createElement(
                              "td",
                              { className: "stat" },
                              selectedUnitData.stockTime
                            ),
                            React.createElement(
                              "span",
                              { className: "tooltiptext" },
                              loc("stock_time", "Stock Time")
                            )
                          ),
                        selectedUnitData.mastermindGroups != "None" &&
                          React.createElement(
                            "tr",
                            {
                              className:
                                "stat-container advanced simple-tooltip",
                            },
                            React.createElement(
                              "td",
                              { className: "label" },
                              locName("mastermind_groups", "Mastermind Groups")
                            ),
                            React.createElement(
                              "td",
                              { className: "stat" },
                              selectedUnitData.mastermindGroups
                            ),
                            React.createElement(
                              "span",
                              { className: "tooltiptext" },
                              loc("mastermind_groups", "Mastermind Groups")
                            )
                          )
                      )
                    )
                  )
                )
              ),
              React.createElement(
                "div",
                { className: "column width-60", style: { marginTop: "-10px" } },
                React.createElement("img", {
                  src: "liveview://codexLiveView",
                  className: "sketchfab-embed",
                  style: {
                    outline: "1px solid rgba(218, 251, 248, 0.15)",
                  },
                  onMouseDown: function (e) {
                    console.log(
                      "codexLiveView mousedown " + e.nativeEvent.which
                    );
                    engine.call("OnCodexUnitDragStart", e.nativeEvent.which);
                  },
                  onWheel: function (e) {
                    engine.call("OnCodexUnitZoom", e.nativeEvent.deltaY);
                    e.nativeEvent.preventDefault();
                  },
                }),
                React.createElement(
                  "div",
                  {
                    className: "change-pose simple-tooltip flipped flipped-y",
                    onMouseDown: function (e) {
                      engine.call("OnRefreshSkinPose");
                    },
                  },
                  React.createElement("span", {
                    className: "tooltiptext medium no-carat",
                    style: { left: "-260px" },
                    dangerouslySetInnerHTML: {
                      __html: loc(
                        "change_pose",
                        "Click to change pose<br>Hold right click & drag character to move<br>Hold left click & drag character to rotate<br>Mouse wheel to zoom"
                      ),
                    },
                  })
                ),
                React.createElement(
                  "table",
                  { className: "abilities-table" },
                  React.createElement(
                    "tr",
                    { className: "stat-container" },
                    React.createElement(
                      "td",
                      { className: "label", style: { width: "70px" } },
                      locName("attack_type", "Attack Type")
                    ),
                    React.createElement(
                      "td",
                      { className: "stat simple-tooltip" },
                      React.createElement("img", {
                        className: "tooltip-icon",
                        src:
                          "hud/img/icons/" +
                          selectedUnitData.attackType +
                          ".png",
                      }),
                      " ",
                      locName(
                        selectedUnitData.attackType + "Attack",
                        selectedUnitData.attackType
                      ),
                      React.createElement("span", {
                        className: "tooltiptext",
                        dangerouslySetInnerHTML: {
                          __html: selectedUnitData.attackTypeDescription,
                        },
                      })
                    )
                  ),
                  React.createElement(
                    "tr",
                    { className: "stat-container" },
                    React.createElement(
                      "td",
                      { className: "label" },
                      locName("defense_type", "Defense Type")
                    ),
                    React.createElement(
                      "td",
                      { className: "stat simple-tooltip" },
                      React.createElement("img", {
                        className: "tooltip-icon",
                        src:
                          "hud/img/icons/" +
                          selectedUnitData.armorType +
                          ".png",
                      }),
                      " ",
                      locName(
                        selectedUnitData.armorType + "Armor",
                        selectedUnitData.armorType
                      ),
                      React.createElement("span", {
                        className: "tooltiptext",
                        dangerouslySetInnerHTML: {
                          __html: selectedUnitData.armorTypeDescription,
                        },
                      })
                    )
                  ),
                  selectedUnitData.abilities.map(function (ability) {
                    return React.createElement(
                      "tr",
                      { className: "stat-container" },
                      !ability.abilityClass &&
                        React.createElement(
                          "td",
                          { className: "label" },
                          locName("ability", "Ability")
                        ),
                      ability.abilityClass == "hidden" &&
                        React.createElement(
                          "td",
                          { className: "label" },
                          locName("hidden_ability", "Hidden Ability")
                        ),
                      ability.abilityClass == "linked" &&
                        React.createElement(
                          "td",
                          { className: "label" },
                          locName("linked_ability", "Linked Ability")
                        ),
                      React.createElement(
                        "td",
                        { className: "stat simple-tooltip" },
                        !ability.abilityClass &&
                          React.createElement("img", {
                            className: "tooltip-icon",
                            src: "hud/img/" + ability.image,
                          }),
                        ability.abilityClass &&
                          React.createElement("img", {
                            className: "tooltip-icon",
                            src: "hud/img/icons/NoIcon.png",
                          }),
                        " ",
                        React.createElement(
                          "span",
                          { style: { color: "#ff8800" } },
                          ability.name
                        ),
                        !ability.abilityClass && ": ",
                        !ability.abilityClass &&
                          React.createElement("span", {
                            dangerouslySetInnerHTML: {
                              __html: ability.description,
                            },
                          }),
                        React.createElement("span", {
                          className: "tooltiptext wider ",
                          dangerouslySetInnerHTML: {
                            __html: ability.extendedDescription,
                          },
                        })
                      )
                    );
                  })
                )
              )
            )
        )
      )
    );
  },
});

var CodexSpells = React.createClass({
  getInitialState: function () {
    return {};
  },
  componentWillMount: function () {},
  render: function () {
    if (!globalState.codexContent) return null;
    if (!globalState.codexContent.powerups)
      return "Error: could not find spells";

    return React.createElement(
      "div",
      { className: "codex credits-menu" },
      React.createElement(
        "div",
        { className: "category" },
        loc("legion_spells", "Legion Spells")
      ),
      //React.createElement('p', {
      //    dangerouslySetInnerHTML: {
      //        __html: "Legion Spells are in-game upgrades that boost your economy, empower your units, or provide other benefits. At the start of the game, all players are given the same three randomized Legion Spells. After wave 10 ends, you must select and use one Legion Spell before wave 11 spawns."
      //    }
      //}),
      React.createElement(
        "table",
        { className: "generic-content-list" },
        globalState.codexContent.powerups.map(function (powerup) {
          return React.createElement(
            "tr",
            {},
            React.createElement(
              "td",
              { className: "simple-tooltip" },
              React.createElement(
                "td",
                {},
                React.createElement("img", {
                  className: "with-border",
                  style: { height: "32px" },
                  src: "hud/img/" + powerup.image,
                })
              ),
              powerup.extendedDescription &&
                powerup.extendedDescription.length > 0 &&
                React.createElement("span", {
                  className: "tooltiptext",
                  dangerouslySetInnerHTML: {
                    __html: powerup.extendedDescription,
                  },
                })
            ),
            React.createElement(
              "td",
              { style: { padding: "0 12px", color: "#8ff110" } },
              React.createElement("span", {}, powerup.name)
            ),
            React.createElement(
              "td",
              {},
              React.createElement("span", {}, powerup.description)
            )
          );
        })
      )
    );
  },
});

var CodexKing = React.createClass({
  getInitialState: function () {
    return {};
  },
  componentWillMount: function () {},
  render: function () {
    if (!globalState.codexContent) return null;
    if (!globalState.codexContent.king) return "Error: could not find king";

    return React.createElement(
      "div",
      { className: "codex credits-menu" },
      React.createElement(
        "div",
        { className: "category" },
        locName("king_upgrades_tooltip", "King Upgrades")
      ),
      React.createElement(
        "table",
        { className: "generic-content-list" },
        globalState.codexContent.king.researches.map(function (research) {
          return React.createElement(
            "tr",
            {},
            React.createElement(
              "td",
              {},
              React.createElement("img", {
                className: "with-border",
                style: { height: "32px" },
                src: "hud/img/" + research.image,
              })
            ),
            React.createElement(
              "td",
              { style: { padding: "0 12px", minWidth: "200px" } },
              React.createElement(
                "div",
                { style: { color: "#8ff110" } },
                research.name
              ),
              React.createElement(
                "div",
                {},
                React.createElement("img", {
                  className: "resource-icon",
                  src: "hud/img/icons/Mythium.png",
                }),
                React.createElement("span", {}, research.mythiumCost)
              )
            ),
            React.createElement(
              "td",
              {},
              React.createElement(
                "span",
                { style: { verticalAlign: "top" } },
                research.description
              )
            )
          );
        })
      ),
      React.createElement(
        "div",
        { className: "category" },
        locName("king_spells", "King Spells")
      ),
      React.createElement(
        "table",
        { className: "generic-content-list" },
        globalState.codexContent.king.abilities.map(function (ability) {
          return React.createElement(
            "tr",
            {},
            React.createElement(
              "td",
              {},
              React.createElement("img", {
                className: "with-border",
                style: { height: "32px" },
                src: "hud/img/" + ability.image,
              })
            ),
            React.createElement(
              "td",
              { style: { padding: "0 12px", minWidth: "200px" } },
              React.createElement(
                "div",
                { style: { color: "#8ff110" } },
                ability.name
              ),
              React.createElement(
                "div",
                {},
                React.createElement("img", {
                  className: "resource-icon",
                  src: "hud/img/icons/Mana.png",
                }),
                ability.manaCost > 0 &&
                  React.createElement("span", {}, ability.manaCost),
                ability.manaCost == 0 &&
                  React.createElement(
                    "span",
                    {},
                    loc("passive_ability", "Passive")
                  )
              )
            ),
            React.createElement(
              "td",
              {},
              React.createElement(
                "span",
                { style: { verticalAlign: "top" } },
                ability.description
              )
            )
          );
        })
      )
    );
  },
});

var CodexEmojis = React.createClass({
  render: function () {
    if (!globalState.codexContent) return null;
    if (!globalState.codexContent.emojis) return "Error: could not find emojis";

    return React.createElement(
      "div",
      { className: "codex credits-menu" },
      React.createElement(
        "div",
        { className: "category" },
        locName("emojis", "Emojis")
      ),
      React.createElement(
        "table",
        { className: "generic-content-list" },
        globalState.codexContent.emojis.map(function (emoji) {
          return React.createElement(
            "tr",
            {},
            React.createElement(
              "td",
              {},
              React.createElement("img", {
                style: { height: "32px" },
                src: "hud/img/" + emoji.image,
              })
            ),
            React.createElement(
              "td",
              { style: { padding: "0 12px", color: "#8ff110" } },
              React.createElement("span", {}, emoji.name)
            )
          );
        })
      )
    );
  },
});

var CodexDebugCommands = React.createClass({
  render: function () {
    if (!globalState.codexContent) return null;
    if (!globalState.codexContent.debugCommands)
      return "Error: could not find debug commands";

    return React.createElement(
      "div",
      { className: "codex credits-menu" },
      React.createElement(
        "div",
        { className: "category" },
        locName("debug_commands", "Debug Commands")
      ),
      React.createElement(
        "table",
        { className: "generic-content-list" },
        globalState.codexContent.debugCommands.map(function (command) {
          if (command.commandType != "debug") return null;

          return React.createElement(
            "tr",
            {},
            React.createElement(
              "td",
              {
                style: {
                  padding: "0 12px",
                  color: "#8ff110",
                  minWidth: "150px",
                },
              },
              React.createElement("span", {
                dangerouslySetInnerHTML: {
                  __html: command.name,
                },
              })
            ),
            React.createElement(
              "td",
              {},
              React.createElement("span", {
                dangerouslySetInnerHTML: {
                  __html: command.description,
                },
              })
            )
          );
        })
      ),
      React.createElement(
        "div",
        { className: "category" },
        locName("misc_commands", "Misc Commands")
      ),
      React.createElement(
        "table",
        { className: "generic-content-list" },
        globalState.codexContent.debugCommands.map(function (command) {
          if (command.commandType != "") return null;

          return React.createElement(
            "tr",
            {},
            React.createElement(
              "td",
              {
                style: {
                  padding: "0 12px",
                  color: "#8ff110",
                  minWidth: "150px",
                },
              },
              React.createElement("span", {}, command.name)
            ),
            React.createElement(
              "td",
              {},
              React.createElement("span", {}, command.description)
            )
          );
        }),
        globalState.codexContent.debugCommands.map(function (command) {
          if (command.commandType != "lobby") return null;

          return React.createElement(
            "tr",
            {},
            React.createElement(
              "td",
              {
                style: {
                  padding: "0 12px",
                  color: "#8ff110",
                  minWidth: "150px",
                },
              },
              React.createElement("span", {}, command.name)
            ),
            React.createElement(
              "td",
              {},
              React.createElement("span", {}, command.description)
            )
          );
        })
      )
    );
  },
});
